-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-01-2018 a las 05:39:18
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ivonnecl_restaurant`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(128) NOT NULL,
  `descripcion_categoria` varchar(255) DEFAULT NULL,
  `fecha_categoria` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre_categoria`, `descripcion_categoria`, `fecha_categoria`) VALUES
(1, 'bebidas', 'bebidas personales', NULL),
(2, 'desayuno', '', NULL),
(3, 'Almuerzo', '', NULL),
(4, 'Cena', '', NULL),
(5, 'Platos principales', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `id_detalle_venta` int(11) NOT NULL,
  `descripcion_detalle_venta` varchar(255) DEFAULT NULL,
  `importe_detalle_venta` decimal(5,2) DEFAULT NULL,
  `fecha_detalle_venta` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_venta` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id_detalle_venta`, `descripcion_detalle_venta`, `importe_detalle_venta`, `fecha_detalle_venta`, `id_venta`, `id_mesa`) VALUES
(6, NULL, '0.00', '2018-01-30 23:35:56', 8, 1),
(7, NULL, '90.00', '2018-01-30 23:36:54', 9, 1),
(8, '', '408.00', '2018-01-30 23:37:45', 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id_mesa` int(11) NOT NULL,
  `nombre_mesa` varchar(64) DEFAULT NULL,
  `descripcion_mesa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `nombre_mesa`, `descripcion_mesa`) VALUES
(1, 'mesa 1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plato`
--

CREATE TABLE `plato` (
  `id_plato` int(11) NOT NULL,
  `nombre_plato` varchar(128) NOT NULL,
  `importe_plato` decimal(5,2) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plato`
--

INSERT INTO `plato` (`id_plato`, `nombre_plato`, `importe_plato`, `id_categoria`) VALUES
(1, 'cocacola', '3.00', 1),
(2, 'saltado', '3.50', 3),
(3, 'Abadejo a la parilla con alcachofas rehogadas', '15.00', 5),
(4, 'Abadejo asado con cobertura de patata y ajo', '28.00', 5),
(5, 'aji de gallina', '10.00', 5),
(6, 'Almejas con salsa de habichuelas negras', '16.00', 5),
(7, 'ALOKÓ', '16.00', 5),
(8, 'ARROZ A LA PIMIENTA', '620.00', 5),
(9, 'Arroz a la Valenciana', '120.00', 5),
(10, 'arroz chaufa a la jamonada', '25.00', 5),
(11, 'arroz con bacalao', '60.00', 5),
(12, 'Arroz con gandule', '20.00', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` int(11) NOT NULL,
  `descuento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `descuento`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_has_plato`
--

CREATE TABLE `venta_has_plato` (
  `id_venta` int(11) NOT NULL,
  `id_plato` int(11) NOT NULL,
  `fecha_venta_has_plato` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta_has_plato`
--

INSERT INTO `venta_has_plato` (`id_venta`, `id_plato`, `fecha_venta_has_plato`, `cantidad`) VALUES
(4, 3, '2018-01-30 23:19:47', 2),
(4, 4, '2018-01-30 23:19:47', 1),
(5, 3, '2018-01-30 23:21:14', 1),
(5, 10, '2018-01-30 23:21:14', 4),
(6, 3, '2018-01-30 23:29:03', 2),
(6, 10, '2018-01-30 23:29:03', 1),
(7, 3, '2018-01-30 23:34:56', 2),
(7, 10, '2018-01-30 23:34:56', 1),
(8, 3, '2018-01-30 23:35:56', 1),
(8, 10, '2018-01-30 23:35:56', 3),
(9, 3, '2018-01-30 23:36:54', 1),
(9, 10, '2018-01-30 23:36:54', 3),
(10, 6, '2018-01-30 23:37:45', 3),
(10, 9, '2018-01-30 23:37:45', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_mesa` (`id_mesa`) USING BTREE;

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id_mesa`),
  ADD KEY `id_mesa` (`id_mesa`);

--
-- Indices de la tabla `plato`
--
ALTER TABLE `plato`
  ADD PRIMARY KEY (`id_plato`,`id_categoria`),
  ADD KEY `fk_plato_categoria_idx` (`id_categoria`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE;

--
-- Indices de la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD PRIMARY KEY (`id_venta`,`id_plato`) USING BTREE,
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_plato` (`id_plato`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `plato`
--
ALTER TABLE `plato`
  MODIFY `id_plato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_mesa` FOREIGN KEY (`id_mesa`) REFERENCES `mesa` (`id_mesa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `plato`
--
ALTER TABLE `plato`
  ADD CONSTRAINT `fk_plato_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD CONSTRAINT `idplato` FOREIGN KEY (`id_plato`) REFERENCES `plato` (`id_plato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idventa` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
