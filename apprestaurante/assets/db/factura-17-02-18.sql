-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-02-2018 a las 12:14:20
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ivonnecl_restaurant`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id_factura` int(11) NOT NULL,
  `nombre_razon_social_cliente` varchar(150) NOT NULL,
  `numeracion_factura` varchar(8) NOT NULL,
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_facturacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ruc_cliente` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id_factura`, `nombre_razon_social_cliente`, `numeracion_factura`, `id_detalle_venta`, `fecha_facturacion`, `ruc_cliente`) VALUES
(1, 'GEOSRL', 'F0000001', 4, '2018-02-11 15:31:26', ''),
(2, 'GEO2', 'F0000002', 5, '2018-02-11 15:54:59', ''),
(3, 'GEOS', 'F0000003', 6, '2018-02-11 15:56:25', ''),
(4, 'GEOS TRAVELS', 'F0000004', 8, '2018-02-17 01:07:46', ''),
(5, 'GEO SEL', 'F0000005', 9, '2018-02-17 04:52:51', '1234566780');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id_factura`,`numeracion_factura`,`id_detalle_venta`) USING BTREE,
  ADD UNIQUE KEY `id_detalle_venta` (`id_detalle_venta`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `id_detalle_venta` FOREIGN KEY (`id_detalle_venta`) REFERENCES `detalle_venta` (`id_detalle_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
