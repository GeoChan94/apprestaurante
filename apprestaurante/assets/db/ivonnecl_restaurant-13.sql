-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-02-2018 a las 14:21:11
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ivonnecl_restaurant`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE `boleta` (
  `id_boleta` int(11) NOT NULL,
  `nombre_razon_social_cliente` varchar(150) NOT NULL,
  `documento_cliente` varchar(20) NOT NULL,
  `numeracion_boleta` varchar(8) NOT NULL,
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_boleta` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(128) NOT NULL,
  `descripcion_categoria` varchar(255) DEFAULT NULL,
  `fecha_categoria` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre_categoria`, `descripcion_categoria`, `fecha_categoria`) VALUES
(6, 'bebidas', '', '2018-02-06 01:14:46'),
(7, 'extras', '', '2018-02-08 05:16:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `id_detalle_venta` int(11) NOT NULL,
  `importe_detalle_venta` decimal(5,2) DEFAULT NULL,
  `fecha_detalle_venta` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_venta` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `estado_detalle_venta` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id_detalle_venta`, `importe_detalle_venta`, `fecha_detalle_venta`, `id_venta`, `id_mesa`, `estado_detalle_venta`) VALUES
(4, '11.00', '2018-02-10 04:39:13', 48, 5, 1),
(5, '12.00', '2018-02-11 15:39:41', 49, 5, 1),
(6, '9.00', '2018-02-11 15:56:11', 50, 5, 1),
(7, '14.00', '2018-02-11 16:19:25', 51, 5, 0),
(8, '30.00', '2018-02-11 23:30:37', 52, 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id_factura` int(11) NOT NULL,
  `nombre_razon_social_cliente` varchar(150) NOT NULL,
  `documento_cliente` varchar(20) NOT NULL,
  `numeracion_factura` varchar(8) NOT NULL,
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_facturacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id_factura`, `nombre_razon_social_cliente`, `documento_cliente`, `numeracion_factura`, `id_detalle_venta`, `fecha_facturacion`) VALUES
(1, 'GEOSRL', '70346227', 'F0000001', 4, '2018-02-11 15:31:26'),
(2, 'GEO2', '7216496365', 'F0000002', 5, '2018-02-11 15:54:59'),
(3, 'GEOS', '96132171254', 'F0000003', 6, '2018-02-11 15:56:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id_mesa` int(11) NOT NULL,
  `nombre_mesa` varchar(64) DEFAULT NULL,
  `descripcion_mesa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `nombre_mesa`, `descripcion_mesa`) VALUES
(5, 'mesa 1', ''),
(6, 'mesa 2', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plato`
--

CREATE TABLE `plato` (
  `id_plato` int(11) NOT NULL,
  `nombre_plato` varchar(128) NOT NULL,
  `importe_plato` decimal(5,2) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plato`
--

INSERT INTO `plato` (`id_plato`, `nombre_plato`, `importe_plato`, `id_categoria`) VALUES
(13, 'pepsi', '3.00', 6),
(14, 'cocacola', '4.00', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurante`
--

CREATE TABLE `restaurante` (
  `id_restaurante` int(11) NOT NULL,
  `razon_social_restaurante` varchar(150) NOT NULL,
  `ruc_restaurante` varchar(11) NOT NULL,
  `direccion_restaurante` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `restaurante`
--

INSERT INTO `restaurante` (`id_restaurante`, `razon_social_restaurante`, `ruc_restaurante`, `direccion_restaurante`) VALUES
(0, 'los incas', '10703322274', 'jr nueva esperanza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` int(11) NOT NULL,
  `descuento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `descuento`) VALUES
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_has_plato`
--

CREATE TABLE `venta_has_plato` (
  `id_venta` int(11) NOT NULL,
  `id_plato` int(11) NOT NULL,
  `fecha_venta_has_plato` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta_has_plato`
--

INSERT INTO `venta_has_plato` (`id_venta`, `id_plato`, `fecha_venta_has_plato`, `cantidad`) VALUES
(48, 13, '2018-02-10 04:39:13', 1),
(48, 14, '2018-02-10 04:39:13', 2),
(49, 14, '2018-02-11 15:39:41', 3),
(50, 13, '2018-02-11 15:56:11', 3),
(51, 13, '2018-02-11 16:19:25', 2),
(51, 14, '2018-02-11 16:19:25', 2),
(52, 13, '2018-02-11 23:30:37', 2),
(52, 14, '2018-02-11 23:30:37', 6);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD PRIMARY KEY (`id_boleta`,`numeracion_boleta`,`id_detalle_venta`) USING BTREE,
  ADD UNIQUE KEY `id_detalle_venta` (`id_detalle_venta`) USING BTREE;

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_mesa` (`id_mesa`) USING BTREE;

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id_factura`,`numeracion_factura`,`id_detalle_venta`) USING BTREE,
  ADD UNIQUE KEY `id_detalle_venta` (`id_detalle_venta`) USING BTREE;

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id_mesa`),
  ADD KEY `id_mesa` (`id_mesa`);

--
-- Indices de la tabla `plato`
--
ALTER TABLE `plato`
  ADD PRIMARY KEY (`id_plato`,`id_categoria`),
  ADD KEY `fk_plato_categoria_idx` (`id_categoria`);

--
-- Indices de la tabla `restaurante`
--
ALTER TABLE `restaurante`
  ADD PRIMARY KEY (`id_restaurante`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE;

--
-- Indices de la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD PRIMARY KEY (`id_venta`,`id_plato`) USING BTREE,
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_plato` (`id_plato`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `boleta`
--
ALTER TABLE `boleta`
  MODIFY `id_boleta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `plato`
--
ALTER TABLE `plato`
  MODIFY `id_plato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD CONSTRAINT `id_detalle_ventas` FOREIGN KEY (`id_detalle_venta`) REFERENCES `detalle_venta` (`id_detalle_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_mesa` FOREIGN KEY (`id_mesa`) REFERENCES `mesa` (`id_mesa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `id_detalle_venta` FOREIGN KEY (`id_detalle_venta`) REFERENCES `detalle_venta` (`id_detalle_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plato`
--
ALTER TABLE `plato`
  ADD CONSTRAINT `fk_plato_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD CONSTRAINT `idplato` FOREIGN KEY (`id_plato`) REFERENCES `plato` (`id_plato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idventa` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
