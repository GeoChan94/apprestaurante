-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 20-02-2018 a las 07:13:55
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ivonnecl_restaurant`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE `boleta` (
  `id_boleta` int(11) NOT NULL,
  `nombre_razon_social_cliente` varchar(150) NOT NULL,
  `documento_cliente` varchar(20) NOT NULL,
  `numeracion_boleta` varchar(8) NOT NULL,
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_boleta` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(128) NOT NULL,
  `descripcion_categoria` varchar(255) DEFAULT NULL,
  `fecha_categoria` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre_categoria`, `descripcion_categoria`, `fecha_categoria`) VALUES
(6, 'bebidas', '', '2018-02-06 01:14:46'),
(7, 'extras', '', '2018-02-08 05:16:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `id_detalle_venta` int(11) NOT NULL,
  `importe_detalle_venta` decimal(5,2) DEFAULT NULL,
  `fecha_detalle_venta` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_venta` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `estado_detalle_venta` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id_detalle_venta`, `importe_detalle_venta`, `fecha_detalle_venta`, `id_venta`, `id_mesa`, `estado_detalle_venta`) VALUES
(4, '11.00', '2018-02-10 04:39:13', 48, 5, 1),
(5, '12.00', '2018-02-11 15:39:41', 49, 5, 1),
(6, '9.00', '2018-02-11 15:56:11', 50, 5, 1),
(8, '30.00', '2018-02-11 23:30:37', 52, 5, 1),
(9, NULL, '2018-02-17 01:24:36', 53, 5, 1),
(10, NULL, '2018-02-19 19:59:23', 54, 6, 1),
(11, NULL, '2018-02-19 20:09:20', 55, 6, 1),
(12, NULL, '2018-02-19 21:45:13', 56, 6, 1),
(13, NULL, '2018-02-19 21:46:55', 57, 6, 1),
(14, NULL, '2018-02-19 22:09:41', 58, 6, 1),
(15, NULL, '2018-02-19 22:11:42', 59, 6, 1),
(16, NULL, '2018-02-19 22:17:26', 60, 6, 1),
(17, NULL, '2018-02-19 22:18:36', 61, 6, 1),
(18, NULL, '2018-02-19 22:22:01', 62, 6, 1),
(19, NULL, '2018-02-19 22:29:45', 63, 6, 1),
(20, NULL, '2018-02-19 22:30:58', 64, 6, 1),
(21, NULL, '2018-02-19 22:33:30', 65, 6, 1),
(22, NULL, '2018-02-19 22:34:24', 66, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id_factura` int(11) NOT NULL,
  `nombre_razon_social_cliente` varchar(150) NOT NULL,
  `numeracion_factura` varchar(8) NOT NULL,
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_facturacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ruc_cliente` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id_factura`, `nombre_razon_social_cliente`, `numeracion_factura`, `id_detalle_venta`, `fecha_facturacion`, `ruc_cliente`) VALUES
(1, 'GEOSRL', 'F0000001', 4, '2018-02-11 15:31:26', ''),
(2, 'GEO2', 'F0000002', 5, '2018-02-11 15:54:59', ''),
(3, 'GEOS', 'F0000003', 6, '2018-02-11 15:56:25', ''),
(4, 'GEOS TRAVELS', 'F0000004', 8, '2018-02-17 01:07:46', ''),
(5, 'GEO SEL', 'F0000005', 9, '2018-02-17 04:52:51', '1234566780'),
(6, 'EDIWN', 'F0000006', 10, '2018-02-19 20:06:40', ''),
(7, 'EDWIN', 'F0000007', 11, '2018-02-19 20:09:42', '9512789829844'),
(8, 'JUAN', 'F0000008', 12, '2018-02-19 21:45:27', '9873447375853'),
(9, 'FDSFA', 'F0000009', 13, '2018-02-19 21:47:11', 'af35636346'),
(10, 'DSSGSD', 'F0000010', 14, '2018-02-19 22:09:54', '45645747'),
(12, 'EWRQW', 'F0000011', 15, '2018-02-19 22:11:55', 'qwrqwrqrw'),
(13, 'WETWETWE', 'F0000013', 16, '2018-02-19 22:17:38', 'wetwetwet'),
(14, 'WRER', 'F0000014', 17, '2018-02-19 22:18:47', 'werwer'),
(15, '34534345', 'F0000015', 18, '2018-02-19 22:22:12', '34634654654'),
(17, 'EEEWRW34', 'F0000016', 19, '2018-02-19 22:29:57', '45tegwg'),
(18, '235234', 'F0000018', 20, '2018-02-19 22:31:09', 'rqwrqwrqw'),
(19, 'EWRWRQERQW', 'F0000019', 21, '2018-02-19 22:33:42', '2435235r'),
(20, '52323523', 'F0000020', 22, '2018-02-19 22:34:36', 'wrwrwrr34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impresora`
--

CREATE TABLE `impresora` (
  `id_impresora` int(11) NOT NULL,
  `nombre_impresora` varchar(128) NOT NULL DEFAULT 'Impresora en Red',
  `ip_impresora` varchar(64) NOT NULL DEFAULT '127.0.0.1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `impresora`
--

INSERT INTO `impresora` (`id_impresora`, `nombre_impresora`, `ip_impresora`) VALUES
(1, 'Impresora en Red', '127.0.0.1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id_mesa` int(11) NOT NULL,
  `nombre_mesa` varchar(64) DEFAULT NULL,
  `descripcion_mesa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `nombre_mesa`, `descripcion_mesa`) VALUES
(5, 'mesa 1', ''),
(6, 'mesa 2', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plato`
--

CREATE TABLE `plato` (
  `id_plato` int(11) NOT NULL,
  `nombre_plato` varchar(128) NOT NULL,
  `importe_plato` decimal(5,2) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plato`
--

INSERT INTO `plato` (`id_plato`, `nombre_plato`, `importe_plato`, `id_categoria`) VALUES
(13, 'pepsi', '3.00', 6),
(14, 'cocacola', '4.00', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurante`
--

CREATE TABLE `restaurante` (
  `id_restaurante` int(11) NOT NULL,
  `razon_social_restaurante` varchar(150) NOT NULL,
  `ruc_restaurante` varchar(11) NOT NULL,
  `direccion_restaurante` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `restaurante`
--

INSERT INTO `restaurante` (`id_restaurante`, `razon_social_restaurante`, `ruc_restaurante`, `direccion_restaurante`) VALUES
(0, 'los incas', '10703322274', 'jr nueva esperanza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` int(11) NOT NULL,
  `descuento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `descuento`) VALUES
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_has_plato`
--

CREATE TABLE `venta_has_plato` (
  `id_venta` int(11) NOT NULL,
  `id_plato` int(11) NOT NULL,
  `fecha_venta_has_plato` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta_has_plato`
--

INSERT INTO `venta_has_plato` (`id_venta`, `id_plato`, `fecha_venta_has_plato`, `cantidad`) VALUES
(48, 13, '2018-02-10 04:39:13', 1),
(48, 14, '2018-02-10 04:39:13', 2),
(49, 14, '2018-02-11 15:39:41', 3),
(50, 13, '2018-02-11 15:56:11', 3),
(51, 13, '2018-02-11 16:19:25', 2),
(51, 14, '2018-02-11 16:19:25', 2),
(52, 13, '2018-02-11 23:30:37', 3),
(52, 14, '2018-02-11 23:30:37', 5),
(53, 14, '2018-02-17 01:24:36', 1),
(54, 13, '2018-02-19 19:59:23', 1),
(54, 14, '2018-02-19 19:59:23', 1),
(55, 13, '2018-02-19 20:09:20', 2),
(56, 13, '2018-02-19 21:45:13', 1),
(56, 14, '2018-02-19 21:45:13', 1),
(57, 13, '2018-02-19 21:46:55', 2),
(57, 14, '2018-02-19 21:46:55', 1),
(58, 13, '2018-02-19 22:09:41', 1),
(59, 13, '2018-02-19 22:11:42', 2),
(60, 13, '2018-02-19 22:17:26', 1),
(60, 14, '2018-02-19 22:17:26', 1),
(61, 13, '2018-02-19 22:18:36', 1),
(61, 14, '2018-02-19 22:18:36', 1),
(62, 13, '2018-02-19 22:22:01', 1),
(63, 13, '2018-02-19 22:29:45', 1),
(63, 14, '2018-02-19 22:29:45', 1),
(64, 13, '2018-02-19 22:30:58', 1),
(64, 14, '2018-02-19 22:30:58', 1),
(65, 13, '2018-02-19 22:33:30', 1),
(66, 13, '2018-02-19 22:34:24', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD PRIMARY KEY (`id_boleta`,`numeracion_boleta`,`id_detalle_venta`) USING BTREE,
  ADD UNIQUE KEY `id_detalle_venta` (`id_detalle_venta`) USING BTREE;

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_mesa` (`id_mesa`) USING BTREE;

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id_factura`,`numeracion_factura`,`id_detalle_venta`) USING BTREE,
  ADD UNIQUE KEY `id_detalle_venta` (`id_detalle_venta`) USING BTREE;

--
-- Indices de la tabla `impresora`
--
ALTER TABLE `impresora`
  ADD PRIMARY KEY (`id_impresora`);

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id_mesa`),
  ADD KEY `id_mesa` (`id_mesa`);

--
-- Indices de la tabla `plato`
--
ALTER TABLE `plato`
  ADD PRIMARY KEY (`id_plato`,`id_categoria`),
  ADD KEY `fk_plato_categoria_idx` (`id_categoria`);

--
-- Indices de la tabla `restaurante`
--
ALTER TABLE `restaurante`
  ADD PRIMARY KEY (`id_restaurante`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE;

--
-- Indices de la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD PRIMARY KEY (`id_venta`,`id_plato`) USING BTREE,
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_plato` (`id_plato`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `boleta`
--
ALTER TABLE `boleta`
  MODIFY `id_boleta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `impresora`
--
ALTER TABLE `impresora`
  MODIFY `id_impresora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `plato`
--
ALTER TABLE `plato`
  MODIFY `id_plato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD CONSTRAINT `id_detalle_ventas` FOREIGN KEY (`id_detalle_venta`) REFERENCES `detalle_venta` (`id_detalle_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_mesa` FOREIGN KEY (`id_mesa`) REFERENCES `mesa` (`id_mesa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `id_detalle_venta` FOREIGN KEY (`id_detalle_venta`) REFERENCES `detalle_venta` (`id_detalle_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plato`
--
ALTER TABLE `plato`
  ADD CONSTRAINT `fk_plato_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD CONSTRAINT `idplato` FOREIGN KEY (`id_plato`) REFERENCES `plato` (`id_plato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idventa` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
