-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-03-2018 a las 13:05:28
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ivonnecl_restaurant`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE `boleta` (
  `id_boleta` int(11) NOT NULL,
  `nombre_razon_social_cliente` varchar(150) NOT NULL,
  `documento_cliente` varchar(20) NOT NULL,
  `numeracion_boleta` varchar(8) NOT NULL,
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_boleta` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `nombre_razon_social_cliente`, `documento_cliente`, `numeracion_boleta`, `id_detalle_venta`, `fecha_boleta`) VALUES
(1, 'PRIMERA', '70346227', 'B0000001', 11, '2018-02-19 22:00:03'),
(2, 'ASD', 'asd', 'B002', 20, '2018-02-26 22:00:01'),
(3, 'ASD', 'asd', 'B003', 21, '2018-02-26 22:00:13'),
(4, 'ASDAS', 'asdas', 'B004', 22, '2018-02-26 22:00:33'),
(5, 'SDASD', 'asdas', 'B005', 23, '2018-02-26 22:01:01'),
(6, 'ASD', 'asd', 'B006', 24, '2018-02-26 22:01:18'),
(7, 'ASD', 'asd', 'B007', 25, '2018-02-26 22:01:30'),
(8, 'ASD', 'asda', 'B008', 27, '2018-02-26 22:01:43'),
(11, 'ASDAS', 'asd', 'B009', 26, '2018-02-26 22:01:49'),
(12, 'ASD', 'asd', 'B012', 28, '2018-02-26 22:02:11'),
(13, 'ASD', 'geos', 'B013', 29, '2018-02-26 22:07:31'),
(14, 'ASD', 'asd', 'B014', 30, '2018-02-26 22:28:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(128) NOT NULL,
  `descripcion_categoria` varchar(255) DEFAULT NULL,
  `fecha_categoria` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre_categoria`, `descripcion_categoria`, `fecha_categoria`) VALUES
(6, 'bebidas', '', '2018-02-06 01:14:46'),
(7, 'extras', '', '2018-02-08 05:16:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_detalle_venta` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_venta` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `estado_detalle_venta` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id_detalle_venta`, `fecha_detalle_venta`, `id_venta`, `id_mesa`, `estado_detalle_venta`) VALUES
(4, '2018-02-10 04:39:13', 48, 5, 1),
(5, '2018-02-11 15:39:41', 49, 5, 1),
(6, '2018-02-11 15:56:11', 50, 5, 1),
(8, '2018-02-11 23:30:37', 52, 5, 1),
(9, '2018-02-17 01:24:36', 53, 5, 1),
(10, '2018-02-19 21:54:57', 54, 5, 1),
(11, '2018-02-19 21:59:46', 55, 5, 1),
(14, '2018-02-20 05:06:15', 58, 6, 1),
(15, '2018-02-25 17:48:08', 59, 5, 1),
(16, '2018-02-26 20:59:07', 60, 5, 1),
(17, '2018-02-26 21:01:17', 61, 5, 1),
(18, '2018-02-26 21:11:51', 62, 5, 1),
(19, '2018-02-26 21:28:15', 63, 5, 1),
(20, '2018-02-26 21:32:28', 64, 5, 1),
(21, '2018-02-26 21:34:06', 65, 6, 1),
(22, '2018-02-26 22:00:29', 66, 6, 1),
(23, '2018-02-26 22:00:40', 67, 6, 1),
(24, '2018-02-26 22:01:10', 68, 6, 1),
(25, '2018-02-26 22:01:14', 69, 5, 1),
(26, '2018-02-26 22:01:35', 70, 6, 1),
(27, '2018-02-26 22:01:39', 71, 5, 1),
(28, '2018-02-26 22:01:58', 72, 5, 1),
(29, '2018-02-26 22:02:17', 73, 5, 1),
(30, '2018-02-26 22:28:26', 74, 6, 1),
(31, '2018-02-27 19:20:31', 75, 5, 1),
(32, '2018-02-27 19:25:31', 76, 5, 1),
(33, '2018-02-27 19:27:45', 77, 5, 1),
(34, '2018-02-27 19:33:33', 78, 5, 1),
(35, '2018-02-28 08:33:17', 79, 5, 1),
(36, '2018-02-28 08:45:07', 80, 5, 1),
(37, '2018-02-28 08:47:14', 81, 5, 1),
(38, '2018-02-28 22:53:37', 82, 5, 1),
(39, '2018-02-28 22:57:34', 83, 5, 1),
(40, '2018-02-28 22:59:18', 84, 5, 1),
(42, '2018-02-28 23:02:11', 86, 5, 1),
(43, '2018-02-28 23:02:11', 87, 5, 1),
(44, '2018-03-03 06:26:28', 88, 5, 1),
(45, '2018-03-03 06:32:00', 89, 5, 1),
(46, '2018-03-03 06:33:19', 90, 5, 1),
(47, '2018-03-03 06:42:25', 91, 5, 1),
(48, '2018-03-03 06:43:24', 92, 5, 1),
(49, '2018-03-03 06:47:19', 93, 5, 1),
(50, '2018-03-03 06:55:23', 94, 5, 1),
(51, '2018-03-03 06:57:05', 95, 5, 1),
(52, '2018-03-03 06:58:31', 96, 5, 1),
(53, '2018-03-03 07:00:50', 97, 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id_factura` int(11) NOT NULL,
  `nombre_razon_social_cliente` varchar(150) NOT NULL,
  `numeracion_factura` varchar(8) NOT NULL,
  `id_detalle_venta` int(11) NOT NULL,
  `fecha_facturacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ruc_cliente` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id_factura`, `nombre_razon_social_cliente`, `numeracion_factura`, `id_detalle_venta`, `fecha_facturacion`, `ruc_cliente`) VALUES
(1, 'GEOSRL', 'F0000001', 4, '2018-02-11 15:31:26', ''),
(2, 'GEO2', 'F0000002', 5, '2018-02-11 15:54:59', ''),
(3, 'GEOS', 'F0000003', 6, '2018-02-11 15:56:25', ''),
(4, 'GEOS TRAVELS', 'F0000004', 8, '2018-02-17 01:07:46', ''),
(5, 'GEO SEL', 'F0000005', 9, '2018-02-17 04:52:51', '1234566780'),
(6, 'FACTURA', 'F0000006', 10, '2018-02-19 21:55:47', '132465789'),
(7, 'GEOS', 'F0000007', 15, '2018-02-26 20:56:03', '20524719585'),
(8, 'GEOSSS', 'F0000008', 14, '2018-02-26 20:57:13', '20524719585'),
(9, 'GEOSSS', 'F0000009', 16, '2018-02-26 20:59:17', '20524719585'),
(10, 'GEOSS', 'F0000010', 17, '2018-02-26 21:01:31', '20524719585'),
(12, 'GEOS', 'F0000011', 18, '2018-02-26 21:11:57', '20524719585'),
(13, 'GEOS', 'F0000013', 19, '2018-02-26 21:28:26', '123º'),
(14, 'GEOS', 'F0000014', 31, '2018-02-27 19:21:19', '7034'),
(15, 'GEOS', 'F0000015', 32, '2018-02-27 19:26:09', '1234567890'),
(16, 'GEOSSS', 'F0000016', 33, '2018-02-27 19:27:58', '12340'),
(17, 'GEOSS', 'F0000017', 34, '2018-02-27 19:33:45', '1700000000'),
(18, 'ASD', 'F0000018', 35, '2018-02-28 08:44:14', '123'),
(19, 'ASD', 'F0000019', 36, '2018-02-28 08:45:49', '4196'),
(20, 'ASD', 'F0000020', 37, '2018-02-28 22:53:06', '123'),
(21, 'A123', 'F0000021', 38, '2018-02-28 22:53:49', 'asd'),
(22, 'ASD', 'F0000022', 39, '2018-02-28 22:57:53', '12334567890'),
(23, 'GE', 'F0000023', 40, '2018-02-28 22:59:32', '123'),
(24, 'GE', 'F0000024', 43, '2018-02-28 23:02:36', '123'),
(25, 'EQ', 'F0000025', 42, '2018-03-03 00:00:00', '321'),
(26, 'ASD', 'F0000026', 44, '2018-03-03 00:00:00', '123'),
(27, 'ASD', 'F0000027', 45, '2018-03-03 00:00:00', '32'),
(28, 'ASDQWE', 'F0000028', 46, '2018-03-03 00:00:00', '213'),
(29, 'GEOS', 'F0000029', 47, '2018-03-03 00:00:00', '123'),
(30, 'GEOS', 'F0000030', 48, '2018-03-03 00:00:00', '132456'),
(31, 'GEOS', 'F0000031', 49, '2018-03-03 00:00:00', '123456789'),
(32, 'GEOS', 'F0000032', 50, '2018-03-03 00:00:00', '1321'),
(33, 'GEOS', 'F0000033', 51, '2018-03-03 00:00:00', '12345'),
(34, 'GEOS', 'F0000034', 52, '2018-03-03 00:00:00', '132456'),
(35, 'GEOS', 'F0000035', 53, '2018-03-03 00:00:00', '132456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id_mesa` int(11) NOT NULL,
  `nombre_mesa` varchar(64) DEFAULT NULL,
  `descripcion_mesa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `nombre_mesa`, `descripcion_mesa`) VALUES
(5, 'mesa 1', ''),
(6, 'mesa 2', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plato`
--

CREATE TABLE `plato` (
  `id_plato` int(11) NOT NULL,
  `nombre_plato` varchar(128) NOT NULL,
  `importe_plato` decimal(5,2) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plato`
--

INSERT INTO `plato` (`id_plato`, `nombre_plato`, `importe_plato`, `id_categoria`) VALUES
(13, 'pepsi', '3.00', 6),
(14, 'cocacola', '4.00', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurante`
--

CREATE TABLE `restaurante` (
  `id_restaurante` int(11) NOT NULL,
  `razon_social_restaurante` varchar(150) NOT NULL,
  `ruc_restaurante` varchar(11) NOT NULL,
  `direccion_restaurante` varchar(150) NOT NULL,
  `codigo_ubigeo` int(6) NOT NULL,
  `urbanizacion` varchar(100) DEFAULT NULL,
  `provincia` varchar(30) NOT NULL,
  `departamento` varchar(30) NOT NULL,
  `distrito` varchar(30) NOT NULL,
  `codigo_pais` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `restaurante`
--

INSERT INTO `restaurante` (`id_restaurante`, `razon_social_restaurante`, `ruc_restaurante`, `direccion_restaurante`, `codigo_ubigeo`, `urbanizacion`, `provincia`, `departamento`, `distrito`, `codigo_pais`) VALUES
(0, 'PALIMITA SAC', '12354678901', 'JR PALAMITAS 411', 123245, '', 'LIMA', 'LIMAS', 'SAN ISIDRO', 'PE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` int(11) NOT NULL,
  `descuento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `descuento`) VALUES
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_has_plato`
--

CREATE TABLE `venta_has_plato` (
  `id_venta` int(11) NOT NULL,
  `id_plato` int(11) NOT NULL,
  `fecha_venta_has_plato` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta_has_plato`
--

INSERT INTO `venta_has_plato` (`id_venta`, `id_plato`, `fecha_venta_has_plato`, `cantidad`) VALUES
(48, 13, '2018-02-10 04:39:13', 1),
(48, 14, '2018-02-10 04:39:13', 2),
(49, 14, '2018-02-11 15:39:41', 3),
(50, 13, '2018-02-11 15:56:11', 3),
(51, 13, '2018-02-11 16:19:25', 2),
(51, 14, '2018-02-11 16:19:25', 2),
(52, 13, '2018-02-11 23:30:37', 3),
(52, 14, '2018-02-11 23:30:37', 5),
(53, 14, '2018-02-17 01:24:36', 1),
(54, 13, '2018-02-19 21:54:57', 1),
(54, 14, '2018-02-19 21:54:57', 3),
(55, 14, '2018-02-19 21:59:46', 2),
(56, 14, '2018-02-19 22:12:39', 1),
(57, 13, '2018-02-19 22:14:27', 4),
(58, 13, '2018-02-20 05:06:14', 1),
(58, 14, '2018-02-20 05:06:14', 2),
(59, 14, '2018-02-25 17:48:08', 1),
(60, 14, '2018-02-26 20:59:07', 3),
(61, 14, '2018-02-26 21:01:17', 3),
(62, 14, '2018-02-26 21:11:50', 4),
(63, 13, '2018-02-26 21:28:15', 3),
(64, 13, '2018-02-26 21:36:23', 1),
(64, 14, '2018-02-26 21:32:27', 3),
(65, 13, '2018-02-26 21:36:54', 2),
(65, 14, '2018-02-26 21:34:06', 2),
(66, 14, '2018-02-26 22:00:28', 2),
(67, 14, '2018-02-26 22:00:40', 3),
(68, 14, '2018-02-26 22:01:10', 1),
(69, 13, '2018-02-26 22:01:14', 2),
(70, 13, '2018-02-26 22:01:35', 2),
(70, 14, '2018-02-26 22:01:35', 2),
(71, 13, '2018-02-26 22:01:39', 1),
(71, 14, '2018-02-26 22:01:39', 1),
(72, 13, '2018-02-26 22:01:58', 1),
(72, 14, '2018-02-26 22:01:58', 2),
(73, 13, '2018-02-26 22:02:17', 1),
(73, 14, '2018-02-26 22:02:17', 1),
(74, 13, '2018-02-26 22:28:26', 3),
(75, 13, '2018-02-27 19:20:31', 3),
(76, 13, '2018-02-27 19:25:31', 2),
(76, 14, '2018-02-27 19:25:31', 1),
(77, 13, '2018-02-27 19:27:45', 2),
(77, 14, '2018-02-27 19:27:45', 2),
(78, 13, '2018-02-27 19:33:33', 2),
(78, 14, '2018-02-27 19:33:33', 2),
(79, 13, '2018-02-28 08:33:17', 2),
(79, 14, '2018-02-28 08:33:17', 3),
(80, 13, '2018-02-28 08:45:06', 2),
(80, 14, '2018-02-28 08:45:07', 2),
(81, 13, '2018-02-28 08:47:14', 2),
(81, 14, '2018-02-28 08:47:14', 2),
(82, 14, '2018-02-28 22:53:37', 4),
(83, 13, '2018-02-28 22:57:34', 4),
(83, 14, '2018-02-28 22:57:34', 2),
(84, 13, '2018-02-28 22:59:18', 3),
(84, 14, '2018-02-28 22:59:18', 3),
(85, 13, '2018-02-28 23:02:11', 3),
(86, 13, '2018-02-28 23:02:11', 3),
(87, 13, '2018-02-28 23:02:11', 3),
(88, 13, '2018-03-03 06:26:28', 2),
(89, 13, '2018-03-03 06:32:00', 2),
(89, 14, '2018-03-03 06:32:00', 1),
(90, 13, '2018-03-03 06:33:19', 2),
(90, 14, '2018-03-03 06:33:19', 1),
(91, 13, '2018-03-03 06:42:25', 2),
(91, 14, '2018-03-03 06:42:25', 1),
(92, 13, '2018-03-03 06:43:24', 2),
(92, 14, '2018-03-03 06:43:24', 1),
(93, 13, '2018-03-03 06:47:18', 3),
(93, 14, '2018-03-03 06:47:19', 2),
(94, 13, '2018-03-03 06:55:23', 2),
(94, 14, '2018-03-03 06:55:23', 1),
(95, 13, '2018-03-03 06:57:05', 3),
(95, 14, '2018-03-03 06:57:05', 2),
(96, 13, '2018-03-03 06:58:31', 2),
(96, 14, '2018-03-03 06:58:31', 1),
(97, 13, '2018-03-03 07:00:50', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD PRIMARY KEY (`id_boleta`,`numeracion_boleta`,`id_detalle_venta`) USING BTREE,
  ADD UNIQUE KEY `id_detalle_venta` (`id_detalle_venta`) USING BTREE;

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_mesa` (`id_mesa`) USING BTREE;

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id_factura`,`numeracion_factura`,`id_detalle_venta`) USING BTREE,
  ADD UNIQUE KEY `id_detalle_venta` (`id_detalle_venta`) USING BTREE;

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id_mesa`),
  ADD KEY `id_mesa` (`id_mesa`);

--
-- Indices de la tabla `plato`
--
ALTER TABLE `plato`
  ADD PRIMARY KEY (`id_plato`,`id_categoria`),
  ADD KEY `fk_plato_categoria_idx` (`id_categoria`);

--
-- Indices de la tabla `restaurante`
--
ALTER TABLE `restaurante`
  ADD PRIMARY KEY (`id_restaurante`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `id_venta` (`id_venta`) USING BTREE;

--
-- Indices de la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD PRIMARY KEY (`id_venta`,`id_plato`) USING BTREE,
  ADD KEY `id_venta` (`id_venta`) USING BTREE,
  ADD KEY `id_plato` (`id_plato`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `boleta`
--
ALTER TABLE `boleta`
  MODIFY `id_boleta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `plato`
--
ALTER TABLE `plato`
  MODIFY `id_plato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `boleta`
--
ALTER TABLE `boleta`
  ADD CONSTRAINT `id_detalle_ventas` FOREIGN KEY (`id_detalle_venta`) REFERENCES `detalle_venta` (`id_detalle_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_mesa` FOREIGN KEY (`id_mesa`) REFERENCES `mesa` (`id_mesa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `id_detalle_venta` FOREIGN KEY (`id_detalle_venta`) REFERENCES `detalle_venta` (`id_detalle_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plato`
--
ALTER TABLE `plato`
  ADD CONSTRAINT `fk_plato_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta_has_plato`
--
ALTER TABLE `venta_has_plato`
  ADD CONSTRAINT `idplato` FOREIGN KEY (`id_plato`) REFERENCES `plato` (`id_plato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idventa` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
