<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Detalle_venta extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Detalle_venta_model');
        $this->load->model('Mesa_model');
        $this->load->model('Plato_model');
        $this->load->model('Venta_model');
        $this->load->model('Venta_has_plato_model');
        $this->load->model('Categoria_model');
        
    } 

    /*
     * Listing of detalle_ventas
     */
    function index()
    {
        $all_mesas=array();
        $temp_all_mesas = $this->Mesa_model->get_all_mesas();
        foreach ($temp_all_mesas as $key => $value) {
               $all_mesas[(int)$value['id_mesa']]=$value['nombre_mesa'];
        }
        $temp_detalle_venta=$this->Detalle_venta_model->get_all_detalle_ventas();
        $detalle_venta=array();
        $detalle_venta_platos=array();
        // $temp_importe_detalle_venta=0;
        foreach ($temp_detalle_venta as $key => $value) {
            // $temp_importe_detalle_venta=0;
             $temp_detalle_venta_platos= $this->Venta_has_plato_model->get_all_venta_has_plato($value['id_venta']);

             foreach ($temp_detalle_venta_platos as $key2 => $value2) {
                // foreach ($value2 as $key3 => $value3) {
                    $temp_detalle_plato=$this->Plato_model->get_plato($value2['id_plato']);
                    $detalle_venta_platos[$value['id_venta']][] = array(
                            'nombre_plato' =>$temp_detalle_plato, 
                            'cantidad' =>$value2['cantidad'],
                            'importe_plato'=>$temp_detalle_plato['importe_plato'],
                                                                );
                
                // }
                    // echo ((int)$value2['cantidad']);
                // $temp_importe_detalle_venta+=$value2['cantidad']*
               
             }
             
          
            $detalle_venta[$value['id_detalle_venta']]= array(
                                'id_detalle_venta'=>(int)$value['id_detalle_venta'],
                                'fecha_detalle_venta'=>$value['fecha_detalle_venta'],
                                'id_venta'=>(int)$value['id_venta'],
                                'id_mesa'=>$all_mesas[(int)$value['id_mesa']]
                            );
        }
        $data['detalle_ventas'] = $detalle_venta;
        // echo var_dump($detalle_venta_platos);
        $data['detalle_iddetalleventa_has_plato']=$detalle_venta_platos;
        
        $data['_view'] = 'detalle_venta/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new detalle_venta
     */
    function add($id_mesa=0)
    {   
		if(isset($_POST) && count($_POST) > 0)     
        {   
            $data_venta = array(
                    'descuento' => 0.00,
                );

               $insert_venta_id = $this->Venta_model->add_venta($data_venta);
               $platos_seleccionados=$this->input->post('platos_seleccionados');
            // Venta has articulo
               // echo var_dump($platos_seleccionados);
            foreach ($platos_seleccionados as $key => $value) {
                // echo var_dump(!empty($value['id_plato']));
                if (!empty(@$value)) {
                    $data_venta_plato=array(
                    'id_venta'=>(int)$insert_venta_id,
                    'id_plato'=>(int)$value['id_plato'],
                    'cantidad'=>(int)$value['cantidad'],
                    );
                    $temp_articulo=$this->Plato_model->get_plato((int)$value['id_plato']);

                $venta_has_plato_id = $this->Venta_has_plato_model->add_venta_has_plato($data_venta_plato);
                }
                
                

            }
            $temp_data_venta=$this->input->post('datos_pedido');
            $temp_final_data_venta=array(
                'id_venta' => (int)$insert_venta_id,
                'id_mesa' => (int)$temp_data_venta['id_mesa'],
                // 'importe_detalle_venta' => (int)$temp_data_venta['importe_detalle_venta'],
            );
            $detalle_venta_id = $this->Detalle_venta_model->add_detalle_venta($temp_final_data_venta);
            echo json_encode('success');
            // redirect('detalle_venta/index');
        }
        else{            
            $data['all_categorias']=$this->Categoria_model->get_all_categorias();
            $data['all_platos'] = $this->Plato_model->get_all_platos();
            // echo json_encode($data['all_platos']);
            $data['all_mesas'] = $this->Mesa_model->get_all_mesas();
            if ($id_mesa>0) {
                $data['mesa_seleccionada']['mesa']=$this->Mesa_model->get_mesa($id_mesa);
            }else{
                $data['mesa_seleccionada']=0;
            }
            
            
            $data['_view'] = 'detalle_venta/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a detalle_venta
     */
    function edit($id_detalle_venta)
    {   
        // check if the detalle_venta exists before trying to edit it
        $temp_detalle_venta=$this->Detalle_venta_model->get_detalle_venta($id_detalle_venta);
        $data['detalle_venta'] = $temp_detalle_venta;
        $platos_seleccionados=$this->input->post('platos_seleccionados');
        
        if(isset($data['detalle_venta']['id_detalle_venta']))
        {

			if(isset($_POST) && count($_POST) > 0)     
            {   
                foreach ($platos_seleccionados as $key => $value) {
                    // $state='error';
                // echo var_dump(!empty($value['id_plato']));
                if (!empty(@$value)) {
                    $state_venta_has_plato_id = $this->Venta_has_plato_model->get_state_venta_has_plato((int)$value['id_venta'],(int)$value['id_plato']);
                    if ($state_venta_has_plato_id==0) {
                            $data_venta_plato=array(
                                'id_venta'=>(int)$value['id_venta'],
                                'id_plato'=>(int)$value['id_plato'],
                                'cantidad'=>(int)$value['cantidad'],
                                );

                        $venta_has_plato_id = $this->Venta_has_plato_model->add_venta_has_plato($data_venta_plato);
                        // $state= 'nuevo';
                    }else{
                        if ((int)$value['cantidad']==0) {
                           $state= $this->Venta_has_plato_model->delete_idventa_hasta_idplato((int)$value['id_venta'],(int)$value['id_plato']);
                        }else{
                            $venta_has_plato_id = $this->Venta_has_plato_model->update_venta_has_plato((int)$value['id_venta'],(int)$value['id_plato'],(int)$value['cantidad']);
                            // $state= 'actualizado';
                        }
                        
                    }
                    // $data_venta_plato=array(
                    // // 'id_venta'=>(int)$value['id_venta'],
                    // // 'id_plato'=>(int)$value['id_plato'],
                    // 'cantidad'=>,
                    // );
                    // $temp_articulo=$this->Plato_model->get_plato((int)$value['id_plato']);
// $id_venta,$id_plato,$cantidad
                
                }
                // echo json_encode($state_venta_has_plato_id);
                // echo '<br><br><br><br><br>';
            }
            echo json_encode('actualizado');
            }
            else
            {
                $temp_all_pedidos=$this->Venta_has_plato_model->get_all_venta_has_plato($temp_detalle_venta['id_venta']);
                // echo $detalle_venta['id_detalle_venta'];
                $detalle_mesa=array();
                // var_dump($temp_detalle_venta);
                $detalle_mesa=$this->Mesa_model->get_mesa((int)$temp_detalle_venta['id_mesa']);
                    

                $all_pedidos=array();
                
                $detalle_venta_platos=array();
                foreach ($temp_all_pedidos as $key => $value) {
                    
                    $detalle_plato=$this->Plato_model->get_plato((int)$value['id_plato']);
                    $detalle_venta_platos[]= 
                    array(
                          'id_plato' =>(int)$detalle_plato['id_plato'],
                          'nombre_plato' =>$detalle_plato['nombre_plato'], 
                          'cantidad' =>(int)$value['cantidad'],
                          'id_venta'=>(int)$value['id_venta'],
                          'id_categoria'=>(int)$detalle_plato['id_categoria'], 
                          'importe_plato'=>(int)$detalle_plato['importe_plato'], 
                    );                   
                }

                $data['detalle_mesa']=$detalle_mesa;
                $data['all_pedidos']=(array)$detalle_venta_platos;
                // echo json_encode($data['all_pedidos']);
                $data['all_categorias']=$this->Categoria_model->get_all_categorias();
                $data['all_platos'] = $this->Plato_model->get_all_platos();
                // echo json_encode($data['all_platos']);
                $data['all_mesas'] = $this->Mesa_model->get_all_mesas();

                $data['_view'] = 'detalle_venta/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The detalle_venta you are trying to edit does not exist.');
    } 

    /*
     * Deleting detalle_venta
     */
    function remove($id_detalle_venta)
    {
        $detalle_venta = $this->Detalle_venta_model->get_detalle_venta($id_detalle_venta);

        // check if the detalle_venta exists before trying to delete it
        if(isset($detalle_venta['id_detalle_venta']))
        {
            $this->Detalle_venta_model->delete_detalle_venta($id_detalle_venta);
            redirect('dashboard');
        }
        else
            show_error('The detalle_venta you are trying to delete does not exist.');
    }
    
}
