<?php 

require 'vendor/pos/autoload.php'; //Nota: si renombraste la carpeta a algo diferente de "ticket" cambia el nombre en esta línea
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class Imprimir extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Detalle_venta_model');
        $this->load->model('Factura_model');
        $this->load->model('Detalle_venta_model');
        $this->load->model('Boleta_model');
        $this->load->model('Impresora_model');
    } 

    public function index(){
        echo "OK";
    }

    public function boleta($data ){
        $ip = $this->Impresora_model->get_impresora();
        //$connector = new WindowsPrintConnector("MP230-series");
        //$printer = new Printer($connector);
        $arrayData = $data;
        //$connector = new FilePrintConnector("php://stdout");
        $connector = new NetworkPrintConnector($ip, 9100);
        //$connector = new NetworkPrintConnector("127.0.0.1", 9100);
        //$connector = new FilePrintConnector("USB001");
        
        $printer = new Printer($connector);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("RESTAURANT - RESTAURANT" . "\n");
        $printer->text("BOLETA DE VENTA ELECTRONICA" . "\n");
        $printer->text("RUC Nº ".@$arrayData['factura']['numeracion_factura'] . "\n");
        

        foreach ($arrayData['platos'] as $key => $value) {
            /*Alinear a la izquierda para la cantidad y el nombre*/
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text($value['cantidad'] . "x" . $value['nombre_plato'] . "\n");     
            /*Y a la derecha para el importe*/
            $printer->setJustification(Printer::JUSTIFY_RIGHT);
            $printer->text(' S/. ' . $value['importe_plato'] . "\n");
        }

        $printer->feed();
        $printer->cut();
        $printer->pulse();
        $printer->close();
    }

    function factura($data){
        $ip = $this->Impresora_model->get_impresora();
        //$arrayData = json_decode($data,true);
        $arrayData = $data;
        //$connector = new FilePrintConnector("php://stdout");
        $connector = new NetworkPrintConnector($ip, 9100);
        //$connector = new NetworkPrintConnector("127.0.0.1", 9100);
        //$connector = new FilePrintConnector("USB001");
        
        $printer = new Printer($connector);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("RESTAURANT - RESTAURANT" . "\n");
        $printer->text("FACTURA DE VENTA ELECTRONICA" . "\n");
        $printer->text("RUC Nº ".$arrayData['factura']['numeracion_factura'] . "\n");
        

        foreach ($arrayData['platos'] as $key => $value) {
            /*Alinear a la izquierda para la cantidad y el nombre*/
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text($value['cantidad'] . "x" . $value['nombre_plato'] . "\n");     
            /*Y a la derecha para el importe*/
            $printer->setJustification(Printer::JUSTIFY_RIGHT);
            $printer->text(' S/. ' . $value['importe_plato'] . "\n");
        }

        $printer->feed();
        $printer->cut();
        $printer->pulse();
        $printer->close();
        //echo json_encode(array("response"=>"success"));
        //echo "imprimiendo...!";
    }

    function documento(){
        $option         = $this->input->post('option');
        $data           = $this->input->post('data');
        switch (mb_strtolower($option)) {
            case 'factura':
                $this->factura($data);
            break;
            case 'boleta':
                $this->boleta($data);
            break;
        }
        /*
        echo json_encode(
            array('data' => $data,"option" => $option )
        );
        */
        echo json_encode(array("response"=>"success","message"=>"Imprimido correctamente..!"));
    }

    function impresora(){
        if( ($handle = @fopen("\\\\127.0.0.1\\MP230-series", "w")) === FALSE){ 
            die("No se pudo imprimir");
        }

        $dato = "Hola Mundo..!";
        fwrite($handle,chr(27). chr(64));//reinicio

        //fwrite($handle, chr(27). chr(112). chr(48));//ABRIR EL CAJON
        fwrite($handle, chr(27). chr(100). chr(0));//salto de linea VACIO
        fwrite($handle, chr(27). chr(33). chr(8));//negrita
        fwrite($handle, chr(27). chr(97). chr(1));//centrado
        fwrite($handle,"=================================");
        fwrite($handle, chr(27). chr(100). chr(1));//salto de linea
        fwrite($handle, chr(27). chr(32). chr(3));//ESTACIO ENTRE LETRAS
        fwrite($handle,"I. C. M. EL APOSENTO ALTO ");
        fwrite($handle, chr(27). chr(32). chr(0));//ESTACIO ENTRE LETRAS
        fwrite($handle, chr(27). chr(100). chr(0));//salto de linea VACIO
        fwrite($handle, chr(27). chr(33). chr(8));//negrita
        fwrite($handle, chr(27). chr(100). chr(0));//salto de linea VACIO
        fwrite($handle, chr(27). chr(100). chr(1));//salto de linea
        fwrite($handle,"Nacimos de Nuevo para ser grandes");
        fwrite($handle, chr(27). chr(100). chr(1));//salto de linea
        fwrite($handle,"=================================");
        fwrite($handle, chr(27). chr(100). chr(1));//salto de linea
        fwrite($handle, chr(27). chr(100). chr(1));//salto de linea
        fwrite($handle,"PALABRA A IMPRIMIR: ".$dato);


        fclose($handle); // cierra el fichero PRN
        $salida = shell_exec('lpr USB001'); //lpr->puerto impresora, imprimir archivo PRN
    }

    function impresoraip(){
        $data = [];
        $response = $this->Impresora_model->get_impresora();
        if (!empty($response)) {
            $data['response']   = "success";
            $data['ip']         = $response['ip_impresora'];
        }else{
            $data['response']   = "error";
            $data['ip']         = "127.0.0.1";
        }
        echo json_encode($data);
    }

    function actualizarip(){
        $ip = $this->input->post('ip');
        $this->Impresora_model->update_impresora( "1",array("ip_impresora"=>$ip) );
        echo json_encode( array("response"=>"success") );
    }
}