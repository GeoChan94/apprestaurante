<?php
require_once 'vendor/dompdf/lib/html5lib/Parser.php';
require_once 'vendor/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'vendor/dompdf/lib/php-svg-lib/src/autoload.php';
require_once 'vendor/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();

use Dompdf\Dompdf;

class Reporte extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Reporte_model');
    } 

    /*
     * Listing of Reporte
     */
    function index(){
        $data['ventas_hoy']     = $this->Reporte_model->get_ventasHoy();
        $data['ventas_semana']  = $this->Reporte_model->get_ventasSemana();
        $data['ventas_mes']     = $this->Reporte_model->get_ventasMes();
        $data['_view'] = 'reporte/index';
        $this->load->view('layouts/main',$data);
    }

    function pdf($option = "hoy"){
        $data = array();
        switch(mb_strtolower($option) ){
            case "hoy":
                $data['data']   = $this->Reporte_model->get_ventasHoy();
                $data['option'] = "hoy";
            break;
            case "semana":
                $data['data']   = $this->Reporte_model->get_ventasSemana();
                $data['option'] = "semana actual";
            break;
            case "mes":
                $data['data']   = $this->Reporte_model->get_ventasMes();
                $data['option'] = "mes actual";     
            break;
        }
        $html = $this->load->view("reporte/pdf",$data,TRUE);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portraint');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser
        $dompdf->stream();
    }
}