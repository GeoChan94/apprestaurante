<div class="row">
	
</div>
<?php 

// echo json_encode($detalle_ventas);
echo "<script> 

var detalle_venta_platos=JSON.parse(`".json_encode($detalle_venta_platos)."`);
 console.log('detalle_venta_platos',detalle_venta_platos);

var detalle_ventas=JSON.parse(`".json_encode($detalle_ventas)."`);
 console.log('detalle_ventas',detalle_ventas);
var mesas=JSON.parse(`".json_encode($mesas)."`);
// console.log('mesas',mesas)
</script>"; 
?>
<div class="row">
	<div class="col-12 col-md">
		
		<div class="row">
			<h4 class="col-12 text-uppercase font-weight-bold">Mesas 
				<!-- <spam class="pull-right">
					<a href="<?php echo site_url('mesa/add'); ?>" class="btn btn-success btn-sm">agregar Mesa</a> 
				</spam> -->
			</h4>
			<?php 

			foreach ($mesas as $key => $value) { ?>
				<div class="col-3 mb-2 cursor-pointer">
					<div id="mesa_<?=$value['id_mesa']?>" data-mesa="<?=$value['id_mesa']?>" data-nombremesa="<?=$value['nombre_mesa']?>" class="bg-white border rounded text-center font-weight-bold text-uppercase p-4 btn-mesa">
						<spam class="fa fa-cutlery fa-4x"></spam>
						<div>
							<?=$value['nombre_mesa'] ?>
						</div>
					</div>
				</div>	
			<?php } ?>


		</div>
		
	</div>
	<div class="col-12 col-md">
		<div class="text-uppercase font-weight-bold form-group">
			<h4 class="font-weight-bold">Pedido: <span class="txt_mesa bg-success text-white border rounded p-1">-</span><span class="txt_editar_pedido float-right text-white"></span> </h4>
		</div>
		<div class="detalle_mesa">
			<p>Seleccione mesa</p>
		</div>
		<!-- <div class="row text-center bg-primary text-light p-2 border-0 rounded-top text-uppercase">
			<div class="col-2">
				cantidad				
			</div>
			<div class="col">
				pedido
			</div>
			<div class="col">
				Importe
			</div>
		</div>
		<div class="row text-center bg-white  border text-uppercase">
			<div class="col-2">
				1				
			</div>
			<div class="col">
				lomo saltado
			</div>
			<div class="col">
				S/. 8.00
			</div>
		</div>
		<div class="row text-center bg-white  border text-uppercase">
			<div class="col-2">
				1				
			</div>
			<div class="col">
				gaseosa incacola
			</div>
			<div class="col">
				S/. 8.00
			</div>
		</div>
		<div class="row text-center p-1 bg-secondary text-light  border text-uppercase">
			<div class="col-2">
								
			</div>
			<div class="col">
				TOTAL
			</div>
			<div class="col">
				S/. 16.00
			</div>
		</div> -->
	</div>
</div>
<div class="pull-right">
    <?php //echo $this->pagination->create_links(); ?>    
</div>
<!-- Modal -->
<div class="modal fade" id="modal_cancelar_pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-uppercase" id="exampleModalLabel">Facturacion de Pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8" async defer>
	function confirmDel(){
		var agree=confirm("¿Realmente desea eliminarlo? ");
		  if (agree) 
		  	return true;
		  else
		  	return false;
	}
	var total_importe_pedido=0;
	var id_mesa_temp;
	$(document).on('click', '.btn-mesa', function(event) {
		$('.txt_editar_pedido').html('');
		event.preventDefault();
		/* Act on the event */
		// console.lo
		
		$('.txt_mesa').html($(this).data('nombremesa'));
		id_mesa_temp=$(this).data('mesa');
		$('.btn-mesa').removeClass('bg-success text-white').addClass('bg-white');

		$(this).removeClass('bg-white').addClass('bg-success text-white');
		var temp_a = (detalle_ventas[$(this).data('mesa')]?detalle_ventas[$(this).data('mesa')]:[]);
		// console.log(temp_a.length);
		$('.detalle_mesa').html();
		
		if (temp_a.length>0) {
			var detalle_venta_txt=`
			<div class="row text-center bg-primary text-light p-2 border-0 rounded-top text-uppercase">
				<div class="col-2">
					cantidad				
				</div>
				<div class="col">
					pedido
				</div>
				<div class="col">
					Importe
				</div>
			</div>
			`;
			// console.log(detalle_ventas[$(this).data('mesa')]);

			detalle_ventas[$(this).data('mesa')].forEach( function(val, index) {
				// console.log('index',index);
				// if (index==0) {
					// console.log('dentro',parseInt(val.id_venta));
					$('.txt_editar_pedido').html(`
						<a href="<?=base_url()?>detalle_venta/edit/${parseInt(val.id_detalle_venta)}" class="btn btn-warning ">Editar</a>
						<a href="<?=base_url()?>detalle_venta/remove/${parseInt(val.id_detalle_venta)}" onclick="return confirmDel();" class="btn btn-outline-danger ">Eliminar</a>
						`);
					// console.log('detalle venta',detalle_venta_platos);
					var temp_importe_total_detalle_venta=0;
					detalle_venta_platos[parseInt(val.id_venta)].forEach( function(val2, index2) {
					
					detalle_venta_txt+=`
							<div class="row text-center bg-white  border text-uppercase">
								<div class="col-2">
									${val2.cantidad}		
								</div>
								<div class="col">
									${val2.nombre_plato.nombre_plato}	
								</div>
								<div class="col">
									S/. ${val2.nombre_plato.importe_plato}	
								</div>
							</div>
							`;
					temp_importe_total_detalle_venta+=val2.cantidad*val2.nombre_plato.importe_plato;
					});
					// console.log(detalle_venta_platos[val.id_detalle_venta]);
					detalle_venta_txt+=`
					<div class="row text-center p-1 bg-secondary text-light  border text-uppercase">
						<div class="col-2">
											
						</div>
						<div class="col">
							TOTAL
						</div>
						<div class="col">
							S/. ${temp_importe_total_detalle_venta.toFixed(2)}
						</div>
					</div>
					`;
					total_importe_pedido=temp_importe_total_detalle_venta.toFixed(2);


				// }
				
				
			});
			detalle_venta_txt+=`
			<div class="row mt-4 ">
				<span class="text-uppercase font-weight-bold btn btn-danger btn-lg float-right w-100 btn_pagar_pedido" data-iddetalleventa="${detalle_ventas[$(this).data('mesa')][0].id_detalle_venta}" data-idmesa="${$(this).data('mesa')}">Pagar pedido</span>
			</div>
			`;
			$('.detalle_mesa').html(detalle_venta_txt);
		}else{
			$('.detalle_mesa').html('no tiene pedidos <a class="btn btn-danger" href="<?=base_url();?>detalle_venta/add/'+id_mesa_temp+'" title="">Agregar Pedido</a>');
		}

	});
	function generar_numeracion_factura(data){
		var numeracion_factura_txt='';
		var temp_numeracion='';
		if (data==0){
			temp_numeracion=1+'';
		}else{
			temp_numeracion=(parseInt(data)+1)+'';;
		}
		// console.log('temp_numeracion',temp_numeracion);
		// console.log('temp_numeracion.length',temp_numeracion.length);
		while (temp_numeracion.length<=7) {

			temp_numeracion='0'+temp_numeracion;
		}
		numeracion_factura_txt=numeracion_factura_txt+temp_numeracion;
		return numeracion_factura_txt;
	}
	function generar_numeracion_boleta(data){
		var numeracion_factura_txt='';
		var temp_numeracion='';
		if (data==0){
			temp_numeracion=1+'';
		}else{
			temp_numeracion=(parseInt(data)+1)+'';;
		}
		// console.log('temp_numeracion',temp_numeracion);
		// console.log('temp_numeracion.length',temp_numeracion.length);
		while (temp_numeracion.length<=7) {

			temp_numeracion='0'+temp_numeracion;
		}
		numeracion_factura_txt=numeracion_factura_txt+temp_numeracion;
		return numeracion_factura_txt;
	}
	$(document).on('click', '.btn_pagar_pedido', function(event) {
		event.preventDefault();
		$('#modal_cancelar_pedido .modal-footer').html('');
		var cancelar_pedido_idmesa=$(this).data('idmesa');
		var cancelar_pedido_iddetalleventa=$(this).data('iddetalleventa');
		$('#modal_cancelar_pedido').modal();
		var opcion_de_pago=`
			<div class="row text-center">
			<div class="col-12">
				<p>selecciona opcion para generar el pago de pedido</p>
			</div>
				<div class="col">
					<div class="btn btn-danger btn-lg btn_opcion_pago" data-opcionpago="1">
						Boleta
					</div>
				</div>
				<div class="col">
					<div class="btn btn-danger btn-lg btn_opcion_pago" data-opcionpago="2">
						Factura
					</div>
				</div>
			</div>
		`;
		$('#modal_cancelar_pedido .modal-body').html(opcion_de_pago);
		$(document).on('click', '.btn_opcion_pago', function(event) {
			event.preventDefault();
			var opcion = $(this).data('opcionpago');
			if (opcion==1) {
				// console.log('boleta'+opcion);
				$.get(base_url+'Boleta/ultimate_boleta', function(data) {
					// console.log('data',data);
					var modal_body_cancelar_pedido_txt=`
					<div class="row">
						<div class="col-12">
						<h5 class="font-weight-bold text-capitalize">datos de Boleta</h5>
							<div class="form-group">
								 <label for="serie_boleta">Serie de Boleta</label>
								    <input type="email" class="form-control" id="serie_boleta" aria-describedby="emailHelp" placeholder="F0000001" value="B001" readonly>
							</div>
							<div class="form-group">
								 <label for="numeracion_boleta">Numeración de Boleta</label>
								    <input type="email" class="form-control" id="numeracion_boleta" aria-describedby="emailHelp" placeholder="F0000001" value="${generar_numeracion_boleta(data)}" readonly>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col">
										<label for="fecha_boleta">Fecha de boleta</label>
								    	<input type="email" class="form-control" id="fecha_boleta" aria-describedby="emailHelp" placeholder="Fecha" value="<?=date("Y-m-d") ?>"  readonly>
									</div>
									<div class="col">
										<label for="importe_total">Importe total</label>
								    	<input type="email" class="form-control" id="importe_total" aria-describedby="emailHelp" placeholder="F0000001" value="${total_importe_pedido}" readonly>
									</div>
								</div>
								 
							</div>

						<h5 class="font-weight-bold text-capitalize">datos del adquirente o cliente</h5>
							<div class="form-group">
							 	<label for="documento_cliente">Número de documento</label>
							    <input type="email" class="form-control" id="documento_cliente" aria-describedby="emailHelp" placeholder="número de documento">
							 </div>
							<div class="form-group">
								<label for="nombre_razon_social_cliente">Apellidos y nombres,denominación o razón social</label>
							    <input type="email" class="form-control" id="nombre_razon_social_cliente" aria-describedby="emailHelp" placeholder="Apellidos y nombres,denominación o razón social">
							 </div>
						</div>
						
					</div>
					`;
					// $('#modal_cancelar_pedido').modal();
					$('#modal_cancelar_pedido .modal-body').html(modal_body_cancelar_pedido_txt);
					$('#modal_cancelar_pedido .modal-footer').html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn_generar_boleta_pedido">Generar Boleta</button>`);

					$(document).on('click', '.btn_generar_boleta_pedido', function(event) {
						event.preventDefault();
						/* Act on the event */
						if ($('#numeracion_boleta').val().length>0&&$('#importe_total').val().length>0) {
							if ($('#nombre_razon_social_cliente').val().length>0&&$('#documento_cliente').val().length>0) {
								var boletacion=
								[{
									id_detalle_venta:cancelar_pedido_iddetalleventa,
									numeracion:$('#numeracion_boleta').val(),
									nombre_razon_social:$('#nombre_razon_social_cliente').val(),
									documento_cliente:$('#documento_cliente').val(),

								}];
								// console.log('boletacion',boletacion);
								$.ajax({
									url: base_url+'Boleta/add',
									type: 'post',
									dataType: 'json',
									data: {boletacion:boletacion},
								})
								.done(function(data) {
									console.log("success",data);
									if (data) {
										alert('Boleta exitosa');
										$('#modal_cancelar_pedido').modal('hide');
										location.reload();
									}
									
									
								})
								.fail(function(data) {
									// console.log("error",data.responseText);
									alert('No se pudo registrar esta boleta');
								})
								.always(function() {
									// console.log("complete",data.responseText);
								});


							}else{
								alert('Llene Datos Del Adquirente O Cliente');
							}
						}else{
							alert('Error al Datos De boleta');
						}
						

					});
				});
			}else{
				// console.log('factura'+opcion);
				$.get(base_url+'Factura/ultimate_factura', function(data) {
					// console.log('data',data);
					var modal_body_cancelar_pedido_txt=`
					<div class="row">
						<div class="col-12">
						<h5 class="font-weight-bold text-capitalize">datos de facturacion</h5>
							<div class="form-group">
								 <label for="serie_factura">Serie de factura</label>
								    <input type="email" class="form-control" id="serie_factura" aria-describedby="emailHelp" value="F001" readonly>
							</div>
							<div class="form-group">
								 <label for="numeracion_factura">Numeración de factura</label>
								    <input type="email" class="form-control" id="numeracion_factura" aria-describedby="emailHelp" placeholder="" value="${generar_numeracion_factura(data)}" readonly>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col">
										<label for="fecha_facturacion">Fecha de factura</label>
								    	<input type="email" class="form-control" id="fecha_facturacion" aria-describedby="emailHelp" placeholder="Fecha" value="<?=date("Y-m-d") ?>"  readonly>
									</div>
									<div class="col">
										<label for="importe_total">Importe total</label>
								    	<input type="email" class="form-control" id="importe_total" aria-describedby="emailHelp" placeholder="F0000001" value="${total_importe_pedido}" readonly>
									</div>
								</div>
								 
							</div>

						<h5 class="font-weight-bold text-capitalize">datos del adquirente o cliente</h5>
							<div class="form-group">
							 	<label for="ruc_cliente">Número de Ruc</label>
							    <input type="text" class="form-control" id="ruc_cliente" aria-describedby="emailHelp" placeholder="Número de Ruc">
							 </div>
							<div class="form-group">

								<label for="nombre_razon_social_cliente">Apellidos y nombres,denominación o razón social</label>
							    <input type="email" class="form-control" id="nombre_razon_social_cliente" aria-describedby="emailHelp" placeholder="Apellidos y nombres,denominación o razón social">
							 </div>
							 
						</div>
						
					</div>
					`;
					// $('#modal_cancelar_pedido').modal();
					$('#modal_cancelar_pedido .modal-body').html(modal_body_cancelar_pedido_txt);

					$('#modal_cancelar_pedido .modal-footer').html(`<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn_generar_factura_pedido">Generar Factura</button>`);

					$(document).on('click', '.btn_generar_factura_pedido', function(event) {
						event.preventDefault();
						/* Act on the event */
						if ($('#numeracion_factura').val().length>0&&$('#importe_total').val().length>0) {
							if ($('#nombre_razon_social_cliente').val().length>0) {
								var facturacion=
								[{
									id_detalle_venta:cancelar_pedido_iddetalleventa,
									serie_factura:$('#serie_factura').val(),
									numeracion:$('#numeracion_factura').val(),
									nombre_razon_social:$('#nombre_razon_social_cliente').val(),
									ruc_cliente:$('#ruc_cliente').val(),
									fecha_facturacion:$('#fecha_facturacion').val(),
									leyenda_monto:NumeroALetras($('#importe_total').val()),

								}];
								
								$.ajax({
									url: base_url+'Factura/add',
									type: 'post',
									dataType: 'json',
									data: {facturacion:facturacion},
								})
								.done(function(data) {
									console.log("success",data);
									// imprimir("factura",data.success);	
									if (data) {
										alert('facturacion exitosa');
										$('#modal_cancelar_pedido').modal('hide');

										location.reload();
									}
									
								})
								.fail(function(data) {
									console.log("error",data);
									alert('No se pudo registrar esta factura');
								})
								.always(function() {
									// console.log("complete",data.responseText);
								});


							}else{
								alert('Llene Datos Del Adquirente O Cliente');
							}
						}else{
							alert('Error al Datos De Facturacion');
						}
						

					});
				});
			}
			/* Act on the event */
		});
		
		// $.ajax({
		// 	url: base_url+'Factura/ultimate_factura',
		// 	type: 'default GET (Other values: POST)',
		// 	dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
		// 	data: {param1: 'value1'},
		// })
		// .done(function() {
		// 	console.log("success");
		// })
		// .fail(function() {
		// 	console.log("error");
		// })
		// .always(function() {
		// 	console.log("complete");
		// });
		

		
		
		
		
		

		/* Act on the event */

		function imprimir(option = 'boleta',data){
			$.ajax({
				url: '<?=base_url()?>imprimir/documento',
				type: 'POST',
				dataType: 'json',
				data: {option:option, data: data },
			}).done(function(data) {
				console.log(data);
				$("#modal_cancelar_pedido").modal('hide');
			}).fail(function(e) {
				console.log(e.responseText);
			});
		}
	});
// cambio numeros letras
/*************************************************************/
// NumeroALetras
// @author   Rodolfo Carmona
/*************************************************************/
function Unidades(num){

  switch(num)
  {
    case 1: return "UN";
    case 2: return "DOS";
    case 3: return "TRES";
    case 4: return "CUATRO";
    case 5: return "CINCO";
    case 6: return "SEIS";
    case 7: return "SIETE";
    case 8: return "OCHO";
    case 9: return "NUEVE";
  }

  return "";
}

function Decenas(num){

  decena = Math.floor(num/10);
  unidad = num - (decena * 10);

  switch(decena)
  {
    case 1:   
      switch(unidad)
      {
        case 0: return "DIEZ";
        case 1: return "ONCE";
        case 2: return "DOCE";
        case 3: return "TRECE";
        case 4: return "CATORCE";
        case 5: return "QUINCE";
        default: return "DIECI" + Unidades(unidad);
      }
    case 2:
      switch(unidad)
      {
        case 0: return "VEINTE";
        default: return "VEINTI" + Unidades(unidad);
      }
    case 3: return DecenasY("TREINTA", unidad);
    case 4: return DecenasY("CUARENTA", unidad);
    case 5: return DecenasY("CINCUENTA", unidad);
    case 6: return DecenasY("SESENTA", unidad);
    case 7: return DecenasY("SETENTA", unidad);
    case 8: return DecenasY("OCHENTA", unidad);
    case 9: return DecenasY("NOVENTA", unidad);
    case 0: return Unidades(unidad);
  }
}//Unidades()

function DecenasY(strSin, numUnidades){
  if (numUnidades > 0)
    return strSin + " Y " + Unidades(numUnidades)

  return strSin;
}//DecenasY()

function Centenas(num){

  centenas = Math.floor(num / 100);
  decenas = num - (centenas * 100);

  switch(centenas)
  {
    case 1:
      if (decenas > 0)
        return "CIENTO " + Decenas(decenas);
      return "CIEN ";
    case 2: return "DOSCIENTOS " + Decenas(decenas);
    case 3: return "TRESCIENTOS " + Decenas(decenas);
    case 4: return "CUATROCIENTOS " + Decenas(decenas);
    case 5: return "QUINIENTOS " + Decenas(decenas);
    case 6: return "SEISCIENTOS " + Decenas(decenas);
    case 7: return "SETECIENTOS " + Decenas(decenas);
    case 8: return "OCHOCIENTOS " + Decenas(decenas);
    case 9: return "NOVECIENTOS " + Decenas(decenas);
  }

  return Decenas(decenas);
}//Centenas()

function Seccion(num, divisor, strSingular, strPlural){
  cientos = Math.floor(num / divisor)
  resto = num - (cientos * divisor)

  letras = "";

  if (cientos > 0)
    if (cientos > 1)
      letras = Centenas(cientos) + " " + strPlural;
    else
      letras = strSingular;

  if (resto > 0)
    letras += "";

  return letras;
}//Seccion()

function Miles(num){
  divisor = 1000;
  cientos = Math.floor(num / divisor)
  resto = num - (cientos * divisor)
  strMiles = Seccion(num, divisor, "MIL", "MIL");
  strCentenas = Centenas(resto);

  if(strMiles == "")
    return strCentenas;

  return strMiles + " " + strCentenas;

  //return Seccion(num, divisor, "UN MIL", "MIL") + " " + Centenas(resto);
}//Miles()

function Millones(num){
  divisor = 1000000;
  cientos = Math.floor(num / divisor)
  resto = num - (cientos * divisor)

  strMillones = Seccion(num, divisor, "UN MILLON", "MILLONES");
  strMiles = Miles(resto);

  if(strMillones == "")
    return strMiles;

  return strMillones + " " + strMiles;

  //return Seccion(num, divisor, "UN MILLON", "MILLONES") + " " + Miles(resto);
}//Millones()

function NumeroALetras(num){
  var data = {
    numero: num,
    enteros: Math.floor(num),
    centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
    letrasCentavos: "",
    letrasMonedaPlural: "NUEVOS SOLES",
    letrasMonedaSingular: "NUEVOS SOLES"
  };
console.log('enteros',data.enteros);
console.log('centavos',data.centavos);
console.log('letrasCentavos',data.letrasCentavos);
console.log('letrasMonedaPlural',data.letrasMonedaPlural);
console.log('letrasMonedaSingular',data.letrasMonedaSingular);
console.log('Millones(data.enteros)',Millones(data.enteros));



  // if (data.centavos > 0)
    data.letrasCentavos = "Y " + data.centavos + "/100";

  if(data.enteros == 0)
    return "CERO " + data.letrasCentavos+" "+ data.letrasMonedaPlural;
  if (data.enteros == 1)
    return Millones(data.enteros) + " " +(data.letrasCentavos?data.letrasCentavos + " ":'')+ data.letrasMonedaSingular ;
  else
    return Millones(data.enteros) + " " +(data.letrasCentavos?data.letrasCentavos + " ":'')+ data.letrasMonedaPlural ;
}//NumeroALetras()
</script>
