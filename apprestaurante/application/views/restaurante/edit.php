<div class="container">

		

<?php echo form_open('restaurante/edit/'.$restaurante['id_restaurante'],array("class"=>"form-horizontal row")); ?>
		<div class="col">
			<div class="form-group">
				<label for="razon_social_restaurante" class="control-label"><span class="text-danger">*</span>Razon Social Restaurante</label>
				<div class="">
					<input type="text" name="razon_social_restaurante" value="<?php echo ($this->input->post('razon_social_restaurante') ? $this->input->post('razon_social_restaurante') : $restaurante['razon_social_restaurante']); ?>" class="form-control" id="razon_social_restaurante" />
					<span class="text-danger"><?php echo form_error('razon_social_restaurante');?></span>
				</div>
			</div>
			<div class="form-group">
				<label for="direccion_restaurante" class="control-label"><span class="text-danger">*</span>Direccion Restaurante</label>
				<div class="">
					<input type="text" name="direccion_restaurante" value="<?php echo ($this->input->post('direccion_restaurante') ? $this->input->post('direccion_restaurante') : $restaurante['direccion_restaurante']); ?>" class="form-control" id="direccion_restaurante" />
					<span class="text-danger"><?php echo form_error('direccion_restaurante');?></span>
				</div>
			</div>
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>RUC Restaurante</label>
				<div class="">
					<input type="text" name="ruc_restaurante" value="<?php echo ($this->input->post('ruc_restaurante') ? $this->input->post('ruc_restaurante') : $restaurante['ruc_restaurante']); ?>" class="form-control" id="ruc_restaurante" />
					<span class="text-danger"><?php echo form_error('ruc_restaurante');?></span>
				</div>
			</div>

			<div class="form-group">
				<label for="codigo_ubigeo" class="control-label"><span class="text-danger">*</span>Código de ubigeo</label>
				<div class="">
					<input type="text" name="codigo_ubigeo" value="<?php echo ($this->input->post('codigo_ubigeo') ? $this->input->post('codigo_ubigeo') : $restaurante['codigo_ubigeo']); ?>" class="form-control" id="codigo_ubigeo" />
					<span class="text-danger"><?php echo form_error('codigo_ubigeo');?></span>
				</div>
			</div>
			<div class="form-group">
				<label for="urbanizacion" class="control-label"><span class="text-danger">*</span>Urbanización</label>
				<div class="">
					<input type="text" name="urbanizacion" value="<?php echo ($this->input->post('urbanizacion') ? $this->input->post('urbanizacion') : $restaurante['urbanizacion']); ?>" class="form-control" id="urbanizacion" />
					<span class="text-danger"><?php echo form_error('urbanizacion');?></span>
				</div>
			</div>


		</div>
		<div class="col">
			<div class="form-group">
				<label for="provincia" class="control-label"><span class="text-danger">*</span>Provincia</label>
				<div class="">
					<input type="text" name="provincia" value="<?php echo ($this->input->post('provincia') ? $this->input->post('provincia') : $restaurante['provincia']); ?>" class="form-control" id="provincia" />
					<span class="text-danger"><?php echo form_error('provincia');?></span>
				</div>
			</div>
			<div class="form-group">
				<label for="departamento" class="control-label"><span class="text-danger">*</span>Departamento</label>
				<div class="">
					<input type="text" name="departamento" value="<?php echo ($this->input->post('departamento') ? $this->input->post('departamento') : $restaurante['departamento']); ?>" class="form-control" id="departamento" />
					<span class="text-danger"><?php echo form_error('departamento');?></span>
				</div>
			</div>
			<div class="form-group">
				<label for="distrito" class="control-label"><span class="text-danger">*</span>Distrito</label>
				<div class="">
					<input type="text" name="distrito" value="<?php echo ($this->input->post('distrito') ? $this->input->post('distrito') : $restaurante['distrito']); ?>" class="form-control" id="distrito" />
					<span class="text-danger"><?php echo form_error('distrito');?></span>
				</div>
			</div>
			<div class="form-group">
				<label for="codigo_pais" class="control-label"><span class="text-danger">*</span>Código de país </label>
				<div class="">
					<input type="text" name="codigo_pais" value="<?php echo ($this->input->post('codigo_pais') ? $this->input->post('codigo_pais') : $restaurante['codigo_pais']); ?>" class="form-control" id="codigo_pais" />
					<span class="text-danger"><?php echo form_error('codigo_pais');?></span>
				</div>
			</div>
		</div>
	
	
	<div class="col-12 form-group text-center">
		<div class="">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>
</div>