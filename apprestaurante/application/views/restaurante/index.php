 <div class="container">
 	<?php foreach($restaurantes as $r){ ?>
	<div class="row form-group">
		<div class="col-12 p-0">

			<h4 class="font-weight-bold text-capitalize">Datos de Restaurante</h4>
			<a href="<?php echo site_url('restaurante/edit/'.$r['id_restaurante']); ?>" class="btn btn-success btn-sm float-right">Edit</a>
		</div>
	</div>

	
	<div class="row text-capitalize" >
		<div class="col">
			<div class="form-group">
				<label for="razon_social_restaurante" class="control-label"><span class="text-danger">*</span>Razon Social Restaurante</label>
				<div class="">
					<input readonly type="text" name="razon_social_restaurante" value="<?php echo $r['razon_social_restaurante']; ?>" class="form-control" id="razon_social_restaurante" />
				</div>
			</div>
			<div class="form-group">
				<label for="direccion_restaurante" class="control-label"><span class="text-danger">*</span>Direccion Restaurante</label>
				<div class="">
					<input readonly type="text" name="direccion_restaurante" value="<?php echo $r['direccion_restaurante']; ?>" class="form-control" id="direccion_restaurante" />
				</div>
			</div>
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>RUC Restaurante</label>
				<div class="">
					<input readonly type="text" name="ruc_restaurante" value="<?php echo $r['ruc_restaurante']; ?>" class="form-control" id="ruc_restaurante" />
				</div>
			</div>
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>Código de ubigeo</label>
				<div class="">
					<input readonly type="text" name="ruc_restaurante" value="<?php echo $r['codigo_ubigeo']; ?>" class="form-control" id="ruc_restaurante" />
				</div>
			</div>
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>Urbanización</label>
				<div class="">
					<input readonly type="text" name="ruc_restaurante" value="<?php echo $r['urbanizacion']; ?>" class="form-control" id="ruc_restaurante" />
				</div>
			</div>

			
		</div>
		<div class="col">
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>Provincia</label>
				<div class="">
					<input readonly type="text" name="ruc_restaurante" value="<?php echo $r['provincia']; ?>" class="form-control" id="ruc_restaurante" />
				</div>
			</div>
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>Departamento</label>
				<div class="">
					<input readonly type="text" name="ruc_restaurante" value="<?php echo $r['departamento']; ?>" class="form-control" id="ruc_restaurante" />
				</div>
			</div>
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>Distrito</label>
				<div class="">
					<input readonly type="text" name="ruc_restaurante" value="<?php echo $r['distrito']; ?>" class="form-control" id="ruc_restaurante" />
				</div>
			</div>
			<div class="form-group">
				<label for="ruc_restaurante" class="control-label"><span class="text-danger">*</span>Código de país </label>
				<div class="">
					<input readonly type="text" name="ruc_restaurante" value="<?php echo $r['codigo_pais']; ?>" class="form-control" id="ruc_restaurante" />
				</div>
			</div>
		</div>
		
	</div>
	

	<div class="pull-right">
	    <?php //echo $this->pagination->create_links(); ?>    
	</div>
</div>

<!-- --- -->

	

	<?php } ?>