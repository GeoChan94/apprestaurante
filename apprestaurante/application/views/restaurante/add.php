<?php echo form_open('restaurant/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="razon_social_restaurant" class="col-md-4 control-label"><span class="text-danger">*</span>Razon Social Restaurant</label>
		<div class="col-md-8">
			<input type="text" name="razon_social_restaurant" value="<?php echo $this->input->post('razon_social_restaurant'); ?>" class="form-control" id="razon_social_restaurant" />
			<span class="text-danger"><?php echo form_error('razon_social_restaurant');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="direccion_restaurant" class="col-md-4 control-label"><span class="text-danger">*</span>Direccion Restaurant</label>
		<div class="col-md-8">
			<input type="text" name="direccion_restaurant" value="<?php echo $this->input->post('direccion_restaurant'); ?>" class="form-control" id="direccion_restaurant" />
			<span class="text-danger"><?php echo form_error('direccion_restaurant');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="ruc_restaurant" class="col-md-4 control-label"><span class="text-danger">*</span>RUC Restaurant</label>
		<div class="col-md-8">
			<input type="text" name="ruc_restaurant" value="<?php echo $this->input->post('ruc_restaurant'); ?>" class="form-control" id="ruc_restaurant" />
			<span class="text-danger"><?php echo form_error('ruc_restaurant');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>