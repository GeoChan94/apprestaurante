 <div class="container">


<div class="row form-group">
		<div class="col-12 p-0">
			<div class="float-right">
				<a href="<?php echo site_url('detalle_venta/add'); ?>" class="btn btn-success text-capitalize">Agregar Pedido</a> 
			</div>
			<h4 class="font-weight-bold text-capitalize">Lista de Pedidos</h4>
		</div>
	</div>



	<!-- <div id="test-list">
	    <input type="text" class="search form-control form-group col-md-4" /> -->
	    <div class="row p-2 bg-primary text-light rounded-top text-center text-capitalize">
			<div class="col-1">Nº</div>
			<div class="col">Mesa</div>
			<div class="col">Importe de pedido</div>
			<!-- <div class="col">Tiempo pedido</div> -->
			<!-- <div class="col">Estado de pedido</div> -->
			<div class="col">Opciones</div>
		</div>
	    <!-- <div class="list"> -->

	    	<?php
	    	// echo json_encode($detalle_iddetalleventa_has_plato[51]);
	    	 foreach($detalle_ventas as $d){ ?>
				<div class="row bg-white border p-2 text-capitalize text-center" >
					<div class="col-1"><?php echo $d['id_detalle_venta']; ?></div>
				 	<div class="col"><?php echo $d['id_mesa']; ?></div>
					<div class="col"><?php
					$temp_suma_importe=0;
					// echo var_dump();
					if (!empty($detalle_iddetalleventa_has_plato[(int)$d['id_venta']])) {
						foreach ($detalle_iddetalleventa_has_plato[(int)$d['id_venta']] as $key => $value) {
							// echo $value['importe_plato'].'<br>';
							$temp_suma_importe+=$value['importe_plato']*$value['cantidad'];

						}
					}
					
					// echo $temp_suma_importe;
					// echo money_format('%i', $temp_suma_importe);
					echo 'S/. '.number_format($temp_suma_importe ,2)
					 // echo $d['id_detalle_venta']; 
					?></div>
					<!-- <div class="col" id='liveclock_<?=$d['id_detalle_venta']?>'>1 min<?php //echo $d['fecha_detalle_venta']; ?></div> -->
					<!-- <div class="col">espera</div> -->
					<div class="col text-center">
						<!-- <a href="<?php echo site_url('detalle_venta/edit/'.$d['id_detalle_venta']); ?>" class="btn btn-info btn-xs">Edit</a>   -->
						<a href="<?php echo site_url('detalle_venta/edit/'.$d['id_detalle_venta']); ?>" title="" class="btn btn-success btn-sm" data-iddetalleventa="<?=$d['id_detalle_venta'];?>">Editar</a>
						<!-- <a href="<?php echo site_url('detalle_venta/edit/'.$d['id_detalle_venta']); ?>" title="" class="btn btn-success btn-sm btn_detalle_pedido" data-iddetalleventa="<?=$d['id_detalle_venta'];?>">Editar</a> -->
						

				        <a href="<?php echo site_url('detalle_venta/remove/'.$d['id_detalle_venta']); ?>" class="btn btn-danger btn-sm">Eliminar</a>
				    </div>
				</div>
			<?php } ?>
	    <!-- </div>
	    <nav aria-label="...">
		  <ul class="pagination">
		  </ul>
		</nav>

	  </div> -->

	<!-- <table class="table table-striped table-bordered">
	    <tr>
			<th>Nº Detalle Pedido</th>
			<th>Cantidad</th>
			<th>Importe de pedido</th>
			<th>Fecha de pedido</th>
			<th>Estado de pedido</th>
			<th>Opciones</th>
	    </tr>
		<?php foreach($detalle_ventas as $d){ ?>
	    <tr>
			<td><?php echo $d['id_detalle_venta']; ?></td>
			<td><?php echo $d['descripcion_detalle_venta']; ?></td>
			<td><?php echo $d['importe_detalle_venta']; ?></td>
			<td><?php echo $d['fecha_detalle_venta']; ?></td>
			<td>
	            <a href="<?php echo site_url('detalle_venta/edit/'.$d['id_detalle_venta']); ?>" class="btn btn-info btn-xs">Edit</a> 
	            <a href="<?php echo site_url('detalle_venta/remove/'.$d['id_detalle_venta']); ?>" class="btn btn-danger btn-xs">Delete</a>
	        </td>
	    </tr>
		<?php } ?>
	</table> -->
	<div class="pull-right">
	    <?php echo $this->pagination->create_links(); ?>    
	</div>

</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" charset="utf-8" async defer>
	// var monkeyList = new List('test-list', {
	//   valueNames: ['mesa','importe','tiempo'],
	//   page: 2,
	//   pagination: true
	// });
	$(document).on('click', '.btn_detalle_pedido', function(event) {
		event.preventDefault();
		console.log($(this).data('iddetalleventa'));
		var text_pedidos=`lorem`;

		$('#exampleModal').modal('show')

		$('#exampleModal .modal-body').html(text_pedidos);
		console.log(text_pedidos);


		
		/* Act on the event */
	});
</script>
<style type="text/css" media="screen">
/*	.pagination {
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    padding-left: 0;
	    list-style: none;
	    border-radius: .25rem;
	}
	.pagination li.active a{
		z-index: 1;
	    color: #fff;
	    background-color: #007bff;
	    border-color: #007bff;
	}
	.pagination a {
	    position: relative;
	    display: block;
	    padding: .5rem .75rem;
	    margin-left: -1px;
	    line-height: 1.25;
	    color: #007bff;
	    background-color: #fff;
	    border: 1px solid #dee2e6;
	}*/
</style>