<!-- <?php echo form_open('detalle_venta/edit/'.$detalle_venta['id_detalle_venta'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="descripcion_detalle_venta" class="col-md-4 control-label"><span class="text-danger">*</span>Descripcion Detalle Venta</label>
		<div class="col-md-8">
			<input type="text" name="descripcion_detalle_venta" value="<?php echo ($this->input->post('descripcion_detalle_venta') ? $this->input->post('descripcion_detalle_venta') : $detalle_venta['descripcion_detalle_venta']); ?>" class="form-control" id="descripcion_detalle_venta" />
			<span class="text-danger"><?php echo form_error('descripcion_detalle_venta');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="cantidad_detalle_venta" class="col-md-4 control-label"><span class="text-danger">*</span>Cantidad Detalle Venta</label>
		<div class="col-md-8">
			<input type="text" name="cantidad_detalle_venta" value="<?php echo ($this->input->post('cantidad_detalle_venta') ? $this->input->post('cantidad_detalle_venta') : $detalle_venta['cantidad_detalle_venta']); ?>" class="form-control" id="cantidad_detalle_venta" />
			<span class="text-danger"><?php echo form_error('cantidad_detalle_venta');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="importe_detalle_venta" class="col-md-4 control-label"><span class="text-danger">*</span>Importe Detalle Venta</label>
		<div class="col-md-8">
			<input type="text" name="importe_detalle_venta" value="<?php echo ($this->input->post('importe_detalle_venta') ? $this->input->post('importe_detalle_venta') : $detalle_venta['importe_detalle_venta']); ?>" class="form-control" id="importe_detalle_venta" />
			<span class="text-danger"><?php echo form_error('importe_detalle_venta');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="fecha_detalle_venta" class="col-md-4 control-label">Fecha Detalle Venta</label>
		<div class="col-md-8">
			<input type="text" name="fecha_detalle_venta" value="<?php echo ($this->input->post('fecha_detalle_venta') ? $this->input->post('fecha_detalle_venta') : $detalle_venta['fecha_detalle_venta']); ?>" class="form-control" id="fecha_detalle_venta" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_administrador" class="col-md-4 control-label">Id Administrador</label>
		<div class="col-md-8">
			<input type="text" name="id_administrador" value="<?php echo ($this->input->post('id_administrador') ? $this->input->post('id_administrador') : $detalle_venta['id_administrador']); ?>" class="form-control" id="id_administrador" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?> -->

<?php echo "<script> 
	var platos=JSON.parse(`".json_encode($all_platos)."`); 
	console.log('platos',platos);
	var categorias=JSON.parse(`".json_encode($all_categorias)."`); 
	console.log('categorias',categorias);
	var mesas=JSON.parse(`".json_encode($all_mesas)."`); 
	console.log('mesas',mesas);
	var detalle_venta=JSON.parse(`".json_encode($detalle_venta)."`); 
	console.log('detalle_venta',detalle_venta);
	var detalle_mesa=JSON.parse(`".json_encode($detalle_mesa)."`); 
	console.log('detalle_mesa',detalle_mesa);
	
	
	var pedidos=JSON.parse(`".json_encode((array)$all_pedidos)."`); 
	// var pedidos= [];
	// for(var i in temp_pedidos){
	// 	pedidos[temp_pedidos[i].id_plato]={
	// 		cantidad:temp_pedidos[i].cantidad,
	// 		id_categoria:temp_pedidos[i].id_categoria,
	// 		id_plato:temp_pedidos[i].id_plato,
	// 		nombre_plato:temp_pedidos[i].nombre_plato,
	// 		venta:temp_pedidos[i].venta,
	// 		cantidad:temp_pedidos[i].cantidad,
	// 		importe_plato:temp_pedidos[i].importe_plato

	// 	};
	// }
	console.log('pedidos',pedidos);
	</script>"; 
	// echo $detalle_venta['id_detalle_venta'];
	// echo var_dump($detalle_venta);
?>
<div class="container-fluid h-100">
	<div class="row">
				<!-- <div class="col-12 form-group p-0"> -->
						<h4 class="font-weight-bold text-capitalize">Editar Pedido</h4>
					<!-- </div> -->
			</div>
	<div class="row">
		<div class="col-4">
			<div class="row div-mostrar-pedidos">
			<h4>Lista de platos</h4>

				<div class="col-12 pedidos p-0">	</div>

				<div class="align-self-end col-12 mt-5 btn btn-danger btn-lg font-weight-bold text-uppercase text-white" id="editar_pedido">
					<div class="">
						Actualizar pedido
					</div>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="row">
				<div class="col-12 mb-3">
					<div class="row align-items-center">
						<div class="col-auto ">
							<!-- <div class="row div_mesas_seleccionadas">
								
							</div> -->
							<span class="font-weight-bold h4">Mesa:</span>
							<span class="div_mesa_seleccionada">
								<span class=" bg-danger btn-sm ml-3 text-uppercase text-white font-weight-bold"><spam class="fa fa-cutlery "></spam> <?=$detalle_mesa['nombre_mesa'];?></span>
							</span>

						</div>
						<div class="col">
							<!-- <span class="fa fa-plus btn btn-success btn-lg btn_agregar_pedido_mesa"></span> -->
						</div>
					</div>
				</div>
				<div class="row m-0 pr-3 pl-3 mb-3">
					<?php foreach ($all_categorias as $key => $value): ?>
						<div class="col-auto p-0">
							<div class="cursor-pointer text-center text-uppercase font-weight-bold p-2 border bg-white text-dark btn_categoria" data-idcategoria="<?=$value['id_categoria']?>"><?=$value['nombre_categoria'] ?></div>
						</div>
					<?php endforeach ?>				
				</div>
				
					
			</div>
			<div class="row div_mostrar_filtrados_categoria">
		
			</div>
		</div>
	</div>

	
	


</div>
<!-- Modal mesa-->
<div class="modal fade" id="modal_seleccion_mesa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Selecionar de mesa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Seleccionar</button>
      </div> -->
      <div class="modal-footer text-center" id="mesa_error_msn">
      </div>
      			
    </div>
  </div>
</div>
<style type="text/css" media="screen">
	.div_mostrar_filtrados{
	   	height: 400px;
	   	overflow: auto;
	   	display: none;
	} 
</style>
<script type="text/javascript" charset="utf-8" async defer>
	// mostrar_mesas_no_disponibles();
	var all_platos='';
	var text_locations='';
	function mostrar_filtrados(){
		text_locations=`
			          <div class="row bg-primary text-light div-txt-search-destiny p-2" >
			           <div class="col">Nombre plato</div>
			            <div class="col">Precio/unidad</div>
			       		<div class="col">cantidad a comprar</div>
			            <div class="col btn-agregar-plato text-center">opciones</div>
			          </div>
			        `;
		let text = $("#input-search").val()?$("#input-search").val():'';
      text = text.trim().toLowerCase()
		// console.log('22222'+all_platos);
		all_platos  = platos.filter(function(plato){
	        return plato.nombre_plato.toLowerCase().indexOf(text) != -1;
	      }).sort(function(a,b){
	        if (a.count < b.count ) {
	          return 1;
	        }
	        if (a.count > b.count) {
	          return -1;
	        }
	        return 0;
	      });
	      
	      // console.log('all_platos:',all_platos);
	      // $('.div_mostrar_filtrados').html(all_platos);
	      all_platos.forEach(function(platos){
			        text_locations = text_locations + `
			          <div class="row div-txt-search-destiny border bg-white" >
			           <div class="col">${platos.nombre_plato}
			            </div>
			            <div class="col">${platos.importe_plato}</div>
			       		<div class="col"><input type="number" class="form-control" id="cantidad_${platos.id_plato}"></div>
			            <div class="col btn-agregar-plato text-center" data-idplato='${platos.id_plato}'><span class="btn btn-success btn-sm">agregar</span></div>
			          </div>
			        `;
			      });
	      // console.log('enraasas',all_platos.length);
	      if (text=='') {
	      	$('.div_mostrar_filtrados').css('display','none');
	      }else{
	      	$('.div_mostrar_filtrados').css('display','block');
	      }
	      if (all_platos.length>0) {
	      	$('.div_mostrar_filtrados').html(text_locations);
	      }else{
	      	$('.div_mostrar_filtrados').html('<div class="alert alert-danger mt-2 mb-2"><span class="fa fa-close"></span> plato no encontrado</div>');
	      }
	      
	}
	var platos_seleccionados=pedidos;
	mostrar_pedidos();
	console.log('platos_seleccionados',platos_seleccionados);
	$(document).on('click', '.btn-agregar-plato', function(event) {
		event.preventDefault();
		id =$(this).data('idplato');
		a_cantidad =$('#cantidad_'+id).val();
		// console.log('a_cantidada',a_cantidad);
		// if (a_cantidad>0) {
			platos.forEach(function(val,index){
				if (val.id_plato==id) {
					var platos_seleccionados_index=recorres_platos_seleccionados(val.id_plato);
					console.log('platos_seleccionados_index',platos_seleccionados_index);
					if (platos_seleccionados_index<0){
						val.cantidad=1;
						val.id_venta=parseInt(detalle_venta.id_venta);
						platos_seleccionados.push(val);

					}else{
						platos_seleccionados[platos_seleccionados_index].cantidad+=1;
					}
					console.log('platos_seleccionados',platos_seleccionados);

						// if (platos_seleccionados[id]===undefined) {

							
						// 	console.log('val.cantidad',(platos_seleccionados[id]));
						// 	console.log('test111');
						// 	val.cantidad=1;
						// 	platos_seleccionados[id]=val;
						// }else{
						// console.log('val.cantidad',isNaN(platos_seleccionados[id]));
						// console.log('test222');
						// if (!isNaN(platos_seleccionados[id].cantidad)){
						// 	val.cantidad=platos_seleccionados[id].cantidad+1;
							
						// }else{
							
						// 	if (isNaN(val.cantidad)) {
						// 		val.cantidad=1;
								
						// 	}else{
						// 		val.cantidad=val.cantidad+1;
						// 	}
						// }
						// console.log('platos_seleccionados[id]',platos_seleccionados[id]);
						// platos_seleccionados[id]=val;	
						// }
						
						// if (platos_seleccionados.id_plato!=id) {
							
						// }
						// if (platos_seleccionados.id_plato==id){
						// 	val.cantidad=platos_seleccionados.cantidad+1;
						// 	platos_seleccionados.push(val);
						// }
						
					// platos.splice(index,1);
					// console.log('ahor',platos_seleccionados.indexOf(id_plato)==id);
					mostrar_pedidos();
					// mostrar_filtrados();
					$('.div_mostrar_filtrados').css('display', 'none');
					$('#input-search').val('');
					// console.log('sibad');
					$('#input-search').focus();
					// }else{
						// alert('cantidad excede al stock');
					// }
				}
			});	
		// }else{
			// alert('ingrese cantidad a comprar');
		// }		
	});
	$(document).on('click', '.btn-eliminiar-plato', function(event) {
		event.preventDefault();
		var id=$(this).data('idplato');
		platos_seleccionados.forEach(function(val,index){
			if (val.id_plato==id){
				var platos_seleccionados_index=recorres_platos_seleccionados(val.id_plato);
				if (platos_seleccionados[platos_seleccionados_index].cantidad==1) {
					platos_seleccionados[platos_seleccionados_index].cantidad-=1;
					// platos_seleccionados.splice(index,1);
				}else{
					if (platos_seleccionados[platos_seleccionados_index].cantidad>1) {
						platos_seleccionados[platos_seleccionados_index].cantidad-=1;
					}
					
				}
				
				console.log('platos_seleccionados',platos_seleccionados);
			}
			mostrar_pedidos();
			mostrar_filtrados();
			});	

		/* Act on the event */
	});
function recorres_platos_seleccionados(key){
	console.log('index',index);
	var index=-1;
	platos_seleccionados.forEach((value,i)=>{
		if (parseInt(value.id_plato)==parseInt(key)){
			index = i;
		}
	});
	return index;
}
var precio_total;
function mostrar_pedidos(){
	precio_total=0;
	text_pedidos='';
	text_pedidos=`
			        `;
	platos_seleccionados.forEach(function(pedido,index){
		if (pedido.cantidad>0) {
			text_pedidos = text_pedidos + `
			          <div class="row m-0 border bg-white" >
			           <div class="col">
				           	<div class="font-weight-bold">${pedido.nombre_plato}</div>
				           	<div>
				           		<small>${pedido.cantidad} Unidades a S/.${pedido.importe_plato}/unidad</small>
				           	</div>

			            </div>
			            <div class="col-auto p-0">S/.${pedido.cantidad*pedido.importe_plato}</div>
			            <div class="col-auto btn-eliminiar-plato text-center " data-idplato='${pedido.id_plato}'><span class="btn btn-danger btn-sm fa fa-minus"></span></div>
			          </div>
			        `;
			        precio_total+=parseFloat(pedido.importe_plato)*parseFloat(pedido.cantidad);
		}
		
			        // console.log(index,precio_total);
			        // $('#total_venta').val(parseFloat(precio_total));
	});
	// ---------
	// for(var i in platos_seleccionados){

	// 	text_pedidos = text_pedidos + `
	// 		          <div class="row m-0 border bg-white" >
	// 		           <div class="col">
	// 			           	<div class="font-weight-bold">${platos_seleccionados[i].nombre_plato}</div>
	// 			           	<div>
	// 			           		<small>${platos_seleccionados[i].cantidad} Unidades a S/.${platos_seleccionados[i].importe_plato}/unidad</small>
	// 			           	</div>

	// 		            </div>
	// 		            <div class="col-auto p-0">S/.${platos_seleccionados[i].cantidad*platos_seleccionados[i].importe_plato}</div>
	// 		            <div class="col-auto btn-eliminiar-plato text-center " data-idplato='${platos_seleccionados[i].id_plato}'><span class="btn btn-danger btn-sm fa fa-minus"></span></div>
	// 		          </div>
	// 		        `;
	// 		        precio_total+=parseFloat(platos_seleccionados[i].importe_plato)*parseFloat(platos_seleccionados[i].cantidad);
	// 		        // console.log(index,precio_total);
	// 		        // $('#total_venta').val(parseFloat(precio_total));
	// }
	text_pedidos = text_pedidos + `
		<div class="row m-0 border bg-danger  text-white" >
			<div class="col-12 text-center p-1 font-weight-bold">
				<span>TOTAL  S/. ${parseFloat(precio_total)}</span>
			</div>
		</div>
	`;
	console.log('test',platos_seleccionados.length);
	if (platos_seleccionados.length>0) {
	      	$('.pedidos').html(text_pedidos);
	      }else{
	      	$('.pedidos').html('');
	      }
}

	 $(document).on('keyup',"#input-search",mostrar_filtrados);
	 $(document).on('keyup', '#impuesto', function(event) {
	 	event.preventDefault();
	 	if ($('#impuesto').val()!=''){
	 		if (platos_seleccionados.length>0) {
	 			$('#total_venta').val(parseFloat(precio_total)+parseFloat(this.value));
		 	}else{
		 		$('#total_venta').val(0);
		 	}
	 	}else{
			$('#total_venta').val(parseFloat(precio_total));
	 	}
	 	

	 	
	 	/* Act on the event */
	 });
		
      // $('').click(abrir_div_resultados);
      $(document).on('click', '.btn_agregar_pedido_mesa', function(event) {
      	$('#modal_seleccion_mesa .modal-footer').html('');
      	event.preventDefault();
      	mostrar_mesas_modal();
      	$('#modal_seleccion_mesa').modal();
      	$(document).on('click', '.btn_mesa_selecionar', function(event) {
	      	event.preventDefault();
	      	/* Act on the event */
	      	btn_mesa_selecionar=`<span class=" bg-danger btn-sm ml-3 text-uppercase text-white font-weight-bold"><spam class="fa fa-cutlery "></spam> ${$(this).data('nombremesa')}</span>`;
	      	$.ajax({
	      		url: base_url+'Mesa/state_mesa_pedido/'+$(this).data('idmesa'),
				type: 'post',
				dataType: 'json',
				// data: {facturacion:facturacion},
	      	})
	      	.done(function(data) {
	      		console.log("success",data);
	      		if (data==0) {
	      			$('.div_mesa_seleccionada').html(btn_mesa_selecionar);
	      			$('#modal_seleccion_mesa').modal('hide');
	      		}else{
	      			$('#mesa_error_msn').html(`<div class="text-danger font-weight-bold text-center w-100"> Mesa con pedido sin pagar <a href="<?=base_url()?>" class="btn btn-danger">Ver detalles</a></div>`);
	      		}
	      	})
	      	.fail(function(data) {
	      		console.log("error",data);
	      	});
	      	
	      	

	    });
      });
      
      
      $(document).on('click', '.btn_categoria', function(event) {
      	event.preventDefault();
      	text_locations='';
      	mostrar_filtrados_categoria($(this).data('idcategoria'));
      	all_platos.forEach(function(platos){
			        text_locations = text_locations + `
			          <div class="col-3">
					        <div class="row p-3 m-0 mb-1 mt-1 text-center weight-bold div-txt-search-destiny border bg-white cursor-pointer btn-agregar-plato" data-idplato="${platos.id_plato}"">
				          		<div class="col-12  text-uppercase">${platos.nombre_plato}
				            	</div>
				            	<div class="col-12">S/. ${platos.importe_plato}</div>
				            	<!--<div class="col-12">
				            		<div><spam class="float-left text-white fa fa-minus bg-danger fa-2x p-3"></spam></div>
				            		<div><spam class="float-right text-white fa fa-plus bg-success fa-2x p-3"></spam></div>
				            	</div>-->
				          	</div>
			          </div>
			        `;
			      });
      	$('.div_mostrar_filtrados_categoria').html(text_locations);
      	/* Act on the event */
      });
      var text_mesas='';
      var idmesa=0;
      idmesa=detalle_mesa.id_mesa;
      function mostrar_mesas_modal(){
      	text_mesas='<div class="row">';
      	mesas.forEach( function(mesa, index) {
      		text_mesas+=`
      		<div class="col-3 mb-2 cursor-pointer">
					<div id="mesa_${mesa.id_mesa}" data-idmesa="${mesa.id_mesa}" data-nombremesa="${mesa.nombre_mesa}" class="bg-white border rounded text-center font-weight-bold text-uppercase p-4 btn_mesa_selecionar">
						<spam class="fa fa-cutlery fa-4x"></spam>
						<div>${mesa.nombre_mesa}</div>
					</div>
				</div>
      		`;
      		
      	});
      	text_mesas+='</div>';
      	$('#modal_seleccion_mesa .modal-body').html(text_mesas);
      	
      }
      var text_mesas_no_disponibles='';
    //   function mostrar_mesas_no_disponibles(){
    //   	text_mesas_no_disponibles=''
    //   	mesas.forEach( function(mesa, index) {
    //   		text_mesas_no_disponibles+=`
    //   		<div class="col cursor-pointer">
				// 	<div id="mesa_${mesa.id_mesa}" data-idmesa="${mesa.id_mesa}" data-nombremesa="${mesa.nombre_mesa}" class="bg-white border rounded text-center font-weight-bold text-uppercase p-1">
				// 		<span class="fa fa-cutlery"></span>
				// 		<span> ${mesa.nombre_mesa}</span>
				// 	</div>
				// </div>
    //   		`;
    //   	});
    //   	$('.div_mesas_seleccionadas').html(text_mesas_no_disponibles);
      	
    //   }
      function mostrar_filtrados_categoria(idcategoria){
      // 	let text = $("#input-search").val()?$("#input-search").val():'';
      // text = text.trim().toLowerCase()
      	all_platos  = platos.filter(function(plato){
	        return plato.id_categoria.indexOf(idcategoria) != -1;
	      }).sort(function(a,b){
	        if (a.count < b.count ) {
	          return 1;
	        }
	        if (a.count > b.count) {
	          return -1;
	        }
	        return 0;
	      });
	      console.log('all_platos_categorias',all_platos);
      }
// add pedido
	$(document).ready(function(){
		$('#editar_pedido').click(function(event) {
			if (idmesa>0) {
				if (platos_seleccionados.length>0) {
					var datos_pedido ={
						'id_mesa':idmesa,
						'importe_detalle_venta':precio_total,
						'id_detalle_venta':detalle_venta.id_detalle_venta
					}
					console.log('platos_seleccionados',platos_seleccionados);
					console.log('datos_pedido',datos_pedido);
					$.ajax({
						type: "POST",
						url: "<?=base_url(); ?>" + "Detalle_venta/edit/"+detalle_venta.id_detalle_venta,
						dataType: 'json',
						data: {platos_seleccionados:platos_seleccionados,datos_pedido:datos_pedido},
					})
					.done(function(res) {
						console.log("success");
						console.log('entro 11',res);
						window.location = `<?=base_url()?>`;
					})
					.fail(function() {
						console.log("error");
					});
					

				}else{
					alert('Agrege Platos');
				}

			}else{
				alert('Seleccione mesa');
			}
			
		});
	});


	</script>