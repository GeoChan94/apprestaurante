<?php //echo form_open('mesa/add',array("class"=>"form-horizontal")); ?>

	<!-- <div class="form-group">
		<label for="nombre_mesa" class="col-md-4 control-label"><span class="text-danger">*</span>Nombre Mesa</label>
		<div class="col-md-8">
			<input type="text" name="nombre_mesa" value="<?php echo $this->input->post('nombre_mesa'); ?>" class="form-control" id="nombre_mesa" />
			<span class="text-danger"><?php echo form_error('nombre_mesa');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="numero_mesa" class="col-md-4 control-label"><span class="text-danger">*</span>Numero Mesa</label>
		<div class="col-md-8">
			<input type="text" name="numero_mesa" value="<?php echo $this->input->post('numero_mesa'); ?>" class="form-control" id="numero_mesa" />
			<span class="text-danger"><?php echo form_error('numero_mesa');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion_mesa" class="col-md-4 control-label">Descripcion Mesa</label>
		<div class="col-md-8">
			<input type="text" name="descripcion_mesa" value="<?php echo $this->input->post('descripcion_mesa'); ?>" class="form-control" id="descripcion_mesa" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_administrador" class="col-md-4 control-label">Id Administrador</label>
		<div class="col-md-8">
			<input type="text" name="id_administrador" value="<?php echo $this->input->post('id_administrador'); ?>" class="form-control" id="id_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="imagen_mesa" class="col-md-4 control-label">Imagen Mesa</label>
		<div class="col-md-8">
			<input type="text" name="imagen_mesa" value="<?php echo $this->input->post('imagen_mesa'); ?>" class="form-control" id="imagen_mesa" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div> -->

<?php //echo form_close(); ?>
<div class="container">
<?php echo form_open('mesa/add',array("class"=>"form-horizontal")); ?>
	<div class="form-group">
		<div class="col-12 form-group p-0">
				<h4 class="font-weight-bold text-capitalize">Agregar nuevo Plato</h4>
			</div>
	</div>
	<div class="row form-group">
		<div class="col">
			<label for="nombre_mesa" class="control-label"><span class="text-danger">*</span>Nombre Mesa</label>
			<div class="">
				<input type="text" name="nombre_mesa" value="<?php echo $this->input->post('nombre_mesa'); ?>" class="form-control" id="nombre_mesa" />
				<span class="text-danger"><?php echo form_error('nombre_mesa');?></span>
			</div>
		</div>
		<div class="col">
			<label for="descripcion_mesa" class="control-label">Descripcion Mesa</label>
			<div class="">
				<input type="text" name="descripcion_mesa" value="<?php echo $this->input->post('descripcion_mesa'); ?>" class="form-control" id="descripcion_mesa" />
			</div>
		</div>
	</div>
	<div class="form-group row">
			<div class="col">
				<button type="submit" class="float-right btn btn-success">Agregar Mesa</button>
	        </div>
	</div>	

<?php echo form_close(); ?>	
</div>