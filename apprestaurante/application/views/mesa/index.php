<div class="container">
		<div class="row form-group">
			<div class="col-12 p-0">
				<div class="float-right">
					<a href="<?php echo site_url('mesa/add'); ?>" class="btn btn-success text-capitalize">agregar Mesa</a> 
				</div>
				<h4 class="font-weight-bold text-capitalize">Lista Mesas </h4>
			</div>
		</div>
		<div class="row bg-primary text-light p-2 text-capitalize" >
			<div class="col">#</div>
		 	<div class="col">Nombre Mesa</div>
			<div class="col">Descripcion Mesa</div>
			<div class="col text-center">opciones</div>
		</div>
		<?php foreach($mesas as $m){ ?>
		<div class="row bg-white border p-2 text-capitalize" >
			<div class="col"><?php echo $m['id_mesa']; ?></div>
		 	<div class="col"><?php echo $m['nombre_mesa']; ?></div>
			<div class="col"><?php echo $m['descripcion_mesa']; ?></div>
			<div class="col text-center">
				<a href="<?php echo site_url('mesa/edit/'.$m['id_mesa']); ?>" class="btn btn-success btn-sm">Edit</a>  
		        <a href="<?php echo site_url('mesa/remove/'.$m['id_mesa']); ?>" class="btn btn-danger btn-sm">Eliminar</a>
		    </div>
		</div>
		<?php } ?>
		
</div>
<!-- <table class="table table-striped table-bordered">
    <tr>
		<th>Id Mesa</th>
		<th>Nombre Mesa</th>
		<th>Numero Mesa</th>
		<th>Descripcion Mesa</th>
		<th>Id Administrador</th>
		<th>Imagen Mesa</th>
		<th>Actions</th>
    </tr>
	<?php foreach($mesas as $m){ ?>
    <tr>
		<td><?php echo $m['id_mesa']; ?></td>
		<td><?php echo $m['nombre_mesa']; ?></td>
		<td><?php echo $m['numero_mesa']; ?></td>
		<td><?php echo $m['descripcion_mesa']; ?></td>
		<td><?php echo $m['id_administrador']; ?></td>
		<td><?php echo $m['imagen_mesa']; ?></td>
		<td>
            <a href="<?php echo site_url('mesa/edit/'.$m['id_mesa']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('mesa/remove/'.$m['id_mesa']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table> -->
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
