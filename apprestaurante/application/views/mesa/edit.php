<?php echo form_open('mesa/edit/'.$mesa['id_mesa'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="nombre_mesa" class="col-md-4 control-label"><span class="text-danger">*</span>Nombre Mesa</label>
		<div class="col-md-8">
			<input type="text" name="nombre_mesa" value="<?php echo ($this->input->post('nombre_mesa') ? $this->input->post('nombre_mesa') : $mesa['nombre_mesa']); ?>" class="form-control" id="nombre_mesa" />
			<span class="text-danger"><?php echo form_error('nombre_mesa');?></span>
		</div>
	</div>

	<div class="form-group">
		<label for="descripcion_mesa" class="col-md-4 control-label">Descripcion Mesa</label>
		<div class="col-md-8">
			<input type="text" name="descripcion_mesa" value="<?php echo ($this->input->post('descripcion_mesa') ? $this->input->post('descripcion_mesa') : $mesa['descripcion_mesa']); ?>" class="form-control" id="descripcion_mesa" />
		</div>
	</div>
	

	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>