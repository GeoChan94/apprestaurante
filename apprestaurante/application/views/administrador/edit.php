<?php echo form_open('administrador/edit/'.$administrador['id_administrador'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="password_administrador" class="col-md-4 control-label">Password Administrador</label>
		<div class="col-md-8">
			<input type="text" name="password_administrador" value="<?php echo ($this->input->post('password_administrador') ? $this->input->post('password_administrador') : $administrador['password_administrador']); ?>" class="form-control" id="password_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="nombres_administrador" class="col-md-4 control-label">Nombres Administrador</label>
		<div class="col-md-8">
			<input type="text" name="nombres_administrador" value="<?php echo ($this->input->post('nombres_administrador') ? $this->input->post('nombres_administrador') : $administrador['nombres_administrador']); ?>" class="form-control" id="nombres_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="apellidos_administrador" class="col-md-4 control-label">Apellidos Administrador</label>
		<div class="col-md-8">
			<input type="text" name="apellidos_administrador" value="<?php echo ($this->input->post('apellidos_administrador') ? $this->input->post('apellidos_administrador') : $administrador['apellidos_administrador']); ?>" class="form-control" id="apellidos_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="email_administrador" class="col-md-4 control-label">Email Administrador</label>
		<div class="col-md-8">
			<input type="text" name="email_administrador" value="<?php echo ($this->input->post('email_administrador') ? $this->input->post('email_administrador') : $administrador['email_administrador']); ?>" class="form-control" id="email_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="telefono_administrador" class="col-md-4 control-label">Telefono Administrador</label>
		<div class="col-md-8">
			<input type="text" name="telefono_administrador" value="<?php echo ($this->input->post('telefono_administrador') ? $this->input->post('telefono_administrador') : $administrador['telefono_administrador']); ?>" class="form-control" id="telefono_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="usuario_administrador" class="col-md-4 control-label">Usuario Administrador</label>
		<div class="col-md-8">
			<input type="text" name="usuario_administrador" value="<?php echo ($this->input->post('usuario_administrador') ? $this->input->post('usuario_administrador') : $administrador['usuario_administrador']); ?>" class="form-control" id="usuario_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_administrador" class="col-md-4 control-label">Tipo Administrador</label>
		<div class="col-md-8">
			<input type="text" name="tipo_administrador" value="<?php echo ($this->input->post('tipo_administrador') ? $this->input->post('tipo_administrador') : $administrador['tipo_administrador']); ?>" class="form-control" id="tipo_administrador" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_restaurant" class="col-md-4 control-label">Id Restaurant</label>
		<div class="col-md-8">
			<input type="text" name="id_restaurant" value="<?php echo ($this->input->post('id_restaurant') ? $this->input->post('id_restaurant') : $administrador['id_restaurant']); ?>" class="form-control" id="id_restaurant" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>