<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Document</title>
    <?php
        $this->load->view('recursos/css');
        $this->load->view('recursos/js');
    ?>
</head>
<body class="bg-light">
        <?php
            $this->load->view('menu/menu');
        ?>
        <content class="row m-0 pt-3 pb-3">
        	<div class="col-12 ">
                <?php	
                    // $data['ventas_hoy'] = $ventas_hoy;
                    // $data['ventas_semana'] = $ventas_semana;
                    // $data['ventas_mes'] = $ventas_mes;
	            	if(isset($_view) && $_view)
					$this->load->view($_view);
				?>
        	</div>
        </content>
        <footer class="footer">
            <?php 
            $this->load->view('footer/footer'); 
            ?>
        </footer>
</body>
</html>