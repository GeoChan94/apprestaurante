<nav class=" navbar sticky-top navbar-expand-lg navbar-dark bg-primary text-light navbar-main">
        <div class="container">
            <a class="navbar-brand " href="#"><span >App</span><span class="font-weight-bold">Restaurante</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?=base_url()?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>detalle_venta">Pedidos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>reporte">Reporte</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Configuración
                          </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?=base_url()?>categoria">Categorias</a>
                            <a class="dropdown-item" href="<?=base_url()?>plato">Platos</a>
                             <div role="separator" class="dropdown-divider"></div>
                             <a class="dropdown-item" href="<?=base_url()?>mesa">Mesas</a>
                            <!--<a class="dropdown-item" href="#three">three</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a> -->
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?=base_url()?>restaurante">Restaurante</a>
                            <!-- <div class="dropdown-divider"></div> -->
                            <!-- <a class="dropdown-item" href="<?=base_url()?>factura">Factura</a> -->
                            <!-- <a class="dropdown-item" href="<?=base_url()?>boleta">Boleta</a> -->
                            <!-- <div class="dropdown-divider"></div> -->
                            <!-- <a class="dropdown-item" href="javascript:void(0)"  data-toggle="modal" data-target="#modalImpresora">Impresora</a> -->
                        </div>
                    </li>
                </ul>

            </div>
        </div>

    </nav>


    <div id="modalImpresora" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Comfigurar Impresora</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <p>Ingrese IP de la Impresora</p>
                <input type="text" name="txtIp" class="form-control" id="txtIp">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success btn-guardar-ip">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(document).on('click', '.btn-guardar-ip', function(event) {
                event.preventDefault();
                var ip = document.getElementById("txtIp").value;
                if (ip.length > 3 ) {
                    $.ajax({
                        url: '<?=base_url()?>imprimir/actualizarip',
                        type: 'post',
                        dataType: 'json',
                        data: {ip: ip},
                    }).done(function(data) {
                        $("#modalImpresora").modal('hide');
                    }).fail(function(e) {
                        console.log(e.responseText);
                    });                    
                }else {
                    alert("Ingrese una IP vàlida.:!");
                    $("#txtIp").focus();
                }
            });
            $('#modalImpresora').on('show.bs.modal', function (event) {
                $("#txtIp").focus();
                $.ajax({
                    url: '<?=base_url()?>imprimir/impresoraip',
                    type: 'POST',
                    dataType: 'json',
                    data: {data: Date()},
                }).done(function(data) {
                    if (data.response === "success") {
                        $("#txtIp").empty().val(data.ip);
                    }
                }).fail(function(e) {
                    console.log(e.responseText);
                });
                
            });
        });
    </script>