<div class="container">
<?php echo form_open('plato/add',array("class"=>"form-horizontal")); ?>
<div class="form-group">
	<div class="col-12 form-group p-0">
			<h4 class="font-weight-bold text-capitalize">Agregar nuevo Plato</h4>
		</div>
</div>
<div class="row form-group">
	<div class="col">
		<label for="nombre_plato" class=" control-label"><span class="text-danger">*</span>Nombre Plato</label>
		<div class="">
			<input type="text" name="nombre_plato" value="<?php echo $this->input->post('nombre_plato'); ?>" class="form-control" id="nombre_plato" />
			<span class="text-danger"><?php echo form_error('nombre_plato');?></span>
		</div>
	</div>
	<div class="col">
		<label for="importe_plato" class=" control-label"><span class="text-danger">*</span>Importe Plato</label>
		<div class="">
			<input type="text" name="importe_plato" value="<?php echo $this->input->post('importe_plato'); ?>" class="form-control" id="importe_plato" />
			<span class="text-danger"><?php echo form_error('importe_plato');?></span>
		</div>
	</div>
	<div class="col">
	    <label><span class="text-danger">*</span>Categoria</label>
	    <select name="id_categoria" class="form-control" id="cat_plato">
					<option value="">Seleccione categoria</option>
					<?php 
					foreach($all_categorias as $categoria)
					{
						$selected = ($categoria['id_categoria'] == $this->input->post('id_categoria')) ? ' selected="selected"' : "";

						echo '<option value="'.$categoria['id_categoria'].'" '.$selected.'>'.$categoria['nombre_categoria'].'</option>';
					} 
					?>
		</select>
	 </div>


</div>
<div class="form-group row">
		<div class="col">
			<button type="submit" class="float-right btn btn-success">Agregar Plato</button>
        </div>
	</div>

	
<!-- 	<div class="form-group">
		<label for="imagen_plato" class="col-md-4 control-label">Imagen Plato</label>
		<div class="col-md-8">
			<input type="text" name="imagen_plato" value="<?php echo $this->input->post('imagen_plato'); ?>" class="form-control" id="imagen_plato" />
		</div>
	</div> -->
	<!-- <div class="form-group">
		<label for="id_administrador" class="col-md-4 control-label">Id Administrador</label>
		<div class="col-md-8">
			<input type="text" name="id_administrador" value="<?php echo $this->input->post('id_administrador'); ?>" class="form-control" id="id_administrador" />
		</div>
	</div> -->
	
	

<?php echo form_close(); ?>	
</div>
