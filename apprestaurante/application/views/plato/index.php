<!-- <div class="pull-right">
	<a href="<?php echo site_url('plato/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Plato</th>
		<th>Id Categoria</th>
		<th>Nombre Plato</th>
		<th>Importe Plato</th>
		<th>Imagen Plato</th>
		<th>Id Administrador</th>
		<th>Actions</th>
    </tr>
	<?php foreach($platos as $p){ ?>
    <tr>
		<td><?php echo $p['id_plato']; ?></td>
		<td><?php echo $p['id_categoria']; ?></td>
		<td><?php echo $p['nombre_plato']; ?></td>
		<td><?php echo $p['importe_plato']; ?></td>
		<td><?php echo $p['imagen_plato']; ?></td>
		<td><?php echo $p['id_administrador']; ?></td>
		<td>
            <a href="<?php echo site_url('plato/edit/'.$p['id_plato']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('plato/remove/'.$p['id_plato']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
 -->
 <div class="container">
	<div class="row form-group">
		<div class="col-12 p-0">
			<div class="float-right">
				<a href="<?php echo site_url('plato/add'); ?>" class="btn btn-success text-capitalize">Agregar plato</a> 
			</div>
			<h4 class="font-weight-bold text-capitalize">Lista de platos</h4>
		</div>
	</div>
	<div class="row bg-primary text-light p-2 text-capitalize" >
		<div class="col">#</div>
	 	<div class="col">Nombre plato</div>
		<div class="col">Categoria</div>
		<div class="col">precio</div>
		<div class="col text-center">opciones</div>
	</div>
	<?php foreach($platos as $p){ ?>
	<div class="row bg-white border p-2 text-capitalize" >
		<div class="col"><?php echo $p['id_plato']; ?></div>
	 	<div class="col"><?php echo $p['nombre_plato']; ?></div>
		<div class="col"><?php echo $p['nombre_categoria']['nombre_categoria']; ?></div>
		<div class="col"><?php echo $p['importe_plato']; ?></div>
		<div class="col text-center">
			<a href="<?php echo site_url('plato/edit/'.$p['id_plato']); ?>" class="btn btn-success btn-sm">Edit</a>  
	        <a href="<?php echo site_url('plato/remove/'.$p['id_plato']); ?>" class="btn btn-danger btn-sm">Eliminar</a>
	    </div>
	</div>
	<?php } ?>

	<div class="pull-right">
	    <?php //echo $this->pagination->create_links(); ?>    
	</div>
</div>