<?php echo form_open('plato/edit/'.$plato['id_plato'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="nombre_plato" class="col-md-4 control-label"><span class="text-danger">*</span>Nombre Plato</label>
		<div class="col-md-8">
			<input type="text" name="nombre_plato" value="<?php echo ($this->input->post('nombre_plato') ? $this->input->post('nombre_plato') : $plato['nombre_plato']); ?>" class="form-control" id="nombre_plato" />
			<span class="text-danger"><?php echo form_error('nombre_plato');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="importe_plato" class="col-md-4 control-label"><span class="text-danger">*</span>Importe Plato</label>
		<div class="col-md-8">
			<input type="text" name="importe_plato" value="<?php echo ($this->input->post('importe_plato') ? $this->input->post('importe_plato') : $plato['importe_plato']); ?>" class="form-control" id="importe_plato" />
			<span class="text-danger"><?php echo form_error('importe_plato');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<label for="id_categoria" class="col-md-4 control-label">Categoria</label>
		<div class="col-md-8">

			<select name="id_categoria" class="form-control" id="cat_plato">
					<option value="<?php echo ($this->input->post('id_categoria') ? $this->input->post('id_categoria') : (int)$categoria['id_categoria']); ?>"><?php echo $categoria['nombre_categoria'] ?></option>
					<?php 
					foreach($all_categorias as $categoria)
					{
						$selected = ($categoria['id_categoria'] == $this->input->post('id_categoria')) ? ' selected="selected"' : "";

						echo '<option value="'.$categoria['id_categoria'].'" '.$selected.'>'.$categoria['nombre_categoria'].'</option>';
					} 
					?>
		</select>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>