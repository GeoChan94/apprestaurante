<div class="container">
	<?php echo form_open('categoria/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group row">
		<div class="col-12 form-group">
			<h4 class="font-weight-bold">Agregar nueva categoria</h4>
		</div>

		<div class="col-6">
			<label for="nombre_categoria" class="control-label"><span class="text-danger">*</span>Nombre Categoria</label>
			<input type="text" name="nombre_categoria" value="<?php echo $this->input->post('nombre_categoria'); ?>" class="form-control" id="nombre_categoria" />
			<span class="text-danger"><?php echo form_error('nombre_categoria');?></span>
		</div>
		<div class="col-6">
			<label for="descripcion_categoria" class=" control-label">Descripcion Categoria</label>
			<input type="text" name="descripcion_categoria" value="<?php echo $this->input->post('descripcion_categoria'); ?>" class="form-control" id="descripcion_categoria" />
			<span class="text-danger"><?php echo form_error('descripcion_categoria');?></span>
		</div>
	</div>

	<!-- <div class="form-group">
		<label for="fecha_categoria" class="col-md-4 control-label">Fecha Categoria</label>
		<div class="col-md-8">
			<input type="text" name="fecha_categoria" value="<?php echo $this->input->post('fecha_categoria'); ?>" class="form-control" id="fecha_categoria" />
		</div>
	</div> -->
	<!-- <div class="form-group">
		<label for="imagen_categoria" class="col-md-4 control-label"><span class="text-danger">*</span>Imagen Categoria</label>
		<div class="col-md-8">
			<input type="text" name="imagen_categoria" value="<?php echo $this->input->post('imagen_categoria'); ?>" class="form-control" id="imagen_categoria" />
			<span class="text-danger"><?php echo form_error('imagen_categoria');?></span>
		</div>
	</div> -->
	<!-- <div class="form-group">
		<label for="id_administrador" class="col-md-4 control-label">Id Administrador</label>
		<div class="col-md-8">
			<input type="text" name="id_administrador" value="<?php echo $this->input->post('id_administrador'); ?>" class="form-control" id="id_administrador" />
		</div>
	</div> -->
	
	<div class="form-group row">
		<div class="col">
			<button type="submit" class="float-right btn btn-success">Agregar Categoria</button>
        </div>
	</div>

<?php echo form_close(); ?>
</div>
