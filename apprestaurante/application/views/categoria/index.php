<!-- <table class="table table-striped table-bordered">
    <tr>
		<th>Id Categoria</th>
		<th>Nombre Categoria</th>
		<th>Descripcion Categoria</th>
		<th>Fecha Categoria</th>
		<th>Imagen Categoria</th>
		<th>Id Administrador</th>
		<th>Actions</th>
    </tr>
	<?php foreach($categorias as $c){ ?>
    <tr>
		<td><?php echo $c['id_categoria']; ?></td>
		<td><?php echo $c['nombre_categoria']; ?></td>
		<td><?php echo $c['descripcion_categoria']; ?></td>
		<td><?php echo $c['fecha_categoria']; ?></td>
		<td><?php echo $c['imagen_categoria']; ?></td>
		<td><?php echo $c['id_administrador']; ?></td>
		<td>
            <a href="<?php echo site_url('categoria/edit/'.$c['id_categoria']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('categoria/remove/'.$c['id_categoria']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table> -->
<div class="container">
	<div class="row form-group">
		<div class="col-12 p-0">
			<div class="float-right">
				<a href="<?php echo site_url('categoria/add'); ?>" class="btn btn-success text-capitalize">Agregar Categoria</a> 
			</div>
			<h4 class="font-weight-bold text-capitalize">Lista de categorias</h4>
		</div>
	</div>
	<div class="row bg-primary text-light p-2 text-capitalize" >
		<div class="col">#</div>
	 	<div class="col">Nombre Categoria</div>
		<div class="col">Descripcion Categoria</div>
		<div class="col text-center">opciones</div>
	</div>
	<?php foreach($categorias as $c){ ?>
	<div class="row bg-white border p-2 text-capitalize" >
		<div class="col"><?php echo $c['id_categoria']; ?></div>
	 	<div class="col"><?php echo $c['nombre_categoria']; ?></div>
		<div class="col"><?php echo $c['descripcion_categoria']; ?></div>
		<div class="col text-center">
			<a href="<?php echo site_url('categoria/edit/'.$c['id_categoria']); ?>" class="btn btn-success btn-sm">Edit</a> 
	        <a href="<?php echo site_url('categoria/remove/'.$c['id_categoria']); ?>" class="btn btn-danger btn-sm">Eliminar</a>
	    </div>
	</div>
	<?php } ?>

	<div class="pull-right">
	    <?php echo $this->pagination->create_links(); ?>    
	</div>
</div>