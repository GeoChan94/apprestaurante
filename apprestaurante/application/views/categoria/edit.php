<?php echo form_open('categoria/edit/'.$categoria['id_categoria'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="nombre_categoria" class="col-md-4 control-label"><span class="text-danger">*</span>Nombre Categoria</label>
		<div class="col-md-8">
			<input type="text" name="nombre_categoria" value="<?php echo ($this->input->post('nombre_categoria') ? $this->input->post('nombre_categoria') : $categoria['nombre_categoria']); ?>" class="form-control" id="nombre_categoria" />
			<span class="text-danger"><?php echo form_error('nombre_categoria');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion_categoria" class="col-md-4 control-label"><span class="text-danger">*</span>Descripcion Categoria</label>
		<div class="col-md-8">
			<input type="text" name="descripcion_categoria" value="<?php echo ($this->input->post('descripcion_categoria') ? $this->input->post('descripcion_categoria') : $categoria['descripcion_categoria']); ?>" class="form-control" id="descripcion_categoria" />
			<span class="text-danger"><?php echo form_error('descripcion_categoria');?></span>
		</div>
	</div>


	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>