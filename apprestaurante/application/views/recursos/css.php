<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet" media="all" onload="if(media!='all')media='all'">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style type="text/css" media="screen">
body{

    font-family: 'Quicksand' !important;

}
body {
  display: flex;
  flex-direction: column;
  min-height: 100vh;
}

content {
  flex: 1;
}
.cursor-pointer{
  cursor: pointer;
}
.bg-custom{
  background: #1e6496!important;
}
.text-custom{
  color: #1e6496!important;
}
footer a{
  color: #fff;
}
footer a:hover{
  color: #c5c5c5;
}
</style>