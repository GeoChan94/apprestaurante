<?php echo form_open('cliente/edit/'.$cliente['id_cliente'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="nombres_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Nombres Cliente</label>
		<div class="col-md-8">
			<input type="text" name="nombres_cliente" value="<?php echo ($this->input->post('nombres_cliente') ? $this->input->post('nombres_cliente') : $cliente['nombres_cliente']); ?>" class="form-control" id="nombres_cliente" />
			<span class="text-danger"><?php echo form_error('nombres_cliente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="apellidos_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Apellidos Cliente</label>
		<div class="col-md-8">
			<input type="text" name="apellidos_cliente" value="<?php echo ($this->input->post('apellidos_cliente') ? $this->input->post('apellidos_cliente') : $cliente['apellidos_cliente']); ?>" class="form-control" id="apellidos_cliente" />
			<span class="text-danger"><?php echo form_error('apellidos_cliente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="email_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Email Cliente</label>
		<div class="col-md-8">
			<input type="text" name="email_cliente" value="<?php echo ($this->input->post('email_cliente') ? $this->input->post('email_cliente') : $cliente['email_cliente']); ?>" class="form-control" id="email_cliente" />
			<span class="text-danger"><?php echo form_error('email_cliente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="telefono_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Telefono Cliente</label>
		<div class="col-md-8">
			<input type="text" name="telefono_cliente" value="<?php echo ($this->input->post('telefono_cliente') ? $this->input->post('telefono_cliente') : $cliente['telefono_cliente']); ?>" class="form-control" id="telefono_cliente" />
			<span class="text-danger"><?php echo form_error('telefono_cliente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="id_administrador" class="col-md-4 control-label">Id Administrador</label>
		<div class="col-md-8">
			<input type="text" name="id_administrador" value="<?php echo ($this->input->post('id_administrador') ? $this->input->post('id_administrador') : $cliente['id_administrador']); ?>" class="form-control" id="id_administrador" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>