
<?php
    echo "<script> var ventas_hoy=JSON.parse(`".json_encode($ventas_hoy)."`);console.log('ventas_hoy',ventas_hoy);";
    echo "var ventas_semana=JSON.parse(`".json_encode($ventas_semana)."`);console.log('ventas_semana',ventas_semana);";
    echo "var ventas_mes=JSON.parse(`".json_encode($ventas_mes)."`);console.log('ventas_mes',ventas_mes);</script>";
?>

<hr/>
<script src="http://www.chartjs.org/dist/2.7.1/Chart.bundle.js"></script>
    <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
    <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
    <div class="container bg-white border">
        <canvas id="canvas"></canvas>

    </div>
    <br>
    <div class="container">
        <div class="pull-left">
            <button id="reporte_mes">Mes</button>
            <button id="reporte_semana">Semana</button>
            <button id="reporte_hoy">Hoy</button>
        </div>
        <div class="pull-right">
            <a href="<?=base_url()?>reporte/pdf/hoy" target="_blank" class="btn btn-danger btn-xs"><span class="fa fa-file" title="Imprimir"></span> HOY</a>
            <a href="<?=base_url()?>reporte/pdf/semana" target="_blank" class="btn btn-danger btn-xs"><span class="fa fa-file" title="Imprimir"></span> SEMANA</a>
            <a href="<?=base_url()?>reporte/pdf/mes" target="_blank" class="btn btn-danger btn-xs"><span class="fa fa-file" title="Imprimir"></span> MES</a>
        </div><br/><hr/>
        <div class="row">
            <div class="col-12"><h4 class="font-weight-bold">Detalle de reporte</h4></div>
            <div class="col-12 div_detalle_reporte container p-2">
                
            </div>
        </div>
    </div>
    
    <!-- <button id="randomizeData">Randomize Data</button> -->

    <!-- Modal -->
    <div class="modal fade" id="modal_browser_help" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">AppRestaurante V.1.0</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    
    <script type="text/javascript" charset="utf-8" async defer>

        var ventas_plato=[];
        var ventas_cantidad=[];
        var ingreso_x_planto=[];
        function mostrar_ventas(param){
        ventas_plato=[];
        ventas_cantidad=[];
        ingreso_x_planto=[]
        var txt_reporte_ventas=`<div class="row border bg-primary text-white p-3 m-0">
                                            <div class="col">nombre plato</div>
                                            <div class="col text-center">cantidad</div>
                                            <div class="col text-center">precio unitario</div>
                                            <div class="col text-center">precio total</div>
                                        </div>`;;
            param.forEach(function(val,index){
                // console.log(val);
                ventas_plato.push(val.nombre_plato);
                ventas_cantidad.push(val.cantidad);
                ingreso_x_planto.push(val.precio_total);
                console.log('val.precio_total',val.precio_total);
                console.log('ingreso_x_planto',ingreso_x_planto);

                txt_reporte_ventas+=`<div class="row border bg-white p-2 m-0">
                                            <div class="col">${val.nombre_plato}</div>
                                            <div class="col text-center">${val.cantidad}</div>
                                            <div class="col text-center">${val.precio_unitario}</div>
                                            <div class="col text-center">${val.precio_total}</div>
                                        </div>`;
            });
            // document.getElementsByClassName('div_detalle_reporte').innerHTML=txt_reporte_ventas;
            $('.div_detalle_reporte').html(txt_reporte_ventas);
        }
        mostrar_ventas(ventas_hoy);
        
    </script>
    <script>
        var MONTHS = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Augosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        var config = {
            type: 'line',
            data: {
                labels: ventas_plato,
                datasets: [{
                    label: "Platos Vendidos",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: ventas_cantidad,
                    fill: false,
                },{
                    label: "ingreso subtotal",
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: ingreso_x_planto,
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Reporte de Hoy'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Cantidad'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };


        document.getElementById('reporte_mes').addEventListener('click',function(){
            mostrar_ventas(ventas_mes);
            config.data.labels.length=0;
            config.options.scales.xAxes[0]['scaleLabel'].labelString='Reporte Ultimo Mes';
            config.data.datasets[0].data.length=0;
            config.data.datasets[1].data.length=0;

            config.data.datasets[0].data=ventas_cantidad;
            config.data.datasets[1].data=ingreso_x_planto;

            config.data.labels=ventas_plato;

            window.myLine.update();
            

        });

        document.getElementById('reporte_semana').addEventListener('click',function(){
            mostrar_ventas(ventas_semana);
            config.data.labels.length=0;
            config.options.scales.xAxes[0]['scaleLabel'].labelString='Reporte Ultima semana';
            config.data.datasets[0].data.length=0;
            config.data.datasets[1].data.length=0;

            config.data.datasets[0].data=ventas_cantidad;
            config.data.datasets[1].data=ingreso_x_planto;

            config.data.labels=ventas_plato;

            window.myLine.update();

        });
       
        document.getElementById('reporte_hoy').addEventListener('click',function(){
            mostrar_ventas(ventas_hoy);
            config.data.labels.length=0;
            config.options.scales.xAxes[0]['scaleLabel'].labelString='Reporte de Hoy';
            config.data.datasets[0].data.length=0;
            config.data.datasets[1].data.length=0;

            config.data.datasets[0].data=ventas_cantidad;
            config.data.datasets[1].data=ingreso_x_planto
            config.data.labels=ventas_plato;

            window.myLine.update();


        });
    </script>