<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reporte de Ventas - <?=mb_strtoupper(@$option)?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" media="print"  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!--
    <script src="main.js"></script>
    -->
</head>
<body>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col justify-content-md-center">
                <h3 class="text-center text-primary" style="text-align:center;">REPORTE DE VENTAS - <?=mb_strtoupper(@$option)?></h3>
                <div class="table-responsive">
                    <table class="table" style="width:100%; border:1px solid gray;" border="1" cellpadding="3" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center" style="text-align:center;">CANTIDAD</th>
                                <th>PEDIDO</th>
                                <th class="text-center" style="text-align:right;">PRECIO UNITARIO</th>
                                <th class="text-center" style="text-align:right;">PRECIO TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $precioTotal = 0;
                            foreach($data as $key => $value){
                            ?>
                            <tr>
                                <td class="text-center" style="text-align:center;"><?=$value["cantidad"]?></td>
                                <td><?=$value["nombre_plato"]?></td>
                                <td class="text-right" style="text-align:right;"><?=$value["precio_unitario"]?></td>
                                <td class="text-right" style="text-align:right;"><?=$value["precio_total"]?></td>
                            </tr>
                            <?php    
                            $precioTotal += (Double)$value["precio_total"];
                            }
                            ?>
                            <tr>
                                <td colspan="3" class="text-right" style="text-align:right;"><strong>PRECIO TOTAL</strong></td>
                                <td class="text-right" style="text-align:right;"><?=number_format($precioTotal,2,"."," ")?></td> 
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>