 <div class="container">
	<div class="row form-group">
		<div class="col-12 p-0">

			<h4 class="font-weight-bold text-capitalize">Lista de boletas</h4>
		</div>
	</div>
	<div class="row bg-primary text-light p-2 text-capitalize" >
	 	<div class="col">Boleta Nº</div>
		<div class="col">nombre o razon social</div>
		<div class="col">Documento</div>
		<!-- <div class="col text-center">monto</div> -->
		<!-- <div class="col text-center">opcion</div> -->
	</div>
	<?php foreach($boletas as $f){ ?>
	<div class="row bg-white border p-2 text-capitalize" >
		<div class="col"><?php echo $f['numeracion_boleta']; ?></div>
	 	<div class="col"><?php echo $f['nombre_razon_social_cliente']; ?></div>
		<div class="col"><?php echo $f['documento_cliente']; ?></div>
		<!-- <div class="col"><?php echo "monto" ?></div> -->
		<!-- <div class="col text-center">
			<a href="<?php echo site_url('boleta/edit/'.$f['id_boleta']); ?>" class="btn btn-success btn-sm">Edit</a>
	    </div> -->
	</div>
	<?php } ?>

	<div class="pull-right">
	    <?php //echo $this->pagination->create_links(); ?>    
	</div>
</div>
