 <div class="container">
	<div class="row form-group">
		<div class="col-12 p-0">

			<h4 class="font-weight-bold text-capitalize">Lista de facturas generadas</h4>
		</div>
	</div>
	<div class="row bg-primary text-light p-2 text-capitalize" >
	 	<div class="col">Factura Nº</div>
		<div class="col">nombre o razon social</div>
		<!-- <div class="col">Documento</div> -->
		<div class="col text-center">RUC</div>
		<!-- <div class="col text-center">opcion</div> -->
	</div>
	<?php foreach($facturas as $f){ ?>
	<div class="row bg-white border p-2 text-capitalize" >
		<div class="col"><?php echo $f['numeracion_factura']; ?></div>
	 	<div class="col"><?php echo $f['nombre_razon_social_cliente']; ?></div>

		<div class="col"><?php echo @$f['ruc_cliente']; ?></div>
		<!-- <div class="col text-center">
			<a href="<?php echo site_url('factura/edit/'.$f['id_factura']); ?>" class="btn btn-success btn-sm">Edit</a>
	    </div> -->
	</div>
	<?php } ?>

	<div class="pull-right">
	    <?php //echo $this->pagination->create_links(); ?>    
	</div>
</div>
