
<?php echo form_open('factura/edit/'.$factura['id_factura'],array("class"=>"form-horizontal")); ?>
	<div class="form-group">
		<label for="nombre_razon_social_cliente" class="col-md-4 control-label">nombre razon social cliente</label>
		<div class="col-md-8">
			<input type="text" name="nombre_razon_social_cliente" value="<?php echo ($this->input->post('nombre_razon_social_cliente') ? $this->input->post('nombre_razon_social_cliente') : $factura['nombre_razon_social_cliente']); ?>" class="form-control" id="nombre_razon_social_cliente" />
		</div>
	</div>
	<div class="form-group">
		<label for="nombre_razon_social_cliente" class="col-md-4 control-label">Fecha Emision Factura</label>
		<div class="col-md-8">
			<input readonly type="text" name="fecha_facturacion" value="<?php echo ($this->input->post('fecha_facturacion') ? $this->input->post('fecha_facturacion') : $factura['fecha_facturacion']); ?>" class="form-control" id="fecha_facturacion" />
		</div>
	</div>
	<div class="form-group">
		<label for="numeracion_factura" class="col-md-4 control-label">Numeracion Factura</label>
		<div class="col-md-8">
			<input readonly type="text" name="numeracion_factura" value="<?php echo ($this->input->post('numeracion_factura') ? $this->input->post('numeracion_factura') : $factura['numeracion_factura']); ?>" class="form-control" id="numeracion_factura" />
		</div>
	</div>
	<div class="form-group">
		<label for="ruc_cliente" class="col-md-4 control-label">RUC cliente</label>
		<div class="col-md-8">
			<input type="text" name="ruc_cliente" value="<?php echo ($this->input->post('ruc_cliente') ? $this->input->post('ruc_cliente') : @$factura['ruc_cliente']); ?>" class="form-control" id="ruc_cliente" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>