<?php

class Impresora_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_impresora(){
    	return $this->db->query("SELECT * FROM impresora WHERE id_impresora = 1;")->row_array();
    }

    function update_impresora($id_impresora,$params){
    	$this->db->where('id_impresora',$id_impresora);
        return $this->db->update('impresora',$params);
    } 
}