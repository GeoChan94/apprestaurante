<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# Home
Route::get('/', function(){

	return "welcome to factura";

});

# Home
Route::get('dashboard', 'HomeController@index');


# Procesamiento individual de documentos
Route::post('process-txt', 'Admin\\ProcessInputTxtController@upload');

# Home
Route::get('/test-php', function(){

	echo phpinfo();

});
