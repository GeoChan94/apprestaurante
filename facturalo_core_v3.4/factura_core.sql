/*
Navicat MySQL Data Transfer

Source Server         : cursosdev
Source Server Version : 50635
Source Host           : cursosdev.ctyznshddian.us-east-2.rds.amazonaws.com:3306
Source Database       : factura_full

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2017-12-22 05:58:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for acc_account
-- ----------------------------
DROP TABLE IF EXISTS `acc_account`;
CREATE TABLE `acc_account` (
  `n_id_account` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_supplier` int(11) DEFAULT NULL,
  `n_id_account_type` int(11) NOT NULL,
  `c_user` varchar(120) NOT NULL,
  `password` varchar(60) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `c_status` enum('visible','hidden','deleted') NOT NULL,
  `d_date_register_account` datetime NOT NULL,
  `d_date_update_account` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_account`),
  KEY `fk_supplier_account` (`n_id_supplier`),
  KEY `fk_account_type_account` (`n_id_account_type`),
  CONSTRAINT `fk_account_type_account` FOREIGN KEY (`n_id_account_type`) REFERENCES `acc_account_type` (`n_id_account_type`),
  CONSTRAINT `fk_supplier_account` FOREIGN KEY (`n_id_supplier`) REFERENCES `sup_supplier` (`n_id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acc_account
-- ----------------------------
INSERT INTO `acc_account` VALUES ('7', '6', '2', 'ninosimeon', '$2y$10$mQ60h2bfuUx4dSOLb7SkD.v.vVyUsuxja1uH./V10fSaZI0HAV0Aa', '8ISXYTPS740tf7iyv4oiDyB1Jr2MYZoK39XLzpwgtZDRmyjl5PGwPrNJSoui', 'visible', '2015-02-12 10:38:21', '2016-07-23 12:49:39');
INSERT INTO `acc_account` VALUES ('8', null, '1', 'admin', '$2y$10$zEg.0msJ73OIUgCZCr8eCu1ylp.k1vmo2fJR2SLLEba9MR/q6UTQS', 'JfVa87qEsb0mHqTrXMHu7Ghgv934HnCSFtXGwTN8C9002HD6I6NAIxioifmB', 'visible', '2015-04-13 18:09:39', '2016-07-06 22:39:51');
INSERT INTO `acc_account` VALUES ('13', '6', '2', 'miguel', '$2y$10$yVb89/fNgOjmuT2t.84.oenO/docU0LyqGjxDH.BSx12oGOsMazPy', 'SXTgXMOX6384DVvNQ2ZjHz6A1L5Da55SfI1c2wSx8CwWXrcB7gHBQXRF65sA', 'visible', '2015-08-03 17:19:31', '2015-08-03 17:19:47');
INSERT INTO `acc_account` VALUES ('14', null, '2', 'system', '', null, 'visible', '2015-08-14 12:27:59', '2015-08-14 12:28:01');

-- ----------------------------
-- Table structure for acc_account_type
-- ----------------------------
DROP TABLE IF EXISTS `acc_account_type`;
CREATE TABLE `acc_account_type` (
  `n_id_account_type` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(200) NOT NULL,
  `c_status` enum('visible','hidden','deleted') NOT NULL,
  `d_date_register_account_type` datetime NOT NULL,
  `d_date_update_account_type` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_account_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acc_account_type
-- ----------------------------
INSERT INTO `acc_account_type` VALUES ('1', 'Administrador', 'visible', '2015-02-03 10:25:51', null);
INSERT INTO `acc_account_type` VALUES ('2', 'Emisor', 'visible', '2015-02-03 10:26:08', null);

-- ----------------------------
-- Table structure for acc_account_user
-- ----------------------------
DROP TABLE IF EXISTS `acc_account_user`;
CREATE TABLE `acc_account_user` (
  `n_id_account` int(11) NOT NULL,
  `c_user_name` varchar(255) NOT NULL,
  `c_user_last_name` varchar(255) NOT NULL,
  `c_telephone` varchar(255) DEFAULT NULL,
  `c_email` varchar(100) NOT NULL,
  `d_date_register_account_user` datetime NOT NULL,
  `d_date_update_account_user` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_account`),
  CONSTRAINT `fk_account_account_user` FOREIGN KEY (`n_id_account`) REFERENCES `acc_account` (`n_id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acc_account_user
-- ----------------------------
INSERT INTO `acc_account_user` VALUES ('7', 'Nino', 'Simeon', '951717379', 'ninosimeon@gmail.com', '2015-02-12 10:38:21', '2016-07-02 20:23:05');
INSERT INTO `acc_account_user` VALUES ('8', 'ADMINISTRADOR', '.', null, 'ninosimeon@gmail.com', '2015-04-13 18:11:41', null);
INSERT INTO `acc_account_user` VALUES ('13', 'Miguel', 'Jaramillo', '998661551', 'mjaraeche@hotmail.com', '2015-08-03 17:19:31', '2015-08-03 17:19:31');
INSERT INTO `acc_account_user` VALUES ('14', '.', '.', '.', '.', '2015-08-14 12:28:28', '2015-08-14 12:28:30');

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `c_iso` varchar(5) NOT NULL,
  `c_name_large` varchar(255) DEFAULT NULL,
  `c_name_short` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`c_iso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('PE', null, 'PERU');

-- ----------------------------
-- Table structure for cus_customer
-- ----------------------------
DROP TABLE IF EXISTS `cus_customer`;
CREATE TABLE `cus_customer` (
  `n_id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_supplier` int(11) NOT NULL,
  `c_customer_assigned_account_id` varchar(15) NOT NULL,
  `c_additional_account_id` varchar(1) NOT NULL,
  `c_party_party_legal_entity_registration_name` varchar(100) NOT NULL,
  `c_party_physical_location_description` varchar(100) DEFAULT NULL,
  `c_postal_address_id` varchar(6) DEFAULT NULL,
  `c_postal_address_street_name` varchar(100) DEFAULT NULL,
  `c_postal_address_city_subdivision_name` varchar(25) DEFAULT NULL,
  `c_postal_address_city_name` varchar(30) DEFAULT NULL,
  `c_postal_address_country_subentity` varchar(30) DEFAULT NULL,
  `c_postal_address_district` varchar(30) DEFAULT NULL,
  `c_postal_address_country_identification_code` varchar(2) DEFAULT NULL,
  `d_date_register_customer` datetime NOT NULL,
  `d_date_update_customer` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_customer`),
  KEY `fk_supplier_customer` (`n_id_supplier`),
  CONSTRAINT `fk_supplier_customer` FOREIGN KEY (`n_id_supplier`) REFERENCES `sup_supplier` (`n_id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cus_customer
-- ----------------------------
INSERT INTO `cus_customer` VALUES ('49', '6', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null, '2017-12-17 10:28:03', '2017-12-17 10:28:03');

-- ----------------------------
-- Table structure for doc_cdr_status
-- ----------------------------
DROP TABLE IF EXISTS `doc_cdr_status`;
CREATE TABLE `doc_cdr_status` (
  `n_id_cdr_status` int(11) NOT NULL AUTO_INCREMENT,
  `n_index` int(11) NOT NULL,
  `c_name` varchar(200) NOT NULL,
  `c_description` text,
  `d_date_register` datetime NOT NULL,
  `d_date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_cdr_status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_cdr_status
-- ----------------------------
INSERT INTO `doc_cdr_status` VALUES ('1', '1', 'ACEPTADO', 'CDR sin Nota', '2015-04-23 17:17:38', null);
INSERT INTO `doc_cdr_status` VALUES ('2', '3', 'RECHAZADO', 'CDR con Error', '2015-04-23 17:17:54', null);
INSERT INTO `doc_cdr_status` VALUES ('3', '2', 'OBSERVADO', 'CDR con Nota', '2015-04-23 17:18:05', null);
INSERT INTO `doc_cdr_status` VALUES ('4', '4', 'SIN CDR', null, '2015-04-23 17:18:19', null);
INSERT INTO `doc_cdr_status` VALUES ('5', '5', 'ANULADO', 'Documento que fue anulado por una COMUNICACION DE BAJA', '2015-10-15 12:51:13', null);

-- ----------------------------
-- Table structure for doc_document_currency_code_type
-- ----------------------------
DROP TABLE IF EXISTS `doc_document_currency_code_type`;
CREATE TABLE `doc_document_currency_code_type` (
  `c_document_currency_code` varchar(3) NOT NULL,
  `c_description` varchar(200) NOT NULL,
  `c_symbol` varchar(10) NOT NULL,
  PRIMARY KEY (`c_document_currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_document_currency_code_type
-- ----------------------------
INSERT INTO `doc_document_currency_code_type` VALUES ('PEN', 'NUEVO SOL', 'S/.');
INSERT INTO `doc_document_currency_code_type` VALUES ('USD', 'US DOLLAR', '$');

-- ----------------------------
-- Table structure for doc_invoice
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice`;
CREATE TABLE `doc_invoice` (
  `n_id_invoice` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice_related` int(11) DEFAULT NULL,
  `n_id_customer` int(11) DEFAULT NULL,
  `n_id_supplier` int(11) NOT NULL,
  `c_ubl_version_id` varchar(10) NOT NULL,
  `c_customization_id` varchar(10) NOT NULL,
  `c_serie` varchar(4) DEFAULT NULL,
  `c_correlative` varchar(8) DEFAULT NULL,
  `n_correlative` int(11) NOT NULL,
  `c_id` varchar(17) NOT NULL COMMENT 'Numeración conformada por serie y número correlativo F###-NNNNNN',
  `d_issue_date` date NOT NULL,
  `c_invoice_type_code` varchar(2) NOT NULL COMMENT 'Tipo de Documento (Factura)',
  `c_document_currency_code` varchar(3) DEFAULT NULL,
  `d_reference_date` date DEFAULT NULL,
  `c_additional_information_sunat_transaction_id` varchar(2) DEFAULT NULL,
  `c_status_invoice` enum('visible','hidden','deleted') NOT NULL,
  `d_expiry_date` date DEFAULT NULL,
  `c_order_reference_id` varchar(20) DEFAULT NULL,
  `d_date_register_invoice` datetime NOT NULL,
  `d_date_update_invoice` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  KEY `fk_customer_invoice` (`n_id_customer`),
  KEY `supplier_invoice_id` (`n_id_supplier`,`c_id`) USING BTREE,
  KEY `fk_invoice_invoice_type_code` (`c_invoice_type_code`),
  KEY `fk_document_currency_code_invoice` (`c_document_currency_code`),
  KEY `fk_invoice_invoice` (`n_id_invoice_related`),
  CONSTRAINT `fk_customer_invoice` FOREIGN KEY (`n_id_customer`) REFERENCES `cus_customer` (`n_id_customer`),
  CONSTRAINT `fk_document_currency_code_invoice` FOREIGN KEY (`c_document_currency_code`) REFERENCES `doc_document_currency_code_type` (`c_document_currency_code`),
  CONSTRAINT `fk_invoice_invoice` FOREIGN KEY (`n_id_invoice_related`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_invoice_invoice_type_code` FOREIGN KEY (`c_invoice_type_code`) REFERENCES `doc_invoice_type_code` (`c_invoice_type_code`),
  CONSTRAINT `fk_supplier_invoice` FOREIGN KEY (`n_id_supplier`) REFERENCES `sup_supplier` (`n_id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice
-- ----------------------------
INSERT INTO `doc_invoice` VALUES ('130', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:28:21', '2017-12-17 10:28:44');
INSERT INTO `doc_invoice` VALUES ('131', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:28:44', '2017-12-17 10:29:07');
INSERT INTO `doc_invoice` VALUES ('132', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:29:07', '2017-12-17 10:29:28');
INSERT INTO `doc_invoice` VALUES ('133', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:29:28', '2017-12-17 10:29:51');
INSERT INTO `doc_invoice` VALUES ('134', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:29:51', '2017-12-17 10:30:11');
INSERT INTO `doc_invoice` VALUES ('135', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:30:11', '2017-12-17 10:30:29');
INSERT INTO `doc_invoice` VALUES ('136', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:30:29', '2017-12-17 10:30:48');
INSERT INTO `doc_invoice` VALUES ('137', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:30:48', '2017-12-17 10:31:02');
INSERT INTO `doc_invoice` VALUES ('138', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:31:02', '2017-12-17 10:31:20');
INSERT INTO `doc_invoice` VALUES ('139', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:31:20', '2017-12-17 10:31:38');
INSERT INTO `doc_invoice` VALUES ('140', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:31:38', '2017-12-17 10:31:59');
INSERT INTO `doc_invoice` VALUES ('141', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:31:59', '2017-12-17 10:32:28');
INSERT INTO `doc_invoice` VALUES ('142', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:32:28', '2017-12-17 10:32:45');
INSERT INTO `doc_invoice` VALUES ('143', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:32:45', '2017-12-17 10:33:06');
INSERT INTO `doc_invoice` VALUES ('144', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:33:06', '2017-12-17 10:33:25');
INSERT INTO `doc_invoice` VALUES ('145', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:33:25', '2017-12-17 10:33:40');
INSERT INTO `doc_invoice` VALUES ('146', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:33:40', '2017-12-17 10:33:59');
INSERT INTO `doc_invoice` VALUES ('147', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:33:59', '2017-12-17 10:34:20');
INSERT INTO `doc_invoice` VALUES ('148', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:34:20', '2017-12-17 10:34:40');
INSERT INTO `doc_invoice` VALUES ('149', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:34:40', '2017-12-17 10:36:18');
INSERT INTO `doc_invoice` VALUES ('150', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:36:18', '2017-12-17 10:36:51');
INSERT INTO `doc_invoice` VALUES ('151', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:36:51', '2017-12-17 10:37:09');
INSERT INTO `doc_invoice` VALUES ('152', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:37:09', '2017-12-17 10:40:35');
INSERT INTO `doc_invoice` VALUES ('153', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:40:35', '2017-12-17 10:46:06');
INSERT INTO `doc_invoice` VALUES ('154', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:46:07', '2017-12-17 10:46:43');
INSERT INTO `doc_invoice` VALUES ('155', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:46:43', '2017-12-17 10:54:46');
INSERT INTO `doc_invoice` VALUES ('156', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:54:46', '2017-12-17 10:55:30');
INSERT INTO `doc_invoice` VALUES ('157', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:55:31', '2017-12-17 10:56:26');
INSERT INTO `doc_invoice` VALUES ('158', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:56:26', '2017-12-17 10:57:01');
INSERT INTO `doc_invoice` VALUES ('159', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 10:57:01', '2017-12-17 11:00:12');
INSERT INTO `doc_invoice` VALUES ('160', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:00:12', '2017-12-17 11:01:07');
INSERT INTO `doc_invoice` VALUES ('161', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:01:07', '2017-12-17 11:02:11');
INSERT INTO `doc_invoice` VALUES ('162', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:02:11', '2017-12-17 11:03:31');
INSERT INTO `doc_invoice` VALUES ('163', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:03:31', '2017-12-17 11:07:30');
INSERT INTO `doc_invoice` VALUES ('164', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:07:31', '2017-12-17 11:08:09');
INSERT INTO `doc_invoice` VALUES ('165', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:08:09', '2017-12-17 11:08:44');
INSERT INTO `doc_invoice` VALUES ('166', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:08:45', '2017-12-17 11:09:15');
INSERT INTO `doc_invoice` VALUES ('167', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:09:15', '2017-12-17 11:10:11');
INSERT INTO `doc_invoice` VALUES ('168', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:10:11', '2017-12-17 11:39:12');
INSERT INTO `doc_invoice` VALUES ('169', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:39:12', '2017-12-17 11:39:53');
INSERT INTO `doc_invoice` VALUES ('170', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:39:53', '2017-12-17 11:41:02');
INSERT INTO `doc_invoice` VALUES ('171', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:41:02', '2017-12-17 11:41:30');
INSERT INTO `doc_invoice` VALUES ('172', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:41:30', '2017-12-17 11:41:49');
INSERT INTO `doc_invoice` VALUES ('173', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:41:49', '2017-12-17 11:42:13');
INSERT INTO `doc_invoice` VALUES ('174', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:42:13', '2017-12-17 11:43:35');
INSERT INTO `doc_invoice` VALUES ('175', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:43:35', '2017-12-17 11:44:41');
INSERT INTO `doc_invoice` VALUES ('176', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:44:41', '2017-12-17 11:45:18');
INSERT INTO `doc_invoice` VALUES ('177', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:45:18', '2017-12-17 11:48:08');
INSERT INTO `doc_invoice` VALUES ('178', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:48:08', '2017-12-17 11:48:45');
INSERT INTO `doc_invoice` VALUES ('179', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:48:45', '2017-12-17 11:49:45');
INSERT INTO `doc_invoice` VALUES ('180', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:49:45', '2017-12-17 11:50:50');
INSERT INTO `doc_invoice` VALUES ('181', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:50:50', '2017-12-17 11:51:35');
INSERT INTO `doc_invoice` VALUES ('182', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:51:35', '2017-12-17 11:51:57');
INSERT INTO `doc_invoice` VALUES ('183', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:51:57', '2017-12-17 11:52:58');
INSERT INTO `doc_invoice` VALUES ('184', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:52:58', '2017-12-17 11:53:41');
INSERT INTO `doc_invoice` VALUES ('185', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 11:53:41', '2017-12-17 21:58:58');
INSERT INTO `doc_invoice` VALUES ('186', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 21:58:58', '2017-12-17 21:59:23');
INSERT INTO `doc_invoice` VALUES ('187', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 21:59:23', '2017-12-17 22:02:05');
INSERT INTO `doc_invoice` VALUES ('188', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 22:02:06', '2017-12-17 22:23:59');
INSERT INTO `doc_invoice` VALUES ('189', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 22:24:00', '2017-12-17 22:26:44');
INSERT INTO `doc_invoice` VALUES ('190', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-17 22:26:44', '2017-12-21 08:29:15');
INSERT INTO `doc_invoice` VALUES ('191', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 08:29:15', '2017-12-21 08:31:08');
INSERT INTO `doc_invoice` VALUES ('192', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 08:31:08', '2017-12-21 08:31:50');
INSERT INTO `doc_invoice` VALUES ('193', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 08:31:50', '2017-12-21 10:22:35');
INSERT INTO `doc_invoice` VALUES ('194', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:22:36', '2017-12-21 10:23:47');
INSERT INTO `doc_invoice` VALUES ('195', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:23:47', '2017-12-21 10:24:14');
INSERT INTO `doc_invoice` VALUES ('196', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:24:14', '2017-12-21 10:25:31');
INSERT INTO `doc_invoice` VALUES ('197', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:25:31', '2017-12-21 10:26:12');
INSERT INTO `doc_invoice` VALUES ('198', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:26:12', '2017-12-21 10:27:59');
INSERT INTO `doc_invoice` VALUES ('199', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:27:59', '2017-12-21 10:28:41');
INSERT INTO `doc_invoice` VALUES ('200', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:28:42', '2017-12-21 10:29:42');
INSERT INTO `doc_invoice` VALUES ('201', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:29:42', '2017-12-21 10:35:29');
INSERT INTO `doc_invoice` VALUES ('202', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:35:29', '2017-12-21 10:36:18');
INSERT INTO `doc_invoice` VALUES ('203', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:36:19', '2017-12-21 10:36:50');
INSERT INTO `doc_invoice` VALUES ('204', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:36:50', '2017-12-21 10:37:21');
INSERT INTO `doc_invoice` VALUES ('205', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:37:21', '2017-12-21 10:40:47');
INSERT INTO `doc_invoice` VALUES ('206', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:40:47', '2017-12-21 10:45:44');
INSERT INTO `doc_invoice` VALUES ('207', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:45:44', '2017-12-21 10:47:52');
INSERT INTO `doc_invoice` VALUES ('208', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:47:52', '2017-12-21 10:53:54');
INSERT INTO `doc_invoice` VALUES ('209', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:53:55', '2017-12-21 10:54:27');
INSERT INTO `doc_invoice` VALUES ('210', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:54:27', '2017-12-21 10:55:08');
INSERT INTO `doc_invoice` VALUES ('211', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:55:08', '2017-12-21 10:57:29');
INSERT INTO `doc_invoice` VALUES ('212', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:57:29', '2017-12-21 10:58:01');
INSERT INTO `doc_invoice` VALUES ('213', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:58:01', '2017-12-21 10:58:16');
INSERT INTO `doc_invoice` VALUES ('214', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:58:16', '2017-12-21 10:58:37');
INSERT INTO `doc_invoice` VALUES ('215', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 10:58:37', '2017-12-21 11:06:10');
INSERT INTO `doc_invoice` VALUES ('216', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:06:11', '2017-12-21 11:07:11');
INSERT INTO `doc_invoice` VALUES ('217', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:07:11', '2017-12-21 11:09:47');
INSERT INTO `doc_invoice` VALUES ('218', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:09:47', '2017-12-21 11:10:08');
INSERT INTO `doc_invoice` VALUES ('219', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:10:08', '2017-12-21 11:10:32');
INSERT INTO `doc_invoice` VALUES ('220', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:10:32', '2017-12-21 11:21:53');
INSERT INTO `doc_invoice` VALUES ('221', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:21:53', '2017-12-21 11:22:13');
INSERT INTO `doc_invoice` VALUES ('222', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:22:13', '2017-12-21 11:22:53');
INSERT INTO `doc_invoice` VALUES ('223', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:22:53', '2017-12-21 11:24:32');
INSERT INTO `doc_invoice` VALUES ('224', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:24:33', '2017-12-21 11:26:49');
INSERT INTO `doc_invoice` VALUES ('225', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:26:49', '2017-12-21 11:27:27');
INSERT INTO `doc_invoice` VALUES ('226', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:27:28', '2017-12-21 11:30:29');
INSERT INTO `doc_invoice` VALUES ('227', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:30:29', '2017-12-21 11:31:47');
INSERT INTO `doc_invoice` VALUES ('228', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:31:47', '2017-12-21 11:37:49');
INSERT INTO `doc_invoice` VALUES ('229', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:37:49', '2017-12-21 11:38:27');
INSERT INTO `doc_invoice` VALUES ('230', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:38:27', '2017-12-21 11:40:21');
INSERT INTO `doc_invoice` VALUES ('231', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-21 11:40:21', '2017-12-22 02:22:50');
INSERT INTO `doc_invoice` VALUES ('232', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:22:50', '2017-12-22 02:29:19');
INSERT INTO `doc_invoice` VALUES ('233', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:29:19', '2017-12-22 02:30:45');
INSERT INTO `doc_invoice` VALUES ('234', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:30:45', '2017-12-22 02:36:55');
INSERT INTO `doc_invoice` VALUES ('235', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:36:56', '2017-12-22 02:41:40');
INSERT INTO `doc_invoice` VALUES ('236', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:41:40', '2017-12-22 02:43:20');
INSERT INTO `doc_invoice` VALUES ('237', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:43:21', '2017-12-22 02:44:40');
INSERT INTO `doc_invoice` VALUES ('238', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:44:40', '2017-12-22 02:47:26');
INSERT INTO `doc_invoice` VALUES ('239', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:47:26', '2017-12-22 02:48:56');
INSERT INTO `doc_invoice` VALUES ('240', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:48:56', '2017-12-22 02:53:32');
INSERT INTO `doc_invoice` VALUES ('241', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:53:32', '2017-12-22 02:54:47');
INSERT INTO `doc_invoice` VALUES ('242', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 02:54:47', '2017-12-22 03:02:08');
INSERT INTO `doc_invoice` VALUES ('243', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 03:02:08', '2017-12-22 03:05:08');
INSERT INTO `doc_invoice` VALUES ('244', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 03:05:08', '2017-12-22 03:07:34');
INSERT INTO `doc_invoice` VALUES ('245', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 03:07:35', '2017-12-22 08:15:44');
INSERT INTO `doc_invoice` VALUES ('246', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 03:13:18', '2017-12-22 06:26:55');
INSERT INTO `doc_invoice` VALUES ('247', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 06:26:55', '2017-12-22 06:28:48');
INSERT INTO `doc_invoice` VALUES ('248', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 06:28:49', '2017-12-22 06:34:44');
INSERT INTO `doc_invoice` VALUES ('249', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 06:34:44', '2017-12-22 06:37:36');
INSERT INTO `doc_invoice` VALUES ('250', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 06:37:36', '2017-12-22 07:26:51');
INSERT INTO `doc_invoice` VALUES ('251', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:26:51', '2017-12-22 07:27:52');
INSERT INTO `doc_invoice` VALUES ('252', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:27:52', '2017-12-22 07:32:34');
INSERT INTO `doc_invoice` VALUES ('253', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:32:34', '2017-12-22 07:33:48');
INSERT INTO `doc_invoice` VALUES ('254', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:33:48', '2017-12-22 07:35:26');
INSERT INTO `doc_invoice` VALUES ('255', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:35:26', '2017-12-22 07:41:04');
INSERT INTO `doc_invoice` VALUES ('256', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:41:04', '2017-12-22 07:41:59');
INSERT INTO `doc_invoice` VALUES ('257', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:42:00', '2017-12-22 07:45:46');
INSERT INTO `doc_invoice` VALUES ('258', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:45:47', '2017-12-22 07:50:13');
INSERT INTO `doc_invoice` VALUES ('259', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:50:13', '2017-12-22 07:52:12');
INSERT INTO `doc_invoice` VALUES ('260', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:52:12', '2017-12-22 07:54:29');
INSERT INTO `doc_invoice` VALUES ('261', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 07:54:29', '2017-12-22 07:58:17');
INSERT INTO `doc_invoice` VALUES ('262', null, '49', '6', '2.0', '1.0', 'FF11', '00000002', '2', 'FF11-00000002', '2016-10-01', '01', 'PEN', null, null, 'visible', null, null, '2017-12-22 07:58:17', '2017-12-22 07:58:17');
INSERT INTO `doc_invoice` VALUES ('263', null, '49', '6', '2.0', '1.0', 'FF11', '00000001', '1', 'FF11-00000001', '2016-10-01', '01', 'PEN', null, null, 'visible', null, null, '2017-12-22 08:15:44', '2017-12-22 08:15:44');
INSERT INTO `doc_invoice` VALUES ('264', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:17:59', '2017-12-22 08:19:42');
INSERT INTO `doc_invoice` VALUES ('265', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:19:43', '2017-12-22 08:27:32');
INSERT INTO `doc_invoice` VALUES ('266', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:27:33', '2017-12-22 08:36:40');
INSERT INTO `doc_invoice` VALUES ('267', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:36:40', '2017-12-22 08:38:23');
INSERT INTO `doc_invoice` VALUES ('268', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:38:23', '2017-12-22 08:39:54');
INSERT INTO `doc_invoice` VALUES ('269', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:39:54', '2017-12-22 08:41:21');
INSERT INTO `doc_invoice` VALUES ('270', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:41:21', '2017-12-22 08:45:03');
INSERT INTO `doc_invoice` VALUES ('271', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'deleted', null, null, '2017-12-22 08:45:03', '2017-12-22 08:46:03');
INSERT INTO `doc_invoice` VALUES ('272', null, '49', '6', '2.0', '1.0', 'FF11', '00000003', '3', 'FF11-00000003', '2016-10-01', '01', 'PEN', null, null, 'visible', null, null, '2017-12-22 08:46:04', '2017-12-22 08:46:04');

-- ----------------------------
-- Table structure for doc_invoice_additional_account_id
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_additional_account_id`;
CREATE TABLE `doc_invoice_additional_account_id` (
  `c_id_invoice_additional_account_id` varchar(1) NOT NULL,
  `c_description` varchar(100) NOT NULL,
  PRIMARY KEY (`c_id_invoice_additional_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contiene el Catalogo No 06.';

-- ----------------------------
-- Records of doc_invoice_additional_account_id
-- ----------------------------
INSERT INTO `doc_invoice_additional_account_id` VALUES ('0', 'DOC. TRIB. NO. DOM. SIN. RUC	');
INSERT INTO `doc_invoice_additional_account_id` VALUES ('1', 'DOC. NACIONAL DE IDENTIDAD');
INSERT INTO `doc_invoice_additional_account_id` VALUES ('4', 'CARNET DE EXTRANJERIA');
INSERT INTO `doc_invoice_additional_account_id` VALUES ('6', 'REG. UNICO DE CONTRIBUYENTES');
INSERT INTO `doc_invoice_additional_account_id` VALUES ('7', 'PASAPORTE');
INSERT INTO `doc_invoice_additional_account_id` VALUES ('A', 'CED. DIPLOMATICA DE IDENTIDAD');

-- ----------------------------
-- Table structure for doc_invoice_additional_document_reference
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_additional_document_reference`;
CREATE TABLE `doc_invoice_additional_document_reference` (
  `n_id_invoice_additional_document_reference` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `c_id` varchar(30) DEFAULT NULL COMMENT 'Numero de documento relacionado',
  `c_document_type_code` varchar(2) DEFAULT NULL COMMENT 'Tipo de documento - Catalogo No 12',
  PRIMARY KEY (`n_id_invoice_additional_document_reference`),
  KEY `fk_invoice_invoice_additional_document_reference` (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_additional_document_reference` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_additional_document_reference
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_additional_information_additional_monetary_total
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_additional_information_additional_monetary_total`;
CREATE TABLE `doc_invoice_additional_information_additional_monetary_total` (
  `n_id_invoice_additional_information_monetary_total` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `c_id` varchar(4) NOT NULL,
  `c_payable_amount` varchar(15) DEFAULT NULL COMMENT 'Monto',
  `c_reference_amount` varchar(15) DEFAULT NULL,
  `c_total_amount` varchar(15) DEFAULT NULL,
  `c_percent` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice_additional_information_monetary_total`),
  UNIQUE KEY `invoice_additional_information_additional_monetary_total_id` (`n_id_invoice`,`c_id`) USING BTREE,
  CONSTRAINT `fk_invoice_invoice_amount_type` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=538 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_additional_information_additional_monetary_total
-- ----------------------------
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('121', '134', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('122', '134', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('123', '134', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('124', '135', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('125', '135', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('126', '135', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('127', '136', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('128', '136', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('129', '136', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('130', '137', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('131', '137', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('132', '137', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('133', '138', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('134', '138', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('135', '138', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('136', '139', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('137', '139', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('138', '139', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('139', '140', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('140', '140', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('141', '140', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('142', '141', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('143', '141', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('144', '141', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('145', '142', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('146', '142', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('147', '142', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('148', '143', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('149', '143', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('150', '143', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('151', '144', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('152', '144', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('153', '144', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('154', '145', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('155', '145', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('156', '145', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('157', '146', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('158', '146', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('159', '146', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('160', '147', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('161', '147', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('162', '147', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('163', '148', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('164', '148', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('165', '148', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('166', '149', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('167', '149', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('168', '149', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('169', '150', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('170', '150', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('171', '150', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('172', '151', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('173', '151', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('174', '151', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('175', '152', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('176', '152', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('177', '152', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('178', '153', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('179', '153', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('180', '153', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('181', '154', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('182', '154', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('183', '154', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('184', '155', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('185', '155', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('186', '155', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('187', '156', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('188', '156', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('189', '156', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('190', '157', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('191', '157', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('192', '157', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('193', '158', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('194', '158', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('195', '158', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('196', '159', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('197', '159', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('198', '159', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('199', '160', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('200', '160', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('201', '160', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('202', '161', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('203', '161', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('204', '161', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('205', '162', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('206', '162', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('207', '162', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('208', '163', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('209', '163', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('210', '163', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('211', '164', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('212', '164', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('213', '164', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('214', '165', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('215', '165', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('216', '165', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('217', '166', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('218', '166', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('219', '166', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('220', '167', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('221', '167', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('222', '167', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('223', '168', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('224', '168', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('225', '168', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('226', '169', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('227', '169', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('228', '169', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('229', '170', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('230', '170', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('231', '170', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('232', '171', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('233', '171', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('234', '171', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('235', '172', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('236', '172', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('237', '172', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('238', '173', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('239', '173', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('240', '173', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('241', '174', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('242', '174', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('243', '174', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('244', '175', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('245', '175', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('246', '175', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('247', '176', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('248', '176', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('249', '176', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('250', '177', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('251', '177', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('252', '177', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('253', '178', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('254', '178', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('255', '178', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('256', '179', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('257', '179', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('258', '179', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('259', '180', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('260', '180', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('261', '180', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('262', '181', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('263', '181', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('264', '181', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('265', '182', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('266', '182', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('267', '182', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('268', '183', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('269', '183', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('270', '183', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('271', '184', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('272', '184', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('273', '184', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('274', '185', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('275', '185', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('276', '185', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('277', '186', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('278', '186', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('279', '186', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('280', '187', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('281', '187', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('282', '187', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('283', '188', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('284', '188', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('285', '188', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('286', '189', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('287', '189', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('288', '189', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('289', '190', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('290', '190', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('291', '190', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('292', '191', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('293', '191', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('294', '191', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('295', '192', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('296', '192', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('297', '192', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('298', '193', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('299', '193', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('300', '193', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('301', '194', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('302', '194', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('303', '194', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('304', '195', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('305', '195', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('306', '195', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('307', '196', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('308', '196', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('309', '196', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('310', '197', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('311', '197', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('312', '197', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('313', '198', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('314', '198', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('315', '198', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('316', '199', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('317', '199', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('318', '199', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('319', '200', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('320', '200', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('321', '200', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('322', '201', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('323', '201', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('324', '201', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('325', '202', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('326', '202', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('327', '202', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('328', '203', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('329', '203', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('330', '203', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('331', '204', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('332', '204', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('333', '204', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('334', '205', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('335', '205', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('336', '205', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('337', '206', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('338', '206', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('339', '206', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('340', '207', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('341', '207', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('342', '207', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('343', '208', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('344', '208', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('345', '208', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('346', '209', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('347', '209', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('348', '209', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('349', '210', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('350', '210', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('351', '210', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('352', '211', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('353', '211', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('354', '211', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('355', '212', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('356', '212', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('357', '212', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('358', '213', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('359', '213', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('360', '213', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('361', '214', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('362', '214', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('363', '214', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('364', '215', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('365', '215', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('366', '215', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('367', '216', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('368', '216', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('369', '216', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('370', '217', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('371', '217', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('372', '217', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('373', '218', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('374', '218', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('375', '218', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('376', '219', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('377', '219', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('378', '219', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('379', '220', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('380', '220', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('381', '220', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('382', '221', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('383', '221', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('384', '221', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('385', '222', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('386', '222', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('387', '222', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('388', '223', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('389', '223', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('390', '223', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('391', '224', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('392', '224', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('393', '224', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('394', '225', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('395', '225', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('396', '225', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('397', '226', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('398', '226', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('399', '226', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('400', '227', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('401', '227', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('402', '227', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('403', '228', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('404', '228', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('405', '228', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('406', '229', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('407', '229', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('408', '229', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('409', '230', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('410', '230', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('411', '230', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('412', '231', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('413', '231', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('414', '231', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('415', '232', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('416', '232', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('417', '232', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('418', '233', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('419', '233', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('420', '233', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('421', '234', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('422', '234', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('423', '234', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('424', '235', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('425', '235', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('426', '235', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('427', '236', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('428', '236', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('429', '236', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('430', '237', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('431', '237', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('432', '237', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('433', '238', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('434', '238', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('435', '238', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('436', '239', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('437', '239', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('438', '239', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('439', '240', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('440', '240', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('441', '240', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('442', '241', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('443', '241', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('444', '241', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('445', '242', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('446', '242', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('447', '242', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('448', '243', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('449', '243', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('450', '243', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('451', '244', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('452', '244', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('453', '244', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('454', '245', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('455', '245', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('456', '245', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('457', '246', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('458', '246', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('459', '246', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('460', '247', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('461', '247', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('462', '247', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('463', '248', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('464', '248', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('465', '248', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('466', '249', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('467', '249', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('468', '249', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('469', '250', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('470', '250', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('471', '250', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('472', '251', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('473', '251', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('474', '251', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('475', '252', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('476', '252', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('477', '252', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('478', '253', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('479', '253', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('480', '253', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('481', '254', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('482', '254', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('483', '254', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('484', '255', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('485', '255', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('486', '255', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('487', '256', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('488', '256', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('489', '256', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('490', '257', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('491', '257', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('492', '257', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('493', '258', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('494', '258', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('495', '258', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('496', '259', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('497', '259', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('498', '259', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('499', '260', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('500', '260', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('501', '260', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('502', '261', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('503', '261', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('504', '261', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('505', '262', '1001', '200.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('506', '262', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('507', '262', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('508', '263', '1001', '300.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('509', '263', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('510', '263', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('511', '264', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('512', '264', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('513', '264', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('514', '265', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('515', '265', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('516', '265', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('517', '266', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('518', '266', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('519', '266', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('520', '267', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('521', '267', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('522', '267', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('523', '268', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('524', '268', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('525', '268', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('526', '269', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('527', '269', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('528', '269', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('529', '270', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('530', '270', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('531', '270', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('532', '271', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('533', '271', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('534', '271', '1003', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('535', '272', '1001', '100.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('536', '272', '1002', '0.00', null, null, null);
INSERT INTO `doc_invoice_additional_information_additional_monetary_total` VALUES ('537', '272', '1003', '0.00', null, null, null);

-- ----------------------------
-- Table structure for doc_invoice_additional_information_additional_property
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_additional_information_additional_property`;
CREATE TABLE `doc_invoice_additional_information_additional_property` (
  `n_id_invoice_additional_information_additional_property` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `c_id` varchar(4) NOT NULL,
  `c_name` varchar(100) DEFAULT NULL,
  `c_value` varchar(150) NOT NULL,
  PRIMARY KEY (`n_id_invoice_additional_information_additional_property`),
  KEY `invoice_additional_information_additional_propery` (`n_id_invoice`,`c_id`) USING BTREE,
  CONSTRAINT `fk_invoice_invoice_additional_information_additonal_property` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_additional_information_additional_property
-- ----------------------------
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('41', '132', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('42', '133', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('43', '134', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('44', '135', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('45', '136', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('46', '137', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('47', '138', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('48', '139', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('49', '140', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('50', '141', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('51', '142', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('52', '143', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('53', '144', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('54', '145', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('55', '146', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('56', '147', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('57', '148', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('58', '149', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('59', '150', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('60', '151', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('61', '152', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('62', '153', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('63', '154', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('64', '155', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('65', '156', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('66', '157', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('67', '158', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('68', '159', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('69', '160', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('70', '161', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('71', '162', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('72', '163', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('73', '164', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('74', '165', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('75', '166', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('76', '167', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('77', '168', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('78', '169', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('79', '170', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('80', '171', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('81', '172', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('82', '173', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('83', '174', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('84', '175', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('85', '176', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('86', '177', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('87', '178', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('88', '179', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('89', '180', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('90', '181', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('91', '182', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('92', '183', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('93', '184', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('94', '185', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('95', '186', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('96', '187', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('97', '188', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('98', '189', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('99', '190', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('100', '191', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('101', '192', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('102', '193', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('103', '194', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('104', '195', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('105', '196', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('106', '197', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('107', '198', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('108', '199', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('109', '200', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('110', '201', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('111', '202', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('112', '203', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('113', '204', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('114', '205', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('115', '206', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('116', '207', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('117', '208', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('118', '209', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('119', '210', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('120', '211', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('121', '212', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('122', '213', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('123', '214', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('124', '215', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('125', '216', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('126', '217', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('127', '218', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('128', '219', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('129', '220', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('130', '221', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('131', '222', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('132', '223', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('133', '224', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('134', '225', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('135', '226', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('136', '227', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('137', '228', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('138', '229', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('139', '230', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('140', '231', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('141', '232', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('142', '233', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('143', '234', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('144', '235', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('145', '236', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('146', '237', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('147', '238', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('148', '239', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('149', '240', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('150', '241', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('151', '242', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('152', '243', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('153', '244', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('154', '245', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('155', '246', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('156', '247', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('157', '248', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('158', '249', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('159', '250', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('160', '251', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('161', '252', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('162', '253', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('163', '254', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('164', '255', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('165', '256', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('166', '257', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('167', '258', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('168', '259', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('169', '260', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('170', '261', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('171', '262', '1000', null, 'DOSCIENTOS TREINTISEIS Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('172', '263', '1000', null, 'TRESCIENTOS CINCUENTICUATRO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('173', '264', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('174', '265', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('175', '266', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('176', '267', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('177', '268', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('178', '269', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('179', '270', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('180', '271', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');
INSERT INTO `doc_invoice_additional_information_additional_property` VALUES ('181', '272', '1000', null, 'CIENTO DIECIOCHO Y 00/100 SOLES');

-- ----------------------------
-- Table structure for doc_invoice_anticipos
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_anticipos`;
CREATE TABLE `doc_invoice_anticipos` (
  `ant_id` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `ant_paid_amount` varchar(18) DEFAULT NULL,
  `ant_cbc_id` varchar(20) DEFAULT NULL,
  `ant_cbc_id_scheme_id` varchar(2) DEFAULT NULL,
  `ant_instruction_id` varchar(15) DEFAULT NULL,
  `ant_instruction_id_scheme_id` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ant_id`),
  KEY `n_id_invoice` (`n_id_invoice`),
  CONSTRAINT `fk_invoice_anticipos` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_anticipos
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_billing_reference_invoice_document_reference
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_billing_reference_invoice_document_reference`;
CREATE TABLE `doc_invoice_billing_reference_invoice_document_reference` (
  `n_id_invoice` int(11) NOT NULL,
  `c_id` varchar(13) NOT NULL,
  `c_document_type_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_billing_reference_invoice_document_reference` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_billing_reference_invoice_document_reference
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_cdr
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_cdr`;
CREATE TABLE `doc_invoice_cdr` (
  `n_id_invoice` int(11) NOT NULL,
  `c_ubl_version_id` varchar(255) DEFAULT NULL,
  `c_customization_id` varchar(255) DEFAULT NULL,
  `c_id` varchar(255) DEFAULT NULL,
  `d_issue_date` date DEFAULT NULL,
  `d_issue_time` time DEFAULT NULL,
  `d_response_date` date DEFAULT NULL,
  `d_response_time` time DEFAULT NULL,
  `d_date_register` datetime NOT NULL,
  `d_date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_cdr` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_cdr
-- ----------------------------
INSERT INTO `doc_invoice_cdr` VALUES ('262', null, null, '1513929525487', '2016-10-01', '00:00:00', '2017-12-22', '02:58:45', '2017-12-22 07:58:45', '2017-12-22 07:58:45');
INSERT INTO `doc_invoice_cdr` VALUES ('263', null, null, '1513930576139', '2016-10-01', '00:00:00', '2017-12-22', '03:16:16', '2017-12-22 08:16:16', '2017-12-22 08:16:16');

-- ----------------------------
-- Table structure for doc_invoice_cdr_document_response
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_cdr_document_response`;
CREATE TABLE `doc_invoice_cdr_document_response` (
  `n_id_invoice` int(11) NOT NULL,
  `c_response_reference_id` varchar(255) DEFAULT NULL,
  `c_response_response_code` varchar(255) DEFAULT NULL,
  `c_response_description` varchar(255) DEFAULT NULL,
  `c_document_reference_id` varchar(255) DEFAULT NULL,
  `c_recipient_party_party_identification_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  KEY `c_response_response_code` (`c_response_response_code`),
  CONSTRAINT `fk_error_code_invoice_cdr_document_response` FOREIGN KEY (`c_response_response_code`) REFERENCES `err_error_code` (`c_id_error_code`),
  CONSTRAINT `fk_invoice_cdr_invoice_cdr_document_response` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice_cdr` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_cdr_document_response
-- ----------------------------
INSERT INTO `doc_invoice_cdr_document_response` VALUES ('262', 'FF11-00000002', '0', 'La Factura numero FF11-00000002, ha sido aceptada', 'FF11-00000002', '6-20112273922');
INSERT INTO `doc_invoice_cdr_document_response` VALUES ('263', 'FF11-00000001', '0', 'La Factura numero FF11-00000001, ha sido aceptada', 'FF11-00000001', '6-20112273922');

-- ----------------------------
-- Table structure for doc_invoice_cdr_note
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_cdr_note`;
CREATE TABLE `doc_invoice_cdr_note` (
  `n_id_invoice_cdr_note` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `c_note` varchar(255) NOT NULL,
  `c_code` varchar(30) NOT NULL,
  `c_description` varchar(255) NOT NULL,
  PRIMARY KEY (`n_id_invoice_cdr_note`),
  KEY `fk_invoice_cdr_invoice_cdr_note` (`n_id_invoice`),
  KEY `c_code` (`c_code`),
  CONSTRAINT `fk_error_code_invoice_cdr_note` FOREIGN KEY (`c_code`) REFERENCES `err_error_code` (`c_id_error_code`),
  CONSTRAINT `fk_invoice_cdr_invoice_cdr_note` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice_cdr` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_cdr_note
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_cdr_receiver_party
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_cdr_receiver_party`;
CREATE TABLE `doc_invoice_cdr_receiver_party` (
  `n_id_invoice` int(11) NOT NULL,
  `c_party_identification_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_cdr_invoice_cdr_receiver_party` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice_cdr` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_cdr_receiver_party
-- ----------------------------
INSERT INTO `doc_invoice_cdr_receiver_party` VALUES ('262', '20524719585');
INSERT INTO `doc_invoice_cdr_receiver_party` VALUES ('263', '20524719585');

-- ----------------------------
-- Table structure for doc_invoice_cdr_sender_party
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_cdr_sender_party`;
CREATE TABLE `doc_invoice_cdr_sender_party` (
  `n_id_invoice` int(11) NOT NULL,
  `c_party_identification_id` varchar(255) NOT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_cdr_invoice_cdr_sender_party` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice_cdr` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_cdr_sender_party
-- ----------------------------
INSERT INTO `doc_invoice_cdr_sender_party` VALUES ('262', '20131312955');
INSERT INTO `doc_invoice_cdr_sender_party` VALUES ('263', '20131312955');

-- ----------------------------
-- Table structure for doc_invoice_cdr_status
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_cdr_status`;
CREATE TABLE `doc_invoice_cdr_status` (
  `n_id_invoice` int(11) NOT NULL,
  `n_id_cdr_status` int(11) NOT NULL,
  `d_date_register` datetime NOT NULL,
  `d_date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  KEY `fk_cdr_status_invoice_cdr_status` (`n_id_cdr_status`),
  CONSTRAINT `fk_cdr_status_invoice_cdr_status` FOREIGN KEY (`n_id_cdr_status`) REFERENCES `doc_cdr_status` (`n_id_cdr_status`),
  CONSTRAINT `fk_invoice_invoice_cdr_status` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_cdr_status
-- ----------------------------
INSERT INTO `doc_invoice_cdr_status` VALUES ('131', '4', '2017-12-17 10:28:44', '2017-12-17 10:28:44');
INSERT INTO `doc_invoice_cdr_status` VALUES ('132', '4', '2017-12-17 10:29:07', '2017-12-17 10:29:07');
INSERT INTO `doc_invoice_cdr_status` VALUES ('133', '4', '2017-12-17 10:29:28', '2017-12-17 10:29:28');
INSERT INTO `doc_invoice_cdr_status` VALUES ('134', '4', '2017-12-17 10:29:51', '2017-12-17 10:29:51');
INSERT INTO `doc_invoice_cdr_status` VALUES ('135', '4', '2017-12-17 10:30:11', '2017-12-17 10:30:11');
INSERT INTO `doc_invoice_cdr_status` VALUES ('136', '4', '2017-12-17 10:30:30', '2017-12-17 10:30:30');
INSERT INTO `doc_invoice_cdr_status` VALUES ('137', '4', '2017-12-17 10:30:48', '2017-12-17 10:30:48');
INSERT INTO `doc_invoice_cdr_status` VALUES ('138', '4', '2017-12-17 10:31:02', '2017-12-17 10:31:02');
INSERT INTO `doc_invoice_cdr_status` VALUES ('139', '4', '2017-12-17 10:31:21', '2017-12-17 10:31:21');
INSERT INTO `doc_invoice_cdr_status` VALUES ('140', '4', '2017-12-17 10:31:39', '2017-12-17 10:31:39');
INSERT INTO `doc_invoice_cdr_status` VALUES ('141', '4', '2017-12-17 10:31:59', '2017-12-17 10:31:59');
INSERT INTO `doc_invoice_cdr_status` VALUES ('142', '4', '2017-12-17 10:32:29', '2017-12-17 10:32:29');
INSERT INTO `doc_invoice_cdr_status` VALUES ('143', '4', '2017-12-17 10:32:45', '2017-12-17 10:32:45');
INSERT INTO `doc_invoice_cdr_status` VALUES ('144', '4', '2017-12-17 10:33:06', '2017-12-17 10:33:06');
INSERT INTO `doc_invoice_cdr_status` VALUES ('145', '4', '2017-12-17 10:33:26', '2017-12-17 10:33:26');
INSERT INTO `doc_invoice_cdr_status` VALUES ('146', '4', '2017-12-17 10:33:40', '2017-12-17 10:33:40');
INSERT INTO `doc_invoice_cdr_status` VALUES ('147', '4', '2017-12-17 10:34:00', '2017-12-17 10:34:00');
INSERT INTO `doc_invoice_cdr_status` VALUES ('148', '4', '2017-12-17 10:34:20', '2017-12-17 10:34:20');
INSERT INTO `doc_invoice_cdr_status` VALUES ('149', '4', '2017-12-17 10:34:40', '2017-12-17 10:34:40');
INSERT INTO `doc_invoice_cdr_status` VALUES ('150', '4', '2017-12-17 10:36:18', '2017-12-17 10:36:18');
INSERT INTO `doc_invoice_cdr_status` VALUES ('151', '4', '2017-12-17 10:36:51', '2017-12-17 10:36:51');
INSERT INTO `doc_invoice_cdr_status` VALUES ('152', '4', '2017-12-17 10:37:09', '2017-12-17 10:37:09');
INSERT INTO `doc_invoice_cdr_status` VALUES ('153', '4', '2017-12-17 10:40:36', '2017-12-17 10:40:36');
INSERT INTO `doc_invoice_cdr_status` VALUES ('154', '4', '2017-12-17 10:46:07', '2017-12-17 10:46:07');
INSERT INTO `doc_invoice_cdr_status` VALUES ('155', '4', '2017-12-17 10:46:43', '2017-12-17 10:46:43');
INSERT INTO `doc_invoice_cdr_status` VALUES ('156', '4', '2017-12-17 10:54:47', '2017-12-17 10:54:47');
INSERT INTO `doc_invoice_cdr_status` VALUES ('157', '4', '2017-12-17 10:55:31', '2017-12-17 10:55:31');
INSERT INTO `doc_invoice_cdr_status` VALUES ('158', '4', '2017-12-17 10:56:26', '2017-12-17 10:56:26');
INSERT INTO `doc_invoice_cdr_status` VALUES ('159', '4', '2017-12-17 10:57:02', '2017-12-17 10:57:02');
INSERT INTO `doc_invoice_cdr_status` VALUES ('160', '4', '2017-12-17 11:00:12', '2017-12-17 11:00:12');
INSERT INTO `doc_invoice_cdr_status` VALUES ('161', '4', '2017-12-17 11:01:08', '2017-12-17 11:01:08');
INSERT INTO `doc_invoice_cdr_status` VALUES ('162', '4', '2017-12-17 11:02:11', '2017-12-17 11:02:11');
INSERT INTO `doc_invoice_cdr_status` VALUES ('163', '4', '2017-12-17 11:03:32', '2017-12-17 11:03:32');
INSERT INTO `doc_invoice_cdr_status` VALUES ('164', '4', '2017-12-17 11:07:31', '2017-12-17 11:07:31');
INSERT INTO `doc_invoice_cdr_status` VALUES ('165', '4', '2017-12-17 11:08:10', '2017-12-17 11:08:10');
INSERT INTO `doc_invoice_cdr_status` VALUES ('166', '4', '2017-12-17 11:08:45', '2017-12-17 11:08:45');
INSERT INTO `doc_invoice_cdr_status` VALUES ('167', '4', '2017-12-17 11:09:15', '2017-12-17 11:09:15');
INSERT INTO `doc_invoice_cdr_status` VALUES ('168', '4', '2017-12-17 11:10:11', '2017-12-17 11:10:11');
INSERT INTO `doc_invoice_cdr_status` VALUES ('169', '4', '2017-12-17 11:39:12', '2017-12-17 11:39:12');
INSERT INTO `doc_invoice_cdr_status` VALUES ('170', '4', '2017-12-17 11:39:54', '2017-12-17 11:39:54');
INSERT INTO `doc_invoice_cdr_status` VALUES ('171', '4', '2017-12-17 11:41:02', '2017-12-17 11:41:02');
INSERT INTO `doc_invoice_cdr_status` VALUES ('172', '4', '2017-12-17 11:41:30', '2017-12-17 11:41:30');
INSERT INTO `doc_invoice_cdr_status` VALUES ('173', '4', '2017-12-17 11:41:49', '2017-12-17 11:41:49');
INSERT INTO `doc_invoice_cdr_status` VALUES ('174', '4', '2017-12-17 11:42:13', '2017-12-17 11:42:13');
INSERT INTO `doc_invoice_cdr_status` VALUES ('175', '4', '2017-12-17 11:43:35', '2017-12-17 11:43:35');
INSERT INTO `doc_invoice_cdr_status` VALUES ('176', '4', '2017-12-17 11:44:41', '2017-12-17 11:44:41');
INSERT INTO `doc_invoice_cdr_status` VALUES ('177', '4', '2017-12-17 11:45:19', '2017-12-17 11:45:19');
INSERT INTO `doc_invoice_cdr_status` VALUES ('178', '4', '2017-12-17 11:48:08', '2017-12-17 11:48:08');
INSERT INTO `doc_invoice_cdr_status` VALUES ('179', '4', '2017-12-17 11:48:45', '2017-12-17 11:48:45');
INSERT INTO `doc_invoice_cdr_status` VALUES ('180', '4', '2017-12-17 11:49:45', '2017-12-17 11:49:45');
INSERT INTO `doc_invoice_cdr_status` VALUES ('181', '4', '2017-12-17 11:50:50', '2017-12-17 11:50:50');
INSERT INTO `doc_invoice_cdr_status` VALUES ('182', '4', '2017-12-17 11:51:35', '2017-12-17 11:51:35');
INSERT INTO `doc_invoice_cdr_status` VALUES ('183', '4', '2017-12-17 11:51:57', '2017-12-17 11:51:57');
INSERT INTO `doc_invoice_cdr_status` VALUES ('184', '4', '2017-12-17 11:52:58', '2017-12-17 11:52:58');
INSERT INTO `doc_invoice_cdr_status` VALUES ('185', '4', '2017-12-17 11:53:41', '2017-12-17 11:53:41');
INSERT INTO `doc_invoice_cdr_status` VALUES ('186', '4', '2017-12-17 21:58:58', '2017-12-17 21:58:58');
INSERT INTO `doc_invoice_cdr_status` VALUES ('187', '4', '2017-12-17 21:59:23', '2017-12-17 21:59:23');
INSERT INTO `doc_invoice_cdr_status` VALUES ('188', '4', '2017-12-17 22:02:06', '2017-12-17 22:02:06');
INSERT INTO `doc_invoice_cdr_status` VALUES ('189', '4', '2017-12-17 22:24:00', '2017-12-17 22:24:00');
INSERT INTO `doc_invoice_cdr_status` VALUES ('190', '4', '2017-12-17 22:26:44', '2017-12-17 22:26:44');
INSERT INTO `doc_invoice_cdr_status` VALUES ('191', '4', '2017-12-21 08:29:15', '2017-12-21 08:29:15');
INSERT INTO `doc_invoice_cdr_status` VALUES ('192', '4', '2017-12-21 08:31:08', '2017-12-21 08:31:08');
INSERT INTO `doc_invoice_cdr_status` VALUES ('193', '4', '2017-12-21 08:31:50', '2017-12-21 08:31:50');
INSERT INTO `doc_invoice_cdr_status` VALUES ('194', '4', '2017-12-21 10:22:36', '2017-12-21 10:22:36');
INSERT INTO `doc_invoice_cdr_status` VALUES ('195', '4', '2017-12-21 10:23:47', '2017-12-21 10:23:47');
INSERT INTO `doc_invoice_cdr_status` VALUES ('196', '4', '2017-12-21 10:24:14', '2017-12-21 10:24:14');
INSERT INTO `doc_invoice_cdr_status` VALUES ('197', '4', '2017-12-21 10:25:31', '2017-12-21 10:25:31');
INSERT INTO `doc_invoice_cdr_status` VALUES ('198', '4', '2017-12-21 10:26:12', '2017-12-21 10:26:12');
INSERT INTO `doc_invoice_cdr_status` VALUES ('199', '4', '2017-12-21 10:27:59', '2017-12-21 10:27:59');
INSERT INTO `doc_invoice_cdr_status` VALUES ('200', '4', '2017-12-21 10:28:42', '2017-12-21 10:28:42');
INSERT INTO `doc_invoice_cdr_status` VALUES ('201', '4', '2017-12-21 10:29:42', '2017-12-21 10:29:42');
INSERT INTO `doc_invoice_cdr_status` VALUES ('202', '4', '2017-12-21 10:35:29', '2017-12-21 10:35:29');
INSERT INTO `doc_invoice_cdr_status` VALUES ('203', '4', '2017-12-21 10:36:19', '2017-12-21 10:36:19');
INSERT INTO `doc_invoice_cdr_status` VALUES ('204', '4', '2017-12-21 10:36:50', '2017-12-21 10:36:50');
INSERT INTO `doc_invoice_cdr_status` VALUES ('205', '4', '2017-12-21 10:37:21', '2017-12-21 10:37:21');
INSERT INTO `doc_invoice_cdr_status` VALUES ('206', '4', '2017-12-21 10:40:47', '2017-12-21 10:40:47');
INSERT INTO `doc_invoice_cdr_status` VALUES ('207', '4', '2017-12-21 10:45:45', '2017-12-21 10:45:45');
INSERT INTO `doc_invoice_cdr_status` VALUES ('208', '4', '2017-12-21 10:47:52', '2017-12-21 10:47:52');
INSERT INTO `doc_invoice_cdr_status` VALUES ('209', '4', '2017-12-21 10:53:55', '2017-12-21 10:53:55');
INSERT INTO `doc_invoice_cdr_status` VALUES ('210', '4', '2017-12-21 10:54:27', '2017-12-21 10:54:27');
INSERT INTO `doc_invoice_cdr_status` VALUES ('211', '4', '2017-12-21 10:55:08', '2017-12-21 10:55:08');
INSERT INTO `doc_invoice_cdr_status` VALUES ('212', '4', '2017-12-21 10:57:29', '2017-12-21 10:57:29');
INSERT INTO `doc_invoice_cdr_status` VALUES ('213', '4', '2017-12-21 10:58:01', '2017-12-21 10:58:01');
INSERT INTO `doc_invoice_cdr_status` VALUES ('214', '4', '2017-12-21 10:58:17', '2017-12-21 10:58:17');
INSERT INTO `doc_invoice_cdr_status` VALUES ('215', '4', '2017-12-21 10:58:38', '2017-12-21 10:58:38');
INSERT INTO `doc_invoice_cdr_status` VALUES ('216', '4', '2017-12-21 11:06:11', '2017-12-21 11:06:11');
INSERT INTO `doc_invoice_cdr_status` VALUES ('217', '4', '2017-12-21 11:07:11', '2017-12-21 11:07:11');
INSERT INTO `doc_invoice_cdr_status` VALUES ('218', '4', '2017-12-21 11:09:47', '2017-12-21 11:09:47');
INSERT INTO `doc_invoice_cdr_status` VALUES ('219', '4', '2017-12-21 11:10:08', '2017-12-21 11:10:08');
INSERT INTO `doc_invoice_cdr_status` VALUES ('220', '4', '2017-12-21 11:10:32', '2017-12-21 11:10:32');
INSERT INTO `doc_invoice_cdr_status` VALUES ('221', '4', '2017-12-21 11:21:53', '2017-12-21 11:21:53');
INSERT INTO `doc_invoice_cdr_status` VALUES ('222', '4', '2017-12-21 11:22:13', '2017-12-21 11:22:13');
INSERT INTO `doc_invoice_cdr_status` VALUES ('223', '4', '2017-12-21 11:22:53', '2017-12-21 11:22:53');
INSERT INTO `doc_invoice_cdr_status` VALUES ('224', '4', '2017-12-21 11:24:33', '2017-12-21 11:24:33');
INSERT INTO `doc_invoice_cdr_status` VALUES ('225', '4', '2017-12-21 11:26:49', '2017-12-21 11:26:49');
INSERT INTO `doc_invoice_cdr_status` VALUES ('226', '4', '2017-12-21 11:27:28', '2017-12-21 11:27:28');
INSERT INTO `doc_invoice_cdr_status` VALUES ('227', '4', '2017-12-21 11:30:29', '2017-12-21 11:30:29');
INSERT INTO `doc_invoice_cdr_status` VALUES ('228', '4', '2017-12-21 11:31:47', '2017-12-21 11:31:47');
INSERT INTO `doc_invoice_cdr_status` VALUES ('229', '4', '2017-12-21 11:37:49', '2017-12-21 11:37:49');
INSERT INTO `doc_invoice_cdr_status` VALUES ('230', '4', '2017-12-21 11:38:27', '2017-12-21 11:38:27');
INSERT INTO `doc_invoice_cdr_status` VALUES ('231', '4', '2017-12-21 11:40:21', '2017-12-21 11:40:21');
INSERT INTO `doc_invoice_cdr_status` VALUES ('232', '4', '2017-12-22 02:22:50', '2017-12-22 02:22:50');
INSERT INTO `doc_invoice_cdr_status` VALUES ('233', '4', '2017-12-22 02:29:19', '2017-12-22 02:29:19');
INSERT INTO `doc_invoice_cdr_status` VALUES ('234', '4', '2017-12-22 02:30:45', '2017-12-22 02:30:45');
INSERT INTO `doc_invoice_cdr_status` VALUES ('235', '4', '2017-12-22 02:36:56', '2017-12-22 02:36:56');
INSERT INTO `doc_invoice_cdr_status` VALUES ('236', '4', '2017-12-22 02:41:40', '2017-12-22 02:41:40');
INSERT INTO `doc_invoice_cdr_status` VALUES ('237', '4', '2017-12-22 02:43:21', '2017-12-22 02:43:21');
INSERT INTO `doc_invoice_cdr_status` VALUES ('238', '4', '2017-12-22 02:44:41', '2017-12-22 02:44:41');
INSERT INTO `doc_invoice_cdr_status` VALUES ('239', '4', '2017-12-22 02:47:26', '2017-12-22 02:47:26');
INSERT INTO `doc_invoice_cdr_status` VALUES ('240', '4', '2017-12-22 02:48:56', '2017-12-22 02:48:56');
INSERT INTO `doc_invoice_cdr_status` VALUES ('241', '4', '2017-12-22 02:53:33', '2017-12-22 02:53:33');
INSERT INTO `doc_invoice_cdr_status` VALUES ('242', '4', '2017-12-22 02:54:48', '2017-12-22 02:54:48');
INSERT INTO `doc_invoice_cdr_status` VALUES ('243', '4', '2017-12-22 03:02:09', '2017-12-22 03:02:09');
INSERT INTO `doc_invoice_cdr_status` VALUES ('244', '4', '2017-12-22 03:05:08', '2017-12-22 03:05:08');
INSERT INTO `doc_invoice_cdr_status` VALUES ('245', '4', '2017-12-22 03:07:35', '2017-12-22 03:07:35');
INSERT INTO `doc_invoice_cdr_status` VALUES ('246', '4', '2017-12-22 03:13:18', '2017-12-22 03:13:18');
INSERT INTO `doc_invoice_cdr_status` VALUES ('247', '4', '2017-12-22 06:26:55', '2017-12-22 06:26:55');
INSERT INTO `doc_invoice_cdr_status` VALUES ('248', '4', '2017-12-22 06:28:49', '2017-12-22 06:28:49');
INSERT INTO `doc_invoice_cdr_status` VALUES ('249', '4', '2017-12-22 06:34:44', '2017-12-22 06:34:44');
INSERT INTO `doc_invoice_cdr_status` VALUES ('250', '4', '2017-12-22 06:37:37', '2017-12-22 06:37:37');
INSERT INTO `doc_invoice_cdr_status` VALUES ('251', '4', '2017-12-22 07:26:52', '2017-12-22 07:26:52');
INSERT INTO `doc_invoice_cdr_status` VALUES ('252', '4', '2017-12-22 07:27:53', '2017-12-22 07:27:53');
INSERT INTO `doc_invoice_cdr_status` VALUES ('253', '4', '2017-12-22 07:32:34', '2017-12-22 07:32:34');
INSERT INTO `doc_invoice_cdr_status` VALUES ('254', '4', '2017-12-22 07:33:49', '2017-12-22 07:33:49');
INSERT INTO `doc_invoice_cdr_status` VALUES ('255', '4', '2017-12-22 07:35:26', '2017-12-22 07:35:26');
INSERT INTO `doc_invoice_cdr_status` VALUES ('256', '4', '2017-12-22 07:41:04', '2017-12-22 07:41:04');
INSERT INTO `doc_invoice_cdr_status` VALUES ('257', '4', '2017-12-22 07:42:00', '2017-12-22 07:42:00');
INSERT INTO `doc_invoice_cdr_status` VALUES ('258', '4', '2017-12-22 07:45:47', '2017-12-22 07:45:47');
INSERT INTO `doc_invoice_cdr_status` VALUES ('259', '4', '2017-12-22 07:50:13', '2017-12-22 07:50:13');
INSERT INTO `doc_invoice_cdr_status` VALUES ('260', '4', '2017-12-22 07:52:12', '2017-12-22 07:52:12');
INSERT INTO `doc_invoice_cdr_status` VALUES ('261', '4', '2017-12-22 07:54:29', '2017-12-22 07:54:29');
INSERT INTO `doc_invoice_cdr_status` VALUES ('262', '1', '2017-12-22 07:58:17', '2017-12-22 07:58:47');
INSERT INTO `doc_invoice_cdr_status` VALUES ('263', '1', '2017-12-22 08:15:44', '2017-12-22 08:16:18');
INSERT INTO `doc_invoice_cdr_status` VALUES ('264', '4', '2017-12-22 08:17:59', '2017-12-22 08:17:59');
INSERT INTO `doc_invoice_cdr_status` VALUES ('265', '4', '2017-12-22 08:19:43', '2017-12-22 08:19:43');
INSERT INTO `doc_invoice_cdr_status` VALUES ('266', '4', '2017-12-22 08:27:33', '2017-12-22 08:27:33');
INSERT INTO `doc_invoice_cdr_status` VALUES ('267', '4', '2017-12-22 08:36:40', '2017-12-22 08:36:40');
INSERT INTO `doc_invoice_cdr_status` VALUES ('268', '4', '2017-12-22 08:38:23', '2017-12-22 08:38:23');
INSERT INTO `doc_invoice_cdr_status` VALUES ('269', '4', '2017-12-22 08:39:54', '2017-12-22 08:39:54');
INSERT INTO `doc_invoice_cdr_status` VALUES ('270', '4', '2017-12-22 08:41:22', '2017-12-22 08:41:22');
INSERT INTO `doc_invoice_cdr_status` VALUES ('271', '4', '2017-12-22 08:45:03', '2017-12-22 08:45:03');
INSERT INTO `doc_invoice_cdr_status` VALUES ('272', '4', '2017-12-22 08:46:04', '2017-12-22 08:46:04');

-- ----------------------------
-- Table structure for doc_invoice_customer
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_customer`;
CREATE TABLE `doc_invoice_customer` (
  `n_id_invoice` int(11) NOT NULL,
  `n_id_customer` int(11) NOT NULL,
  `c_customer_assigned_account_id` varchar(15) NOT NULL,
  `c_additional_account_id` varchar(1) NOT NULL,
  `c_party_party_legal_entity_registration_name` varchar(100) NOT NULL,
  `c_party_physical_location_description` varchar(100) DEFAULT NULL,
  `c_postal_address_id` varchar(6) DEFAULT NULL,
  `c_postal_address_street_name` varchar(100) DEFAULT NULL,
  `c_postal_address_city_subdivision_name` varchar(25) DEFAULT NULL,
  `c_postal_address_city_name` varchar(30) DEFAULT NULL,
  `c_postal_address_country_subentity` varchar(30) DEFAULT NULL,
  `c_postal_address_district` varchar(30) DEFAULT NULL,
  `c_postal_address_country_identification_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  UNIQUE KEY `fk_invoice_invoice_customer` (`n_id_invoice`),
  KEY `fk_customer_invoice_customer` (`n_id_customer`),
  CONSTRAINT `fk_customer_invoice_customer` FOREIGN KEY (`n_id_customer`) REFERENCES `cus_customer` (`n_id_customer`),
  CONSTRAINT `fk_invoice_invoice_customer` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_customer
-- ----------------------------
INSERT INTO `doc_invoice_customer` VALUES ('138', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('139', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('140', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('141', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('142', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('143', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('144', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('145', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('146', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('147', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('148', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('149', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('150', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('151', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('152', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('153', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('154', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('155', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('156', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('157', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('158', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('159', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('160', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('161', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('162', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('163', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('164', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('165', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('166', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('167', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('168', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('169', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('170', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('171', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('172', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('173', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('174', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('175', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('176', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('177', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('178', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('179', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('180', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('181', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('182', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('183', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('184', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('185', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('186', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('187', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('188', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('189', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('190', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('191', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('192', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('193', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('194', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('195', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('196', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('197', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('198', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('199', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('200', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('201', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('202', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('203', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('204', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('205', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('206', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('207', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('208', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('209', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('210', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('211', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('212', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('213', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('214', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('215', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('216', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('217', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('218', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('219', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('220', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('221', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('222', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('223', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('224', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('225', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('226', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('227', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('228', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('229', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('230', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('231', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('232', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('233', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('234', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('235', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('236', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('237', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('238', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('239', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('240', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('241', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('242', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('243', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('244', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('245', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('246', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('247', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('248', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('249', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('250', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('251', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('252', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('253', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('254', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('255', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('256', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('257', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('258', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('259', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('260', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('261', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('262', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('263', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('264', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('265', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('266', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('267', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('268', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('269', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('270', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('271', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_customer` VALUES ('272', '49', '20112273922', '6', 'MAESTRO PERU S.A.', 'JR. SAN LORENZO N° 881 SURQUILLO-LIMA', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for doc_invoice_despatch_document_reference
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_despatch_document_reference`;
CREATE TABLE `doc_invoice_despatch_document_reference` (
  `n_id_invoice_despatch_document_reference` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `c_id` varchar(30) DEFAULT NULL COMMENT 'Numero de guia',
  `c_document_type_code` varchar(2) DEFAULT NULL COMMENT 'Tipo de documento - Catalogo No 01',
  PRIMARY KEY (`n_id_invoice_despatch_document_reference`),
  KEY `fk_invoice_invoice_despatch_document_reference` (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_despatch_document_reference` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_despatch_document_reference
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_discrepancy_response
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_discrepancy_response`;
CREATE TABLE `doc_invoice_discrepancy_response` (
  `n_id_invoice` int(11) NOT NULL,
  `c_reference_id` varchar(13) NOT NULL,
  `c_response_code` varchar(2) NOT NULL,
  `c_description` varchar(250) NOT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_discrepancy_response` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_discrepancy_response
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_extra_data
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_extra_data`;
CREATE TABLE `doc_invoice_extra_data` (
  `n_id_invoice` int(11) NOT NULL,
  `c_customer_email` varchar(100) DEFAULT NULL,
  `c_exchange_rate` varchar(10) NOT NULL,
  `c_email_was_sent` enum('yes','no') NOT NULL DEFAULT 'no' COMMENT 'Envio un correo al cliente.',
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_extra_data` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_extra_data
-- ----------------------------
INSERT INTO `doc_invoice_extra_data` VALUES ('148', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('149', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('150', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('151', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('152', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('153', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('154', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('155', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('156', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('157', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('158', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('159', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('160', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('161', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('162', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('163', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('164', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('165', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('166', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('167', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('168', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('169', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('170', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('171', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('172', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('173', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('174', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('175', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('176', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('177', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('178', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('179', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('180', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('181', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('182', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('183', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('184', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('185', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('186', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('187', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('188', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('189', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('190', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('191', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('192', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('193', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('194', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('195', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('196', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('197', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('198', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('199', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('200', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('201', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('202', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('203', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('204', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('205', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('206', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('207', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('208', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('209', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('210', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('211', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('212', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('213', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('214', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('215', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('216', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('217', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('218', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('219', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('220', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('221', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('222', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('223', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('224', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('225', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('226', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('227', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('228', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('229', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('230', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('231', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('232', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('233', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('234', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('235', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('236', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('237', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('238', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('239', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('240', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('241', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('242', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('243', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('244', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('245', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('246', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('247', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('248', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('249', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('250', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('251', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('252', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('253', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('254', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('255', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('256', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('257', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('258', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('259', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('260', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('261', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('262', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('263', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('264', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('265', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('266', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('267', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('268', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('269', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('270', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('271', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');
INSERT INTO `doc_invoice_extra_data` VALUES ('272', 'mjaramillo@mjesoftwaretecnology.com', '3.341', 'no');

-- ----------------------------
-- Table structure for doc_invoice_file
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_file`;
CREATE TABLE `doc_invoice_file` (
  `n_id_invoice` int(11) NOT NULL,
  `c_has_document` enum('yes','no') NOT NULL,
  `c_document_name` varchar(200) DEFAULT NULL,
  `d_date_document_created` datetime DEFAULT NULL,
  `c_is_sent` enum('yes','no') NOT NULL DEFAULT 'no',
  `c_has_cdr` enum('yes','no') NOT NULL,
  `c_cdr_name` varchar(200) DEFAULT NULL,
  `d_date_cdr_created` datetime DEFAULT NULL,
  `c_has_pdf` enum('yes','no') NOT NULL,
  `c_pdf_name` varchar(200) DEFAULT NULL,
  `c_has_sunat_response` enum('yes','no') DEFAULT NULL,
  `c_has_sunat_successfully_passed` enum('yes','no') DEFAULT NULL,
  `c_is_cdr_processed_dispatched` enum('yes','no') DEFAULT NULL,
  `c_cdr_processed_requested_by_account` tinyint(1) NOT NULL DEFAULT '0',
  `c_cdr_processed_requested_account` int(11) NOT NULL DEFAULT '14',
  `c_input_name` varchar(255) DEFAULT NULL,
  `c_input_hash` varchar(255) DEFAULT NULL,
  `d_date_input_created` date DEFAULT NULL,
  `d_date_register` datetime NOT NULL,
  `d_date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  KEY `c_cdr_processed_requested_account` (`c_cdr_processed_requested_account`),
  CONSTRAINT `fk_account_invoice_file` FOREIGN KEY (`c_cdr_processed_requested_account`) REFERENCES `acc_account` (`n_id_account`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_invoice_invoice_file` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_file
-- ----------------------------
INSERT INTO `doc_invoice_file` VALUES ('163', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:03:41', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 11:03:41', '2017-12-17 11:03:41');
INSERT INTO `doc_invoice_file` VALUES ('164', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:07:38', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 11:07:38', '2017-12-17 11:07:38');
INSERT INTO `doc_invoice_file` VALUES ('165', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:08:15', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 11:08:15', '2017-12-17 11:08:15');
INSERT INTO `doc_invoice_file` VALUES ('166', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:08:48', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 11:08:48', '2017-12-17 11:08:48');
INSERT INTO `doc_invoice_file` VALUES ('167', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:09:18', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 11:09:18', '2017-12-17 11:09:18');
INSERT INTO `doc_invoice_file` VALUES ('168', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:10:14', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 11:10:14', '2017-12-17 11:10:14');
INSERT INTO `doc_invoice_file` VALUES ('169', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:39:23', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 11:39:23', '2017-12-17 11:39:23');
INSERT INTO `doc_invoice_file` VALUES ('170', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:40:03', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:40:03', '2017-12-17 11:40:04');
INSERT INTO `doc_invoice_file` VALUES ('171', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:41:05', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:41:05', '2017-12-17 11:41:05');
INSERT INTO `doc_invoice_file` VALUES ('172', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:41:34', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:41:34', '2017-12-17 11:41:34');
INSERT INTO `doc_invoice_file` VALUES ('173', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:41:52', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:41:52', '2017-12-17 11:41:52');
INSERT INTO `doc_invoice_file` VALUES ('174', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:42:16', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:42:16', '2017-12-17 11:42:16');
INSERT INTO `doc_invoice_file` VALUES ('175', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:43:39', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:43:39', '2017-12-17 11:43:39');
INSERT INTO `doc_invoice_file` VALUES ('176', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:44:46', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:44:46', '2017-12-17 11:44:46');
INSERT INTO `doc_invoice_file` VALUES ('177', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:45:21', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:45:21', '2017-12-17 11:45:21');
INSERT INTO `doc_invoice_file` VALUES ('178', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:48:12', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:48:12', '2017-12-17 11:48:12');
INSERT INTO `doc_invoice_file` VALUES ('179', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:48:49', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:48:49', '2017-12-17 11:48:49');
INSERT INTO `doc_invoice_file` VALUES ('180', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:49:48', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:49:48', '2017-12-17 11:49:48');
INSERT INTO `doc_invoice_file` VALUES ('181', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:50:54', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:50:54', '2017-12-17 11:50:54');
INSERT INTO `doc_invoice_file` VALUES ('182', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:51:37', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:51:37', '2017-12-17 11:51:38');
INSERT INTO `doc_invoice_file` VALUES ('183', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:52:00', 'yes', 'no', null, null, 'no', null, 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:52:00', '2017-12-17 11:52:01');
INSERT INTO `doc_invoice_file` VALUES ('184', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:53:01', 'yes', 'no', null, null, 'no', null, 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:53:01', '2017-12-17 11:53:03');
INSERT INTO `doc_invoice_file` VALUES ('185', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 11:53:44', 'yes', 'no', null, null, 'no', null, 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 11:53:44', '2017-12-17 11:53:45');
INSERT INTO `doc_invoice_file` VALUES ('186', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 21:59:03', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 21:59:03', '2017-12-17 21:59:03');
INSERT INTO `doc_invoice_file` VALUES ('187', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 21:59:35', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 21:59:35', '2017-12-17 21:59:35');
INSERT INTO `doc_invoice_file` VALUES ('188', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 22:02:17', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-17 22:02:17', '2017-12-17 22:02:17');
INSERT INTO `doc_invoice_file` VALUES ('189', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-17 22:24:13', 'yes', 'no', null, null, 'no', null, 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-17 22:24:13', '2017-12-17 22:24:15');
INSERT INTO `doc_invoice_file` VALUES ('192', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 08:31:25', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 08:31:25', '2017-12-21 08:31:25');
INSERT INTO `doc_invoice_file` VALUES ('193', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 08:31:58', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 08:31:58', '2017-12-21 08:31:59');
INSERT INTO `doc_invoice_file` VALUES ('194', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:22:46', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:22:46', '2017-12-21 10:22:46');
INSERT INTO `doc_invoice_file` VALUES ('195', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:23:50', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:23:50', '2017-12-21 10:23:51');
INSERT INTO `doc_invoice_file` VALUES ('196', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:24:20', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:24:20', '2017-12-21 10:24:20');
INSERT INTO `doc_invoice_file` VALUES ('197', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:25:38', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:25:38', '2017-12-21 10:25:38');
INSERT INTO `doc_invoice_file` VALUES ('198', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:26:15', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:26:15', '2017-12-21 10:26:15');
INSERT INTO `doc_invoice_file` VALUES ('199', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:28:03', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:28:03', '2017-12-21 10:28:04');
INSERT INTO `doc_invoice_file` VALUES ('200', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:28:53', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:28:53', '2017-12-21 10:28:53');
INSERT INTO `doc_invoice_file` VALUES ('201', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:29:45', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:29:45', '2017-12-21 10:29:45');
INSERT INTO `doc_invoice_file` VALUES ('202', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:35:33', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:35:33', '2017-12-21 10:35:33');
INSERT INTO `doc_invoice_file` VALUES ('203', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:36:23', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:36:23', '2017-12-21 10:36:23');
INSERT INTO `doc_invoice_file` VALUES ('204', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:36:54', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:36:54', '2017-12-21 10:36:54');
INSERT INTO `doc_invoice_file` VALUES ('205', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:37:26', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:37:26', '2017-12-21 10:37:26');
INSERT INTO `doc_invoice_file` VALUES ('206', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:40:52', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:40:52', '2017-12-21 10:40:52');
INSERT INTO `doc_invoice_file` VALUES ('207', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:45:49', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:45:49', '2017-12-21 10:45:49');
INSERT INTO `doc_invoice_file` VALUES ('208', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:47:56', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:47:56', '2017-12-21 10:47:57');
INSERT INTO `doc_invoice_file` VALUES ('209', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:53:59', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:53:59', '2017-12-21 10:53:59');
INSERT INTO `doc_invoice_file` VALUES ('210', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:54:30', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:54:30', '2017-12-21 10:54:30');
INSERT INTO `doc_invoice_file` VALUES ('211', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:55:11', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:55:11', '2017-12-21 10:55:11');
INSERT INTO `doc_invoice_file` VALUES ('212', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:57:33', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:57:33', '2017-12-21 10:57:33');
INSERT INTO `doc_invoice_file` VALUES ('213', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:58:05', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:58:05', '2017-12-21 10:58:05');
INSERT INTO `doc_invoice_file` VALUES ('214', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:58:20', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:58:20', '2017-12-21 10:58:20');
INSERT INTO `doc_invoice_file` VALUES ('215', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 10:58:41', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 10:58:41', '2017-12-21 10:58:41');
INSERT INTO `doc_invoice_file` VALUES ('216', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:06:15', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:06:15', '2017-12-21 11:06:15');
INSERT INTO `doc_invoice_file` VALUES ('217', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:07:15', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:07:15', '2017-12-21 11:07:15');
INSERT INTO `doc_invoice_file` VALUES ('218', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:09:51', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:09:51', '2017-12-21 11:09:51');
INSERT INTO `doc_invoice_file` VALUES ('219', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:10:12', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:10:12', '2017-12-21 11:10:12');
INSERT INTO `doc_invoice_file` VALUES ('220', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:10:36', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:10:36', '2017-12-21 11:10:36');
INSERT INTO `doc_invoice_file` VALUES ('221', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:21:58', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:21:58', '2017-12-21 11:21:58');
INSERT INTO `doc_invoice_file` VALUES ('222', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:22:18', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:22:18', '2017-12-21 11:22:18');
INSERT INTO `doc_invoice_file` VALUES ('223', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:22:56', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:22:56', '2017-12-21 11:22:56');
INSERT INTO `doc_invoice_file` VALUES ('224', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:24:37', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:24:37', '2017-12-21 11:24:37');
INSERT INTO `doc_invoice_file` VALUES ('225', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:26:53', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:26:53', '2017-12-21 11:26:53');
INSERT INTO `doc_invoice_file` VALUES ('226', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:27:31', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:27:31', '2017-12-21 11:27:32');
INSERT INTO `doc_invoice_file` VALUES ('227', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:30:33', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:30:33', '2017-12-21 11:30:33');
INSERT INTO `doc_invoice_file` VALUES ('228', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:31:51', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:31:51', '2017-12-21 11:31:51');
INSERT INTO `doc_invoice_file` VALUES ('229', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:37:53', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:37:53', '2017-12-21 11:37:54');
INSERT INTO `doc_invoice_file` VALUES ('230', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:38:31', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:38:31', '2017-12-21 11:38:32');
INSERT INTO `doc_invoice_file` VALUES ('231', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-21 11:40:25', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-21 11:40:25', '2017-12-21 11:40:26');
INSERT INTO `doc_invoice_file` VALUES ('232', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:23:11', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', null, null, null, '2017-12-22 02:23:11', '2017-12-22 02:23:11');
INSERT INTO `doc_invoice_file` VALUES ('233', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:29:40', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 02:29:40', '2017-12-22 02:29:41');
INSERT INTO `doc_invoice_file` VALUES ('237', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:43:42', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 02:43:42', '2017-12-22 02:43:43');
INSERT INTO `doc_invoice_file` VALUES ('238', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:45:02', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 02:45:02', '2017-12-22 02:45:03');
INSERT INTO `doc_invoice_file` VALUES ('239', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:47:47', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 02:47:47', '2017-12-22 02:47:54');
INSERT INTO `doc_invoice_file` VALUES ('240', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:49:17', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 02:49:17', '2017-12-22 02:49:24');
INSERT INTO `doc_invoice_file` VALUES ('241', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:53:54', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 02:53:54', '2017-12-22 02:54:01');
INSERT INTO `doc_invoice_file` VALUES ('242', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 02:55:09', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 02:55:09', '2017-12-22 02:55:15');
INSERT INTO `doc_invoice_file` VALUES ('243', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 03:02:30', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 03:02:30', '2017-12-22 03:02:37');
INSERT INTO `doc_invoice_file` VALUES ('244', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 03:05:29', 'yes', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 03:05:29', '2017-12-22 03:05:39');
INSERT INTO `doc_invoice_file` VALUES ('245', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 03:07:56', 'yes', 'no', null, null, 'yes', '20524719585-01-FF11-00000001.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 03:07:56', '2017-12-22 03:08:06');
INSERT INTO `doc_invoice_file` VALUES ('246', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 03:13:35', 'yes', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 03:13:35', '2017-12-22 03:13:45');
INSERT INTO `doc_invoice_file` VALUES ('247', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 06:27:12', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 06:27:12', '2017-12-22 06:27:19');
INSERT INTO `doc_invoice_file` VALUES ('248', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 06:29:06', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 06:29:06', '2017-12-22 06:29:13');
INSERT INTO `doc_invoice_file` VALUES ('249', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 06:35:01', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 06:35:01', '2017-12-22 06:35:08');
INSERT INTO `doc_invoice_file` VALUES ('250', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 06:37:54', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 06:37:54', '2017-12-22 06:38:00');
INSERT INTO `doc_invoice_file` VALUES ('251', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:27:09', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:27:09', '2017-12-22 07:27:15');
INSERT INTO `doc_invoice_file` VALUES ('252', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:28:09', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:28:09', '2017-12-22 07:28:16');
INSERT INTO `doc_invoice_file` VALUES ('253', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:32:51', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:32:51', '2017-12-22 07:32:57');
INSERT INTO `doc_invoice_file` VALUES ('254', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:34:05', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:34:05', '2017-12-22 07:34:12');
INSERT INTO `doc_invoice_file` VALUES ('255', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:35:43', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:35:43', '2017-12-22 07:35:50');
INSERT INTO `doc_invoice_file` VALUES ('256', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:41:21', 'no', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:41:21', '2017-12-22 07:41:28');
INSERT INTO `doc_invoice_file` VALUES ('257', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:42:17', 'yes', 'no', null, null, 'yes', '20524719585-01-FF11-00000002.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:42:17', '2017-12-22 07:42:27');
INSERT INTO `doc_invoice_file` VALUES ('258', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:46:04', 'yes', 'yes', 'R-20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:46:14', 'yes', '20524719585-01-FF11-00000002.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:46:04', '2017-12-22 07:46:14');
INSERT INTO `doc_invoice_file` VALUES ('259', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:50:30', 'yes', 'yes', 'R-20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:50:41', 'yes', '20524719585-01-FF11-00000002.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:50:30', '2017-12-22 07:50:41');
INSERT INTO `doc_invoice_file` VALUES ('260', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:52:29', 'yes', 'yes', 'R-20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:52:39', 'yes', '20524719585-01-FF11-00000002.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:52:29', '2017-12-22 07:52:39');
INSERT INTO `doc_invoice_file` VALUES ('261', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:54:46', 'yes', 'yes', 'R-20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:54:57', 'yes', '20524719585-01-FF11-00000002.PDF', 'yes', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:54:46', '2017-12-22 07:54:57');
INSERT INTO `doc_invoice_file` VALUES ('262', 'yes', '20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:58:34', 'yes', 'yes', 'R-20524719585-01-FF11-00000002.ZIP', '2017-12-22 07:58:45', 'yes', '20524719585-01-FF11-00000002.PDF', 'yes', 'yes', 'no', '0', '14', '01102016-20524719585-01-FF11-00000002.txt', '918235736681958808e8101c13ac2f5a', '2016-10-01', '2017-12-22 07:58:34', '2017-12-22 07:58:47');
INSERT INTO `doc_invoice_file` VALUES ('263', 'yes', '20524719585-01-FF11-00000001.ZIP', '2017-12-22 08:16:05', 'yes', 'yes', 'R-20524719585-01-FF11-00000001.ZIP', '2017-12-22 08:16:15', 'yes', '20524719585-01-FF11-00000001.PDF', 'yes', 'yes', 'no', '0', '14', '01102016-20524719585-01-FF11-00000001.txt', '732a07b57c90a393f112444634625e6d', '2016-10-01', '2017-12-22 08:16:05', '2017-12-22 08:16:18');
INSERT INTO `doc_invoice_file` VALUES ('264', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:18:12', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:18:12', '2017-12-22 08:18:13');
INSERT INTO `doc_invoice_file` VALUES ('265', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:19:56', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:19:56', '2017-12-22 08:19:57');
INSERT INTO `doc_invoice_file` VALUES ('266', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:27:46', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:27:46', '2017-12-22 08:27:47');
INSERT INTO `doc_invoice_file` VALUES ('267', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:36:53', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:36:53', '2017-12-22 08:36:55');
INSERT INTO `doc_invoice_file` VALUES ('268', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:38:36', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:38:36', '2017-12-22 08:38:38');
INSERT INTO `doc_invoice_file` VALUES ('269', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:40:07', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:40:07', '2017-12-22 08:40:09');
INSERT INTO `doc_invoice_file` VALUES ('270', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:41:34', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:41:34', '2017-12-22 08:41:36');
INSERT INTO `doc_invoice_file` VALUES ('271', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:45:16', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:45:16', '2017-12-22 08:45:18');
INSERT INTO `doc_invoice_file` VALUES ('272', 'yes', '20524719585-01-FF11-00000003.ZIP', '2017-12-22 08:46:17', 'no', 'no', null, null, 'no', null, 'no', 'no', 'no', '0', '14', '01102016-20524719585-01-FF11-00000003.txt', '3a8846bfd51e571e2b3a4c266c4de7e0', '2016-10-01', '2017-12-22 08:46:17', '2017-12-22 08:46:18');

-- ----------------------------
-- Table structure for doc_invoice_item
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item`;
CREATE TABLE `doc_invoice_item` (
  `n_id_invoice_item` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `n_id` int(3) DEFAULT NULL COMMENT 'Número de orden del Item',
  `c_invoiced_quantity_unit_code` varchar(3) DEFAULT NULL COMMENT 'Unidad de medida - Catalogo No 03',
  `c_invoiced_quantity` varchar(16) DEFAULT NULL COMMENT 'Cantidad',
  `c_line_extension_amount` varchar(15) DEFAULT NULL COMMENT 'Valor de venta por item',
  `c_item_sellers_item_identification_id` varchar(30) DEFAULT NULL COMMENT 'Codigo del Producto',
  `c_price_price_amount` varchar(15) DEFAULT NULL COMMENT 'Valor unitario por item',
  `c_document_type_code` varchar(2) DEFAULT NULL,
  `c_document_serial_id` varchar(4) DEFAULT NULL,
  `c_document_number_id` varchar(8) DEFAULT NULL,
  `c_void_reason_description` varchar(100) DEFAULT NULL,
  `c_start_document_number_id` varchar(8) DEFAULT NULL,
  `c_end_document_number_id` varchar(8) DEFAULT NULL,
  `c_total_amount` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice_item`),
  KEY `fk_invoice_invoice_item` (`n_id_invoice`),
  KEY `fk_invoice_type_code_invoice_item` (`c_document_type_code`),
  CONSTRAINT `fk_invoice_invoice_item` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_invoice_type_code_invoice_item` FOREIGN KEY (`c_document_type_code`) REFERENCES `doc_invoice_type_code` (`c_invoice_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=522 DEFAULT CHARSET=utf8 COMMENT='Invoice Inline';

-- ----------------------------
-- Records of doc_invoice_item
-- ----------------------------
INSERT INTO `doc_invoice_item` VALUES ('170', '140', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('171', '141', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('172', '142', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('173', '143', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('174', '144', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('175', '145', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('176', '146', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('177', '146', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('178', '146', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('179', '147', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('180', '147', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('181', '147', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('182', '148', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('183', '148', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('184', '148', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('185', '149', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('186', '149', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('187', '149', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('188', '150', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('189', '150', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('190', '150', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('191', '151', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('192', '151', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('193', '151', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('194', '152', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('195', '152', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('196', '152', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('197', '153', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('198', '153', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('199', '153', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('200', '154', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('201', '154', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('202', '154', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('203', '155', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('204', '155', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('205', '155', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('206', '156', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('207', '156', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('208', '156', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('209', '157', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('210', '157', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('211', '157', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('212', '158', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('213', '158', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('214', '158', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('215', '159', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('216', '159', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('217', '159', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('218', '160', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('219', '160', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('220', '160', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('221', '161', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('222', '161', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('223', '161', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('224', '162', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('225', '162', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('226', '162', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('227', '163', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('228', '163', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('229', '163', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('230', '164', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('231', '164', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('232', '164', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('233', '165', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('234', '165', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('235', '165', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('236', '166', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('237', '166', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('238', '166', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('239', '167', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('240', '167', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('241', '167', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('242', '168', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('243', '168', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('244', '168', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('245', '169', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('246', '169', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('247', '169', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('248', '170', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('249', '170', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('250', '170', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('251', '171', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('252', '171', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('253', '171', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('254', '172', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('255', '172', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('256', '172', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('257', '173', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('258', '173', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('259', '173', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('260', '174', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('261', '174', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('262', '174', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('263', '175', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('264', '175', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('265', '175', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('266', '176', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('267', '176', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('268', '176', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('269', '177', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('270', '177', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('271', '177', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('272', '178', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('273', '178', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('274', '178', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('275', '179', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('276', '179', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('277', '179', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('278', '180', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('279', '180', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('280', '180', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('281', '181', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('282', '181', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('283', '181', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('284', '182', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('285', '182', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('286', '182', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('287', '183', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('288', '183', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('289', '183', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('290', '184', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('291', '184', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('292', '184', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('293', '185', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('294', '185', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('295', '185', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('296', '186', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('297', '186', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('298', '186', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('299', '187', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('300', '187', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('301', '187', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('302', '188', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('303', '188', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('304', '188', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('305', '189', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('306', '189', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('307', '189', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('308', '190', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('309', '190', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('310', '190', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('311', '191', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('312', '191', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('313', '191', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('314', '192', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('315', '192', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('316', '192', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('317', '193', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('318', '193', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('319', '193', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('320', '194', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('321', '194', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('322', '194', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('323', '195', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('324', '195', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('325', '195', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('326', '196', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('327', '196', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('328', '196', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('329', '197', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('330', '197', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('331', '197', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('332', '198', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('333', '198', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('334', '198', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('335', '199', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('336', '199', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('337', '199', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('338', '200', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('339', '200', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('340', '200', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('341', '201', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('342', '201', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('343', '201', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('344', '202', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('345', '202', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('346', '202', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('347', '203', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('348', '203', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('349', '203', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('350', '204', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('351', '204', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('352', '204', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('353', '205', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('354', '205', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('355', '205', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('356', '206', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('357', '206', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('358', '206', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('359', '207', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('360', '207', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('361', '207', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('362', '208', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('363', '208', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('364', '208', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('365', '209', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('366', '209', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('367', '209', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('368', '210', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('369', '210', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('370', '210', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('371', '211', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('372', '211', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('373', '211', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('374', '212', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('375', '212', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('376', '212', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('377', '213', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('378', '213', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('379', '213', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('380', '214', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('381', '214', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('382', '214', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('383', '215', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('384', '215', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('385', '215', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('386', '216', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('387', '216', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('388', '216', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('389', '217', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('390', '217', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('391', '217', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('392', '218', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('393', '218', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('394', '218', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('395', '219', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('396', '219', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('397', '219', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('398', '220', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('399', '220', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('400', '220', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('401', '221', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('402', '221', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('403', '221', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('404', '222', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('405', '222', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('406', '222', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('407', '223', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('408', '223', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('409', '223', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('410', '224', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('411', '224', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('412', '224', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('413', '225', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('414', '225', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('415', '225', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('416', '226', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('417', '226', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('418', '226', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('419', '227', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('420', '227', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('421', '227', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('422', '228', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('423', '228', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('424', '228', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('425', '229', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('426', '229', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('427', '229', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('428', '230', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('429', '230', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('430', '230', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('431', '231', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('432', '231', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('433', '231', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('434', '232', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('435', '232', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('436', '232', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('437', '233', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('438', '233', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('439', '233', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('440', '234', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('441', '234', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('442', '234', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('443', '235', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('444', '235', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('445', '235', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('446', '236', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('447', '236', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('448', '236', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('449', '237', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('450', '237', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('451', '237', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('452', '238', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('453', '238', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('454', '238', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('455', '239', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('456', '239', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('457', '239', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('458', '240', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('459', '240', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('460', '240', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('461', '241', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('462', '241', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('463', '241', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('464', '242', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('465', '242', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('466', '242', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('467', '243', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('468', '243', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('469', '243', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('470', '244', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('471', '244', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('472', '244', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('473', '245', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('474', '245', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('475', '245', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('476', '246', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('477', '246', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('478', '247', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('479', '247', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('480', '248', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('481', '248', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('482', '249', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('483', '249', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('484', '250', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('485', '250', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('486', '251', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('487', '251', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('488', '252', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('489', '252', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('490', '253', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('491', '253', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('492', '254', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('493', '254', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('494', '255', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('495', '255', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('496', '256', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('497', '256', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('498', '257', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('499', '257', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('500', '258', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('501', '258', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('502', '259', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('503', '259', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('504', '260', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('505', '260', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('506', '261', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('507', '261', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('508', '262', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('509', '262', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('510', '263', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('511', '263', '2', 'NIU', '1.00', '100.00', '103', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('512', '263', '3', 'NIU', '1.00', '100.00', '105', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('513', '264', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('514', '265', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('515', '266', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('516', '267', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('517', '268', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('518', '269', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('519', '270', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('520', '271', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);
INSERT INTO `doc_invoice_item` VALUES ('521', '272', '1', 'NIU', '1.00', '100.00', '101', '100.00', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for doc_invoice_item_allowancecharge
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_allowancecharge`;
CREATE TABLE `doc_invoice_item_allowancecharge` (
  `n_id_invoice_item` int(11) NOT NULL,
  `c_charge_indicator` varchar(5) NOT NULL,
  `c_amount` varchar(15) NOT NULL,
  PRIMARY KEY (`n_id_invoice_item`),
  CONSTRAINT `fk_invoice_item_invoice_item_allowancecharge` FOREIGN KEY (`n_id_invoice_item`) REFERENCES `doc_invoice_item` (`n_id_invoice_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_allowancecharge
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_item_billing_payment
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_billing_payment`;
CREATE TABLE `doc_invoice_item_billing_payment` (
  `n_id_invoice_item_billing_payment` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice_item` int(11) NOT NULL,
  `c_paid_amount` varchar(15) CHARACTER SET latin1 NOT NULL,
  `c_instruction_id` varchar(2) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`n_id_invoice_item_billing_payment`),
  KEY `fk_invoice_item_invoice_item_billing_payment` (`n_id_invoice_item`),
  CONSTRAINT `fk_invoice_item_invoice_item_billing_payment` FOREIGN KEY (`n_id_invoice_item`) REFERENCES `doc_invoice_item` (`n_id_invoice_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_billing_payment
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_item_description
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_description`;
CREATE TABLE `doc_invoice_item_description` (
  `n_id_invoice_item_description` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice_item` int(11) NOT NULL,
  `n_index` int(11) NOT NULL,
  `c_description` varchar(250) NOT NULL,
  PRIMARY KEY (`n_id_invoice_item_description`),
  KEY `fk_invoice_item_description` (`n_id_invoice_item`),
  CONSTRAINT `fk_invoice_item_description` FOREIGN KEY (`n_id_invoice_item`) REFERENCES `doc_invoice_item` (`n_id_invoice_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_description
-- ----------------------------
INSERT INTO `doc_invoice_item_description` VALUES ('90', '171', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('91', '172', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('92', '173', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('93', '174', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('94', '175', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('95', '176', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('96', '177', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('97', '178', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('98', '179', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('99', '180', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('100', '181', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('101', '182', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('102', '183', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('103', '184', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('104', '185', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('105', '186', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('106', '187', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('107', '188', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('108', '189', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('109', '190', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('110', '191', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('111', '192', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('112', '193', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('113', '194', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('114', '195', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('115', '196', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('116', '197', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('117', '198', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('118', '199', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('119', '200', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('120', '201', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('121', '202', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('122', '203', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('123', '204', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('124', '205', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('125', '206', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('126', '207', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('127', '208', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('128', '209', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('129', '210', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('130', '211', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('131', '212', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('132', '213', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('133', '214', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('134', '215', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('135', '216', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('136', '217', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('137', '218', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('138', '219', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('139', '220', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('140', '221', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('141', '222', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('142', '223', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('143', '224', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('144', '225', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('145', '226', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('146', '227', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('147', '228', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('148', '229', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('149', '230', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('150', '231', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('151', '232', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('152', '233', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('153', '234', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('154', '235', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('155', '236', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('156', '237', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('157', '238', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('158', '239', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('159', '240', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('160', '241', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('161', '242', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('162', '243', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('163', '244', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('164', '245', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('165', '246', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('166', '247', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('167', '248', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('168', '249', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('169', '250', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('170', '251', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('171', '252', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('172', '253', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('173', '254', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('174', '255', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('175', '256', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('176', '257', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('177', '258', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('178', '259', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('179', '260', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('180', '261', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('181', '262', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('182', '263', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('183', '264', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('184', '265', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('185', '266', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('186', '267', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('187', '268', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('188', '269', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('189', '270', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('190', '271', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('191', '272', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('192', '273', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('193', '274', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('194', '275', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('195', '276', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('196', '277', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('197', '278', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('198', '279', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('199', '280', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('200', '281', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('201', '282', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('202', '283', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('203', '284', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('204', '285', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('205', '286', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('206', '287', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('207', '288', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('208', '289', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('209', '290', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('210', '291', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('211', '292', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('212', '293', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('213', '294', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('214', '295', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('215', '296', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('216', '297', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('217', '298', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('218', '299', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('219', '300', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('220', '301', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('221', '302', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('222', '303', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('223', '304', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('224', '305', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('225', '306', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('226', '307', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('227', '308', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('228', '309', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('229', '310', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('230', '311', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('231', '312', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('232', '313', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('233', '314', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('234', '315', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('235', '316', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('236', '317', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('237', '318', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('238', '319', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('239', '320', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('240', '321', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('241', '322', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('242', '323', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('243', '324', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('244', '325', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('245', '326', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('246', '327', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('247', '328', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('248', '329', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('249', '330', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('250', '331', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('251', '332', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('252', '333', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('253', '334', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('254', '335', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('255', '336', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('256', '337', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('257', '338', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('258', '339', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('259', '340', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('260', '341', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('261', '342', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('262', '343', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('263', '344', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('264', '345', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('265', '346', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('266', '347', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('267', '348', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('268', '349', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('269', '350', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('270', '351', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('271', '352', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('272', '353', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('273', '354', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('274', '355', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('275', '356', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('276', '357', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('277', '358', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('278', '359', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('279', '360', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('280', '361', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('281', '362', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('282', '363', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('283', '364', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('284', '365', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('285', '366', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('286', '367', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('287', '368', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('288', '369', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('289', '370', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('290', '371', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('291', '372', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('292', '373', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('293', '374', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('294', '375', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('295', '376', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('296', '377', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('297', '378', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('298', '379', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('299', '380', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('300', '381', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('301', '382', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('302', '383', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('303', '384', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('304', '385', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('305', '386', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('306', '387', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('307', '388', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('308', '389', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('309', '390', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('310', '391', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('311', '392', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('312', '393', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('313', '394', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('314', '395', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('315', '396', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('316', '397', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('317', '398', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('318', '399', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('319', '400', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('320', '401', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('321', '402', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('322', '403', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('323', '404', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('324', '405', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('325', '406', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('326', '407', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('327', '408', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('328', '409', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('329', '410', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('330', '411', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('331', '412', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('332', '413', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('333', '414', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('334', '415', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('335', '416', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('336', '417', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('337', '418', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('338', '419', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('339', '420', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('340', '421', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('341', '422', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('342', '423', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('343', '424', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('344', '425', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('345', '426', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('346', '427', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('347', '428', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('348', '429', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('349', '430', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('350', '431', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('351', '432', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('352', '433', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('353', '434', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('354', '435', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('355', '436', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('356', '437', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('357', '438', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('358', '439', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('359', '440', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('360', '441', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('361', '442', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('362', '443', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('363', '444', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('364', '445', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('365', '446', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('366', '447', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('367', '448', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('368', '449', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('369', '450', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('370', '451', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('371', '452', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('372', '453', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('373', '454', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('374', '455', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('375', '456', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('376', '457', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('377', '458', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('378', '459', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('379', '460', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('380', '461', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('381', '462', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('382', '463', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('383', '464', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('384', '465', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('385', '466', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('386', '467', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('387', '468', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('388', '469', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('389', '470', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('390', '471', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('391', '472', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('392', '473', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('393', '474', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('394', '475', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('395', '476', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('396', '477', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('397', '478', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('398', '479', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('399', '480', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('400', '481', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('401', '482', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('402', '483', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('403', '484', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('404', '485', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('405', '486', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('406', '487', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('407', '488', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('408', '489', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('409', '490', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('410', '491', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('411', '492', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('412', '493', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('413', '494', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('414', '495', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('415', '496', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('416', '497', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('417', '498', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('418', '499', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('419', '500', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('420', '501', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('421', '502', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('422', '503', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('423', '504', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('424', '505', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('425', '506', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('426', '507', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('427', '508', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('428', '509', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('429', '510', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('430', '511', '0', 'LICENCIA DE USO DEL MÓDULO DE INVENTARIOS');
INSERT INTO `doc_invoice_item_description` VALUES ('431', '512', '0', 'LICENCIA DE USO DEL MÓDULO DE FACTURACIÓN');
INSERT INTO `doc_invoice_item_description` VALUES ('432', '513', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('433', '514', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('434', '515', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('435', '516', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('436', '517', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('437', '518', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('438', '519', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('439', '520', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');
INSERT INTO `doc_invoice_item_description` VALUES ('440', '521', '0', 'LICENCIA DE USO DEL MÓDULO DE COMPRAS');

-- ----------------------------
-- Table structure for doc_invoice_item_pricing_reference_alternative_condition_price
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_pricing_reference_alternative_condition_price`;
CREATE TABLE `doc_invoice_item_pricing_reference_alternative_condition_price` (
  `n_id_invoice_item_pricing_reference_alternative_condition_price` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice_item` int(11) NOT NULL,
  `c_price_amount` varchar(15) DEFAULT NULL COMMENT 'Monto de Precio de Venta',
  `c_price_type_code` varchar(2) DEFAULT NULL COMMENT 'Codigo de tipo de precio - Catalogo No 16',
  PRIMARY KEY (`n_id_invoice_item_pricing_reference_alternative_condition_price`),
  KEY `fk_invoice_item_invoice_item_pricing_reference` (`n_id_invoice_item`),
  CONSTRAINT `fk_invoice_item_invoice_item_pricing_reference` FOREIGN KEY (`n_id_invoice_item`) REFERENCES `doc_invoice_item` (`n_id_invoice_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=440 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_pricing_reference_alternative_condition_price
-- ----------------------------
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('90', '172', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('91', '173', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('92', '174', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('93', '175', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('94', '176', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('95', '177', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('96', '178', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('97', '179', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('98', '180', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('99', '181', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('100', '182', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('101', '183', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('102', '184', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('103', '185', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('104', '186', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('105', '187', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('106', '188', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('107', '189', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('108', '190', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('109', '191', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('110', '192', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('111', '193', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('112', '194', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('113', '195', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('114', '196', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('115', '197', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('116', '198', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('117', '199', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('118', '200', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('119', '201', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('120', '202', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('121', '203', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('122', '204', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('123', '205', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('124', '206', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('125', '207', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('126', '208', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('127', '209', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('128', '210', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('129', '211', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('130', '212', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('131', '213', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('132', '214', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('133', '215', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('134', '216', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('135', '217', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('136', '218', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('137', '219', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('138', '220', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('139', '221', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('140', '222', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('141', '223', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('142', '224', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('143', '225', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('144', '226', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('145', '227', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('146', '228', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('147', '229', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('148', '230', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('149', '231', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('150', '232', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('151', '233', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('152', '234', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('153', '235', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('154', '236', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('155', '237', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('156', '238', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('157', '239', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('158', '240', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('159', '241', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('160', '242', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('161', '243', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('162', '244', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('163', '245', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('164', '246', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('165', '247', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('166', '248', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('167', '249', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('168', '250', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('169', '251', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('170', '252', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('171', '253', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('172', '254', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('173', '255', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('174', '256', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('175', '257', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('176', '258', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('177', '259', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('178', '260', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('179', '261', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('180', '262', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('181', '263', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('182', '264', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('183', '265', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('184', '266', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('185', '267', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('186', '268', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('187', '269', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('188', '270', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('189', '271', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('190', '272', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('191', '273', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('192', '274', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('193', '275', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('194', '276', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('195', '277', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('196', '278', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('197', '279', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('198', '280', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('199', '281', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('200', '282', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('201', '283', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('202', '284', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('203', '285', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('204', '286', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('205', '287', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('206', '288', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('207', '289', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('208', '290', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('209', '291', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('210', '292', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('211', '293', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('212', '294', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('213', '295', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('214', '296', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('215', '297', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('216', '298', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('217', '299', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('218', '300', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('219', '301', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('220', '302', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('221', '303', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('222', '304', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('223', '305', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('224', '306', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('225', '307', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('226', '308', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('227', '309', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('228', '310', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('229', '311', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('230', '312', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('231', '313', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('232', '314', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('233', '315', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('234', '316', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('235', '317', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('236', '318', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('237', '319', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('238', '320', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('239', '321', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('240', '322', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('241', '323', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('242', '324', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('243', '325', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('244', '326', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('245', '327', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('246', '328', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('247', '329', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('248', '330', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('249', '331', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('250', '332', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('251', '333', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('252', '334', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('253', '335', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('254', '336', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('255', '337', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('256', '338', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('257', '339', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('258', '340', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('259', '341', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('260', '342', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('261', '343', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('262', '344', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('263', '345', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('264', '346', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('265', '347', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('266', '348', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('267', '349', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('268', '350', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('269', '351', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('270', '352', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('271', '353', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('272', '354', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('273', '355', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('274', '356', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('275', '357', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('276', '358', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('277', '359', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('278', '360', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('279', '361', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('280', '362', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('281', '363', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('282', '364', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('283', '365', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('284', '366', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('285', '367', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('286', '368', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('287', '369', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('288', '370', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('289', '371', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('290', '372', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('291', '373', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('292', '374', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('293', '375', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('294', '376', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('295', '377', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('296', '378', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('297', '379', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('298', '380', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('299', '381', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('300', '382', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('301', '383', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('302', '384', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('303', '385', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('304', '386', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('305', '387', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('306', '388', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('307', '389', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('308', '390', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('309', '391', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('310', '392', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('311', '393', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('312', '394', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('313', '395', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('314', '396', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('315', '397', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('316', '398', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('317', '399', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('318', '400', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('319', '401', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('320', '402', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('321', '403', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('322', '404', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('323', '405', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('324', '406', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('325', '407', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('326', '408', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('327', '409', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('328', '410', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('329', '411', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('330', '412', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('331', '413', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('332', '414', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('333', '415', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('334', '416', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('335', '417', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('336', '418', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('337', '419', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('338', '420', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('339', '421', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('340', '422', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('341', '423', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('342', '424', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('343', '425', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('344', '426', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('345', '427', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('346', '428', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('347', '429', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('348', '430', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('349', '431', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('350', '432', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('351', '433', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('352', '434', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('353', '435', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('354', '436', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('355', '437', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('356', '438', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('357', '439', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('358', '440', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('359', '441', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('360', '442', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('361', '443', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('362', '444', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('363', '445', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('364', '446', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('365', '447', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('366', '448', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('367', '449', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('368', '450', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('369', '451', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('370', '452', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('371', '453', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('372', '454', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('373', '455', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('374', '456', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('375', '457', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('376', '458', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('377', '459', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('378', '460', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('379', '461', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('380', '462', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('381', '463', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('382', '464', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('383', '465', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('384', '466', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('385', '467', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('386', '468', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('387', '469', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('388', '470', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('389', '471', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('390', '472', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('391', '473', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('392', '474', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('393', '475', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('394', '476', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('395', '477', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('396', '478', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('397', '479', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('398', '480', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('399', '481', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('400', '482', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('401', '483', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('402', '484', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('403', '485', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('404', '486', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('405', '487', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('406', '488', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('407', '489', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('408', '490', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('409', '491', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('410', '492', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('411', '493', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('412', '494', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('413', '495', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('414', '496', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('415', '497', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('416', '498', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('417', '499', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('418', '500', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('419', '501', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('420', '502', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('421', '503', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('422', '504', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('423', '505', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('424', '506', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('425', '507', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('426', '508', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('427', '509', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('428', '510', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('429', '511', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('430', '512', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('431', '513', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('432', '514', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('433', '515', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('434', '516', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('435', '517', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('436', '518', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('437', '519', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('438', '520', '118.00', '01');
INSERT INTO `doc_invoice_item_pricing_reference_alternative_condition_price` VALUES ('439', '521', '118.00', '01');

-- ----------------------------
-- Table structure for doc_invoice_item_tax_total
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_tax_total`;
CREATE TABLE `doc_invoice_item_tax_total` (
  `n_id_invoice_item_tax_total` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice_item` int(11) NOT NULL,
  `c_tax_amount` varchar(15) NOT NULL,
  PRIMARY KEY (`n_id_invoice_item_tax_total`),
  KEY `pk_invoice_item_invoice_item_tax_total` (`n_id_invoice_item`) USING BTREE,
  CONSTRAINT `fk_invoice_item_invoice_item_tax_total` FOREIGN KEY (`n_id_invoice_item`) REFERENCES `doc_invoice_item` (`n_id_invoice_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=679 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_tax_total
-- ----------------------------
INSERT INTO `doc_invoice_item_tax_total` VALUES ('330', '173', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('331', '174', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('332', '175', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('333', '176', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('334', '177', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('335', '178', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('336', '179', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('337', '180', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('338', '181', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('339', '182', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('340', '183', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('341', '184', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('342', '185', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('343', '186', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('344', '187', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('345', '188', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('346', '189', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('347', '190', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('348', '191', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('349', '192', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('350', '193', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('351', '194', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('352', '195', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('353', '196', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('354', '197', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('355', '198', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('356', '199', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('357', '200', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('358', '201', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('359', '202', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('360', '203', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('361', '204', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('362', '205', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('363', '206', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('364', '207', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('365', '208', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('366', '209', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('367', '210', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('368', '211', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('369', '212', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('370', '213', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('371', '214', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('372', '215', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('373', '216', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('374', '217', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('375', '218', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('376', '219', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('377', '220', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('378', '221', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('379', '222', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('380', '223', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('381', '224', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('382', '225', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('383', '226', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('384', '227', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('385', '228', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('386', '229', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('387', '230', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('388', '231', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('389', '232', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('390', '233', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('391', '234', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('392', '235', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('393', '236', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('394', '237', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('395', '238', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('396', '239', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('397', '240', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('398', '241', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('399', '242', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('400', '243', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('401', '244', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('402', '245', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('403', '246', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('404', '247', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('405', '248', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('406', '249', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('407', '250', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('408', '251', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('409', '252', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('410', '253', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('411', '254', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('412', '255', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('413', '256', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('414', '257', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('415', '258', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('416', '259', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('417', '260', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('418', '261', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('419', '262', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('420', '263', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('421', '264', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('422', '265', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('423', '266', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('424', '267', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('425', '268', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('426', '269', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('427', '270', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('428', '271', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('429', '272', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('430', '273', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('431', '274', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('432', '275', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('433', '276', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('434', '277', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('435', '278', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('436', '279', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('437', '280', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('438', '281', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('439', '282', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('440', '283', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('441', '284', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('442', '285', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('443', '286', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('444', '287', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('445', '288', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('446', '289', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('447', '290', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('448', '291', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('449', '292', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('450', '293', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('451', '294', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('452', '295', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('453', '296', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('454', '297', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('455', '298', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('456', '299', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('457', '300', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('458', '301', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('459', '302', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('460', '303', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('461', '304', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('462', '305', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('463', '306', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('464', '307', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('465', '308', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('466', '309', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('467', '310', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('468', '311', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('469', '312', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('470', '313', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('471', '314', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('472', '315', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('473', '316', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('474', '317', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('475', '318', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('476', '319', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('477', '320', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('478', '321', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('479', '322', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('480', '323', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('481', '324', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('482', '325', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('483', '326', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('484', '327', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('485', '328', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('486', '329', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('487', '330', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('488', '331', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('489', '332', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('490', '333', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('491', '334', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('492', '335', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('493', '336', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('494', '337', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('495', '338', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('496', '339', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('497', '340', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('498', '341', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('499', '342', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('500', '343', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('501', '344', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('502', '345', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('503', '346', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('504', '347', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('505', '348', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('506', '349', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('507', '350', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('508', '351', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('509', '352', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('510', '353', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('511', '354', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('512', '355', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('513', '356', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('514', '357', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('515', '358', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('516', '359', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('517', '360', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('518', '361', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('519', '362', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('520', '363', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('521', '364', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('522', '365', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('523', '366', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('524', '367', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('525', '368', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('526', '369', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('527', '370', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('528', '371', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('529', '372', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('530', '373', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('531', '374', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('532', '375', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('533', '376', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('534', '377', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('535', '378', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('536', '379', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('537', '380', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('538', '381', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('539', '382', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('540', '383', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('541', '384', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('542', '385', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('543', '386', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('544', '387', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('545', '388', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('546', '389', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('547', '390', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('548', '391', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('549', '392', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('550', '393', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('551', '394', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('552', '395', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('553', '396', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('554', '397', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('555', '398', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('556', '399', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('557', '400', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('558', '401', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('559', '402', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('560', '403', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('561', '404', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('562', '405', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('563', '406', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('564', '407', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('565', '408', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('566', '409', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('567', '410', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('568', '411', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('569', '412', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('570', '413', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('571', '414', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('572', '415', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('573', '416', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('574', '417', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('575', '418', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('576', '419', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('577', '420', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('578', '421', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('579', '422', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('580', '423', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('581', '424', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('582', '425', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('583', '426', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('584', '427', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('585', '428', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('586', '429', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('587', '430', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('588', '431', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('589', '432', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('590', '433', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('591', '434', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('592', '435', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('593', '436', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('594', '437', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('595', '438', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('596', '439', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('597', '440', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('598', '441', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('599', '442', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('600', '443', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('601', '444', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('602', '445', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('603', '446', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('604', '447', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('605', '448', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('606', '449', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('607', '450', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('608', '451', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('609', '452', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('610', '453', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('611', '454', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('612', '455', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('613', '456', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('614', '457', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('615', '458', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('616', '459', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('617', '460', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('618', '461', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('619', '462', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('620', '463', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('621', '464', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('622', '465', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('623', '466', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('624', '467', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('625', '468', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('626', '469', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('627', '470', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('628', '471', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('629', '472', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('630', '473', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('631', '474', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('632', '475', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('633', '476', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('634', '477', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('635', '478', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('636', '479', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('637', '480', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('638', '481', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('639', '482', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('640', '483', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('641', '484', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('642', '485', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('643', '486', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('644', '487', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('645', '488', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('646', '489', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('647', '490', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('648', '491', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('649', '492', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('650', '493', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('651', '494', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('652', '495', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('653', '496', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('654', '497', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('655', '498', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('656', '499', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('657', '500', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('658', '501', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('659', '502', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('660', '503', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('661', '504', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('662', '505', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('663', '506', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('664', '507', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('665', '508', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('666', '509', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('667', '510', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('668', '511', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('669', '512', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('670', '513', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('671', '514', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('672', '515', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('673', '516', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('674', '517', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('675', '518', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('676', '519', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('677', '520', '18.00');
INSERT INTO `doc_invoice_item_tax_total` VALUES ('678', '521', '18.00');

-- ----------------------------
-- Table structure for doc_invoice_item_tax_total_igv
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_tax_total_igv`;
CREATE TABLE `doc_invoice_item_tax_total_igv` (
  `n_id_invoice_item_tax_total` int(11) NOT NULL,
  `c_tax_subtotal_tax_category_tax_exemption_reason_code` varchar(2) NOT NULL COMMENT 'Afectacion al IGV - Catalogo No 07',
  PRIMARY KEY (`n_id_invoice_item_tax_total`),
  KEY `pk_invoice_item_tax_total_igv` (`n_id_invoice_item_tax_total`) USING BTREE,
  CONSTRAINT `fk_invoice_item_tax_total_invoice_item_taxa_total_igv` FOREIGN KEY (`n_id_invoice_item_tax_total`) REFERENCES `doc_invoice_item_tax_total` (`n_id_invoice_item_tax_total`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_tax_total_igv
-- ----------------------------
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('333', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('334', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('335', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('336', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('337', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('338', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('339', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('340', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('341', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('342', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('343', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('344', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('345', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('346', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('347', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('348', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('349', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('350', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('351', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('352', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('353', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('354', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('355', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('356', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('357', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('358', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('359', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('360', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('361', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('362', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('363', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('364', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('365', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('366', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('367', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('368', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('369', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('370', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('371', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('372', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('373', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('374', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('375', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('376', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('377', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('378', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('379', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('380', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('381', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('382', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('383', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('384', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('385', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('386', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('387', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('388', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('389', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('390', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('391', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('392', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('393', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('394', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('395', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('396', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('397', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('398', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('399', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('400', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('401', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('402', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('403', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('404', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('405', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('406', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('407', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('408', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('409', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('410', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('411', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('412', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('413', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('414', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('415', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('416', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('417', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('418', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('419', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('420', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('421', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('422', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('423', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('424', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('425', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('426', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('427', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('428', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('429', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('430', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('431', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('432', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('433', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('434', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('435', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('436', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('437', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('438', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('439', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('440', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('441', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('442', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('443', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('444', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('445', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('446', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('447', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('448', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('449', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('450', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('451', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('452', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('453', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('454', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('455', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('456', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('457', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('458', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('459', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('460', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('461', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('462', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('463', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('464', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('465', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('466', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('467', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('468', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('469', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('470', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('471', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('472', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('473', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('474', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('475', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('476', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('477', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('478', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('479', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('480', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('481', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('482', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('483', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('484', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('485', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('486', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('487', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('488', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('489', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('490', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('491', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('492', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('493', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('494', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('495', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('496', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('497', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('498', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('499', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('500', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('501', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('502', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('503', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('504', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('505', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('506', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('507', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('508', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('509', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('510', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('511', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('512', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('513', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('514', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('515', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('516', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('517', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('518', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('519', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('520', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('521', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('522', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('523', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('524', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('525', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('526', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('527', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('528', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('529', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('530', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('531', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('532', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('533', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('534', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('535', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('536', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('537', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('538', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('539', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('540', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('541', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('542', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('543', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('544', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('545', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('546', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('547', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('548', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('549', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('550', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('551', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('552', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('553', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('554', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('555', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('556', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('557', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('558', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('559', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('560', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('561', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('562', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('563', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('564', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('565', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('566', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('567', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('568', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('569', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('570', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('571', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('572', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('573', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('574', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('575', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('576', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('577', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('578', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('579', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('580', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('581', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('582', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('583', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('584', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('585', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('586', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('587', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('588', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('589', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('590', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('591', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('592', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('593', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('594', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('595', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('596', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('597', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('598', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('599', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('600', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('601', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('602', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('603', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('604', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('605', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('606', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('607', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('608', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('609', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('610', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('611', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('612', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('613', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('614', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('615', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('616', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('617', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('618', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('619', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('620', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('621', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('622', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('623', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('624', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('625', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('626', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('627', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('628', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('629', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('630', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('631', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('632', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('633', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('634', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('635', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('636', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('637', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('638', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('639', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('640', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('641', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('642', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('643', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('644', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('645', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('646', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('647', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('648', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('649', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('650', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('651', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('652', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('653', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('654', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('655', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('656', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('657', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('658', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('659', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('660', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('661', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('662', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('663', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('664', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('665', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('666', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('667', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('668', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('669', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('670', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('671', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('672', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('673', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('674', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('675', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('676', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('677', '10');
INSERT INTO `doc_invoice_item_tax_total_igv` VALUES ('678', '10');

-- ----------------------------
-- Table structure for doc_invoice_item_tax_total_isc
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_tax_total_isc`;
CREATE TABLE `doc_invoice_item_tax_total_isc` (
  `n_id_invoice_item_tax_total` int(11) NOT NULL,
  `c_tax_subtotal_tax_category_tier_range` varchar(2) NOT NULL COMMENT 'Tipo de Sistema ISC - Catalogo No 08',
  PRIMARY KEY (`n_id_invoice_item_tax_total`),
  KEY `pk_invoice_item_tax_total_isc` (`n_id_invoice_item_tax_total`) USING BTREE,
  CONSTRAINT `fk_invoice_item_tax_total_invoice_item_tax_total_isc` FOREIGN KEY (`n_id_invoice_item_tax_total`) REFERENCES `doc_invoice_item_tax_total` (`n_id_invoice_item_tax_total`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_tax_total_isc
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_item_tax_total_tax_subtotal
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_tax_total_tax_subtotal`;
CREATE TABLE `doc_invoice_item_tax_total_tax_subtotal` (
  `n_id_invoice_item_tax_total` int(11) NOT NULL,
  `c_tax_amount` varchar(15) NOT NULL,
  PRIMARY KEY (`n_id_invoice_item_tax_total`),
  CONSTRAINT `fk_invoice_item_tax_total_invoice_item_tax_total_tax_subtotal` FOREIGN KEY (`n_id_invoice_item_tax_total`) REFERENCES `doc_invoice_item_tax_total` (`n_id_invoice_item_tax_total`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_tax_total_tax_subtotal
-- ----------------------------
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('331', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('332', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('333', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('334', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('335', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('336', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('337', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('338', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('339', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('340', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('341', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('342', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('343', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('344', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('345', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('346', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('347', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('348', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('349', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('350', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('351', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('352', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('353', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('354', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('355', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('356', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('357', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('358', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('359', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('360', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('361', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('362', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('363', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('364', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('365', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('366', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('367', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('368', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('369', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('370', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('371', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('372', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('373', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('374', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('375', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('376', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('377', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('378', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('379', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('380', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('381', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('382', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('383', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('384', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('385', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('386', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('387', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('388', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('389', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('390', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('391', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('392', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('393', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('394', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('395', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('396', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('397', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('398', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('399', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('400', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('401', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('402', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('403', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('404', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('405', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('406', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('407', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('408', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('409', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('410', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('411', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('412', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('413', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('414', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('415', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('416', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('417', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('418', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('419', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('420', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('421', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('422', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('423', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('424', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('425', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('426', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('427', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('428', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('429', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('430', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('431', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('432', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('433', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('434', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('435', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('436', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('437', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('438', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('439', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('440', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('441', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('442', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('443', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('444', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('445', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('446', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('447', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('448', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('449', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('450', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('451', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('452', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('453', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('454', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('455', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('456', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('457', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('458', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('459', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('460', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('461', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('462', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('463', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('464', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('465', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('466', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('467', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('468', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('469', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('470', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('471', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('472', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('473', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('474', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('475', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('476', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('477', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('478', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('479', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('480', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('481', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('482', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('483', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('484', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('485', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('486', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('487', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('488', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('489', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('490', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('491', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('492', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('493', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('494', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('495', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('496', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('497', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('498', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('499', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('500', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('501', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('502', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('503', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('504', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('505', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('506', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('507', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('508', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('509', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('510', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('511', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('512', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('513', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('514', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('515', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('516', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('517', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('518', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('519', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('520', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('521', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('522', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('523', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('524', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('525', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('526', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('527', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('528', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('529', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('530', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('531', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('532', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('533', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('534', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('535', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('536', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('537', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('538', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('539', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('540', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('541', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('542', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('543', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('544', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('545', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('546', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('547', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('548', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('549', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('550', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('551', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('552', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('553', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('554', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('555', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('556', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('557', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('558', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('559', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('560', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('561', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('562', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('563', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('564', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('565', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('566', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('567', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('568', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('569', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('570', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('571', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('572', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('573', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('574', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('575', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('576', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('577', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('578', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('579', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('580', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('581', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('582', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('583', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('584', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('585', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('586', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('587', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('588', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('589', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('590', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('591', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('592', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('593', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('594', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('595', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('596', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('597', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('598', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('599', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('600', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('601', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('602', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('603', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('604', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('605', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('606', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('607', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('608', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('609', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('610', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('611', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('612', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('613', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('614', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('615', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('616', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('617', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('618', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('619', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('620', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('621', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('622', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('623', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('624', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('625', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('626', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('627', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('628', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('629', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('630', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('631', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('632', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('633', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('634', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('635', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('636', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('637', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('638', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('639', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('640', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('641', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('642', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('643', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('644', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('645', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('646', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('647', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('648', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('649', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('650', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('651', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('652', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('653', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('654', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('655', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('656', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('657', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('658', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('659', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('660', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('661', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('662', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('663', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('664', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('665', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('666', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('667', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('668', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('669', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('670', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('671', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('672', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('673', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('674', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('675', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('676', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('677', '18.00');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal` VALUES ('678', '18.00');

-- ----------------------------
-- Table structure for doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme`;
CREATE TABLE `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` (
  `n_id_invoice_item_tax_total` int(11) NOT NULL,
  `c_id` varchar(4) NOT NULL,
  `c_name` varchar(6) NOT NULL,
  `c_tax_type_code` varchar(3) NOT NULL,
  PRIMARY KEY (`n_id_invoice_item_tax_total`),
  CONSTRAINT `fk_invoice_item_tax_total_tax_category_tax_scheme` FOREIGN KEY (`n_id_invoice_item_tax_total`) REFERENCES `doc_invoice_item_tax_total` (`n_id_invoice_item_tax_total`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme
-- ----------------------------
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('332', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('333', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('334', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('335', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('336', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('337', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('338', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('339', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('340', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('341', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('342', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('343', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('344', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('345', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('346', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('347', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('348', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('349', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('350', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('351', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('352', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('353', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('354', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('355', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('356', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('357', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('358', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('359', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('360', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('361', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('362', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('363', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('364', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('365', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('366', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('367', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('368', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('369', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('370', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('371', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('372', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('373', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('374', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('375', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('376', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('377', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('378', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('379', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('380', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('381', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('382', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('383', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('384', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('385', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('386', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('387', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('388', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('389', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('390', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('391', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('392', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('393', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('394', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('395', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('396', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('397', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('398', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('399', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('400', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('401', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('402', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('403', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('404', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('405', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('406', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('407', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('408', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('409', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('410', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('411', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('412', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('413', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('414', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('415', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('416', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('417', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('418', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('419', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('420', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('421', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('422', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('423', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('424', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('425', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('426', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('427', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('428', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('429', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('430', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('431', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('432', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('433', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('434', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('435', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('436', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('437', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('438', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('439', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('440', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('441', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('442', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('443', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('444', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('445', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('446', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('447', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('448', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('449', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('450', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('451', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('452', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('453', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('454', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('455', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('456', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('457', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('458', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('459', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('460', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('461', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('462', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('463', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('464', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('465', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('466', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('467', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('468', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('469', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('470', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('471', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('472', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('473', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('474', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('475', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('476', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('477', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('478', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('479', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('480', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('481', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('482', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('483', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('484', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('485', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('486', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('487', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('488', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('489', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('490', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('491', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('492', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('493', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('494', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('495', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('496', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('497', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('498', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('499', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('500', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('501', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('502', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('503', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('504', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('505', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('506', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('507', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('508', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('509', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('510', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('511', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('512', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('513', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('514', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('515', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('516', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('517', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('518', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('519', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('520', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('521', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('522', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('523', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('524', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('525', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('526', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('527', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('528', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('529', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('530', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('531', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('532', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('533', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('534', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('535', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('536', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('537', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('538', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('539', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('540', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('541', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('542', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('543', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('544', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('545', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('546', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('547', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('548', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('549', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('550', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('551', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('552', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('553', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('554', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('555', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('556', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('557', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('558', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('559', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('560', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('561', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('562', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('563', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('564', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('565', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('566', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('567', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('568', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('569', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('570', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('571', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('572', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('573', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('574', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('575', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('576', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('577', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('578', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('579', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('580', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('581', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('582', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('583', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('584', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('585', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('586', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('587', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('588', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('589', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('590', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('591', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('592', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('593', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('594', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('595', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('596', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('597', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('598', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('599', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('600', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('601', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('602', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('603', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('604', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('605', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('606', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('607', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('608', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('609', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('610', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('611', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('612', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('613', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('614', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('615', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('616', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('617', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('618', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('619', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('620', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('621', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('622', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('623', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('624', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('625', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('626', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('627', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('628', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('629', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('630', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('631', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('632', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('633', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('634', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('635', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('636', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('637', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('638', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('639', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('640', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('641', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('642', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('643', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('644', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('645', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('646', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('647', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('648', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('649', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('650', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('651', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('652', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('653', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('654', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('655', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('656', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('657', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('658', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('659', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('660', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('661', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('662', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('663', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('664', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('665', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('666', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('667', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('668', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('669', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('670', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('671', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('672', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('673', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('674', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('675', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('676', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('677', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_item_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('678', '1000', 'IGV', 'VAT');

-- ----------------------------
-- Table structure for doc_invoice_legal_monetary_total
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_legal_monetary_total`;
CREATE TABLE `doc_invoice_legal_monetary_total` (
  `n_id_invoice` int(11) NOT NULL,
  `c_payable_amount` varchar(15) DEFAULT NULL COMMENT 'Importe total de la venta, sesion en uso o del servicio prestado',
  `c_charge_total_amount` varchar(15) DEFAULT NULL COMMENT 'Sumatoria otros Cargos',
  `c_allowance_total_amount` varchar(15) DEFAULT NULL COMMENT 'Descuentos Globales',
  `c_prepaid_amount` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_legal_monetary_total` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_legal_monetary_total
-- ----------------------------
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('133', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('134', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('135', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('136', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('137', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('138', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('139', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('140', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('141', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('142', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('143', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('144', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('145', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('146', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('147', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('148', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('149', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('150', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('151', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('152', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('153', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('154', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('155', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('156', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('157', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('158', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('159', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('160', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('161', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('162', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('163', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('164', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('165', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('166', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('167', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('168', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('169', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('170', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('171', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('172', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('173', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('174', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('175', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('176', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('177', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('178', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('179', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('180', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('181', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('182', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('183', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('184', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('185', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('186', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('187', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('188', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('189', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('190', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('191', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('192', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('193', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('194', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('195', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('196', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('197', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('198', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('199', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('200', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('201', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('202', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('203', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('204', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('205', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('206', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('207', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('208', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('209', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('210', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('211', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('212', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('213', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('214', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('215', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('216', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('217', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('218', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('219', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('220', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('221', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('222', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('223', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('224', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('225', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('226', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('227', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('228', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('229', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('230', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('231', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('232', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('233', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('234', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('235', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('236', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('237', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('238', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('239', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('240', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('241', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('242', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('243', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('244', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('245', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('246', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('247', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('248', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('249', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('250', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('251', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('252', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('253', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('254', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('255', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('256', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('257', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('258', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('259', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('260', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('261', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('262', '236.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('263', '354.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('264', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('265', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('266', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('267', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('268', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('269', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('270', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('271', '118.00', null, null, null);
INSERT INTO `doc_invoice_legal_monetary_total` VALUES ('272', '118.00', null, null, null);

-- ----------------------------
-- Table structure for doc_invoice_pdf_data
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_pdf_data`;
CREATE TABLE `doc_invoice_pdf_data` (
  `n_id_invoice` int(11) NOT NULL,
  `c_purchase_order` varchar(50) DEFAULT NULL,
  `n_terms_of_payment` int(11) DEFAULT NULL,
  `d_expiration_date` date DEFAULT NULL,
  `c_observation` varchar(255) DEFAULT NULL,
  `c_customer_address` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_pdf_data` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_pdf_data
-- ----------------------------
INSERT INTO `doc_invoice_pdf_data` VALUES ('147', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('148', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('149', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('150', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('151', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('152', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('153', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('154', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('155', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('156', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('157', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('158', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('159', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('160', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('161', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('162', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('163', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('164', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('165', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('166', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('167', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('168', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('169', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('170', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('171', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('172', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('173', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('174', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('175', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('176', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('177', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('178', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('179', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('180', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('181', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('182', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('183', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('184', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('185', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('186', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('187', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('188', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('189', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('190', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('191', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('192', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('193', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('194', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('195', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('196', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('197', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('198', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('199', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('200', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('201', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('202', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('203', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('204', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('205', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('206', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('207', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('208', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('209', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('210', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('211', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('212', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('213', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('214', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('215', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('216', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('217', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('218', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('219', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('220', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('221', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('222', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('223', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('224', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('225', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('226', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('227', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('228', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('229', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('230', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('231', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('232', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('233', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('234', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('235', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('236', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('237', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('238', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('239', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('240', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('241', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('242', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('243', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('244', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('245', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('246', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('247', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('248', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('249', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('250', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('251', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('252', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('253', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('254', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('255', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('256', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('257', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('258', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('259', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('260', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('261', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('262', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('263', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('264', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('265', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('266', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('267', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('268', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('269', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('270', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('271', null, '30', '2016-10-31', null, null);
INSERT INTO `doc_invoice_pdf_data` VALUES ('272', null, '30', '2016-10-31', null, null);

-- ----------------------------
-- Table structure for doc_invoice_pdf_data_custom
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_pdf_data_custom`;
CREATE TABLE `doc_invoice_pdf_data_custom` (
  `n_id_invoice_pdf_data_custom` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `n_index` int(11) NOT NULL,
  `c_name` varchar(50) NOT NULL COMMENT 'Nombre de variable presentada en PDF',
  `c_value` varchar(100) NOT NULL,
  PRIMARY KEY (`n_id_invoice_pdf_data_custom`),
  KEY `n_id_invoice` (`n_id_invoice`),
  CONSTRAINT `fk_invoice_pdf_data_invoice_pdf_data_custom` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice_pdf_data` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_pdf_data_custom
-- ----------------------------
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('66', '149', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('67', '149', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('68', '150', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('69', '150', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('70', '151', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('71', '151', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('72', '152', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('73', '152', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('74', '153', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('75', '153', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('76', '154', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('77', '154', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('78', '155', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('79', '155', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('80', '156', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('81', '156', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('82', '157', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('83', '157', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('84', '158', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('85', '158', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('86', '159', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('87', '159', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('88', '160', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('89', '160', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('90', '161', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('91', '161', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('92', '162', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('93', '162', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('94', '163', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('95', '163', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('96', '164', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('97', '164', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('98', '165', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('99', '165', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('100', '166', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('101', '166', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('102', '167', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('103', '167', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('104', '168', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('105', '168', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('106', '169', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('107', '169', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('108', '170', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('109', '170', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('110', '171', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('111', '171', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('112', '172', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('113', '172', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('114', '173', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('115', '173', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('116', '174', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('117', '174', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('118', '175', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('119', '175', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('120', '176', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('121', '176', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('122', '177', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('123', '177', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('124', '178', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('125', '178', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('126', '179', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('127', '179', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('128', '180', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('129', '180', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('130', '181', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('131', '181', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('132', '182', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('133', '182', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('134', '183', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('135', '183', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('136', '184', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('137', '184', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('138', '185', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('139', '185', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('140', '186', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('141', '186', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('142', '187', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('143', '187', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('144', '188', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('145', '188', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('146', '189', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('147', '189', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('148', '190', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('149', '190', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('150', '191', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('151', '191', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('152', '192', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('153', '192', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('154', '193', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('155', '193', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('156', '194', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('157', '194', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('158', '195', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('159', '195', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('160', '196', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('161', '196', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('162', '197', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('163', '197', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('164', '198', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('165', '198', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('166', '199', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('167', '199', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('168', '200', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('169', '200', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('170', '201', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('171', '201', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('172', '202', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('173', '202', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('174', '203', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('175', '203', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('176', '204', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('177', '204', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('178', '205', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('179', '205', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('180', '206', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('181', '206', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('182', '207', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('183', '207', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('184', '208', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('185', '208', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('186', '209', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('187', '209', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('188', '210', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('189', '210', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('190', '211', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('191', '211', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('192', '212', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('193', '212', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('194', '213', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('195', '213', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('196', '214', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('197', '214', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('198', '215', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('199', '215', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('200', '216', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('201', '216', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('202', '217', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('203', '217', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('204', '218', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('205', '218', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('206', '219', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('207', '219', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('208', '220', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('209', '220', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('210', '221', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('211', '221', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('212', '222', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('213', '222', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('214', '223', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('215', '223', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('216', '224', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('217', '224', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('218', '225', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('219', '225', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('220', '226', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('221', '226', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('222', '227', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('223', '227', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('224', '228', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('225', '228', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('226', '229', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('227', '229', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('228', '230', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('229', '230', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('230', '231', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('231', '231', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('232', '232', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('233', '232', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('234', '233', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('235', '233', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('236', '234', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('237', '234', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('238', '235', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('239', '235', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('240', '236', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('241', '236', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('242', '237', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('243', '237', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('244', '238', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('245', '238', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('246', '239', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('247', '239', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('248', '240', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('249', '240', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('250', '241', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('251', '241', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('252', '242', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('253', '242', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('254', '243', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('255', '243', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('256', '244', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('257', '244', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('258', '245', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('259', '245', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('260', '246', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('261', '246', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('262', '247', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('263', '247', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('264', '248', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('265', '248', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('266', '249', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('267', '249', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('268', '250', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('269', '250', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('270', '251', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('271', '251', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('272', '252', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('273', '252', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('274', '253', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('275', '253', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('276', '254', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('277', '254', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('278', '255', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('279', '255', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('280', '256', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('281', '256', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('282', '257', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('283', '257', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('284', '258', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('285', '258', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('286', '259', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('287', '259', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('288', '260', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('289', '260', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('290', '261', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('291', '261', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('292', '262', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('293', '262', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('294', '263', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('295', '263', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('296', '264', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('297', '264', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('298', '265', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('299', '265', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('300', '266', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('301', '266', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('302', '267', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('303', '267', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('304', '268', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('305', '268', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('306', '269', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('307', '269', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('308', '270', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('309', '270', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('310', '271', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('311', '271', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('312', '272', '0', 'usuario', 'JCHAVEZ');
INSERT INTO `doc_invoice_pdf_data_custom` VALUES ('313', '272', '1', 'correo_contacto', 'www.avanzasoluciones.com.pe');

-- ----------------------------
-- Table structure for doc_invoice_signature
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_signature`;
CREATE TABLE `doc_invoice_signature` (
  `n_id_invoice` int(11) NOT NULL,
  `c_digest_value` varchar(40) NOT NULL,
  `c_signature_value` varchar(400) NOT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_signature_value` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_signature
-- ----------------------------
INSERT INTO `doc_invoice_signature` VALUES ('237', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('238', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('239', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('240', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('241', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('242', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('243', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('244', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('245', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('246', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('247', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('248', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('249', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('250', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('251', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('252', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('253', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('254', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('255', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('256', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('257', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('258', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('259', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('260', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('261', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('262', 'qXh+MWTmc+OdvBllKmSd1ZgQoZY=', 'oG7mPoBbUkHBqGg9z9JZSDSJOQc2gk0CKrbNlbotDSMYt4H6ggcDKSZJKl++6vnFMhaysuxIDUuqnMm67tsgMSrkixmflv76yzmszZWbc0CDbzZ5E1lSCztVgLIj5wQvPgntNEOUNhxL4FLB6lBOAozmnVuNIseWyRn5rcxOuFl+BvZwtfURJdW+dlFAdceb+sA28I7BE1XkSD6diZ6sJlt5Sa23w3vTkGns0HSdx+f1hetWP6KmJG+rM5+gpEA/wL7gzQhdeglxOTa7M7KrnVXeUjyIisWSycz222RkYuCcVSdYfeK1GtLwq3nvc40fow1T4lZQaxhHuViqZfkJDg==');
INSERT INTO `doc_invoice_signature` VALUES ('263', 'CgRPl9zL+Kq3IzyO1Q9pMw9jnWY=', 'J8DjWwidurGv+OhbXSHGiyVTCNga7Paj2ohmYn2XUNuSQIBdjMiIunkp+44kdtox/AqNPQlJ/9WAGoRW7yqgZubd/cS1SlxRRB+Wxle6ndggWkTZpU7lQMLsh5zs1laeTz9VTMpp1ZXMBCEiJPwBKa4Y8ZXZ7Rk9NOc0ATmFQe2DRkPXk5u9yRdCk4/2OP8HmdJF0fYTLC5WJZP6xWkCvElCfb9U5bokYiDXzTsOt58q++KWKyMRVbLE1ZheBPd4+4S6jHet92PvpgYJLmSayj7wUhkX33DFG/F3kj6+WByk0LYKJ/iUSOzX+2/SM2AWQi22XLpWMIBbudRVZpk/Lg==');
INSERT INTO `doc_invoice_signature` VALUES ('264', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('265', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('266', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('267', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('268', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('269', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('270', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('271', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');
INSERT INTO `doc_invoice_signature` VALUES ('272', 'o+nULRwviXVnRIDtqTJwUcNNWOs=', 'jKtnYbfXb10dM+9z+UTZ/L1Ivms9tlEojh/ctU2cGQGLQCZSD4T3Dj8mLWBeA102ybhyPNFo+nEW6833s/UCQOwlVJB7H07kMFGWDYE4BSNyA7AYNNrOLnze38rE8m1wplY1E3gQEJvjfNSKEsML9G3k1MZZI2cO0TUDsJ0SdT2LJQXbZvqlDAkCGXEYrvaD8qDC7l+ahsF4gSUn1kuCwCaiLkWfCESd515ig9qY/z5SGtkeEBa2/QduYqI69ZBrhstqBLW9uIeiYdypY193TDZFCsb1uBrZMUOcovGdd7LWTZ7bbgp7NS45I8HcYzEo2cSPZ+3u4iE/89LgqL7h4g==');

-- ----------------------------
-- Table structure for doc_invoice_sunat_embeded_despatch_advice
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_sunat_embeded_despatch_advice`;
CREATE TABLE `doc_invoice_sunat_embeded_despatch_advice` (
  `n_id_invoice` int(11) NOT NULL DEFAULT '0',
  `iseda_license_plate_id` varchar(30) DEFAULT NULL,
  `iseda_transport_authorization_code` varchar(255) DEFAULT NULL,
  `iseda_brand_name` varchar(255) DEFAULT NULL,
  `iseda_party_identification_id` varchar(255) DEFAULT NULL,
  `iseda_customer_assigned_account` varchar(255) DEFAULT NULL,
  `iseda_additional_account_id` varchar(255) DEFAULT NULL,
  `iseda_registration_name` varchar(255) DEFAULT NULL,
  `iseda_transport_mode_code` varchar(255) DEFAULT NULL,
  `iseda_gross_weight_measure` varchar(255) DEFAULT NULL,
  `iseda_gross_weight_measure_unit_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_sunat_embeded_dispatch_advice` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_sunat_embeded_despatch_advice
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_sunat_embeded_despatch_advice_origin_delivery
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_sunat_embeded_despatch_advice_origin_delivery`;
CREATE TABLE `doc_invoice_sunat_embeded_despatch_advice_origin_delivery` (
  `isedaod_id` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `isedaod_type` enum('origin','delivery') NOT NULL,
  `isedaod_address_id` varchar(6) DEFAULT NULL,
  `isedaod_street_name` varchar(100) DEFAULT NULL,
  `isedaod_city_subdivision_name` varchar(25) DEFAULT NULL,
  `isedaod_city_name` varchar(30) DEFAULT NULL,
  `isedaod_country_subentity` varchar(30) DEFAULT NULL,
  `isedaod_district` varchar(30) DEFAULT NULL,
  `isedaod_country_identification_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`isedaod_id`),
  KEY `n_id_invoice` (`n_id_invoice`),
  CONSTRAINT `fk_invoice_sunat_embeded_despatch_advice_origin_delivery` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice_sunat_embeded_despatch_advice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_sunat_embeded_despatch_advice_origin_delivery
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_supplier
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_supplier`;
CREATE TABLE `doc_invoice_supplier` (
  `n_id_invoice` int(11) NOT NULL,
  `n_id_supplier` int(11) NOT NULL,
  `c_customer_assigned_account_id` varchar(11) NOT NULL,
  `c_additional_account_id` varchar(1) NOT NULL,
  `c_party_postal_address_id` varchar(6) DEFAULT NULL,
  `c_party_postal_address_street_name` varchar(100) DEFAULT NULL,
  `c_party_postal_address_city_subdivision_name` varchar(25) DEFAULT NULL,
  `c_party_postal_address_city_name` varchar(30) DEFAULT NULL,
  `c_party_postal_address_country_subentity` varchar(30) DEFAULT NULL,
  `c_party_postal_address_district` varchar(30) DEFAULT NULL,
  `c_party_postal_address_country_identification_code` varchar(2) DEFAULT NULL,
  `c_party_party_legal_entity_registration_name` varchar(100) NOT NULL,
  `c_party_name_name` varchar(100) DEFAULT NULL,
  `c_telephone` varchar(20) DEFAULT NULL,
  `c_email` varchar(100) DEFAULT NULL,
  `c_detraction_account` varchar(100) DEFAULT NULL,
  `c_sunat_bill_resolution` varchar(100) DEFAULT NULL,
  `c_sunat_invoice_resolution` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  UNIQUE KEY `fk_invoice_invoice_supplier` (`n_id_invoice`) USING BTREE,
  KEY `fk_supplier_invoice_supplier` (`n_id_supplier`),
  KEY `fk_country_invoice_supplier` (`c_party_postal_address_country_identification_code`),
  CONSTRAINT `fk_country_invoice_supplier` FOREIGN KEY (`c_party_postal_address_country_identification_code`) REFERENCES `country` (`c_iso`),
  CONSTRAINT `fk_invoice_invoice_supplier` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_supplier_invoice_supplier` FOREIGN KEY (`n_id_supplier`) REFERENCES `sup_supplier` (`n_id_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_supplier
-- ----------------------------
INSERT INTO `doc_invoice_supplier` VALUES ('139', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('140', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('141', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('142', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('143', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('144', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('145', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('146', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('147', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('148', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('149', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('150', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('151', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('152', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('153', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('154', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('155', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('156', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('157', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('158', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('159', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('160', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('161', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('162', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('163', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('164', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('165', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('166', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('167', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('168', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('169', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('170', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('171', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('172', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('173', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('174', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('175', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('176', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('177', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('178', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('179', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('180', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('181', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('182', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('183', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('184', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('185', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('186', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('187', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('188', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('189', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('190', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('191', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('192', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('193', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('194', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('195', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('196', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('197', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('198', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('199', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('200', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('201', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('202', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('203', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('204', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('205', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('206', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('207', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('208', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('209', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('210', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('211', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('212', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('213', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('214', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('215', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('216', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('217', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('218', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('219', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('220', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('221', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('222', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('223', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('224', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('225', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('226', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('227', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('228', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('229', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('230', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('231', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('232', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('233', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('234', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('235', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('236', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('237', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('238', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('239', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('240', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('241', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('242', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('243', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('244', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('245', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('246', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('247', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('248', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('249', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('250', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('251', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('252', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('253', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('254', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('255', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('256', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('257', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('258', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('259', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('260', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('261', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('262', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('263', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('264', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('265', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('266', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('267', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('268', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('269', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('270', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('271', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');
INSERT INTO `doc_invoice_supplier` VALUES ('272', '6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT');

-- ----------------------------
-- Table structure for doc_invoice_tax_total
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_tax_total`;
CREATE TABLE `doc_invoice_tax_total` (
  `n_id_invoice_tax_total` int(11) NOT NULL AUTO_INCREMENT,
  `n_id_invoice` int(11) NOT NULL,
  `c_tax_amount` varchar(15) NOT NULL,
  PRIMARY KEY (`n_id_invoice_tax_total`),
  KEY `invoice_tax_subtotal_tax_category_tax_scheme_id` (`n_id_invoice`) USING BTREE,
  CONSTRAINT `fk_invoice_invoice_tax_total` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_tax_total
-- ----------------------------
INSERT INTO `doc_invoice_tax_total` VALUES ('41', '135', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('42', '136', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('43', '137', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('44', '138', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('45', '139', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('46', '140', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('47', '141', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('48', '142', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('49', '143', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('50', '144', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('51', '145', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('52', '146', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('53', '147', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('54', '148', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('55', '149', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('56', '150', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('57', '151', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('58', '152', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('59', '153', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('60', '154', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('61', '155', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('62', '156', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('63', '157', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('64', '158', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('65', '159', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('66', '160', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('67', '161', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('68', '162', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('69', '163', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('70', '164', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('71', '165', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('72', '166', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('73', '167', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('74', '168', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('75', '169', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('76', '170', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('77', '171', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('78', '172', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('79', '173', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('80', '174', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('81', '175', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('82', '176', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('83', '177', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('84', '178', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('85', '179', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('86', '180', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('87', '181', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('88', '182', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('89', '183', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('90', '184', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('91', '185', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('92', '186', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('93', '187', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('94', '188', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('95', '189', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('96', '190', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('97', '191', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('98', '192', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('99', '193', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('100', '194', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('101', '195', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('102', '196', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('103', '197', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('104', '198', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('105', '199', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('106', '200', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('107', '201', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('108', '202', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('109', '203', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('110', '204', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('111', '205', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('112', '206', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('113', '207', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('114', '208', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('115', '209', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('116', '210', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('117', '211', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('118', '212', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('119', '213', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('120', '214', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('121', '215', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('122', '216', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('123', '217', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('124', '218', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('125', '219', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('126', '220', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('127', '221', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('128', '222', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('129', '223', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('130', '224', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('131', '225', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('132', '226', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('133', '227', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('134', '228', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('135', '229', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('136', '230', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('137', '231', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('138', '232', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('139', '233', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('140', '234', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('141', '235', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('142', '236', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('143', '237', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('144', '238', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('145', '239', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('146', '240', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('147', '241', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('148', '242', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('149', '243', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('150', '244', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('151', '245', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('152', '246', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('153', '247', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('154', '248', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('155', '249', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('156', '250', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('157', '251', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('158', '252', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('159', '253', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('160', '254', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('161', '255', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('162', '256', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('163', '257', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('164', '258', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('165', '259', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('166', '260', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('167', '261', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('168', '262', '36.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('169', '263', '54.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('170', '264', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('171', '265', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('172', '266', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('173', '267', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('174', '268', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('175', '269', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('176', '270', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('177', '271', '18.00');
INSERT INTO `doc_invoice_tax_total` VALUES ('178', '272', '18.00');

-- ----------------------------
-- Table structure for doc_invoice_tax_total_tax_subtotal
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_tax_total_tax_subtotal`;
CREATE TABLE `doc_invoice_tax_total_tax_subtotal` (
  `n_id_invoice_tax_total` int(11) NOT NULL,
  `c_tax_amount` varchar(15) NOT NULL COMMENT 'Sumatoria de IGV',
  PRIMARY KEY (`n_id_invoice_tax_total`),
  CONSTRAINT `fk_invoice_invoice_tax_total_tax_subtotal` FOREIGN KEY (`n_id_invoice_tax_total`) REFERENCES `doc_invoice_tax_total` (`n_id_invoice_tax_total`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_tax_total_tax_subtotal
-- ----------------------------
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('42', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('43', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('44', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('45', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('46', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('47', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('48', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('49', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('50', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('51', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('52', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('53', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('54', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('55', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('56', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('57', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('58', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('59', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('60', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('61', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('62', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('63', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('64', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('65', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('66', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('67', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('68', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('69', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('70', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('71', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('72', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('73', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('74', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('75', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('76', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('77', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('78', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('79', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('80', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('81', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('82', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('83', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('84', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('85', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('86', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('87', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('88', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('89', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('90', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('91', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('92', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('93', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('94', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('95', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('96', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('97', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('98', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('99', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('100', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('101', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('102', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('103', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('104', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('105', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('106', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('107', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('108', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('109', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('110', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('111', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('112', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('113', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('114', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('115', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('116', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('117', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('118', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('119', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('120', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('121', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('122', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('123', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('124', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('125', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('126', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('127', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('128', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('129', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('130', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('131', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('132', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('133', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('134', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('135', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('136', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('137', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('138', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('139', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('140', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('141', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('142', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('143', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('144', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('145', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('146', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('147', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('148', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('149', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('150', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('151', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('152', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('153', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('154', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('155', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('156', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('157', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('158', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('159', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('160', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('161', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('162', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('163', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('164', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('165', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('166', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('167', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('168', '36.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('169', '54.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('170', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('171', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('172', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('173', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('174', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('175', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('176', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('177', '18.00');
INSERT INTO `doc_invoice_tax_total_tax_subtotal` VALUES ('178', '18.00');

-- ----------------------------
-- Table structure for doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme`;
CREATE TABLE `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` (
  `n_id_invoice_tax_total` int(11) NOT NULL,
  `c_id` varchar(4) NOT NULL COMMENT 'Codigo de tributo - Catalogo No 05',
  `c_name` varchar(6) NOT NULL COMMENT 'Nombre de tributo - Catalogo No 05',
  `c_tax_type_code` varchar(3) NOT NULL COMMENT 'Codigo Internacional tributo - Catalogo No 05',
  PRIMARY KEY (`n_id_invoice_tax_total`),
  CONSTRAINT `fk_invoice_in_tax_total_tax_subtotal_tax_category_tax_scheme` FOREIGN KEY (`n_id_invoice_tax_total`) REFERENCES `doc_invoice_tax_total` (`n_id_invoice_tax_total`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme
-- ----------------------------
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('43', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('44', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('45', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('46', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('47', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('48', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('49', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('50', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('51', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('52', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('53', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('54', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('55', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('56', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('57', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('58', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('59', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('60', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('61', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('62', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('63', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('64', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('65', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('66', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('67', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('68', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('69', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('70', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('71', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('72', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('73', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('74', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('75', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('76', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('77', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('78', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('79', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('80', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('81', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('82', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('83', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('84', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('85', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('86', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('87', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('88', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('89', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('90', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('91', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('92', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('93', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('94', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('95', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('96', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('97', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('98', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('99', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('100', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('101', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('102', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('103', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('104', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('105', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('106', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('107', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('108', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('109', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('110', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('111', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('112', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('113', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('114', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('115', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('116', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('117', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('118', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('119', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('120', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('121', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('122', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('123', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('124', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('125', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('126', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('127', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('128', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('129', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('130', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('131', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('132', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('133', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('134', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('135', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('136', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('137', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('138', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('139', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('140', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('141', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('142', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('143', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('144', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('145', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('146', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('147', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('148', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('149', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('150', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('151', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('152', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('153', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('154', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('155', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('156', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('157', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('158', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('159', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('160', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('161', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('162', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('163', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('164', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('165', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('166', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('167', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('168', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('169', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('170', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('171', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('172', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('173', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('174', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('175', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('176', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('177', '1000', 'IGV', 'VAT');
INSERT INTO `doc_invoice_tax_total_tax_subtotal_tax_category_tax_scheme` VALUES ('178', '1000', 'IGV', 'VAT');

-- ----------------------------
-- Table structure for doc_invoice_ticket
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_ticket`;
CREATE TABLE `doc_invoice_ticket` (
  `n_id_invoice` int(11) NOT NULL,
  `c_has_ticket` enum('yes','no') DEFAULT NULL,
  `c_ticket` varchar(200) NOT NULL,
  `c_ticket_code` varchar(100) DEFAULT NULL,
  `d_date_register` datetime NOT NULL,
  `d_date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_invoice_ticket` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_ticket
-- ----------------------------

-- ----------------------------
-- Table structure for doc_invoice_type_code
-- ----------------------------
DROP TABLE IF EXISTS `doc_invoice_type_code`;
CREATE TABLE `doc_invoice_type_code` (
  `c_invoice_type_code` varchar(2) NOT NULL,
  `c_description_type_code` varchar(200) NOT NULL COMMENT 'Catalogo No 01, codigo del tipo de documento',
  `c_status` enum('visible','hidden','deleted') NOT NULL,
  PRIMARY KEY (`c_invoice_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_invoice_type_code
-- ----------------------------
INSERT INTO `doc_invoice_type_code` VALUES ('01', 'FACTURA ELECTRONICA', 'visible');
INSERT INTO `doc_invoice_type_code` VALUES ('03', 'BOLETA DE VENTA', 'visible');
INSERT INTO `doc_invoice_type_code` VALUES ('07', 'NOTA DE CREDITO', 'visible');
INSERT INTO `doc_invoice_type_code` VALUES ('08', 'NOTA DE DEBITO', 'visible');
INSERT INTO `doc_invoice_type_code` VALUES ('09', 'GUIA DE REMISION REMITENTE', 'deleted');
INSERT INTO `doc_invoice_type_code` VALUES ('12', 'TICKET DE MAQUINA REGISTRADORA', 'deleted');
INSERT INTO `doc_invoice_type_code` VALUES ('13', 'DOCUMENTO EMITIDO POR BANCOS, INSTITUCIONES FINANCIERAS, CREDITICIAS Y DE SEGUROS QUE SE ENCUENTREN BAJO EL CONTROL DE LA SUPERINTENDENCIA DE BANCA Y SEGUROS', 'deleted');
INSERT INTO `doc_invoice_type_code` VALUES ('18', 'DOCUMENTOS EMITIDOS POR LAS AFPS', 'deleted');
INSERT INTO `doc_invoice_type_code` VALUES ('31', 'GUIA DE REMISION TRANSPORTISTA', 'deleted');
INSERT INTO `doc_invoice_type_code` VALUES ('56', 'COMPROBANTE DE PAGO SEAE', 'deleted');
INSERT INTO `doc_invoice_type_code` VALUES ('RA', 'COMUNICACION DE BAJA', 'visible');
INSERT INTO `doc_invoice_type_code` VALUES ('RC', 'RESUMEN DIARIO DE BOLETA', 'visible');

-- ----------------------------
-- Table structure for doc_seller_supplier_party
-- ----------------------------
DROP TABLE IF EXISTS `doc_seller_supplier_party`;
CREATE TABLE `doc_seller_supplier_party` (
  `n_id_invoice` int(11) NOT NULL AUTO_INCREMENT,
  `ssp_party_postal_address_id` varchar(6) DEFAULT NULL,
  `ssp_party_postal_address_street_name` varchar(100) DEFAULT NULL,
  `ssp_party_postal_address_city_subdivision_name` varchar(25) DEFAULT NULL,
  `ssp_party_postal_address_city_name` varchar(30) DEFAULT NULL,
  `ssp_party_postal_address_country_subentity` varchar(30) DEFAULT NULL,
  `ssp_party_postal_address_district` varchar(30) DEFAULT NULL,
  `ssp_party_postal_address_country_identification_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`n_id_invoice`),
  CONSTRAINT `fk_invoice_seller_supplier_party` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc_seller_supplier_party
-- ----------------------------

-- ----------------------------
-- Table structure for err_error_code
-- ----------------------------
DROP TABLE IF EXISTS `err_error_code`;
CREATE TABLE `err_error_code` (
  `c_id_error_code` varchar(4) NOT NULL,
  `n_id_error_code_type` int(11) DEFAULT NULL,
  `c_description` text NOT NULL,
  `c_is_forwardable` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`c_id_error_code`),
  KEY `fk_error_code_type_error_code` (`n_id_error_code_type`),
  CONSTRAINT `fk_error_code_type_error_code` FOREIGN KEY (`n_id_error_code_type`) REFERENCES `err_error_code_type` (`n_id_error_code_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of err_error_code
-- ----------------------------
INSERT INTO `err_error_code` VALUES ('', '1', '', '\0');
INSERT INTO `err_error_code` VALUES ('0', null, '', '\0');
INSERT INTO `err_error_code` VALUES ('0100', '1', 'El sistema no puede responder su solicitud. Intente nuevamente o comuníquese con su Administrador', '');
INSERT INTO `err_error_code` VALUES ('0101', '1', 'El encabezado de seguridad es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('0102', '1', 'Usuario o contraseña incorrectos', '\0');
INSERT INTO `err_error_code` VALUES ('0103', '1', 'El Usuario ingresado no existe', '\0');
INSERT INTO `err_error_code` VALUES ('0104', '1', 'La Clave ingresada es incorrecta', '\0');
INSERT INTO `err_error_code` VALUES ('0105', '1', 'El Usuario no está activo', '\0');
INSERT INTO `err_error_code` VALUES ('0106', '1', 'El Usuario no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('0109', '1', 'El sistema no puede responder su solicitud. (El servicio de autenticación no está disponible)', '');
INSERT INTO `err_error_code` VALUES ('0110', '1', 'No se pudo obtener la informacion del tipo de usuario', '\0');
INSERT INTO `err_error_code` VALUES ('0111', '1', 'No tiene el perfil para enviar comprobantes electronicos', '\0');
INSERT INTO `err_error_code` VALUES ('0112', '1', 'El usuario debe ser secundario', '\0');
INSERT INTO `err_error_code` VALUES ('0113', '1', 'El usuario no esta afiliado a Factura Electronica', '\0');
INSERT INTO `err_error_code` VALUES ('0125', '1', 'No se pudo obtener la constancia', '\0');
INSERT INTO `err_error_code` VALUES ('0126', '1', 'El ticket no le pertenece al usuario', '\0');
INSERT INTO `err_error_code` VALUES ('0127', '1', 'El ticket no existe', '\0');
INSERT INTO `err_error_code` VALUES ('0130', '1', 'El sistema no puede responder su solicitud. (No se pudo obtener el ticket de proceso)', '');
INSERT INTO `err_error_code` VALUES ('0131', '1', 'El sistema no puede responder su solicitud. (No se pudo grabar el archivo en el directorio)', '');
INSERT INTO `err_error_code` VALUES ('0132', '1', 'El sistema no puede responder su solicitud. (No se pudo grabar escribir en el archivo zip)', '');
INSERT INTO `err_error_code` VALUES ('0133', '1', 'El sistema no puede responder su solicitud. (No se pudo grabar la entrada del log)', '');
INSERT INTO `err_error_code` VALUES ('0134', '1', 'El sistema no puede responder su solicitud. (No se pudo grabar en el storage)', '');
INSERT INTO `err_error_code` VALUES ('0135', '1', 'El sistema no puede responder su solicitud. (No se pudo encolar el pedido)', '');
INSERT INTO `err_error_code` VALUES ('0136', '1', 'El sistema no puede responder su solicitud. (No se pudo recibir una respuesta del batch)', '');
INSERT INTO `err_error_code` VALUES ('0137', '1', 'El sistema no puede responder su solicitud. (Se obtuvo una respuesta nula)', '');
INSERT INTO `err_error_code` VALUES ('0138', '1', 'El sistema no puede responder su solicitud. (Error en Base de Datos)', '');
INSERT INTO `err_error_code` VALUES ('0151', '1', 'El nombre del archivo ZIP es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('0152', '1', 'No se puede enviar por este método un archivo de resumen', '\0');
INSERT INTO `err_error_code` VALUES ('0153', '1', 'No se puede enviar por este método un archivo por lotes', '\0');
INSERT INTO `err_error_code` VALUES ('0154', '1', 'El RUC del archivo no corresponde al RUC del usuario', '\0');
INSERT INTO `err_error_code` VALUES ('0155', '1', 'El archivo ZIP esta vacio', '\0');
INSERT INTO `err_error_code` VALUES ('0156', '1', 'El archivo ZIP esta corrupto', '\0');
INSERT INTO `err_error_code` VALUES ('0157', '1', 'El archivo ZIP no contiene comprobantes', '\0');
INSERT INTO `err_error_code` VALUES ('0158', '1', 'El archivo ZIP contiene demasiados comprobantes para este tipo de envío', '\0');
INSERT INTO `err_error_code` VALUES ('0159', '1', 'El nombre del archivo XML es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('0160', '1', 'El archivo XML esta vacio', '\0');
INSERT INTO `err_error_code` VALUES ('0161', '1', 'El nombre del archivo XML no coincide con el nombre del archivo ZIP', '\0');
INSERT INTO `err_error_code` VALUES ('0200', '1', 'No se pudo procesar su solicitud. (Ocurrio un error en el batch)', '');
INSERT INTO `err_error_code` VALUES ('0201', '1', 'No se pudo procesar su solicitud. (Llego un requerimiento nulo al batch)', '');
INSERT INTO `err_error_code` VALUES ('0202', '1', 'No se pudo procesar su solicitud. (No llego información del archivo ZIP)', '');
INSERT INTO `err_error_code` VALUES ('0203', '1', 'No se pudo procesar su solicitud. (No se encontro archivos en la informacion del archivo ZIP)', '');
INSERT INTO `err_error_code` VALUES ('0204', '1', 'No se pudo procesar su solicitud. (Este tipo de requerimiento solo acepta 1 archivo)', '');
INSERT INTO `err_error_code` VALUES ('0250', '1', 'No se pudo procesar su solicitud. (Ocurrio un error desconocido al hacer unzip)', '');
INSERT INTO `err_error_code` VALUES ('0251', '1', 'No se pudo procesar su solicitud. (No se pudo crear un directorio para el unzip)', '');
INSERT INTO `err_error_code` VALUES ('0252', '1', 'No se pudo procesar su solicitud. (No se encontro archivos dentro del zip)', '');
INSERT INTO `err_error_code` VALUES ('0253', '1', 'No se pudo procesar su solicitud. (No se pudo comprimir la constancia)', '');
INSERT INTO `err_error_code` VALUES ('0300', '1', 'No se encontró la raíz documento xml', '\0');
INSERT INTO `err_error_code` VALUES ('0301', '1', 'Elemento raiz del xml no esta definido', '\0');
INSERT INTO `err_error_code` VALUES ('0302', '1', 'Codigo del tipo de comprobante no registrado', '\0');
INSERT INTO `err_error_code` VALUES ('0303', '1', 'No existe el directorio de schemas', '\0');
INSERT INTO `err_error_code` VALUES ('0304', '1', 'No existe el archivo de schema', '\0');
INSERT INTO `err_error_code` VALUES ('0305', '1', 'El sistema no puede procesar el archivo xml', '\0');
INSERT INTO `err_error_code` VALUES ('0306', '1', 'No se puede leer (parsear) el archivo XML', '\0');
INSERT INTO `err_error_code` VALUES ('0307', '1', 'No se pudo recuperar la constancia', '\0');
INSERT INTO `err_error_code` VALUES ('0400', '1', 'No tiene permiso para enviar casos de pruebas', '\0');
INSERT INTO `err_error_code` VALUES ('0401', '1', 'El caso de prueba no existe', '\0');
INSERT INTO `err_error_code` VALUES ('0402', '1', 'La numeracion o nombre del documento ya ha sido enviado anteriormente', '\0');
INSERT INTO `err_error_code` VALUES ('0403', '1', 'El documento afectado por la nota no existe', '\0');
INSERT INTO `err_error_code` VALUES ('0404', '1', 'El documento afectado por la nota se encuentra rechazado', '\0');
INSERT INTO `err_error_code` VALUES ('1001', '1', 'ID - El dato SERIE-CORRELATIVO no cumple con el formato de acuerdo al tipo de comprobante ', '\0');
INSERT INTO `err_error_code` VALUES ('1002', '1', 'El XML no contiene informacion en el tag ID ', '\0');
INSERT INTO `err_error_code` VALUES ('1003', '1', 'InvoiceTypeCode - El valor del tipo de documento es invalido o no coincide con el nombre del archivo ', '\0');
INSERT INTO `err_error_code` VALUES ('1004', '1', 'El XML no contiene el tag o no existe informacion de InvoiceTypeCode ', '\0');
INSERT INTO `err_error_code` VALUES ('1005', '1', 'CustomerAssignedAccountID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('1006', '1', 'El XML no contiene el tag o no existe informacion de CustomerAssignedAccountID del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('1007', '1', 'AdditionalAccountID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('1008', '1', 'El XML no contiene el tag o no existe informacion de AdditionalAccountID del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('1009', '1', 'IssueDate - El dato ingresado  no cumple con el patron YYYY-MM-DD', '\0');
INSERT INTO `err_error_code` VALUES ('1010', '1', 'El XML no contiene el tag IssueDate', '\0');
INSERT INTO `err_error_code` VALUES ('1011', '1', 'IssueDate- El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('1012', '1', 'ID - El dato ingresado no cumple con el patron SERIE-CORRELATIVO', '\0');
INSERT INTO `err_error_code` VALUES ('1013', '1', 'El XML no contiene informacion en el tag ID', '\0');
INSERT INTO `err_error_code` VALUES ('1014', '1', 'CustomerAssignedAccountID - El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1015', '1', 'El XML no contiene el tag o no existe informacion de CustomerAssignedAccountID del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('1016', '1', 'AdditionalAccountID - El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1017', '1', 'El XML no contiene el tag AdditionalAccountID del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('1018', '1', 'IssueDate - El dato ingresado no cumple con el patron YYYY-MM-DD', '\0');
INSERT INTO `err_error_code` VALUES ('1019', '1', 'El XML no contiene el tag IssueDate', '\0');
INSERT INTO `err_error_code` VALUES ('1020', '1', 'IssueDate- El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('1021', '1', 'Error en la validacion de la nota de credito', '\0');
INSERT INTO `err_error_code` VALUES ('1022', '1', 'La serie o numero del documento modificado por la Nota Electrónica no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('1023', '1', 'No se ha especificado el tipo de documento modificado por la Nota electronica ', '\0');
INSERT INTO `err_error_code` VALUES ('1024', '1', 'CustomerAssignedAccountID - El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1025', '1', 'El XML no contiene el tag o no existe informacion de CustomerAssignedAccountID del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('1026', '1', 'AdditionalAccountID - El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1027', '1', 'El XML no contiene el tag AdditionalAccountID del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('1028', '1', 'IssueDate - El dato ingresado no cumple con el patron YYYY-MM-DD', '\0');
INSERT INTO `err_error_code` VALUES ('1029', '1', 'El XML no contiene el tag IssueDate', '\0');
INSERT INTO `err_error_code` VALUES ('1030', '1', 'IssueDate- El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('1031', '1', 'Error en la validacion de la nota de debito', '\0');
INSERT INTO `err_error_code` VALUES ('1032', '1', 'El comprobante fue informado previamente en una comunicacion de baja ', '\0');
INSERT INTO `err_error_code` VALUES ('1033', '1', 'El comprobante fue registrado previamente con otros datos ', '\0');
INSERT INTO `err_error_code` VALUES ('1034', '1', 'Número de RUC del nombre del archivo no coincide con el consignado en el contenido del archivo XML ', '\0');
INSERT INTO `err_error_code` VALUES ('1035', '1', 'Numero de Serie del nombre del archivo no coincide con el consignado en el contenido del archivo XML ', '\0');
INSERT INTO `err_error_code` VALUES ('1036', '1', 'Número de documento en el nombre del archivo no coincide con el consignado en el contenido del XML ', '\0');
INSERT INTO `err_error_code` VALUES ('1037', '1', 'El XML no contiene el tag o no existe informacion de RegistrationName del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('1038', '1', 'RegistrationName - El nombre o razon social del emisor no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('1039', '1', 'Solo se pueden recibir notas electronicas que modifican facturas ', '\0');
INSERT INTO `err_error_code` VALUES ('1040', '1', 'El tipo de documento modificado por la nota electronica no es valido ', '\0');
INSERT INTO `err_error_code` VALUES ('1041', '1', 'cac:PrepaidPayment/cbc:ID - El tag no contiene el atributo @SchemaID. que indica el tipo de documento que realiza el anticipo', '\0');
INSERT INTO `err_error_code` VALUES ('1042', '1', 'cac:PrepaidPayment/cbc:InstructionID – El tag no contiene el atributo @SchemaID. Que indica el tipo de documento del emisor del documento del anticipo', '\0');
INSERT INTO `err_error_code` VALUES ('1043', '1', 'cac:OriginatorDocumentReference/cbc:ID - El tag no contiene el atributo @SchemaID. Que indica el tipo de documento del originador del documento electrónico', '\0');
INSERT INTO `err_error_code` VALUES ('1044', '1', 'cac:PrepaidPayment/cbc:InstructionID – El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1045', '1', 'cac:OriginatorDocumentReference/cbc:ID – El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1046', '1', 'cbc:Amount - El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1047', '1', 'cbc:Quantity - El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('1048', '1', 'El XML no contiene el tag o no existe información de PrepaidAmount para un documento con anticipo', '\0');
INSERT INTO `err_error_code` VALUES ('1049', '1', 'ID - Serie y Número del archivo no coincide con el consignado en el contenido del XML', '\0');
INSERT INTO `err_error_code` VALUES ('1050', '1', 'El XML no contiene información en el tag DespatchAdviceTypeCode', '\0');
INSERT INTO `err_error_code` VALUES ('1051', '1', 'DespatchAdviceTypeCode - El valor del tipo de guía es inválido', '\0');
INSERT INTO `err_error_code` VALUES ('1052', '1', 'DespatchAdviceTypeCode - No coincide con el consignado en el contenido del XML', '\0');
INSERT INTO `err_error_code` VALUES ('1053', '1', 'El XML no contiene información en el tag /DespatchAdvice/cac:OrderReference/cbc:ID', '\0');
INSERT INTO `err_error_code` VALUES ('1054', '1', 'cac:OrderReference/cac:DocumentReference/cbc:ID - El dato SERIE-número no cumple con el formato de acuerdo a la Guía', '\0');
INSERT INTO `err_error_code` VALUES ('1055', '1', 'Serie - No cumple con el formato de acuerdo a guía electrónica (EG01 ó TXXXX)', '\0');
INSERT INTO `err_error_code` VALUES ('1056', '1', 'El XML no contiene información en el tag /DespatchAdvice/cac:OrderReference/cbc:OrderTypeCode', '\0');
INSERT INTO `err_error_code` VALUES ('1057', '1', 'El XML no contiene información en el tag cac:AdditionalDocumentReference/cbc:ID', '\0');
INSERT INTO `err_error_code` VALUES ('1058', '1', 'El XML no contiene información en el tag cac:AdditionalDocumentReference/cbc:DocumentTypeCode', '\0');
INSERT INTO `err_error_code` VALUES ('1059', '1', 'El XML no contiene firma digital', '\0');
INSERT INTO `err_error_code` VALUES ('1060', '1', 'El XML no contiene el tag o no existe información del número de RUC del Remitente', '\0');
INSERT INTO `err_error_code` VALUES ('1061', '1', 'El número de RUC del Remitente no existe', '\0');
INSERT INTO `err_error_code` VALUES ('1062', '1', 'El XML no contiene el tag o no existe información en el tag /DespatchAdvice/cac:Shipment/cbc:HandlingCode', '\0');
INSERT INTO `err_error_code` VALUES ('1063', '1', 'cbc:ShippingPriorityLevelCode: El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('1064', '1', 'El XML no contiene el tag o no existe información en el tag cac:DespatchLine de bienes a transportar', '\0');
INSERT INTO `err_error_code` VALUES ('1065', '1', 'El XML no contiene información en el tag cbc:TransportModeCode', '\0');
INSERT INTO `err_error_code` VALUES ('1066', '1', 'El XML no contiene el tag o no existe información en el tag cac:CarrierParty de datos del transportista', '\0');
INSERT INTO `err_error_code` VALUES ('1067', '1', 'El XML no contiene el tag o no existe información en el tag cac:TransportMeans de datos del vehículo', '\0');
INSERT INTO `err_error_code` VALUES ('1068', '1', 'El XML no contiene el tag o no existe información en el tag cac:DriverPerson de datos del conductor', '\0');
INSERT INTO `err_error_code` VALUES ('1069', '1', 'StartDate: El XML no contiene fecha de inicio de traslado', '\0');
INSERT INTO `err_error_code` VALUES ('1070', '1', 'StartDate - El dato ingresado  no cumple con el patrón YYYY-MM-DD', '\0');
INSERT INTO `err_error_code` VALUES ('1071', '1', 'StartDate - El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('1072', '1', 'Starttime - El dato ingresado  no cumple con el patrón HH:mm:ss.SZ', '\0');
INSERT INTO `err_error_code` VALUES ('1073', '1', 'StartTime - El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('1074', '1', 'No ha consignado punto de llegada', '\0');
INSERT INTO `err_error_code` VALUES ('1075', '1', 'No ha consignado punto de partida', '\0');
INSERT INTO `err_error_code` VALUES ('2010', '2', 'El contribuyente no esta activo ', '\0');
INSERT INTO `err_error_code` VALUES ('2011', '2', 'El contribuyente no esta habido ', '\0');
INSERT INTO `err_error_code` VALUES ('2012', '2', 'El contribuyente no está autorizado a emitir comprobantes electrónicos', '\0');
INSERT INTO `err_error_code` VALUES ('2013', '2', 'El contribuyente no cumple con tipo de empresa o tributos requeridos ', '\0');
INSERT INTO `err_error_code` VALUES ('2014', '2', 'El XML no contiene el tag o no existe informacion de CustomerAssignedAccountID del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2015', '2', 'El XML no contiene el tag o no existe informacion de AdditionalAccountID del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2016', '2', 'AdditionalAccountID - El dato ingresado en el tipo de documento de identidad del receptor no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2017', '2', 'CustomerAssignedAccountID - El numero de documento de identidad del recepetor debe ser RUC ', '\0');
INSERT INTO `err_error_code` VALUES ('2018', '2', 'CustomerAssignedAccountID -  El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('2019', '2', 'El XML no contiene el tag o no existe informacion de RegistrationName del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('2020', '2', 'RegistrationName - El nombre o razon social del emisor no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('2021', '2', 'El XML no contiene el tag o no existe informacion de RegistrationName del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2022', '2', 'RegistrationName - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2023', '2', 'El Numero de orden del item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2024', '2', 'El XML no contiene el tag InvoicedQuantity en el detalle de los Items ', '\0');
INSERT INTO `err_error_code` VALUES ('2025', '2', 'InvoicedQuantity El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2026', '2', 'El XML no contiene el tag cac:Item/cbc:Description en el detalle de los Items', '\0');
INSERT INTO `err_error_code` VALUES ('2027', '2', 'El XML no contiene el tag o no existe informacion de cac:Item/cbc:Description del item ', '\0');
INSERT INTO `err_error_code` VALUES ('2028', '2', 'Debe existir el tag cac:AlternativeConditionPrice con un elemento cbc:PriceTypeCode con valor 01 ', '\0');
INSERT INTO `err_error_code` VALUES ('2029', '2', 'PriceTypeCode El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('2030', '2', 'El XML no contiene el tag cbc:PriceTypeCode', '\0');
INSERT INTO `err_error_code` VALUES ('2031', '2', 'LineExtensionAmount El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('2032', '2', 'El XML no contiene el tag LineExtensionAmount en el detalle de los Items', '\0');
INSERT INTO `err_error_code` VALUES ('2033', '2', 'El dato ingresado en TaxAmount de la linea no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2034', '2', 'TaxAmount es obligatorio', '\0');
INSERT INTO `err_error_code` VALUES ('2035', '2', 'cac:TaxCategory/cac:TaxScheme/cbc:ID El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2036', '2', 'El codigo del tributo es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2037', '2', 'El XML no contiene el tag cac:TaxCategory/cac:TaxScheme/cbc:ID del Item', '\0');
INSERT INTO `err_error_code` VALUES ('2038', '2', 'cac:TaxScheme/cbc:Name del item - No existe el tag o el dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2039', '2', 'El XML no contiene el tag cac:TaxCategory/cac:TaxScheme/cbc:Name del Item', '\0');
INSERT INTO `err_error_code` VALUES ('2040', '2', 'El tipo de afectacion del IGV es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2041', '2', 'El sistema de calculo del ISC es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2042', '2', 'Debe indicar el IGV. Es un campo obligatorio ', '\0');
INSERT INTO `err_error_code` VALUES ('2043', '2', 'El dato ingresado en PayableAmount no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2044', '2', 'PayableAmount es obligatorio', '\0');
INSERT INTO `err_error_code` VALUES ('2045', '2', 'El valor ingresado en AdditionalMonetaryTotal/cbc:ID es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2046', '2', 'AdditionalMonetaryTotal/cbc:ID debe tener valor', '\0');
INSERT INTO `err_error_code` VALUES ('2047', '2', 'Es obligatorio al menos un AdditionalMonetaryTotal con codigo 1001, 1002 o 1003 ', '\0');
INSERT INTO `err_error_code` VALUES ('2048', '2', 'El dato ingresado en TaxAmount no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2049', '2', 'TaxAmount es obligatorio', '\0');
INSERT INTO `err_error_code` VALUES ('2050', '2', 'TaxScheme ID - No existe el tag o el dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2051', '2', 'El codigo del tributo es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2052', '2', 'El XML no contiene el tag TaxScheme ID de impuestos globales', '\0');
INSERT INTO `err_error_code` VALUES ('2053', '2', 'TaxScheme Name - No existe el tag o el dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2054', '2', 'El XML no contiene el tag TaxScheme Name de impuestos globales', '\0');
INSERT INTO `err_error_code` VALUES ('2055', '2', 'TaxScheme TaxTypeCode - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2056', '2', 'El XML no contiene el tag TaxScheme TaxTypeCode de impuestos globales', '\0');
INSERT INTO `err_error_code` VALUES ('2057', '2', 'El Name o TaxTypeCode debe corresponder con el Id para el IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2058', '2', 'El Name o TaxTypeCode debe corresponder con el Id para el ISC ', '\0');
INSERT INTO `err_error_code` VALUES ('2059', '2', 'El dato ingresado en TaxSubtotal/cbc:TaxAmount no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2060', '2', 'TaxSubtotal/cbc:TaxAmount es obligatorio', '\0');
INSERT INTO `err_error_code` VALUES ('2061', '2', 'El tag global cac:TaxTotal/cbc:TaxAmount debe tener el mismo valor que cac:TaxTotal/cac:Subtotal/cbc:TaxAmount ', '\0');
INSERT INTO `err_error_code` VALUES ('2062', '2', 'El dato ingresado en PayableAmount no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2063', '2', 'El XML no contiene el tag PayableAmount', '\0');
INSERT INTO `err_error_code` VALUES ('2064', '2', 'El dato ingresado en ChargeTotalAmount no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2065', '2', 'El dato ingresado en el campo Total Descuentos no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2066', '2', 'Debe indicar una descripcion para el tag sac:AdditionalProperty/cbc:Value', '\0');
INSERT INTO `err_error_code` VALUES ('2067', '2', 'cac:Price/cbc:PriceAmount - El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2068', '2', 'El XML no contiene el tag cac:Price/cbc:PriceAmount en el detalle de los Items ', '\0');
INSERT INTO `err_error_code` VALUES ('2069', '2', 'DocumentCurrencyCode - El dato ingresado no cumple con la estructura ', '\0');
INSERT INTO `err_error_code` VALUES ('2070', '2', 'El XML no contiene el tag o no existe informacion de DocumentCurrencyCode ', '\0');
INSERT INTO `err_error_code` VALUES ('2071', '2', 'La moneda debe ser la misma en todo el documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2072', '2', 'CustomizationID - La versión del documento no es la correcta ', '\0');
INSERT INTO `err_error_code` VALUES ('2073', '2', 'El XML no contiene el tag o no existe informacion de CustomizationID ', '\0');
INSERT INTO `err_error_code` VALUES ('2074', '2', 'UBLVersionID - La versión del UBL no es correcta ', '\0');
INSERT INTO `err_error_code` VALUES ('2075', '2', 'El XML no contiene el tag o no existe informacion de UBLVersionID ', '\0');
INSERT INTO `err_error_code` VALUES ('2076', '2', 'cac:Signature/cbc:ID - Falta el identificador de la firma ', '\0');
INSERT INTO `err_error_code` VALUES ('2077', '2', 'El tag cac:Signature/cbc:ID debe contener informacion ', '\0');
INSERT INTO `err_error_code` VALUES ('2078', '2', 'cac:Signature/cac:SignatoryParty/cac:PartyIdentification/cbc:ID - Debe ser igual al RUC del emisor ', '\0');
INSERT INTO `err_error_code` VALUES ('2079', '2', 'El XML no contiene el tag cac:Signature/cac:SignatoryParty/cac:PartyIdentification/cbc:ID', '\0');
INSERT INTO `err_error_code` VALUES ('2080', '2', 'cac:Signature/cac:SignatoryParty/cac:PartyName/cbc:Name - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2081', '2', 'El XML no contiene el tag cac:Signature/cac:SignatoryParty/cac:PartyName/cbc:Name', '\0');
INSERT INTO `err_error_code` VALUES ('2082', '2', 'cac:Signature/cac:DigitalSignatureAttachment/cac:ExternalReference/cbc:URI - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2083', '2', 'El XML no contiene el tag cac:Signature/cac:DigitalSignatureAttachment/cac:ExternalReference/cbc:URI ', '\0');
INSERT INTO `err_error_code` VALUES ('2084', '2', 'ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/ds:Signature/@Id - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2085', '2', 'El XML no contiene el tag ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/ds:Signature/@Id ', '\0');
INSERT INTO `err_error_code` VALUES ('2086', '2', 'ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:CanonicalizationMethod/@Algorithm - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2087', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:CanonicalizationMethod/@Algorithm ', '\0');
INSERT INTO `err_error_code` VALUES ('2088', '2', 'ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:SignatureMethod/@Algorithm - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2089', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:SignatureMethod/@Algorithm ', '\0');
INSERT INTO `err_error_code` VALUES ('2090', '2', 'ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:Reference/@URI - Debe estar vacio para id ', '\0');
INSERT INTO `err_error_code` VALUES ('2091', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:Reference/@URI', '\0');
INSERT INTO `err_error_code` VALUES ('2092', '2', 'ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/.../ds:Transform@Algorithm - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2093', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:Reference/ds:Transform@Algorithm ', '\0');
INSERT INTO `err_error_code` VALUES ('2094', '2', 'ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestMethod/@Algorithm - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2095', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestMethod/@Algorithm ', '\0');
INSERT INTO `err_error_code` VALUES ('2096', '2', 'ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestValue - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2097', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestValue ', '\0');
INSERT INTO `err_error_code` VALUES ('2098', '2', 'ext:UBLExtensions/.../ds:Signature/ds:SignatureValue - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2099', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:SignatureValue ', '\0');
INSERT INTO `err_error_code` VALUES ('2100', '2', 'ext:UBLExtensions/.../ds:Signature/ds:KeyInfo/ds:X509Data/ds:X509Certificate - No cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2101', '2', 'El XML no contiene el tag ext:UBLExtensions/.../ds:Signature/ds:KeyInfo/ds:X509Data/ds:X509Certificate ', '\0');
INSERT INTO `err_error_code` VALUES ('2102', '2', 'Error al procesar la factura', '\0');
INSERT INTO `err_error_code` VALUES ('2103', '2', 'La serie ingresada no es válida', '\0');
INSERT INTO `err_error_code` VALUES ('2104', '2', 'Numero de RUC del emisor no existe', '\0');
INSERT INTO `err_error_code` VALUES ('2105', '2', 'Factura a dar de baja no se encuentra registrada en SUNAT ', '\0');
INSERT INTO `err_error_code` VALUES ('2106', '2', 'Factura a dar de baja ya se encuentra en estado de baja ', '\0');
INSERT INTO `err_error_code` VALUES ('2107', '2', 'Numero de RUC SOL no coincide con RUC emisor', '\0');
INSERT INTO `err_error_code` VALUES ('2108', '2', 'Presentacion fuera de fecha ', '\0');
INSERT INTO `err_error_code` VALUES ('2109', '2', 'El comprobante fue registrado previamente con otros datos', '\0');
INSERT INTO `err_error_code` VALUES ('2110', '2', 'UBLVersionID - La versión del UBL no es correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2111', '2', 'El XML no contiene el tag o no existe informacion de UBLVersionID', '\0');
INSERT INTO `err_error_code` VALUES ('2112', '2', 'CustomizationID - La version del documento no es correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2113', '2', 'El XML no contiene el tag o no existe informacion de CustomizationID', '\0');
INSERT INTO `err_error_code` VALUES ('2114', '2', 'DocumentCurrencyCode -  El dato ingresado no cumple con la estructura', '\0');
INSERT INTO `err_error_code` VALUES ('2115', '2', 'El XML no contiene el tag o no existe informacion de DocumentCurrencyCode', '\0');
INSERT INTO `err_error_code` VALUES ('2116', '2', 'El tipo de documento modificado por la Nota de credito debe ser factura electronica o ticket ', '\0');
INSERT INTO `err_error_code` VALUES ('2117', '2', 'La serie o numero del documento modificado por la Nota de Credito no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2118', '2', 'Debe indicar las facturas relacionadas a la Nota de Credito ', '\0');
INSERT INTO `err_error_code` VALUES ('2119', '2', 'La factura relacionada en la Nota de credito no esta registrada. ', '\0');
INSERT INTO `err_error_code` VALUES ('2120', '2', 'La factura relacionada en la nota de credito se encuentra de baja ', '\0');
INSERT INTO `err_error_code` VALUES ('2121', '2', 'La factura relacionada en la nota de credito esta registrada como rechazada ', '\0');
INSERT INTO `err_error_code` VALUES ('2122', '2', 'El tag cac:LegalMonetaryTotal/cbc:PayableAmount debe tener informacion valida', '\0');
INSERT INTO `err_error_code` VALUES ('2123', '2', 'RegistrationName -  El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2124', '2', 'El XML no contiene el tag RegistrationName del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('2125', '2', 'ReferenceID - El dato ingresado debe indicar SERIE-CORRELATIVO del documento al que se relaciona la Nota ', '\0');
INSERT INTO `err_error_code` VALUES ('2126', '2', 'El XML no contiene informacion en el tag ReferenceID del documento al que se relaciona la nota ', '\0');
INSERT INTO `err_error_code` VALUES ('2127', '2', 'ResponseCode - El dato ingresado no cumple con la estructura ', '\0');
INSERT INTO `err_error_code` VALUES ('2128', '2', 'El XML no contiene el tag o no existe informacion de ResponseCode ', '\0');
INSERT INTO `err_error_code` VALUES ('2129', '2', 'AdditionalAccountID - El dato ingresado en el tipo de documento de identidad del receptor no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2130', '2', 'El XML no contiene el tag o no existe informacion de AdditionalAccountID del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2131', '2', 'CustomerAssignedAccountID - El numero de documento de identidad del receptor debe ser RUC ', '\0');
INSERT INTO `err_error_code` VALUES ('2132', '2', 'El XML no contiene el tag o no existe informacion de CustomerAssignedAccountID del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2133', '2', 'RegistrationName - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2134', '2', 'El XML no contiene el tag o no existe informacion de RegistrationName del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2135', '2', 'cac:DiscrepancyResponse/cbc:Description - El dato ingresado no cumple con la estructura ', '\0');
INSERT INTO `err_error_code` VALUES ('2136', '2', 'El XML no contiene el tag o no existe informacion de cac:DiscrepancyResponse/cbc:Description ', '\0');
INSERT INTO `err_error_code` VALUES ('2137', '2', 'El Número de orden del item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2138', '2', 'CreditedQuantity/@unitCode - El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2139', '2', 'CreditedQuantity - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2140', '2', 'El PriceTypeCode debe tener el valor 01 ', '\0');
INSERT INTO `err_error_code` VALUES ('2141', '2', 'cac:TaxCategory/cac:TaxScheme/cbc:ID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2142', '2', 'El codigo del tributo es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2143', '2', 'cac:TaxScheme/cbc:Name del item - No existe el tag o el dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2144', '2', 'cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2145', '2', 'El tipo de afectacion del IGV es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2146', '2', 'El Nombre Internacional debe ser VAT', '\0');
INSERT INTO `err_error_code` VALUES ('2147', '2', 'El sistema de calculo del ISC es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2148', '2', 'El Nombre Internacional debe ser EXC ', '\0');
INSERT INTO `err_error_code` VALUES ('2149', '2', 'El dato ingresado en PayableAmount no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2150', '2', 'El valor ingresado en AdditionalMonetaryTotal/cbc:ID es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2151', '2', 'AdditionalMonetaryTotal/cbc:ID debe tener valor ', '\0');
INSERT INTO `err_error_code` VALUES ('2152', '2', 'Es obligatorio al menos un AdditionalInformation', '\0');
INSERT INTO `err_error_code` VALUES ('2153', '2', 'Error al procesar la Nota de Credito', '\0');
INSERT INTO `err_error_code` VALUES ('2154', '2', 'TaxAmount - El dato ingresado en impuestos globales no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2155', '2', 'El XML no contiene el tag TaxAmount de impuestos globales', '\0');
INSERT INTO `err_error_code` VALUES ('2156', '2', 'TaxScheme ID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2157', '2', 'El codigo del tributo es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2158', '2', 'El XML no contiene el tag o no existe informacion de TaxScheme ID de impuestos globales ', '\0');
INSERT INTO `err_error_code` VALUES ('2159', '2', 'TaxScheme Name - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2160', '2', 'El XML no contiene el tag o no existe informacion de TaxScheme Name de impuestos globales ', '\0');
INSERT INTO `err_error_code` VALUES ('2161', '2', 'CustomizationID - La version del documento no es correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2162', '2', 'El XML no contiene el tag o no existe informacion de CustomizationID', '\0');
INSERT INTO `err_error_code` VALUES ('2163', '2', 'UBLVersionID - La versión del UBL no es correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2164', '2', 'El XML no contiene el tag o no existe informacion de UBLVersionID', '\0');
INSERT INTO `err_error_code` VALUES ('2165', '2', 'Error al procesar la Nota de Debito', '\0');
INSERT INTO `err_error_code` VALUES ('2166', '2', 'RegistrationName - El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2167', '2', 'El XML no contiene el tag RegistrationName del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('2168', '2', 'DocumentCurrencyCode -  El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2169', '2', 'El XML no contiene el tag o no existe informacion de DocumentCurrencyCode', '\0');
INSERT INTO `err_error_code` VALUES ('2170', '2', 'ReferenceID - El dato ingresado debe indicar SERIE-CORRELATIVO del documento al que se relaciona la Nota ', '\0');
INSERT INTO `err_error_code` VALUES ('2171', '2', 'El XML no contiene informacion en el tag ReferenceID del documento al que se relaciona la nota ', '\0');
INSERT INTO `err_error_code` VALUES ('2172', '2', 'ResponseCode - El dato ingresado no cumple con la estructura ', '\0');
INSERT INTO `err_error_code` VALUES ('2173', '2', 'El XML no contiene el tag o no existe informacion de ResponseCode ', '\0');
INSERT INTO `err_error_code` VALUES ('2174', '2', 'cac:DiscrepancyResponse/cbc:Description - El dato ingresado no cumple con la estructura ', '\0');
INSERT INTO `err_error_code` VALUES ('2175', '2', 'El XML no contiene el tag o no existe informacion de cac:DiscrepancyResponse/cbc:Description ', '\0');
INSERT INTO `err_error_code` VALUES ('2176', '2', 'AdditionalAccountID - El dato ingresado en el tipo de documento de identidad del receptor no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2177', '2', 'El XML no contiene el tag o no existe informacion de AdditionalAccountID del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2178', '2', 'CustomerAssignedAccountID - El numero de documento de identidad del receptor debe ser RUC. ', '\0');
INSERT INTO `err_error_code` VALUES ('2179', '2', 'El XML no contiene el tag o no existe informacion de CustomerAssignedAccountID del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2180', '2', 'RegistrationName - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2181', '2', 'El XML no contiene el tag o no existe informacion de RegistrationName del receptor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2182', '2', 'TaxScheme ID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2183', '2', 'El codigo del tributo es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2184', '2', 'El XML no contiene el tag o no existe informacion de TaxScheme ID de impuestos globales ', '\0');
INSERT INTO `err_error_code` VALUES ('2185', '2', 'TaxScheme Name - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2186', '2', 'El XML no contiene el tag o no existe informacion de TaxScheme Name de impuestos globales ', '\0');
INSERT INTO `err_error_code` VALUES ('2187', '2', 'El Numero de orden del item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2188', '2', 'DebitedQuantity/@unitCode El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2189', '2', 'DebitedQuantity El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2190', '2', 'El XML no contiene el tag Price/cbc:PriceAmount en el detalle de los Items ', '\0');
INSERT INTO `err_error_code` VALUES ('2191', '2', 'El XML no contiene el tag Price/cbc:LineExtensionAmount en el detalle de los Items', '\0');
INSERT INTO `err_error_code` VALUES ('2192', '2', 'EL PriceTypeCode debe tener el valor 01 ', '\0');
INSERT INTO `err_error_code` VALUES ('2193', '2', 'cac:TaxCategory/cac:TaxScheme/cbc:ID El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2194', '2', 'El codigo del tributo es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2195', '2', 'cac:TaxScheme/cbc:Name del item - No existe el tag o el dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2196', '2', 'cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2197', '2', 'El tipo de afectacion del IGV es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2198', '2', 'El Nombre Internacional debe ser VAT', '\0');
INSERT INTO `err_error_code` VALUES ('2199', '2', 'El sistema de calculo del ISC es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2200', '2', 'El Nombre Internacional debe ser EXC', '\0');
INSERT INTO `err_error_code` VALUES ('2201', '2', 'El tag cac:RequestedMonetaryTotal/cbc:PayableAmount debe tener informacion valida', '\0');
INSERT INTO `err_error_code` VALUES ('2202', '2', 'TaxAmount - El dato ingresado en impuestos globales no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('2203', '2', 'El XML no contiene el tag TaxAmount de impuestos globales', '\0');
INSERT INTO `err_error_code` VALUES ('2204', '2', 'El tipo de documento modificado por la Nota de Debito debe ser factura electronica o ticket ', '\0');
INSERT INTO `err_error_code` VALUES ('2205', '2', 'La serie o numero del documento modificado por la Nota de Debito no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2206', '2', 'Debe indicar los documentos afectados por la Nota de Debito ', '\0');
INSERT INTO `err_error_code` VALUES ('2207', '2', 'La factura relacionada en la nota de debito se encuentra de baja ', '\0');
INSERT INTO `err_error_code` VALUES ('2208', '2', 'La factura relacionada en la nota de debito esta registrada como rechazada ', '\0');
INSERT INTO `err_error_code` VALUES ('2209', '2', 'La factura relacionada en la Nota de debito no esta registrada ', '\0');
INSERT INTO `err_error_code` VALUES ('2210', '2', 'El dato ingresado no cumple con el formato RC-fecha-correlativo ', '\0');
INSERT INTO `err_error_code` VALUES ('2211', '2', 'El XML no contiene el tag ID ', '\0');
INSERT INTO `err_error_code` VALUES ('2212', '2', 'UBLVersionID - La versión del UBL del resumen de boletas no es correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2213', '2', 'El XML no contiene el tag UBLVersionID', '\0');
INSERT INTO `err_error_code` VALUES ('2214', '2', 'CustomizationID - La versión del resumen de boletas no es correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2215', '2', 'El XML no contiene el tag CustomizationID', '\0');
INSERT INTO `err_error_code` VALUES ('2216', '2', 'CustomerAssignedAccountID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2217', '2', 'El XML no contiene el tag CustomerAssignedAccountID del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2218', '2', 'AdditionalAccountID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2219', '2', 'El XML no contiene el tag AdditionalAccountID del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2220', '2', 'El ID debe coincidir con el nombre del archivo ', '\0');
INSERT INTO `err_error_code` VALUES ('2221', '2', 'El RUC debe coincidir con el RUC del nombre del archivo ', '\0');
INSERT INTO `err_error_code` VALUES ('2222', '2', 'El contribuyente no está autorizado a emitir comprobantes electronicos', '\0');
INSERT INTO `err_error_code` VALUES ('2223', '2', 'El archivo ya fue presentado anteriormente ', '\0');
INSERT INTO `err_error_code` VALUES ('2224', '2', 'Numero de RUC SOL no coincide con RUC emisor', '\0');
INSERT INTO `err_error_code` VALUES ('2225', '2', 'Numero de RUC del emisor no existe', '\0');
INSERT INTO `err_error_code` VALUES ('2226', '2', 'El contribuyente no esta activo', '\0');
INSERT INTO `err_error_code` VALUES ('2227', '2', 'El contribuyente no cumple con tipo de empresa o tributos requeridos', '\0');
INSERT INTO `err_error_code` VALUES ('2228', '2', 'RegistrationName - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2229', '2', 'El XML no contiene el tag RegistrationName del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2230', '2', 'IssueDate - El dato ingresado no cumple con el patron YYYY-MM-DD', '\0');
INSERT INTO `err_error_code` VALUES ('2231', '2', 'El XML no contiene el tag IssueDate', '\0');
INSERT INTO `err_error_code` VALUES ('2232', '2', 'IssueDate- El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2233', '2', 'ReferenceDate - El dato ingresado no cumple con el patron YYYY-MM-DD ', '\0');
INSERT INTO `err_error_code` VALUES ('2234', '2', 'El XML no contiene el tag ReferenceDate', '\0');
INSERT INTO `err_error_code` VALUES ('2235', '2', 'ReferenceDate- El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2236', '2', 'La fecha del IssueDate no debe ser mayor al Today ', '\0');
INSERT INTO `err_error_code` VALUES ('2237', '2', 'La fecha del ReferenceDate no debe ser mayor al Today ', '\0');
INSERT INTO `err_error_code` VALUES ('2238', '2', 'LineID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2239', '2', 'LineID - El dato ingresado debe ser correlativo mayor a cero ', '\0');
INSERT INTO `err_error_code` VALUES ('2240', '2', 'El XML no contiene el tag LineID de SummaryDocumentsLine ', '\0');
INSERT INTO `err_error_code` VALUES ('2241', '2', 'DocumentTypeCode - El valor del tipo de documento es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2242', '2', 'El XML no contiene el tag DocumentTypeCode ', '\0');
INSERT INTO `err_error_code` VALUES ('2243', '2', 'El dato ingresado no cumple con el patron SERIE ', '\0');
INSERT INTO `err_error_code` VALUES ('2244', '2', 'El XML no contiene el tag DocumentSerialID ', '\0');
INSERT INTO `err_error_code` VALUES ('2245', '2', 'El dato ingresado en StartDocumentNumberID debe ser numerico ', '\0');
INSERT INTO `err_error_code` VALUES ('2246', '2', 'El XML no contiene el tag StartDocumentNumberID ', '\0');
INSERT INTO `err_error_code` VALUES ('2247', '2', 'El dato ingresado en sac:EndDocumentNumberID debe ser numerico ', '\0');
INSERT INTO `err_error_code` VALUES ('2248', '2', 'El XML no contiene el tag sac:EndDocumentNumberID ', '\0');
INSERT INTO `err_error_code` VALUES ('2249', '2', 'Los rangos deben ser mayores a cero ', '\0');
INSERT INTO `err_error_code` VALUES ('2250', '2', 'En el rango de comprobantes, el EndDocumentNumberID debe ser mayor o igual al StartInvoiceNumberID ', '\0');
INSERT INTO `err_error_code` VALUES ('2251', '2', 'El dato ingresado en TotalAmount debe ser numerico mayor o igual a cero ', '\0');
INSERT INTO `err_error_code` VALUES ('2252', '2', 'El XML no contiene el tag TotalAmount', '\0');
INSERT INTO `err_error_code` VALUES ('2253', '2', 'El dato ingresado en TotalAmount debe ser numerico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2254', '2', 'PaidAmount - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2255', '2', 'El XML no contiene el tag PaidAmount ', '\0');
INSERT INTO `err_error_code` VALUES ('2256', '2', 'InstructionID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2257', '2', 'El XML no contiene el tag InstructionID ', '\0');
INSERT INTO `err_error_code` VALUES ('2258', '2', 'Debe indicar Referencia de Importes asociados a las boletas de venta', '\0');
INSERT INTO `err_error_code` VALUES ('2259', '2', 'Debe indicar 3 Referencias de Importes asociados a las boletas de venta ', '\0');
INSERT INTO `err_error_code` VALUES ('2260', '2', 'PaidAmount - El dato ingresado debe ser mayor o igual a 0.00', '\0');
INSERT INTO `err_error_code` VALUES ('2261', '2', 'cbc:Amount - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2262', '2', 'El XML no contiene el tag cbc:Amount', '\0');
INSERT INTO `err_error_code` VALUES ('2263', '2', 'ChargeIndicator - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2264', '2', 'El XML no contiene el tag ChargeIndicator', '\0');
INSERT INTO `err_error_code` VALUES ('2265', '2', 'Debe indicar Información acerca del Importe Total de Otros Cargos ', '\0');
INSERT INTO `err_error_code` VALUES ('2266', '2', 'Debe indicar cargos mayores o iguales a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2267', '2', 'TaxScheme ID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2268', '2', 'El codigo del tributo es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2269', '2', 'El XML no contiene el tag TaxScheme ID de Información acerca del importe total de un tipo particular de impuesto ', '\0');
INSERT INTO `err_error_code` VALUES ('2270', '2', 'TaxScheme Name - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2271', '2', 'El XML no contiene el tag TaxScheme Name de impuesto ', '\0');
INSERT INTO `err_error_code` VALUES ('2272', '2', 'TaxScheme TaxTypeCode - El dato ingresado no cumple con el estandar', '\0');
INSERT INTO `err_error_code` VALUES ('2273', '2', 'TaxAmount - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2274', '2', 'El XML no contiene el tag TaxAmount', '\0');
INSERT INTO `err_error_code` VALUES ('2275', '2', 'Si el codigo de tributo es 2000, el nombre del tributo debe ser ISC ', '\0');
INSERT INTO `err_error_code` VALUES ('2276', '2', 'Si el codigo de tributo es 1000, el nombre del tributo debe ser IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2277', '2', 'No se ha consignado ninguna informacion del importe total de tributos ', '\0');
INSERT INTO `err_error_code` VALUES ('2278', '2', 'Debe indicar Información acerca del importe total de ISC e IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2279', '2', 'Debe indicar Items de consolidado de documentos', '\0');
INSERT INTO `err_error_code` VALUES ('2280', '2', 'Existen problemas con la informacion del resumen de comprobantes', '\0');
INSERT INTO `err_error_code` VALUES ('2281', '2', 'Error en la validacion de los rangos de los comprobantes', '\0');
INSERT INTO `err_error_code` VALUES ('2282', '2', 'Existe documento ya informado anteriormente ', '\0');
INSERT INTO `err_error_code` VALUES ('2283', '2', 'El dato ingresado no cumple con el formato RA-fecha-correlativo ', '\0');
INSERT INTO `err_error_code` VALUES ('2284', '2', 'El XML no contiene el tag ID', '\0');
INSERT INTO `err_error_code` VALUES ('2285', '2', 'El ID debe coincidir con el nombre del archivo ', '\0');
INSERT INTO `err_error_code` VALUES ('2286', '2', 'El RUC debe coincidir con el RUC del nombre del archivo ', '\0');
INSERT INTO `err_error_code` VALUES ('2287', '2', 'AdditionalAccountID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2288', '2', 'El XML no contiene el tag AdditionalAccountID del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2289', '2', 'CustomerAssignedAccountID - El dato ingresado no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('2290', '2', 'El XML no contiene el tag CustomerAssignedAccountID del emisor del documento', '\0');
INSERT INTO `err_error_code` VALUES ('2291', '2', 'El contribuyente no esta autorizado a emitir comprobantes electrónicos', '\0');
INSERT INTO `err_error_code` VALUES ('2292', '2', 'Numero de RUC SOL no coincide con RUC emisor', '\0');
INSERT INTO `err_error_code` VALUES ('2293', '2', 'Numero de RUC del emisor no existe', '\0');
INSERT INTO `err_error_code` VALUES ('2294', '2', 'El contribuyente no esta activo', '\0');
INSERT INTO `err_error_code` VALUES ('2295', '2', 'El contribuyente no cumple con tipo de empresa o tributos requeridos', '\0');
INSERT INTO `err_error_code` VALUES ('2296', '2', 'RegistrationName - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2297', '2', 'El XML no contiene el tag RegistrationName del emisor del documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2298', '2', 'IssueDate - El dato ingresado no cumple con el patron YYYY-MM-DD', '\0');
INSERT INTO `err_error_code` VALUES ('2299', '2', 'El XML no contiene el tag IssueDate ', '\0');
INSERT INTO `err_error_code` VALUES ('2300', '2', 'IssueDate - El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2301', '2', 'La fecha del IssueDate no debe ser mayor al Today ', '\0');
INSERT INTO `err_error_code` VALUES ('2302', '2', 'ReferenceDate - El dato ingresado no cumple con el patron YYYY-MM-DD ', '\0');
INSERT INTO `err_error_code` VALUES ('2303', '2', 'El XML no contiene el tag ReferenceDate', '\0');
INSERT INTO `err_error_code` VALUES ('2304', '2', 'ReferenceDate - El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2305', '2', 'LineID - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2306', '2', 'LineID - El dato ingresado debe ser correlativo mayor a cero ', '\0');
INSERT INTO `err_error_code` VALUES ('2307', '2', 'El XML no contiene el tag LineID de VoidedDocumentsLine ', '\0');
INSERT INTO `err_error_code` VALUES ('2308', '2', 'DocumentTypeCode - El valor del tipo de documento es invalido ', '\0');
INSERT INTO `err_error_code` VALUES ('2309', '2', 'El XML no contiene el tag DocumentTypeCode ', '\0');
INSERT INTO `err_error_code` VALUES ('2310', '2', 'El dato ingresado no cumple con el patron SERIE ', '\0');
INSERT INTO `err_error_code` VALUES ('2311', '2', 'El XML no contiene el tag DocumentSerialID ', '\0');
INSERT INTO `err_error_code` VALUES ('2312', '2', 'El dato ingresado en DocumentNumberID debe ser numerico y como maximo de 8 digitos ', '\0');
INSERT INTO `err_error_code` VALUES ('2313', '2', 'El XML no contiene el tag DocumentNumberID ', '\0');
INSERT INTO `err_error_code` VALUES ('2314', '2', 'El dato ingresado en VoidReasonDescription debe contener información válida ', '\0');
INSERT INTO `err_error_code` VALUES ('2315', '2', 'El XML no contiene el tag VoidReasonDescription ', '\0');
INSERT INTO `err_error_code` VALUES ('2316', '2', 'Debe indicar Items en VoidedDocumentsLine', '\0');
INSERT INTO `err_error_code` VALUES ('2317', '2', 'Error al procesar el resumen de anulados', '\0');
INSERT INTO `err_error_code` VALUES ('2318', '2', 'CustomizationID - La version del documento no es correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2319', '2', 'El XML no contiene el tag CustomizationID', '\0');
INSERT INTO `err_error_code` VALUES ('2320', '2', 'UBLVersionID - La version del UBL  no es la correcta', '\0');
INSERT INTO `err_error_code` VALUES ('2321', '2', 'El XML no contiene el tag UBLVersionID', '\0');
INSERT INTO `err_error_code` VALUES ('2322', '2', 'Error en la validacion de los rangos', '\0');
INSERT INTO `err_error_code` VALUES ('2323', '2', 'Existe documento ya informado anteriormente en una comunicacion de baja ', '\0');
INSERT INTO `err_error_code` VALUES ('2324', '2', 'El archivo de comunicacion de baja ya fue presentado anteriormente ', '\0');
INSERT INTO `err_error_code` VALUES ('2325', '2', 'El certificado usado no es el comunicado a SUNAT ', '\0');
INSERT INTO `err_error_code` VALUES ('2326', '2', 'El certificado usado se encuentra de baja ', '\0');
INSERT INTO `err_error_code` VALUES ('2327', '2', 'El certificado usado no se encuentra vigente ', '\0');
INSERT INTO `err_error_code` VALUES ('2328', '2', 'El certificado usado se encuentra revocado ', '\0');
INSERT INTO `err_error_code` VALUES ('2329', '2', 'La fecha de emision se encuentra fuera del limite permitido ', '\0');
INSERT INTO `err_error_code` VALUES ('2330', '2', 'La fecha de generación de la comunicación debe ser igual a la fecha consignada en el nombre del archivo ', '\0');
INSERT INTO `err_error_code` VALUES ('2331', '2', 'Número de RUC del nombre del archivo no coincide con el consignado en el contenido del archivo XML', '\0');
INSERT INTO `err_error_code` VALUES ('2332', '2', 'Número de Serie del nombre del archivo no coincide con el consignado en el contenido del archivo XML', '\0');
INSERT INTO `err_error_code` VALUES ('2333', '2', 'Número de documento en el nombre del archivo no coincide con el consignado en el contenido del XML', '\0');
INSERT INTO `err_error_code` VALUES ('2334', '2', 'El documento electrónico ingresado ha sido alterado ', '\0');
INSERT INTO `err_error_code` VALUES ('2335', '2', 'El documento electrónico ingresado ha sido alterado ', '\0');
INSERT INTO `err_error_code` VALUES ('2336', '2', 'Ocurrió un error en el proceso de validación de la firma digital ', '\0');
INSERT INTO `err_error_code` VALUES ('2337', '2', 'La moneda debe ser la misma en todo el documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2338', '2', 'La moneda debe ser la misma en todo el documento ', '\0');
INSERT INTO `err_error_code` VALUES ('2339', '2', 'El dato ingresado en PayableAmount no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2340', '2', 'El valor ingresado en AdditionalMonetaryTotal/cbc:ID es incorrecto ', '\0');
INSERT INTO `err_error_code` VALUES ('2341', '2', 'AdditionalMonetaryTotal/cbc:ID debe tener valor ', '\0');
INSERT INTO `err_error_code` VALUES ('2342', '2', 'Fecha de emision de la factura no coincide con la informada en la comunicacion ', '\0');
INSERT INTO `err_error_code` VALUES ('2343', '2', 'cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount - El dato ingresado no cumple con el estandar ', '\0');
INSERT INTO `err_error_code` VALUES ('2344', '2', 'El XML no contiene el tag cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount', '\0');
INSERT INTO `err_error_code` VALUES ('2345', '2', 'La serie no corresponde al tipo de comprobante ', '\0');
INSERT INTO `err_error_code` VALUES ('2346', '2', 'La fecha de generación del resumen debe ser igual a la fecha consignada en el nombre del archivo ', '\0');
INSERT INTO `err_error_code` VALUES ('2347', '2', 'Los rangos informados en el archivo XML se encuentran duplicados o superpuestos', '\0');
INSERT INTO `err_error_code` VALUES ('2348', '2', 'Los documentos informados en el archivo XML se encuentran duplicados', '\0');
INSERT INTO `err_error_code` VALUES ('2349', '2', 'Debe consignar solo un elemento sac:AdditionalMonetaryTotal con cbc:ID igual a 1001 ', '\0');
INSERT INTO `err_error_code` VALUES ('2350', '2', 'Debe consignar solo un elemento sac:AdditionalMonetaryTotal con cbc:ID igual a 1002 ', '\0');
INSERT INTO `err_error_code` VALUES ('2351', '2', 'Debe consignar solo un elemento sac:AdditionalMonetaryTotal con cbc:ID igual a 1003 ', '\0');
INSERT INTO `err_error_code` VALUES ('2352', '2', 'Debe consignar solo un elemento cac:TaxTotal a nivel global para IGV (cbc:ID igual a 1000) ', '\0');
INSERT INTO `err_error_code` VALUES ('2353', '2', 'Debe consignar solo un elemento cac:TaxTotal a nivel global para ISC (cbc:ID igual a 2000) ', '\0');
INSERT INTO `err_error_code` VALUES ('2354', '2', 'Debe consignar solo un elemento cac:TaxTotal a nivel global para Otros (cbc:ID igual a 9999) ', '\0');
INSERT INTO `err_error_code` VALUES ('2355', '2', 'Debe consignar solo un elemento cac:TaxTotal a nivel de item para IGV (cbc:ID igual a 1000) ', '\0');
INSERT INTO `err_error_code` VALUES ('2356', '2', 'Debe consignar solo un elemento cac:TaxTotal a nivel de item para ISC (cbc:ID igual a 2000) ', '\0');
INSERT INTO `err_error_code` VALUES ('2357', '2', 'Debe consignar solo un elemento sac:BillingPayment a nivel de item con cbc:InstructionID igual a 01 ', '\0');
INSERT INTO `err_error_code` VALUES ('2358', '2', 'Debe consignar solo un elemento sac:BillingPayment a nivel de item con cbc:InstructionID igual a 02 ', '\0');
INSERT INTO `err_error_code` VALUES ('2359', '2', 'Debe consignar solo un elemento sac:BillingPayment a nivel de item con cbc:InstructionID igual a 03 ', '\0');
INSERT INTO `err_error_code` VALUES ('2360', '2', 'Debe consignar solo un elemento sac:BillingPayment a nivel de item con cbc:InstructionID igual a 04', '\0');
INSERT INTO `err_error_code` VALUES ('2361', '2', 'Debe consignar solo un elemento cac:TaxTotal a nivel de item para Otros (cbc:ID igual a 9999) ', '\0');
INSERT INTO `err_error_code` VALUES ('2362', '2', 'Debe consignar solo un tag cac:AccountingSupplierParty/cbc:AdditionalAccountID ', '\0');
INSERT INTO `err_error_code` VALUES ('2363', '2', 'Debe consignar solo un tag cac:AccountingCustomerParty/cbc:AdditionalAccountID ', '\0');
INSERT INTO `err_error_code` VALUES ('2364', '2', 'El comprobante contiene un tipo y número de Guía de Remisión repetido ', '\0');
INSERT INTO `err_error_code` VALUES ('2365', '2', 'El comprobante contiene un tipo y número de Documento Relacionado repetido ', '\0');
INSERT INTO `err_error_code` VALUES ('2366', '2', 'El codigo en el tag sac:AdditionalProperty/cbc:ID debe tener 4 posiciones', '\0');
INSERT INTO `err_error_code` VALUES ('2367', '2', 'El dato ingresado en PriceAmount del Precio de venta unitario por item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2368', '2', 'El dato ingresado en TaxSubtotal/cbc:TaxAmount del item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2369', '2', 'El dato ingresado en PriceAmount del Valor de venta unitario por item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2370', '2', 'El dato ingresado en LineExtensionAmount del item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2371', '2', 'El XML no contiene el tag cbc:TaxExemptionReasonCode de Afectacion al IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2372', '2', 'El tag en el item cac:TaxTotal/cbc:TaxAmount debe tener el mismo valor que cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount ', '\0');
INSERT INTO `err_error_code` VALUES ('2373', '2', 'Si existe monto de ISC en el ITEM debe especificar el sistema de calculo ', '\0');
INSERT INTO `err_error_code` VALUES ('2374', '2', 'La factura a dar de baja tiene una fecha de recepcion fuera del plazo permitido ', '\0');
INSERT INTO `err_error_code` VALUES ('2375', '2', 'Fecha de emision de la boleta no coincide con la fecha de emision consignada en la comunicacion ', '\0');
INSERT INTO `err_error_code` VALUES ('2376', '2', 'La boleta de venta a dar de baja fue informada en un resumen con fecha de recepcion fuera del plazo permitido ', '\0');
INSERT INTO `err_error_code` VALUES ('2377', '2', 'El Name o TaxTypeCode debe corresponder con el Id para el IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2378', '2', 'El Name o TaxTypeCode debe corresponder con el Id para el ISC ', '\0');
INSERT INTO `err_error_code` VALUES ('2379', '2', 'La numeracion de boleta de venta a dar de baja fue generada en una fecha fuera del plazo permitido ', '\0');
INSERT INTO `err_error_code` VALUES ('2380', '2', 'El documento tiene observaciones', '\0');
INSERT INTO `err_error_code` VALUES ('2381', '2', 'Comprobante no cumple con el Grupo 1: No todos los items corresponden a operaciones gravadas a IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2382', '2', 'Comprobante no cumple con el Grupo 2: No todos los items corresponden a operaciones inafectas o exoneradas al IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2383', '2', 'Comprobante no cumple con el Grupo 3: Falta leyenda con codigo 1002 ', '\0');
INSERT INTO `err_error_code` VALUES ('2384', '2', 'Comprobante no cumple con el Grupo 3: Existe item con operación onerosa ', '\0');
INSERT INTO `err_error_code` VALUES ('2385', '2', 'Comprobante no cumple con el Grupo 4: Debe exitir Total descuentos mayor a cero ', '\0');
INSERT INTO `err_error_code` VALUES ('2386', '2', 'Comprobante no cumple con el Grupo 5: Todos los items deben tener operaciones afectas a ISC ', '\0');
INSERT INTO `err_error_code` VALUES ('2387', '2', 'Comprobante no cumple con el Grupo 6: El monto de percepcion no existe o es cero ', '\0');
INSERT INTO `err_error_code` VALUES ('2388', '2', 'Comprobante no cumple con el Grupo 6: Todos los items deben tener código de Afectación al IGV igual a 10 ', '\0');
INSERT INTO `err_error_code` VALUES ('2389', '2', 'Comprobante no cumple con el Grupo 7: El codigo de moneda no es diferente a PEN ', '\0');
INSERT INTO `err_error_code` VALUES ('2390', '2', 'Comprobante no cumple con el Grupo 8: No todos los items corresponden a operaciones gravadas a IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2391', '2', 'Comprobante no cumple con el Grupo 9: No todos los items corresponden a operaciones inafectas o exoneradas al IGV ', '\0');
INSERT INTO `err_error_code` VALUES ('2392', '2', 'Comprobante no cumple con el Grupo 10: Falta leyenda con codigo 1002 ', '\0');
INSERT INTO `err_error_code` VALUES ('2393', '2', 'Comprobante no cumple con el Grupo 10: Existe item con operación onerosa ', '\0');
INSERT INTO `err_error_code` VALUES ('2394', '2', 'Comprobante no cumple con el Grupo 11: Debe existir Total descuentos mayor a cero ', '\0');
INSERT INTO `err_error_code` VALUES ('2395', '2', 'Comprobante no cumple con el Grupo 12: El codigo de moneda no es diferente a PEN ', '\0');
INSERT INTO `err_error_code` VALUES ('2396', '2', 'Si el monto total es mayor a S/. 700.00 debe consignar tipo y numero de documento del adquiriente ', '\0');
INSERT INTO `err_error_code` VALUES ('2397', '2', 'El tipo de documento del adquiriente no puede ser Numero de RUC ', '\0');
INSERT INTO `err_error_code` VALUES ('2398', '2', 'El documento a dar de baja se encuentra rechazado ', '\0');
INSERT INTO `err_error_code` VALUES ('2399', '2', 'El tipo de documento modificado por la Nota de credito debe ser boleta electronica', '\0');
INSERT INTO `err_error_code` VALUES ('2400', '2', 'El tipo de documento modificado por la Nota de debito debe ser boleta electronica ', '\0');
INSERT INTO `err_error_code` VALUES ('2401', '2', 'No se puede leer (parsear) el archivo XML ', '\0');
INSERT INTO `err_error_code` VALUES ('2402', '2', 'El caso de prueba no existe', '\0');
INSERT INTO `err_error_code` VALUES ('2403', '2', 'La numeracion o nombre del documento ya ha sido enviado anteriormente', '\0');
INSERT INTO `err_error_code` VALUES ('2404', '2', 'Documento afectado por la nota electronica no se encuentra autorizado ', '\0');
INSERT INTO `err_error_code` VALUES ('2405', '2', 'Contribuyente no se encuentra autorizado como emisor de boletas electronicas ', '\0');
INSERT INTO `err_error_code` VALUES ('2406', '2', 'Existe mas de un tag sac:AdditionalMonetaryTotal con el mismo ID ', '\0');
INSERT INTO `err_error_code` VALUES ('2407', '2', 'Existe mas de un tag sac:AdditionalProperty con el mismo ID ', '\0');
INSERT INTO `err_error_code` VALUES ('2408', '2', 'El dato ingresado en PriceAmount del Valor referencial unitario por item no cumple con el formato establecido ', '\0');
INSERT INTO `err_error_code` VALUES ('2409', '2', 'Existe mas de un tag cac:AlternativeConditionPrice con el mismo cbc:PriceTypeCode ', '\0');
INSERT INTO `err_error_code` VALUES ('2410', '2', 'Se ha consignado un valor invalido en el campo cbc:PriceTypeCode ', '\0');
INSERT INTO `err_error_code` VALUES ('2411', '2', 'Ha consignado mas de un elemento cac:AllowanceCharge con el mismo campo cbc:ChargeIndicator ', '\0');
INSERT INTO `err_error_code` VALUES ('2412', '2', 'Se ha consignado mas de un documento afectado por la nota (tag cac:BillingReference) ', '\0');
INSERT INTO `err_error_code` VALUES ('2413', '2', 'Se ha consignado mas de un motivo o sustento de la nota (tag cac:DiscrepancyResponse/cbc:Description) ', '\0');
INSERT INTO `err_error_code` VALUES ('2414', '2', 'No se ha consignado en la nota el tag cac:DiscrepancyResponse ', '\0');
INSERT INTO `err_error_code` VALUES ('2415', '2', 'Se ha consignado en la nota mas de un tag cac:DiscrepancyResponse ', '\0');
INSERT INTO `err_error_code` VALUES ('2416', '2', 'Si existe leyenda Transferida Gratuita debe consignar Total Valor de Venta de Operaciones Gratuitas', '\0');
INSERT INTO `err_error_code` VALUES ('2417', '2', 'Debe consignar Valor Referencial unitario por ítem en operaciones no onerosas', '\0');
INSERT INTO `err_error_code` VALUES ('2418', '2', 'Si consigna Valor Referencial unitario por ítem en operaciones no onerosas, la operación debe ser no onerosa', '\0');
INSERT INTO `err_error_code` VALUES ('2419', '2', 'El dato ingresado en AllowanceTotalAmount no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2420', '2', 'Ya transcurrieron mas de 25 dias calendarios para concluir con su proceso de homologacion', '\0');
INSERT INTO `err_error_code` VALUES ('2421', '2', 'Debe indicar  toda la información de  sustento de traslado de bienes', '\0');
INSERT INTO `err_error_code` VALUES ('2422', '2', 'El valor unitario debe ser menor al precio unitario', '\0');
INSERT INTO `err_error_code` VALUES ('2423', '2', 'Si ha consignado monto ISC a nivel de ítem, debe consignar un monto a nivel de total', '\0');
INSERT INTO `err_error_code` VALUES ('2424', '2', 'RC Debe consignar solo un elemento sac:BillingPayment a nivel de ítem con cbc:InstructionID igual a 05', '\0');
INSERT INTO `err_error_code` VALUES ('2425', '2', 'Si la  operación es gratuita PriceTypeCode =02 y cbc:PriceAmount> 0 el código de afectación de igv debe ser  no onerosa es  decir diferente de 10,20,30', '\0');
INSERT INTO `err_error_code` VALUES ('2426', '2', 'Documentos relacionados duplicados en el comprobante', '\0');
INSERT INTO `err_error_code` VALUES ('2427', '2', 'Solo debe de existir un tag AdditionalInformation', '\0');
INSERT INTO `err_error_code` VALUES ('2428', '2', 'Comprobante no cumple con grupo de facturas con detracciones', '\0');
INSERT INTO `err_error_code` VALUES ('2429', '2', 'Comprobante no cumple con grupo de facturas con comercio exterior', '\0');
INSERT INTO `err_error_code` VALUES ('2430', '2', 'Comprobante no cumple con grupo de facturas con tag de factura guía', '\0');
INSERT INTO `err_error_code` VALUES ('2431', '2', 'Comprobante no cumple con grupo de facturas con tags no tributarios', '\0');
INSERT INTO `err_error_code` VALUES ('2432', '2', 'Comprobante no cumple con grupo de boletas con tags no tributarios', '\0');
INSERT INTO `err_error_code` VALUES ('2433', '2', 'Comprobante no cumple con grupo de facturas con tag venta itinerante', '\0');
INSERT INTO `err_error_code` VALUES ('2434', '2', 'Comprobante no cumple con grupo de boletas con tag venta itinerante', '\0');
INSERT INTO `err_error_code` VALUES ('2435', '2', 'Comprobante no cumple con grupo de boletas con ISC', '\0');
INSERT INTO `err_error_code` VALUES ('2436', '2', 'Comprobante no cumple con el grupo de boletas de venta con percepción: El monto de percepción no existe o es cero', '\0');
INSERT INTO `err_error_code` VALUES ('2437', '2', 'Comprobante no cumple con el grupo de boletas de venta con percepción: Todos los ítems deben tener código de Afectación al IGV igual a 10', '\0');
INSERT INTO `err_error_code` VALUES ('2438', '2', 'Comprobante no cumple con grupo de facturas con tag venta anticipada I', '\0');
INSERT INTO `err_error_code` VALUES ('2439', '2', 'Comprobante no cumple con grupo de facturas con tag venta anticipada II', '\0');
INSERT INTO `err_error_code` VALUES ('2500', '2', 'Ingresar descripción y valor venta por ítem para documento de anticipos', '\0');
INSERT INTO `err_error_code` VALUES ('2501', '2', 'Valor venta debe ser mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2502', '2', 'Los valores totales deben ser mayores a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2503', '2', 'PaidAmount: monto anticipado por documento debe ser mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2504', '2', 'Falta referencia de la factura relacionada con anticipo', '\0');
INSERT INTO `err_error_code` VALUES ('2505', '2', 'cac:PrepaidPayment/cbc:ID/@SchemaID: Código de referencia debe ser 02 o 03', '\0');
INSERT INTO `err_error_code` VALUES ('2506', '2', 'cac:PrepaidPayment/cbc:ID: Factura o boleta no existe o comunicada de Baja', '\0');
INSERT INTO `err_error_code` VALUES ('2507', '2', 'Factura relacionada con anticipo no corresponde como factura de anticipo', '\0');
INSERT INTO `err_error_code` VALUES ('2508', '2', 'Ingresar documentos por anticipos', '\0');
INSERT INTO `err_error_code` VALUES ('2509', '2', 'Total de anticipos diferente a los montos anticipados por documento', '\0');
INSERT INTO `err_error_code` VALUES ('2510', '2', 'Nro nombre del documento no tiene el formato correcto', '\0');
INSERT INTO `err_error_code` VALUES ('2511', '2', 'El tipo de documento no es aceptado', '\0');
INSERT INTO `err_error_code` VALUES ('2512', '2', 'No existe información de serie o número', '\0');
INSERT INTO `err_error_code` VALUES ('2513', '2', 'Dato no cumple con formato de acuerdo al número de comprobante', '\0');
INSERT INTO `err_error_code` VALUES ('2514', '2', 'No existe información de receptor de documento', '\0');
INSERT INTO `err_error_code` VALUES ('2515', '2', 'Dato ingresado no cumple con catalogo 6', '\0');
INSERT INTO `err_error_code` VALUES ('2516', '2', 'Debe indicar tipo de documento', '\0');
INSERT INTO `err_error_code` VALUES ('2517', '2', 'Dato no cumple con formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2518', '2', 'Calculo IGV no es correcto', '\0');
INSERT INTO `err_error_code` VALUES ('2519', '2', 'El importe total no coincide con la sumatoria de los valores de venta mas los tributos mas los cargos', '\0');
INSERT INTO `err_error_code` VALUES ('2520', '2', 'cac:PrepaidPayment/cbc:InstructionID/@SchemaID – El tipo documento debe ser 6 del catálogo de tipo de documento', '\0');
INSERT INTO `err_error_code` VALUES ('2521', '2', 'cac:PrepaidPayment/cbc:ID - El dato ingresado debe indicar SERIE-CORRELATIVO del documento que se realizó el anticipo', '\0');
INSERT INTO `err_error_code` VALUES ('2522', '2', 'No existe información del documento del anticipo.', '\0');
INSERT INTO `err_error_code` VALUES ('2523', '2', 'GrossWeightMeasure – El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2524', '2', 'El dato ingresado en Amount no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2525', '2', 'El dato ingresado en Quantity no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2526', '2', 'El dato ingresado en Percent no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2527', '2', 'PrepaidAmount: Monto total anticipado debe ser mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2528', '2', 'cac:OriginatorDocumentReference/cbc:ID/@SchemaID – El tipo documento debe ser 6 del catálogo de tipo de documento', '\0');
INSERT INTO `err_error_code` VALUES ('2529', '2', 'RUC que emitió documento de anticipo, no existe', '\0');
INSERT INTO `err_error_code` VALUES ('2530', '2', 'RUC que solicita la emisión de la factura, no existe', '\0');
INSERT INTO `err_error_code` VALUES ('2531', '2', 'Código del Local Anexo del emisor no existe', '\0');
INSERT INTO `err_error_code` VALUES ('2532', '2', 'No existe información de modalidad de transporte', '\0');
INSERT INTO `err_error_code` VALUES ('2533', '2', 'Si ha consignado Transporte Privado, debe consignar Licencia de conducir, Placa, N constancia de inscripción y marca del vehículo', '\0');
INSERT INTO `err_error_code` VALUES ('2534', '2', 'Si ha consignado Transporte púbico, debe consignar Datos del transportista', '\0');
INSERT INTO `err_error_code` VALUES ('2535', '2', 'La nota de crédito por otros conceptos tributarios debe tener Otros Documentos Relacionados', '\0');
INSERT INTO `err_error_code` VALUES ('2536', '2', 'Serie y número no se encuentra registrado como baja por cambio de destinatario', '\0');
INSERT INTO `err_error_code` VALUES ('2537', '2', 'cac:OrderReference/cac:DocumentReference/cbc:DocumentTypeCode - El tipo de documento de serie y número dado de baja es incorrecta', '\0');
INSERT INTO `err_error_code` VALUES ('2538', '2', 'El contribuyente no se encuentra autorizado como emisor electrónico de Guía o de factura o de boleta Factura GEM', '\0');
INSERT INTO `err_error_code` VALUES ('2539', '2', 'El contribuyente no está activo', '\0');
INSERT INTO `err_error_code` VALUES ('2540', '2', 'El contribuyente no está habido', '\0');
INSERT INTO `err_error_code` VALUES ('2541', '2', 'El XML no contiene el tag o no existe información del tipo de documento identidad del remitente', '\0');
INSERT INTO `err_error_code` VALUES ('2542', '2', 'cac:DespatchSupplierParty/cbc:CustomerAssignedAccountID@schemeID - El valor ingresado como tipo de documento identidad del remitente es incorrecta', '\0');
INSERT INTO `err_error_code` VALUES ('2543', '2', 'El XML no contiene el tag o no existe información de la dirección completa y detallada en domicilio fiscal', '\0');
INSERT INTO `err_error_code` VALUES ('2544', '2', 'El XML no contiene el tag o no existe información de la provincia en domicilio fiscal', '\0');
INSERT INTO `err_error_code` VALUES ('2545', '2', 'El XML no contiene el tag o no existe información del departamento en domicilio fiscal', '\0');
INSERT INTO `err_error_code` VALUES ('2546', '2', 'El XML no contiene el tag o no existe información del distrito en domicilio fiscal', '\0');
INSERT INTO `err_error_code` VALUES ('2547', '2', 'El XML no contiene el tag o no existe información del país en domicilio fiscal', '\0');
INSERT INTO `err_error_code` VALUES ('2548', '2', 'El valor del país inválido', '\0');
INSERT INTO `err_error_code` VALUES ('2549', '2', 'El XML no contiene el tag o no existe información del tipo de documento identidad del destinatario', '\0');
INSERT INTO `err_error_code` VALUES ('2550', '2', 'cac:DeliveryCustomerParty/cbc:CustomerAssignedAccountID@schemeID - El dato ingresado de tipo de documento identidad del destinatario no cumple con el estándar', '\0');
INSERT INTO `err_error_code` VALUES ('2551', '2', 'El XML no contiene el tag o no existe información de CustomerAssignedAccountID del proveedor de servicios', '\0');
INSERT INTO `err_error_code` VALUES ('2552', '2', 'El XML no contiene el tag o no existe información del tipo de documento identidad del proveedor', '\0');
INSERT INTO `err_error_code` VALUES ('2553', '2', 'cac:SellerSupplierParty/cbc:CustomerAssignedAccountID@schemeID - El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2554', '2', 'Para el motivo de traslado ingresado el Destinatario debe ser igual al remitente', '\0');
INSERT INTO `err_error_code` VALUES ('2555', '2', 'Destinatario no debe ser igual al remitente', '\0');
INSERT INTO `err_error_code` VALUES ('2556', '2', 'cbc:TransportModeCode -  dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2557', '2', 'La fecha del StartDate no debe ser menor al Today', '\0');
INSERT INTO `err_error_code` VALUES ('2558', '2', 'El XML no contiene el tag o no existe información en Numero de Ruc del transportista', '\0');
INSERT INTO `err_error_code` VALUES ('2559', '2', '/DespatchAdvice/cac:Shipment/cac:ShipmentStage/cac:CarrierParty/cac:PartyIdentification/cbc:ID  - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2560', '2', 'Transportista  no debe ser igual al remitente o destinatario', '\0');
INSERT INTO `err_error_code` VALUES ('2561', '2', 'El XML no contiene el tag o no existe información del tipo de documento identidad del transportista', '\0');
INSERT INTO `err_error_code` VALUES ('2562', '2', '/DespatchAdvice/cac:Shipment/cac:ShipmentStage/cac:CarrierParty/cac:PartyIdentification/cbc:ID@schemeID  - El dato ingresado no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2563', '2', 'El XML no contiene el tag o no existe información de Apellido, Nombre o razón social del transportista', '\0');
INSERT INTO `err_error_code` VALUES ('2564', '2', 'Razón social transportista - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2565', '2', 'El XML no contiene el tag o no existe información del tipo de unidad de transporte', '\0');
INSERT INTO `err_error_code` VALUES ('2566', '2', 'El XML no contiene el tag o no existe información del Número de placa del vehículo', '\0');
INSERT INTO `err_error_code` VALUES ('2567', '2', 'Número de placa del vehículo - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2568', '2', 'El XML no contiene el tag o no existe información en el Numero de documento de identidad del conductor', '\0');
INSERT INTO `err_error_code` VALUES ('2569', '2', 'Documento identidad del conductor - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2570', '2', 'El XML no contiene el tag o no existe información del tipo de documento identidad del conductor', '\0');
INSERT INTO `err_error_code` VALUES ('2571', '2', 'cac:DriverPerson/ID@schemeID - El valor ingresado de tipo de documento identidad de conductor es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('2572', '2', 'El XML no contiene el tag o no existe información del Numero de licencia del conductor', '\0');
INSERT INTO `err_error_code` VALUES ('2573', '2', 'Numero de licencia del conductor - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('2574', '2', 'El XML no contiene el tag o no existe información de dirección detallada de punto de llegada', '\0');
INSERT INTO `err_error_code` VALUES ('2575', '2', 'El XML no contiene el tag o no existe información de CityName', '\0');
INSERT INTO `err_error_code` VALUES ('2576', '2', 'El XML no contiene el tag o no existe información de District', '\0');
INSERT INTO `err_error_code` VALUES ('2577', '2', 'El XML no contiene el tag o no existe información de dirección detallada de punto de partida', '\0');
INSERT INTO `err_error_code` VALUES ('2578', '2', 'El XML no contiene el tag o no existe información de CityName', '\0');
INSERT INTO `err_error_code` VALUES ('2579', '2', 'El XML no contiene el tag o no existe información de District', '\0');
INSERT INTO `err_error_code` VALUES ('2580', '2', 'El XML No contiene el tag o no existe información de la cantidad del ítem', '\0');
INSERT INTO `err_error_code` VALUES ('2600', '2', 'El comprobante fue enviado fuera del plazo permitido.', '\0');
INSERT INTO `err_error_code` VALUES ('2601', '2', 'Señor contribuyente a la fecha no se encuentra registrado ó habilitado con la condición de Agente de percepción.', '\0');
INSERT INTO `err_error_code` VALUES ('2602', '2', 'El régimen percepción enviado no corresponde con su condición de Agente de percepción.', '\0');
INSERT INTO `err_error_code` VALUES ('2603', '2', 'La tasa de percepción enviada no corresponde con el régimen de percepción.', '\0');
INSERT INTO `err_error_code` VALUES ('2604', '2', 'El Cliente no puede ser el mismo que el Emisor del comprobante de percepción.', '\0');
INSERT INTO `err_error_code` VALUES ('2605', '2', 'Número de RUC del Cliente no existe.', '\0');
INSERT INTO `err_error_code` VALUES ('2606', '2', 'Documento de identidad del Cliente no existe.', '\0');
INSERT INTO `err_error_code` VALUES ('2607', '2', 'La moneda del importe de cobro debe ser la misma que la del documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2608', '2', 'Los montos de pago, percibidos y montos cobrados consignados para el documento relacionado no son correctos.', '\0');
INSERT INTO `err_error_code` VALUES ('2609', '2', 'El comprobante electrónico enviado no se encuentra registrado en la SUNAT.', '\0');
INSERT INTO `err_error_code` VALUES ('2610', '2', 'La fecha de emisión, Importe total del comprobante y la moneda del comprobante electrónico enviado no son los registrados en los Sistemas de SUNAT.', '\0');
INSERT INTO `err_error_code` VALUES ('2611', '2', 'El comprobante electrónico no ha sido emitido al cliente.', '\0');
INSERT INTO `err_error_code` VALUES ('2612', '2', 'La fecha de cobro debe estar entre el primer día calendario del mes al cual corresponde la fecha de emisión del comprobante de percepción o desde la fecha de emisión del comprobante relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2613', '2', 'El Nro. de documento con número de cobro ya se encuentra en la Relación de Documentos Relacionados agregados.', '\0');
INSERT INTO `err_error_code` VALUES ('2614', '2', 'El Nro. de documento con el número de cobro ya se encuentra registrado como pago realizado.', '\0');
INSERT INTO `err_error_code` VALUES ('2615', '2', 'Importe total percibido debe ser igual a la suma de los importes percibidos por cada documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2616', '2', 'Importe total cobrado debe ser igual a la suma de los importe totales cobrados por cada documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2617', '2', 'Señor contribuyente a la fecha no se encuentra registrado ó habilitado con la condición de Agente de retención.', '\0');
INSERT INTO `err_error_code` VALUES ('2618', '2', 'El régimen retención enviado no corresponde con su condición de Agente de retención.', '\0');
INSERT INTO `err_error_code` VALUES ('2619', '2', 'La tasa de retención enviada no corresponde con el régimen de retención.', '\0');
INSERT INTO `err_error_code` VALUES ('2620', '2', 'El Proveedor no puede ser el mismo que el Emisor del comprobante de retención.', '\0');
INSERT INTO `err_error_code` VALUES ('2621', '2', 'Número de RUC del Proveedor no existe.', '\0');
INSERT INTO `err_error_code` VALUES ('2622', '2', 'La moneda del importe de pago debe ser la misma que la del documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2623', '2', 'Los montos de pago, retenidos y montos pagados consignados para el documento relacionado no son correctos.', '\0');
INSERT INTO `err_error_code` VALUES ('2624', '2', 'El comprobante electrónico no ha sido emitido por el proveedor.', '\0');
INSERT INTO `err_error_code` VALUES ('2625', '2', 'La fecha de pago debe estar entre el primer día calendario del mes al cual corresponde la fecha de emisión del comprobante de retención o desde la fecha de emisión del comprobante relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2626', '2', 'El Nro. de documento con el número de pago ya se encuentra en la Relación de Documentos Relacionados agregados.', '\0');
INSERT INTO `err_error_code` VALUES ('2627', '2', 'El Nro. de documento con el número de pago ya se encuentra registrado como pago realizado.', '\0');
INSERT INTO `err_error_code` VALUES ('2628', '2', 'Importe total retenido debe ser igual a la suma de los importes retenidos por cada documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2629', '2', 'Importe total pagado debe ser igual a la suma de los importes pagados por cada documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2630', '2', 'La serie o número del documento(01) modificado por la Nota de Crédito no cumple con el formato establecido para tipo código Nota Crédito 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2631', '2', 'La serie o número del documento(12) modificado por la Nota de Crédito no cumple con el formato establecido para tipo código Nota Crédito 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2632', '2', 'La serie o número del documento(56) modificado por la Nota de Crédito no cumple con el formato establecido para tipo código Nota Crédito 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2633', '2', 'La serie o número del documento(03) modificado por la Nota de Crédito no cumple con el formato establecido para tipo código Nota Crédito 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2634', '2', 'ReferenceID - El dato ingresado debe indicar serie correcta del documento al que se relaciona la Nota tipo 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2635', '2', 'Debe existir DocumentTypeCode de Otros documentos relacionados con valor 99 para un tipo código Nota Crédito 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2636', '2', 'No existe datos del ID de los documentos relacionados con valor 99 para un tipo código Nota Crédito 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2637', '2', 'No existe datos del DocumentType de los documentos relacionados con valor 99 para un tipo código Nota Crédito 10.', '\0');
INSERT INTO `err_error_code` VALUES ('2640', '2', 'Operación gratuita, solo debe consignar un monto referencial', '\0');
INSERT INTO `err_error_code` VALUES ('2641', '2', 'Operación gratuita, debe consignar Total valor venta - operaciones gratuitas mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2642', '2', 'Operaciones de exportación, deben consignar Tipo Afectación igual a 40', '\0');
INSERT INTO `err_error_code` VALUES ('2643', '2', 'Factura de operación sujeta IVAP debe consignar Monto de impuestos por ítem', '\0');
INSERT INTO `err_error_code` VALUES ('2644', '2', 'Factura de operación sujeta IVAP solo debe tener ítems con código afectación IGV 17.', '\0');
INSERT INTO `err_error_code` VALUES ('2645', '2', 'Factura de operación sujeta a IVAP debe consignar ítems con código de tributo 1000', '\0');
INSERT INTO `err_error_code` VALUES ('2646', '2', 'Factura de operación sujeta a IVAP debe consignar ítems con nombre de tributo IVAP', '\0');
INSERT INTO `err_error_code` VALUES ('2647', '2', 'Código tributo UN/ECE debe ser VAT', '\0');
INSERT INTO `err_error_code` VALUES ('2648', '2', 'Factura de operación sujeta al IVAP, solo puede consignar información para operación gravadas', '\0');
INSERT INTO `err_error_code` VALUES ('2649', '2', 'Operación sujeta al IVAP, debe consignar monto en total operaciones gravadas', '\0');
INSERT INTO `err_error_code` VALUES ('2650', '2', 'Factura de operación sujeta al IVAP , no debe consignar valor para ISC o debe ser 0', '\0');
INSERT INTO `err_error_code` VALUES ('2651', '2', 'Factura de operación sujeta al IVAP , no debe consignar valor para IGV o debe ser 0', '\0');
INSERT INTO `err_error_code` VALUES ('2652', '2', 'Factura de operación sujeta al IVAP , debe registrar mensaje 2007', '\0');
INSERT INTO `err_error_code` VALUES ('2653', '2', 'Servicios prestados No domiciliados. Total IGV debe ser mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2654', '2', 'Servicios prestados No domiciliados. Código tributo a consignar debe ser 1000', '\0');
INSERT INTO `err_error_code` VALUES ('2655', '2', 'Servicios prestados No domiciliados. El código de afectación debe ser 40', '\0');
INSERT INTO `err_error_code` VALUES ('2656', '2', 'Servicios prestados No domiciliados. Código tributo UN/ECE debe ser VAT', '\0');
INSERT INTO `err_error_code` VALUES ('2657', '2', 'El Nro. de documento <serie>-<número> ya fue utilizado en la emisión de CPE.', '\0');
INSERT INTO `err_error_code` VALUES ('2658', '2', 'El Nro. de documento <serie>-<número> no se ha informado o no se encuentra en estado Revertido', '\0');
INSERT INTO `err_error_code` VALUES ('2659', '2', 'La fecha de cobro de cada documento relacionado deben ser del mismo Periodo (mm/aaaa), asimismo estas fechas podrán ser menores o iguales a la fecha de emisión del comprobante de percepción', '\0');
INSERT INTO `err_error_code` VALUES ('2660', '2', 'Los datos del CPE revertido no corresponden a los registrados en la SUNAT', '\0');
INSERT INTO `err_error_code` VALUES ('2661', '2', 'La fecha de cobro de cada documento relacionado deben ser del mismo Periodo (mm/aaaa), asimismo estas fechas podrán ser menores o iguales a la fecha de emisión del comprobante de retención', '\0');
INSERT INTO `err_error_code` VALUES ('2662', '2', 'El Nro. de documento <serie>-<número> ya fue utilizado en la emisión de CRE.', '\0');
INSERT INTO `err_error_code` VALUES ('2663', '2', 'El documento indicado no existe no puede ser modificado/eliminado', '\0');
INSERT INTO `err_error_code` VALUES ('2664', '2', 'El cálculo de la base imponible de percepción y el monto de la percepción no coincide con el monto total informado.', '\0');
INSERT INTO `err_error_code` VALUES ('2665', '2', 'El contribuyente no se encuentra autorizado a emitir Tickets', '\0');
INSERT INTO `err_error_code` VALUES ('2666', '2', 'Las percepciones son solo válidas para boletas de venta al contado.', '\0');
INSERT INTO `err_error_code` VALUES ('2667', '2', 'Importe total percibido debe ser igual a la suma de los importes percibidos por cada documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2668', '2', 'Importe total cobrado debe ser igual a la suma de los importes cobrados por cada documento relacionado.', '\0');
INSERT INTO `err_error_code` VALUES ('2669', '2', 'El dato ingresado en TotalInvoiceAmount debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2670', '2', 'La razón social no corresponde al ruc informado.', '\0');
INSERT INTO `err_error_code` VALUES ('2671', '2', 'La fecha de generación de la comunicación debe ser mayor o igual a la fecha de generación del documento revertido.', '\0');
INSERT INTO `err_error_code` VALUES ('2672', '2', 'La fecha de generación del documento revertido debe ser menor o igual a la fecha actual.', '\0');
INSERT INTO `err_error_code` VALUES ('2673', '2', 'El dato ingresado no cumple con el formato RR-fecha-correlativo.', '\0');
INSERT INTO `err_error_code` VALUES ('2674', '2', 'El dato ingresado  no cumple con el formato de DocumentSerialID, para DocumentTypeCode con valor 20.', '\0');
INSERT INTO `err_error_code` VALUES ('2675', '2', 'El dato ingresado  no cumple con el formato de DocumentSerialID, para DocumentTypeCode con valor 40.', '\0');
INSERT INTO `err_error_code` VALUES ('2676', '2', 'El XML no contiene el tag o no existe información del número de RUC del emisor', '\0');
INSERT INTO `err_error_code` VALUES ('2677', '2', 'El valor ingresado como número de RUC del emisor es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('2678', '2', 'El XML no contiene el atributo o no existe información del tipo de documento del emisor', '\0');
INSERT INTO `err_error_code` VALUES ('2679', '2', 'El XML no contiene el tag o no existe información del número de documento de identidad del cliente', '\0');
INSERT INTO `err_error_code` VALUES ('2680', '2', 'El valor ingresado como documento de identidad del cliente es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('2681', '2', 'El XML no contiene el atributo o no existe información del tipo de documento del cliente', '\0');
INSERT INTO `err_error_code` VALUES ('2682', '2', 'El valor ingresado como tipo de documento del cliente es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('2683', '2', 'El XML no contiene el tag o no existe información del Importe total Percibido', '\0');
INSERT INTO `err_error_code` VALUES ('2684', '2', 'El XML no contiene el tag o no existe información de la moneda del Importe total Percibido', '\0');
INSERT INTO `err_error_code` VALUES ('2685', '2', 'El valor de la moneda del Importe total Percibido debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2686', '2', 'El XML no contiene el tag o no existe información del Importe total Cobrado', '\0');
INSERT INTO `err_error_code` VALUES ('2687', '2', 'El dato ingresado en SUNATTotalCashed debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2689', '2', 'El XML no contiene el tag o no existe información de la moneda del Importe total Cobrado', '\0');
INSERT INTO `err_error_code` VALUES ('2690', '2', 'El valor de la moneda del Importe total Cobrado debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2691', '2', 'El XML no contiene el tag o no existe información del tipo de documento relacionado', '\0');
INSERT INTO `err_error_code` VALUES ('2692', '2', 'El tipo de documento relacionado no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2693', '2', 'El XML no contiene el tag o no existe información del número de documento relacionado', '\0');
INSERT INTO `err_error_code` VALUES ('2694', '2', 'El número de documento relacionado no está permitido o no es valido', '\0');
INSERT INTO `err_error_code` VALUES ('2695', '2', 'El XML no contiene el tag o no existe información del Importe total documento Relacionado', '\0');
INSERT INTO `err_error_code` VALUES ('2696', '2', 'El dato ingresado en el importe total documento relacionado debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2697', '2', 'El XML no contiene el tag o no existe información del número de cobro', '\0');
INSERT INTO `err_error_code` VALUES ('2698', '2', 'El dato ingresado en el número de cobro no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2699', '2', 'El XML no contiene el tag o no existe información del Importe del cobro', '\0');
INSERT INTO `err_error_code` VALUES ('2700', '2', 'El dato ingresado en el Importe del cobro debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2701', '2', 'El XML no contiene el tag o no existe información de la moneda del documento Relacionado', '\0');
INSERT INTO `err_error_code` VALUES ('2702', '2', 'El XML no contiene el tag o no existe información de la fecha de cobro del documento Relacionado', '\0');
INSERT INTO `err_error_code` VALUES ('2703', '2', 'La fecha de cobro del documento relacionado no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2704', '2', 'El XML no contiene el tag o no existe información del Importe percibido', '\0');
INSERT INTO `err_error_code` VALUES ('2705', '2', 'El dato ingresado en el Importe percibido debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2706', '2', 'El XML no contiene el tag o no existe información de la moneda de importe percibido', '\0');
INSERT INTO `err_error_code` VALUES ('2707', '2', 'El valor de la moneda de importe percibido debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2708', '2', 'El XML no contiene el tag o no existe información de la Fecha de Percepción', '\0');
INSERT INTO `err_error_code` VALUES ('2709', '2', 'La fecha de percepción no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2710', '2', 'El XML no contiene el tag o no existe información del Monto total a cobrar', '\0');
INSERT INTO `err_error_code` VALUES ('2711', '2', 'El dato ingresado en el Monto total a cobrar debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2712', '2', 'El XML no contiene el tag o no existe información de la moneda del Monto total a cobrar', '\0');
INSERT INTO `err_error_code` VALUES ('2713', '2', 'El valor de la moneda del Monto total a cobrar debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2714', '2', 'El valor de la moneda de referencia para el tipo de cambio no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2715', '2', 'El valor de la moneda objetivo para la Tasa de Cambio debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2716', '2', 'El dato ingresado en el tipo de cambio debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2717', '2', 'La fecha de cambio no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2718', '2', 'El valor de la moneda del documento Relacionado no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2719', '2', 'El XML no contiene el tag o no existe información de la moneda de referencia para el tipo de cambio', '\0');
INSERT INTO `err_error_code` VALUES ('2720', '2', 'El XML no contiene el tag o no existe información de la moneda objetivo para la Tasa de Cambio', '\0');
INSERT INTO `err_error_code` VALUES ('2721', '2', 'El XML no contiene el tag o no existe información del tipo de cambio', '\0');
INSERT INTO `err_error_code` VALUES ('2722', '2', 'El XML no contiene el tag o no existe información de la fecha de cambio', '\0');
INSERT INTO `err_error_code` VALUES ('2723', '2', 'El XML no contiene el tag o no existe información del número de documento de identidad del proveedor', '\0');
INSERT INTO `err_error_code` VALUES ('2724', '2', 'El valor ingresado como documento de identidad del proveedor es incorrecto', '\0');
INSERT INTO `err_error_code` VALUES ('2725', '2', 'El XML no contiene el tag o no existe información del Importe total Retenido', '\0');
INSERT INTO `err_error_code` VALUES ('2726', '2', 'El XML no contiene el tag o no existe información de la moneda del Importe total Retenido', '\0');
INSERT INTO `err_error_code` VALUES ('2727', '2', 'El XML no contiene el tag o no existe información de la moneda del Importe total Retenido', '\0');
INSERT INTO `err_error_code` VALUES ('2728', '2', 'El valor de la moneda del Importe total Retenido debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2729', '2', 'El XML no contiene el tag o no existe información del Importe total Pagado', '\0');
INSERT INTO `err_error_code` VALUES ('2730', '2', 'El dato ingresado en SUNATTotalPaid debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2731', '2', 'El XML no contiene el tag o no existe información de la moneda del Importe total Pagado', '\0');
INSERT INTO `err_error_code` VALUES ('2732', '2', 'El valor de la moneda del Importe total Pagado debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2733', '2', 'El XML no contiene el tag o no existe información del número de pago', '\0');
INSERT INTO `err_error_code` VALUES ('2734', '2', 'El dato ingresado en el número de pago no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2735', '2', 'El XML no contiene el tag o no existe información del Importe del pago', '\0');
INSERT INTO `err_error_code` VALUES ('2736', '2', 'El dato ingresado en el Importe del pago debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2737', '2', 'El XML no contiene el tag o no existe información de la fecha de pago del documento Relacionado', '\0');
INSERT INTO `err_error_code` VALUES ('2738', '2', 'La fecha de pago del documento relacionado no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2739', '2', 'El XML no contiene el tag o no existe información del Importe retenido', '\0');
INSERT INTO `err_error_code` VALUES ('2740', '2', 'El dato ingresado en el Importe retenido debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2741', '2', 'El XML no contiene el tag o no existe información de la moneda de importe retenido', '\0');
INSERT INTO `err_error_code` VALUES ('2742', '2', 'El valor de la moneda de importe retenido debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2743', '2', 'El XML no contiene el tag o no existe información de la Fecha de Retención', '\0');
INSERT INTO `err_error_code` VALUES ('2744', '2', 'La fecha de retención no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('2745', '2', 'El XML no contiene el tag o no existe información del Importe total a pagar (neto)', '\0');
INSERT INTO `err_error_code` VALUES ('2746', '2', 'El dato ingresado en el Importe total a pagar (neto) debe ser numérico mayor a cero', '\0');
INSERT INTO `err_error_code` VALUES ('2747', '2', 'El XML no contiene el tag o no existe información de la Moneda del monto neto pagado', '\0');
INSERT INTO `err_error_code` VALUES ('2748', '2', 'El valor de la Moneda del monto neto pagado debe ser PEN', '\0');
INSERT INTO `err_error_code` VALUES ('2749', '2', 'La moneda de referencia para el tipo de cambio debe ser la misma que la del documento relacionado ', '\0');
INSERT INTO `err_error_code` VALUES ('2750', '2', 'El comprobante que se informa debe existir y estar en estado emitido', '\0');
INSERT INTO `err_error_code` VALUES ('2751', '2', 'El comprobante que se informa ya se encuentra en estado revertido', '\0');
INSERT INTO `err_error_code` VALUES ('2752', '2', 'El número de ítem no puede estar duplicado', '\0');
INSERT INTO `err_error_code` VALUES ('4000', '3', 'El documento ya fue presentado anteriormente.', '\0');
INSERT INTO `err_error_code` VALUES ('4001', '3', 'El numero de RUC del receptor no existe. ', '\0');
INSERT INTO `err_error_code` VALUES ('4002', '3', 'Para el TaxTypeCode, esta usando un valor que no existe en el catalogo. ', '\0');
INSERT INTO `err_error_code` VALUES ('4003', '3', 'El comprobante fue registrado previamente como rechazado.', '\0');
INSERT INTO `err_error_code` VALUES ('4004', '3', 'El DocumentTypeCode de las guias debe existir y tener 2 posiciones ', '\0');
INSERT INTO `err_error_code` VALUES ('4005', '3', 'El DocumentTypeCode de las guias debe ser 09 o 31 ', '\0');
INSERT INTO `err_error_code` VALUES ('4006', '3', 'El ID de las guias debe tener informacion de la SERIE-NUMERO de guia. ', '\0');
INSERT INTO `err_error_code` VALUES ('4007', '3', 'El XML no contiene el ID de las guias.', '\0');
INSERT INTO `err_error_code` VALUES ('4008', '3', 'El DocumentTypeCode de Otros documentos relacionados no cumple con el estandar. ', '\0');
INSERT INTO `err_error_code` VALUES ('4009', '3', 'El DocumentTypeCode de Otros documentos relacionados tiene valores incorrectos. ', '\0');
INSERT INTO `err_error_code` VALUES ('4010', '3', 'El ID de los documentos relacionados no cumplen con el estandar. ', '\0');
INSERT INTO `err_error_code` VALUES ('4011', '3', 'El XML no contiene el tag ID de documentos relacionados.', '\0');
INSERT INTO `err_error_code` VALUES ('4012', '3', 'El ubigeo indicado en el comprobante no es el mismo que esta registrado para el contribuyente. ', '\0');
INSERT INTO `err_error_code` VALUES ('4013', '3', 'El RUC del receptor no esta activo ', '\0');
INSERT INTO `err_error_code` VALUES ('4014', '3', 'El RUC del receptor no esta habido ', '\0');
INSERT INTO `err_error_code` VALUES ('4015', '3', 'Si el tipo de documento del receptor no es RUC, debe tener operaciones de exportacion ', '\0');
INSERT INTO `err_error_code` VALUES ('4016', '3', 'El total valor venta neta de oper. gravadas IGV debe ser mayor a 0.00 o debe existir oper. gravadas onerosas ', '\0');
INSERT INTO `err_error_code` VALUES ('4017', '3', 'El total valor venta neta de oper. inafectas IGV debe ser mayor a 0.00 o debe existir oper. inafectas onerosas o de export. ', '\0');
INSERT INTO `err_error_code` VALUES ('4018', '3', 'El total valor venta neta de oper. exoneradas IGV debe ser mayor a 0.00 o debe existir oper. exoneradas ', '\0');
INSERT INTO `err_error_code` VALUES ('4019', '3', 'El calculo del IGV no es correcto ', '\0');
INSERT INTO `err_error_code` VALUES ('4020', '3', 'El ISC no esta informado correctamente ', '\0');
INSERT INTO `err_error_code` VALUES ('4021', '3', 'Si se utiliza la leyenda con codigo 2000, el importe de percepcion debe ser mayor a 0.00 ', '\0');
INSERT INTO `err_error_code` VALUES ('4022', '3', 'Si se utiliza la leyenda con código 2001, el total de operaciones exoneradas debe ser mayor a 0.00 ', '\0');
INSERT INTO `err_error_code` VALUES ('4023', '3', 'Si se utiliza la leyenda con código 2002, el total de operaciones exoneradas debe ser mayor a 0.00 ', '\0');
INSERT INTO `err_error_code` VALUES ('4024', '3', 'Si se utiliza la leyenda con código 2003, el total de operaciones exoneradas debe ser mayor a 0.00 ', '\0');
INSERT INTO `err_error_code` VALUES ('4025', '3', 'Si usa la leyenda de Transferencia o Servivicio gratuito, todos los items deben ser no onerosos ', '\0');
INSERT INTO `err_error_code` VALUES ('4026', '3', 'No se puede indicar Guia de remision de remitente y Guia de remision de transportista en el mismo documento ', '\0');
INSERT INTO `err_error_code` VALUES ('4027', '3', 'El importe total no coincide con la sumatoria de los valores de venta mas los tributos mas los cargos ', '\0');
INSERT INTO `err_error_code` VALUES ('4028', '3', 'El monto total de la nota de credito debe ser menor o igual al monto de la factura ', '\0');
INSERT INTO `err_error_code` VALUES ('4029', '3', 'El ubigeo indicado en el comprobante no es el mismo que esta registrado para el contribuyente ', '\0');
INSERT INTO `err_error_code` VALUES ('4030', '3', 'El ubigeo indicado en el comprobante no es el mismo que esta registrado para el contribuyente ', '\0');
INSERT INTO `err_error_code` VALUES ('4031', '3', 'Debe indicar el nombre comercial ', '\0');
INSERT INTO `err_error_code` VALUES ('4032', '3', 'Si el código del motivo de emisión de la Nota de Credito es 03, debe existir la descripción del item ', '\0');
INSERT INTO `err_error_code` VALUES ('4033', '3', 'La fecha de generación de la numeración debe ser menor o igual a la fecha de generación de la comunicación ', '\0');
INSERT INTO `err_error_code` VALUES ('4034', '3', 'El comprobante fue registrado previamente como baja', '\0');
INSERT INTO `err_error_code` VALUES ('4035', '3', 'El comprobante fue registrado previamente como rechazado', '\0');
INSERT INTO `err_error_code` VALUES ('4036', '3', 'La fecha de emisión de los rangos debe ser menor o igual a la fecha de generación del resumen ', '\0');
INSERT INTO `err_error_code` VALUES ('4037', '3', 'El calculo del Total de IGV del Item no es correcto ', '\0');
INSERT INTO `err_error_code` VALUES ('4038', '3', 'El resumen contiene menos series por tipo de documento que el envío anterior para la misma fecha de emisión ', '\0');
INSERT INTO `err_error_code` VALUES ('4039', '3', 'No ha consignado información del ubigeo del domicilio fiscal ', '\0');
INSERT INTO `err_error_code` VALUES ('4040', '3', 'Si el importe de percepcion es mayor a 0.00, debe utilizar una leyenda con codigo 2000 ', '\0');
INSERT INTO `err_error_code` VALUES ('4041', '3', 'El codigo de pais debe ser PE ', '\0');
INSERT INTO `err_error_code` VALUES ('4042', '3', 'Para sac:SUNATTransaction/cbc:ID, se está usando un valor que no existe en el catálogo. Nro. 17', '\0');
INSERT INTO `err_error_code` VALUES ('4043', '3', 'Para el TransportModeCode, se está usando un valor que no existe en el catálogo Nro. 18', '\0');
INSERT INTO `err_error_code` VALUES ('4044', '3', 'PrepaidAmount: Monto total anticipado no coincide con la sumatoria de los montos por documento de anticipo', '\0');
INSERT INTO `err_error_code` VALUES ('4045', '3', 'No debe consignar los datos del transportista para la modalidad de transporte 02 – Transporte Privado', '\0');
INSERT INTO `err_error_code` VALUES ('4046', '3', 'No debe consignar información adicional en la dirección para los locales anexos', '\0');
INSERT INTO `err_error_code` VALUES ('4047', '3', 'sac:SUNATTransaction/cbc:ID debe ser igual a 06 cuando ingrese información para sustentar el traslado', '\0');
INSERT INTO `err_error_code` VALUES ('4048', '3', 'cac:AdditionalDocumentReference/cbc:DocumentTypeCode - Contiene un valor no valido para documentos relacionado', '\0');
INSERT INTO `err_error_code` VALUES ('4049', '3', 'El número de DNI del receptor no existe', '\0');
INSERT INTO `err_error_code` VALUES ('4050', '3', 'El número de RUC del proveedor no existe', '\0');
INSERT INTO `err_error_code` VALUES ('4051', '3', 'El RUC del proveedor no está activo', '\0');
INSERT INTO `err_error_code` VALUES ('4052', '3', 'El RUC del proveedor no está habido', '\0');
INSERT INTO `err_error_code` VALUES ('4053', '3', 'Proveedor no debe ser igual al remitente o destinatario', '\0');
INSERT INTO `err_error_code` VALUES ('4054', '3', 'La guía no debe contener datos del proveedor', '\0');
INSERT INTO `err_error_code` VALUES ('4055', '3', 'El XML no contiene información en el tag cbc:Information', '\0');
INSERT INTO `err_error_code` VALUES ('4056', '3', 'El XML no contiene el tag o no existe información en el tag SplitConsignmentIndicator', '\0');
INSERT INTO `err_error_code` VALUES ('4057', '3', 'GrossWeightMeasure – El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4058', '3', 'cbc:TotalPackageQuantity - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4059', '3', 'Numero de bultos o pallets - información válida para importación', '\0');
INSERT INTO `err_error_code` VALUES ('4060', '3', 'La guía no debe contener datos del transportista', '\0');
INSERT INTO `err_error_code` VALUES ('4061', '3', 'El número de RUC del transportista no existe', '\0');
INSERT INTO `err_error_code` VALUES ('4062', '3', 'El RUC del transportista no está activo', '\0');
INSERT INTO `err_error_code` VALUES ('4063', '3', 'El RUC del transportista no está habido', '\0');
INSERT INTO `err_error_code` VALUES ('4064', '3', '/DespatchAdvice/cac:Shipment/cac:ShipmentStage/cac:TransportMeans/cbc:RegistrationNationalityID - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4065', '3', 'cac:TransportMeans/cbc:TransportMeansTypeCode - El valor ingresado como tipo de unidad de transporte es incorrecta', '\0');
INSERT INTO `err_error_code` VALUES ('4066', '3', 'El número de DNI del conductor no existe', '\0');
INSERT INTO `err_error_code` VALUES ('4067', '3', 'El XML no contiene el tag o no existe información del ubigeo del punto de llegada', '\0');
INSERT INTO `err_error_code` VALUES ('4068', '3', 'Dirección de punto de llegada - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4069', '3', 'CityName - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4070', '3', 'District - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4071', '3', 'Numero de Contenedor - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4072', '3', 'Numero de contenedor - información válida para importación', '\0');
INSERT INTO `err_error_code` VALUES ('4073', '3', 'TransEquipmentTypeCode - El valor ingresado como tipo de contenedor es incorrecta', '\0');
INSERT INTO `err_error_code` VALUES ('4074', '3', 'Numero Precinto - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4075', '3', 'El XML no contiene el tag o no existe información del ubigeo del punto de partida', '\0');
INSERT INTO `err_error_code` VALUES ('4076', '3', 'Dirección de punto de partida - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4077', '3', 'CityName - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4078', '3', 'District - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4079', '3', 'Código de Puerto o Aeropuerto - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4080', '3', 'Tipo de Puerto o Aeropuerto - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4081', '3', 'El XML No contiene El tag o No existe información del Numero de orden del ítem', '\0');
INSERT INTO `err_error_code` VALUES ('4082', '3', 'Número de Orden del Ítem - El orden del ítem no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4083', '3', 'Cantidad - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4084', '3', 'Descripción del Ítem - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4085', '3', 'Código del Ítem - El dato ingresado no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4086', '3', 'El emisor y el cliente son Agentes de percepción de combustible en la fecha de emisión.', '\0');
INSERT INTO `err_error_code` VALUES ('4087', '3', 'El Comprobante de Pago Electrónico no está Registrado en los Sistemas de la SUNAT.', '\0');
INSERT INTO `err_error_code` VALUES ('4088', '3', 'El Comprobante de Pago no está autorizado en los Sistemas de la SUNAT.', '\0');
INSERT INTO `err_error_code` VALUES ('4089', '3', 'La operación con este cliente está excluida del sistema de percepción. Es agente de retención.', '\0');
INSERT INTO `err_error_code` VALUES ('4090', '3', 'La operación con este cliente está excluida del sistema de percepción. Es entidad exceptuada de la percepción.', '\0');
INSERT INTO `err_error_code` VALUES ('4091', '3', 'La operación con este proveedor está excluida del sistema de retención. Es agente de percepción, agente de retención o buen contribuyente.', '\0');
INSERT INTO `err_error_code` VALUES ('4092', '3', 'El nombre comercial del emisor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4093', '3', 'El ubigeo del emisor no cumple con el formato establecido o no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('4094', '3', 'La dirección completa y detallada del domicilio fiscal del emisor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4095', '3', 'La urbanización del domicilio fiscal del emisor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4096', '3', 'La provincia del domicilio fiscal del emisor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4097', '3', 'El departamento del domicilio fiscal del emisor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4098', '3', 'El distrito del domicilio fiscal del emisor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4099', '3', 'El nombre comercial del cliente no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4100', '3', 'El ubigeo del cliente no cumple con el formato establecido o no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('4101', '3', 'La dirección completa y detallada del domicilio fiscal del cliente no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4102', '3', 'La urbanización del domicilio fiscal del cliente no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4103', '3', 'La provincia del domicilio fiscal del cliente no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4104', '3', 'El departamento del domicilio fiscal del cliente no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4105', '3', 'El distrito del domicilio fiscal del cliente no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4106', '3', 'El nombre comercial del proveedor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4107', '3', 'El ubigeo del proveedor no cumple con el formato establecido o no es válido', '\0');
INSERT INTO `err_error_code` VALUES ('4108', '3', 'La dirección completa y detallada del domicilio fiscal del proveedor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4109', '3', 'La urbanización del domicilio fiscal del proveedor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4110', '3', 'La provincia del domicilio fiscal del proveedor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4111', '3', 'El departamento del domicilio fiscal del proveedor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('4112', '3', 'El distrito del domicilio fiscal del proveedor no cumple con el formato establecido', '\0');
INSERT INTO `err_error_code` VALUES ('98', null, 'En proceso', '\0');
INSERT INTO `err_error_code` VALUES ('99', null, 'Proceso con errores', '\0');

-- ----------------------------
-- Table structure for err_error_code_type
-- ----------------------------
DROP TABLE IF EXISTS `err_error_code_type`;
CREATE TABLE `err_error_code_type` (
  `n_id_error_code_type` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(200) NOT NULL,
  PRIMARY KEY (`n_id_error_code_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of err_error_code_type
-- ----------------------------
INSERT INTO `err_error_code_type` VALUES ('1', 'Excepciones');
INSERT INTO `err_error_code_type` VALUES ('2', 'Errores que generan rechazo');
INSERT INTO `err_error_code_type` VALUES ('3', 'Observaciones');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for log_log
-- ----------------------------
DROP TABLE IF EXISTS `log_log`;
CREATE TABLE `log_log` (
  `n_id_log` int(11) NOT NULL AUTO_INCREMENT,
  `lgph_id` int(11) unsigned NOT NULL DEFAULT '11',
  `c_id_log_level` varchar(8) NOT NULL,
  `c_message` varchar(500) NOT NULL,
  `c_context` text,
  `n_id_invoice` int(11) DEFAULT NULL,
  `c_invoice_type_code` varchar(255) DEFAULT NULL,
  `c_id` varchar(255) DEFAULT NULL,
  `c_id_error_code` varchar(4) DEFAULT NULL,
  `d_date_register` datetime NOT NULL,
  `d_date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_log`),
  KEY `fk_log_level_log` (`c_id_log_level`),
  KEY `lgph_id` (`lgph_id`),
  KEY `n_id_invoice` (`n_id_invoice`),
  KEY `c_invoice_type_code` (`c_invoice_type_code`),
  KEY `c_id_error_code` (`c_id_error_code`),
  CONSTRAINT `fk_error_code_log` FOREIGN KEY (`c_id_error_code`) REFERENCES `err_error_code` (`c_id_error_code`),
  CONSTRAINT `fk_invoice_log` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_log_level_log` FOREIGN KEY (`c_id_log_level`) REFERENCES `log_log_level` (`c_id_log_level`),
  CONSTRAINT `fk_phase_log` FOREIGN KEY (`lgph_id`) REFERENCES `log_phase` (`lgph_id`),
  CONSTRAINT `fk_type_code_log` FOREIGN KEY (`c_invoice_type_code`) REFERENCES `doc_invoice_type_code` (`c_invoice_type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1609 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log_log
-- ----------------------------

-- ----------------------------
-- Table structure for log_log_level
-- ----------------------------
DROP TABLE IF EXISTS `log_log_level`;
CREATE TABLE `log_log_level` (
  `c_id_log_level` varchar(8) NOT NULL,
  `c_name` varchar(200) NOT NULL,
  `c_description` text,
  PRIMARY KEY (`c_id_log_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log_log_level
-- ----------------------------
INSERT INTO `log_log_level` VALUES ('critical', 'Crítico', null);
INSERT INTO `log_log_level` VALUES ('debug', 'Depurar', null);
INSERT INTO `log_log_level` VALUES ('error', 'Error', null);
INSERT INTO `log_log_level` VALUES ('info', 'Info', null);
INSERT INTO `log_log_level` VALUES ('notice', 'Aviso', null);
INSERT INTO `log_log_level` VALUES ('warning', 'Advertencia', null);

-- ----------------------------
-- Table structure for log_phase
-- ----------------------------
DROP TABLE IF EXISTS `log_phase`;
CREATE TABLE `log_phase` (
  `lgph_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lgph_name` varchar(200) NOT NULL,
  `lgph_index` int(10) unsigned NOT NULL,
  `lpgh_active` tinyint(4) NOT NULL,
  `lgph_fecreg` datetime NOT NULL,
  `lgph_fecmod` datetime DEFAULT NULL,
  PRIMARY KEY (`lgph_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log_phase
-- ----------------------------
INSERT INTO `log_phase` VALUES ('1', 'Generación de array', '2', '1', '2016-06-26 19:27:49', null);
INSERT INTO `log_phase` VALUES ('2', 'Validación de array', '3', '1', '2016-06-26 19:28:06', null);
INSERT INTO `log_phase` VALUES ('3', 'Almacenamiento en BD', '4', '1', '2016-06-26 19:28:24', null);
INSERT INTO `log_phase` VALUES ('4', 'Generación XML', '5', '1', '2016-06-26 19:28:39', null);
INSERT INTO `log_phase` VALUES ('5', 'Envío a SUNAT', '7', '1', '2016-06-26 19:29:02', null);
INSERT INTO `log_phase` VALUES ('6', 'Lectura CDR, grabar en BD', '8', '1', '2016-06-26 19:29:32', null);
INSERT INTO `log_phase` VALUES ('7', 'Almacenamiento Input BD/File', '6', '1', '2016-06-29 18:42:14', null);
INSERT INTO `log_phase` VALUES ('8', 'Generar PDF', '9', '1', '2016-06-29 20:42:46', null);
INSERT INTO `log_phase` VALUES ('9', 'Envío e-mail', '10', '1', '2016-06-29 22:37:28', null);
INSERT INTO `log_phase` VALUES ('10', 'Lectura Ticket SUNAT', '7', '1', '2016-06-29 23:34:58', null);
INSERT INTO `log_phase` VALUES ('11', 'No contemplado', '0', '1', '2016-06-29 23:45:50', null);
INSERT INTO `log_phase` VALUES ('12', 'Verificar documento', '1', '1', '2016-07-05 07:49:36', null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2015_05_15_143610_create_failed_jobs_table', '1');

-- ----------------------------
-- Table structure for out_cdr
-- ----------------------------
DROP TABLE IF EXISTS `out_cdr`;
CREATE TABLE `out_cdr` (
  `cdr_id` int(11) NOT NULL AUTO_INCREMENT,
  `cdr_correlative` varchar(80) NOT NULL,
  `cdr_quantity_cdr` int(11) NOT NULL DEFAULT '0',
  `cdr_response_success` int(11) NOT NULL DEFAULT '0',
  `cdr_response_observed` int(11) NOT NULL DEFAULT '0',
  `cdr_response_rejected` int(11) NOT NULL DEFAULT '0',
  `cdr_created_at` datetime NOT NULL,
  `cdr_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`cdr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of out_cdr
-- ----------------------------

-- ----------------------------
-- Table structure for out_cdr_det
-- ----------------------------
DROP TABLE IF EXISTS `out_cdr_det`;
CREATE TABLE `out_cdr_det` (
  `cdrd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cdr_id` int(11) NOT NULL,
  `n_id_invoice` int(11) NOT NULL,
  `c_customer_assigned_account_id` varchar(11) NOT NULL,
  `c_invoice_type_code` varchar(2) NOT NULL,
  `cdrd_serie` varchar(4) NOT NULL,
  `cdrd_correlative` varchar(8) NOT NULL,
  `n_id_cdr_status` int(11) NOT NULL DEFAULT '0',
  `cdrd_response_description` varchar(200) DEFAULT NULL,
  `cdrd_date` date NOT NULL COMMENT 'Fecha del CDR (brindado por la SUNAT)',
  `n_id_account` int(11) NOT NULL DEFAULT '14',
  `cdrd_file_name` varchar(200) NOT NULL,
  PRIMARY KEY (`cdrd_id`),
  KEY `cdr_id` (`cdr_id`),
  KEY `n_id_invoice` (`n_id_invoice`),
  KEY `c_invoice_type_code` (`c_invoice_type_code`),
  KEY `n_id_cdr_status` (`n_id_cdr_status`),
  KEY `n_id_account` (`n_id_account`),
  CONSTRAINT `fk_account_cdr_det` FOREIGN KEY (`n_id_account`) REFERENCES `acc_account` (`n_id_account`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cdr_cdrd` FOREIGN KEY (`cdr_id`) REFERENCES `out_cdr` (`cdr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cdr_status_cdr_det` FOREIGN KEY (`n_id_cdr_status`) REFERENCES `doc_cdr_status` (`n_id_cdr_status`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_invoice_cdrd` FOREIGN KEY (`n_id_invoice`) REFERENCES `doc_invoice` (`n_id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_invoice_type_code` FOREIGN KEY (`c_invoice_type_code`) REFERENCES `doc_invoice_type_code` (`c_invoice_type_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of out_cdr_det
-- ----------------------------

-- ----------------------------
-- Table structure for sup_supplier
-- ----------------------------
DROP TABLE IF EXISTS `sup_supplier`;
CREATE TABLE `sup_supplier` (
  `n_id_supplier` int(11) NOT NULL AUTO_INCREMENT,
  `c_customer_assigned_account_id` varchar(11) NOT NULL COMMENT 'Número de RUC',
  `c_additional_account_id` varchar(1) DEFAULT NULL COMMENT 'Tipo de Documento - Catálogo No 06',
  `c_party_postal_address_id` varchar(6) DEFAULT NULL COMMENT 'Código de Ubigeo - Catálogo No 13',
  `c_party_postal_address_street_name` varchar(100) DEFAULT NULL COMMENT 'Dirección completa y detalada',
  `c_party_postal_address_city_subdivision_name` varchar(25) DEFAULT NULL COMMENT 'Urbanización',
  `c_party_postal_address_city_name` varchar(30) DEFAULT NULL COMMENT 'Provincia',
  `c_party_postal_address_country_subentity` varchar(30) DEFAULT NULL COMMENT 'Departamento',
  `c_party_postal_address_district` varchar(30) DEFAULT NULL COMMENT 'Distrito',
  `c_party_postal_address_country_identification_code` varchar(2) DEFAULT NULL COMMENT 'Código de Paul - Catálogo No 04',
  `c_party_party_legal_entity_registration_name` varchar(100) DEFAULT NULL COMMENT 'Apellidos y nombres, denominación o razón social',
  `c_party_name_name` varchar(100) DEFAULT NULL COMMENT 'Nombre Comercial',
  `c_telephone` varchar(20) DEFAULT NULL,
  `c_email` varchar(100) DEFAULT NULL,
  `c_detraction_account` varchar(100) DEFAULT NULL COMMENT 'Cuenta de la detraccion (Cuenta corriente Banco de la Nacion), para operaciones que fuesen afectadas.',
  `c_sunat_bill_resolution` varchar(100) DEFAULT NULL COMMENT 'Resolucion otorgada de la SUNAT para la emision de boletas y relacionados.',
  `c_sunat_invoice_resolution` varchar(100) DEFAULT NULL COMMENT 'Resolucion otorgada de la SUNAT para la emision de facturas y relacionados.',
  `c_status_supplier` enum('visible','hidden','deleted') NOT NULL,
  `d_date_register_supplier` datetime NOT NULL,
  `d_date_update_supplier` datetime DEFAULT NULL,
  PRIMARY KEY (`n_id_supplier`),
  KEY `fk_invoice_additional_account_id_supplier` (`c_additional_account_id`),
  KEY `c_party_postal_address_country_identification_code` (`c_party_postal_address_country_identification_code`),
  CONSTRAINT `fk_country_supplier` FOREIGN KEY (`c_party_postal_address_country_identification_code`) REFERENCES `country` (`c_iso`),
  CONSTRAINT `fk_invoice_additional_account_id_supplier` FOREIGN KEY (`c_additional_account_id`) REFERENCES `doc_invoice_additional_account_id` (`c_id_invoice_additional_account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sup_supplier
-- ----------------------------
INSERT INTO `sup_supplier` VALUES ('6', '20524719585', '6', '150131', 'CAL. AGUSTIN DE LA TORRE GONZALES NRO. 194', '', 'LIMA', 'LIMA', 'SAN ISIDRO', 'PE', 'AVANZA SOLUCIONES S.A.C.', 'AVSO S.A.C.', '721-2783', 'facturacion.avanzasoluciones@gmail.com', '00098066799', '034-005-0006241/SUNAT', '034-005-0006241/SUNAT', 'visible', '2015-04-13 17:30:50', '2017-12-16 21:01:13');

-- ----------------------------
-- Table structure for sup_supplier_configuration
-- ----------------------------
DROP TABLE IF EXISTS `sup_supplier_configuration`;
CREATE TABLE `sup_supplier_configuration` (
  `n_id_supplier` int(11) NOT NULL,
  `c_bill_sent_sunat` enum('yes','no') NOT NULL DEFAULT 'no',
  `c_email_sent_customer` enum('yes','no') NOT NULL DEFAULT 'yes' COMMENT 'Correo electronico que se envia al usuario final, despues de un proceso de facturacion electronica.',
  `c_email_sent_supplier` enum('yes','no') NOT NULL DEFAULT 'no',
  `c_public_path_cdr` varchar(255) NOT NULL,
  `c_public_path_cdr_processed` varchar(255) NOT NULL,
  `c_public_path_document` varchar(255) NOT NULL,
  `c_public_path_input` varchar(255) NOT NULL,
  `c_public_path_pdf` varchar(255) NOT NULL,
  PRIMARY KEY (`n_id_supplier`),
  CONSTRAINT `fk_supplier_supplier_configuration` FOREIGN KEY (`n_id_supplier`) REFERENCES `sup_supplier` (`n_id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sup_supplier_configuration
-- ----------------------------
INSERT INTO `sup_supplier_configuration` VALUES ('6', 'yes', 'yes', 'yes', 'cdn/cdr', 'cdn/cdr-processed', 'cdn/document', 'cdn/input', 'cdn/pdf');
SET FOREIGN_KEY_CHECKS=1;
